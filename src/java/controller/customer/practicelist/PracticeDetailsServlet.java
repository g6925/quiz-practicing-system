/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.customer.practicelist;

import dao.DSQDAO;
import dao.DimensonDAO;
import dao.QuestionDAO;
import dao.QuizDAO;
import dao.QuizQuestionDAO;
import dao.RegisterDAO;
import dao.SubjectDAO;
import dao.TopicDAO;
import entity.Dimension;
import entity.Question;
import entity.Quiz;
import entity.Register;
import entity.Subject;
import entity.Topic;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jarnsaurus
 */
public class PracticeDetailsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        RegisterDAO rd = new RegisterDAO();
        List<Register> lr = rd.getRegistrationsByUserId(user.getId());
        request.setAttribute("qSubject", lr);
//        PrintWriter out = response.getWriter();
//        out.write("subject: " + subject_raw);
//        out.write("type: " + type_raw);        
        request.getRequestDispatcher("/view/customer/practicedetails.jsp").forward(request, response);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        TopicDAO td = new TopicDAO();
        DimensonDAO dd = new DimensonDAO();
        QuestionDAO qd = new QuestionDAO();
        QuizDAO qzd = new QuizDAO();
        QuizQuestionDAO qqd = new QuizQuestionDAO();
        DSQDAO dsq = new DSQDAO();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        String num_raw = request.getParameter("num");
        String group_raw = request.getParameter("group");
        String type_raw = request.getParameter("type");
        int type = Integer.parseInt(type_raw);
        String subject_raw = request.getParameter("subject");
        int num;
        if (num_raw == null || num_raw.equals("")) {
            num = 0;
        } else {
            num = Integer.parseInt(num_raw);
        }
        int subject;
        if (subject_raw == null || subject_raw.equals("")) {
            subject = 0;
        } else {
            subject = Integer.parseInt(subject_raw);
        }
        request.setAttribute("subjectId", subject);
        request.setAttribute("num", num);
        RegisterDAO rd = new RegisterDAO();
        List<Register> lr = rd.getRegistrationsByUserId(user.getId());
        request.setAttribute("qSubject", lr);
        if (group_raw == null || group_raw.equals("") || group_raw.isEmpty()) {
            if (subject == 0) {
                request.setAttribute("warning", "Please Select Subject!");
                request.setAttribute("alert", "Failed");
            } else {
                if (type == 1) {
                    List<Topic> t = td.getListTopicsBySubjectID(subject);
                    request.setAttribute("qGroup", t);
                    request.setAttribute("typeId", type);
                }
                if (type == 2) {
                    List<Dimension> d = dd.getAllGroup();
                    request.setAttribute("qGroup", d);
                    request.setAttribute("typeId", type);
                }
                if (type == 3) {
                    List<Dimension> d = dd.getAllDomain();
                    request.setAttribute("qGroup", d);
                    request.setAttribute("typeId", type);
                }
            }
            request.getRequestDispatcher("/view/customer/practicedetails.jsp").forward(request, response);
        } else {
            int group = Integer.parseInt(group_raw);
            if (type == 1) {
                request.setAttribute("typeQuiz", "Subject Topic");
                if (group == 0) {
                    List<Topic> t = td.getListTopicsBySubjectID(subject);
                } else {
                    Topic t = td.getTopicsByID(group);
                }
            }
            if (type == 2) {
                request.setAttribute("typeQuiz", "Group");
                if (group == 0) {
                    List<Dimension> d = dd.getAllGroup();
                } else {
                    Dimension d = dd.getDimension(group);
                }
            }
            if (type == 3) {
                request.setAttribute("typeQuiz", "Domain");
                if (group == 0) {
                    List<Dimension> d = dd.getAllDomain();
                } else {
                    Dimension d = dd.getDimension(group);
                }
            }
            if (group_raw == null || group_raw.equals("") || group_raw.isEmpty()) {
                doPost(request, response);
            } else {
                Quiz qz = new Quiz();
                qz.setName("");
                SubjectDAO sd = new SubjectDAO();
                qz.setSubject(sd.getSubjectByID(sd.getConnection(), subject));
                qz.setLevel("");
                String finalTime = "";
                finalTime = String.format("%02d:%02d:%02d",
                        TimeUnit.HOURS.toHours(0),
                        TimeUnit.MINUTES.toMinutes(0),
                        TimeUnit.SECONDS.toSeconds(0));
                Time time = Time.valueOf(finalTime);
                qz.setDuration(time);
                qz.setPassRate(0);
                qz.setDescription("");
                qz.setType("Practice");
                int count = 0;
                List<Question> quest = new ArrayList<>();
                if (type == 1) {
                    if (group == 0) {
                        List<Topic> t = td.getListTopicsBySubjectID(subject);
                        for (Topic topic : t) {
                            quest = qd.getQByTopicId(topic.getId(), subject);
                            count += quest.size();
                        }
                        if (num > count) {
                            request.setAttribute("warning", "Total > Number Of Questions Existed!");
                            request.setAttribute("alert", "Failed");
                        } else {
                            int qzId = qzd.addQuiz(qz);
                            List<Question> listQ = qd.getQByTopics(t, num);
                            for (Question question : listQ) {
                                qqd.insert(question.getId(), qzId);
                            }
                            request.setAttribute("attributeName", qzId);
                             response.sendRedirect("learning/quiz?quizId=" + qzId + "&group=" + group_raw + "&type=" + type_raw);

                        }
                    } else {
                        Topic t = td.getTopicsByID(group);
                        quest = qd.getQByTopicId(t.getId(), subject);
                        count += quest.size();
                        if (num > count) {
                            request.setAttribute("warning", "Total > Number Of Questions Existed!");
                            request.setAttribute("alert", "Failed");
                        } else {
                            int qzId = qzd.addQuiz(qz);
                            List<Question> listQ = qd.getQByTopic(t.getId(), num);
                            for (Question question : listQ) {
                                qqd.insert(question.getId(), qzId);
                            }
                            request.setAttribute("attributeName", qzId);
                              response.sendRedirect("learning/quiz?quizId=" + qzId + "&group=" + group_raw + "&type=" + type_raw);
                        }
                    }
                }
                if (group == 0 && (type == 2 || type == 3)) {
                    if (type == 2) {
                        List<Dimension> ld = dd.getAllGroup();
                        for (Dimension di : ld) {
                            count += dsq.getQByDomain(subject, di.getId());
                        }
                        if (num > count) {
                            request.setAttribute("warning", "Total > Number Of Questions Existed!");
                            request.setAttribute("alert", "Failed");
                        } else {
                            int qzId = qzd.addQuiz(qz);
                            List<Question> listQ = dsq.getQuestByDimension(ld, num);
                            for (Question question : listQ) {
                                qqd.insert(question.getId(), qzId);
                            }
                            request.setAttribute("attributeName", qzId);
                             response.sendRedirect("learning/quiz?quizId=" + qzId + "&group=" + group_raw + "&type=" + type_raw);
                        }
                    }
                    if (type == 3) {
                        List<Dimension> ld = dd.getAllDomain();
                        for (Dimension di : ld) {
                            count += dsq.getQByDomain(subject, di.getId());
                        }
                        if (num > count) {
                            request.setAttribute("warning", "Total > Number Of Questions Existed!");
                            request.setAttribute("alert", "Failed");
                        } else {
                            int qzId = qzd.addQuiz(qz);
                            List<Question> listQ = dsq.getQuestByDimension(ld, num);
                            for (Question question : listQ) {
                                qqd.insert(question.getId(), qzId);
                            }
                            request.setAttribute("attributeName", qzId);
                              response.sendRedirect("learning/quiz?quizId=" + qzId + "&group=" + group_raw + "&type=" + type_raw);
                        }
                    }
                }
                if (group != 0 && (type == 2 || type == 3)) {
                    Dimension ld = dd.getDimension(group);
                    count += dsq.getQByDomain(subject, ld.getId());
                    if (num > count) {
                        request.setAttribute("warning", "Total > Number Of Questions Existed!");
                        request.setAttribute("alert", "Failed");
                    } else {
                        int qzId = qzd.addQuiz(qz);
                        List<Question> listQ = dsq.getQuestByDimension(ld, num);
                        for (Question question : listQ) {
                            qqd.insert(question.getId(), qzId);
                        }
                        request.setAttribute("attributeName", qzId);
                        response.sendRedirect("learning/quiz?quizId=" + qzId + "&group=" + group_raw + "&type=" + type_raw);
                    }
                }
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
