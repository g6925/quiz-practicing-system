/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.customer.practicelist;

import dao.QuizQuestionDAO;
import dao.RegisterDAO;
import dao.UserQuizDAO;
import entity.Register;
import entity.Subject;
import entity.User;
import entity.UserQuiz;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jarnsaurus
 */
public class PracticeListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PracticeListServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PracticeListServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        String subject_raw = request.getParameter("subject");
        int subject;
        if (subject_raw == null || subject_raw.equals("")) {
            subject = 0;
        } else {
            subject = Integer.parseInt(subject_raw);
        }
        RegisterDAO r = new RegisterDAO();
        List<Register> lr = r.getRegistrationsByUserId(user.getId());
        int id = user.getId();
        UserQuizDAO uq = new UserQuizDAO();
        QuizQuestionDAO qqd = new QuizQuestionDAO();
        if (subject != 0) {
            List<UserQuiz> list = uq.getQuizTakenByUserIDAndSubjectID(id, subject);
            request.setAttribute("list", list);
        } else {
            List<UserQuiz> list = uq.getQuizTakenByUserID(id);
            request.setAttribute("list", list);
        }
//        PrintWriter out = response.getWriter();        
        request.setAttribute("subject", subject);
        request.setAttribute("qqd", qqd);
        request.setAttribute("r", lr);
        request.getRequestDispatcher("/view/customer/practicelist.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
