/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.customer.practicelist;

import dao.DimensonDAO;
import dao.QuizQuestionDAO;
import dao.RegisterDAO;
import dao.TopicDAO;
import dao.UserQuizDAO;
import entity.Dimension;
import entity.Register;
import entity.Topic;
import entity.User;
import entity.UserQuiz;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author taina
 */
public class EditPracticeDetailsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditPracticeDetailsServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditPracticeDetailsServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        String uid_raw = request.getParameter("uid");
        String qid_raw = request.getParameter("qid");
        UserQuizDAO uq = new UserQuizDAO();
        UserQuiz q = uq.getQuizTakenByUserIDandQuizID(Integer.parseInt(uid_raw), Integer.parseInt(qid_raw));
        QuizQuestionDAO qq = new QuizQuestionDAO();
        int num = qq.countQuizQuestionByID(qq.getConnection(), Integer.parseInt(qid_raw));
        if (q.getFilterType().equals("Domain") || q.getFilterType().equals("Group")) {
            if (q.getFilterGroup() != 0) {
                DimensonDAO dd = new DimensonDAO();
                Dimension d = dd.getDimension(q.getFilterGroup());
                request.setAttribute("groupName", d.getName());
            } else {
                request.setAttribute("groupName", "All");
            }
        }
        if (q.getFilterType().equals("Subject Topic")) {
            if (q.getFilterGroup() != 0) {
                TopicDAO td = new TopicDAO();
                Topic t = td.getTopicsBySubjectID(q.getQuiz().getSubject().getId());
                request.setAttribute("groupName", t.getName());
            } else {
                request.setAttribute("groupName", "All");
            }
        }
        request.setAttribute("num", num);
        request.setAttribute("q", q);
        request.getRequestDispatcher("/view/customer/editpracticedetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String qid_raw = request.getParameter("quizID");
        request.setAttribute("attributeName", Integer.parseInt(qid_raw));
        RequestDispatcher rdr = request.getRequestDispatcher("");
        rdr.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
