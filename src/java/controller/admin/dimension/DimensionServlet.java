/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin.dimension;

import dao.DimensonDAO;
import dao.SubjectDAO;
import entity.Dimension;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Duong-PC
 */
public class DimensionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = request.getRequestURI();
        String action = request.getParameter("action");
        String name = request.getParameter("name");
        request.setAttribute("keyWord", name);
        String type = request.getParameter("type");
        request.setAttribute("type", type);
        DimensonDAO dimensonDAO = new DimensonDAO();
        if (action != null) {
            request.setAttribute("action", action);
            if (action.equals("add") || action.equals("edit")) {
                if (action.equals("edit")) {
                    String dimensionId = request.getParameter("dimensionId");
                    if (dimensionId != null) {
                        int lId = Integer.parseInt(dimensionId);

                        Dimension dimension = dimensonDAO.getDimension(lId);
                        request.setAttribute("dimension", dimension);
                    }
                    request.getRequestDispatcher("../view/admin/dimension/add-edit.jsp").forward(request, response);
                }
                if (action.equals("add")) {
                    request.getRequestDispatcher("../view/admin/dimension/add-edit.jsp").forward(request, response);
                }
                return;
            }
        }
        request.setAttribute("dimensions", dimensonDAO.findAllDimension(name, type));
        if (url.contains("/dimension")) {
            request.getRequestDispatcher("../view/admin/dimension.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("view/admin/dimension.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        DimensonDAO dimensonDAO = new DimensonDAO();
        if (action != null) {
            if (action.equals("add") || action.equals("edit")) {
                String name = request.getParameter("name");
                String description = request.getParameter("description");
                String type = request.getParameter("type");
                Dimension dimension = new Dimension(name, type, description);
                String alert = "";
                String message = "";

                if (action.equals("add")) {
                    if (dimensonDAO.addDimension(dimension) != 0) {
                        alert = "success";
                        message = "Add dimension success";
                    } else {
                        alert = "danger";
                        message = "Add dimension failed";
                    }
                }
                if (action.equals("edit")) {
                    dimension.setId(Integer.parseInt(request.getParameter("dimensionId")));
                    if (dimensonDAO.updateDimension(dimension) != 0) {
                        alert = "success";
                        message = "Edit dimension success";
                    } else {
                        alert = "danger";
                        message = "Edit dimension failed";
                    }
                }
                request.setAttribute("alert", alert);
                request.setAttribute("message", message);
            }
        }
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
