/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin.subject.detail;

import dao.CategoryDAO;
import dao.ExpertDAO;
import dao.SubjectDAO;
import entity.Category;
import entity.MyMethod;
import entity.Subject;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author xuant
 */
public class SubjectDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SubjectDetailServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SubjectDetailServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        int sId = Integer.parseInt(request.getParameter("sId"));
        SubjectDAO sdbc = new SubjectDAO();
        Subject subject = sdbc.getSubjectByID(sdbc.getConnection(),sId);
        request.setAttribute("subject", subject);
        CategoryDAO cdao = new CategoryDAO();
        ArrayList<Category> categorys = (ArrayList<Category>) cdao.getAll();
        request.setAttribute("categorys", categorys);
        ExpertDAO edao = new ExpertDAO();
        ArrayList<User> experts = (ArrayList<User>) edao.getAll();
        request.setAttribute("experts", experts);
        request.getRequestDispatcher("../../view/admin/subject/detail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        SubjectDAO sdbc = new SubjectDAO();
        HttpSession session = request.getSession();
        User created = (User) session.getAttribute("user");
        int id = Integer.parseInt(request.getParameter("subject_id"));
        String name = request.getParameter("subject_name");
        int categoryID = Integer.parseInt(request.getParameter("subject_category"));
        boolean featured;
        String row_featued =request.getParameter("featured");
        if (row_featued != null) {
            featured = true;
        } else {
            featured = false;
        }
        int expertId =Integer.parseInt(request.getParameter("expert"));
        int row_status = Integer.parseInt(request.getParameter("status"));
        boolean status;
        if (row_status == 1) {
            status = true;
        } else {
            status = false;
        }
        String tagLine = request.getParameter("tag_line");
        String description = request.getParameter("subject_description");
        Part file = request.getPart("image");
        
        Category c = new Category();
        c.setId(categoryID);
        User expert = new User();
        expert.setId(expertId);
        Subject s = sdbc.getSubjectByID(sdbc.getConnection(),id);
        s.setTitle(name);

        
        if (file.getSize() != 0) {
            String thumnail = MyMethod.upload(request, file, "assets/images/courses");
            s.setThumbnail(thumnail);
        }
        if (!description.isEmpty()) {
            s.setDescription(description);
        }
        s.setFeatured(featured);
        s.setStatus(status);
        s.setCategory(c);
        int oldExpert = s.getTaught().getId();
        s.setTaught(expert);
        s.setTagline(tagLine);
        s.setUpdateDate(MyMethod.getT_now());
        s.setCreated(created);
        sdbc.editSubject(s,oldExpert);
        response.sendRedirect("../subjects");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
