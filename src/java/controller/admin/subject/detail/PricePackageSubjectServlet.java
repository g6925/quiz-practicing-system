/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin.subject.detail;

import dao.PricePackageDAO;
import dao.SubjectDAO;
import entity.PricePackage;
import entity.Subject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author xuant
 */
public class PricePackageSubjectServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PricePackageDAO packageDAO = new PricePackageDAO();
        SubjectDAO sdao = new SubjectDAO();
        int subjectId = Integer.parseInt(request.getParameter("subjectId"));
        List<PricePackage> packages = packageDAO.getAllBySubjectId(subjectId);
        List<PricePackage> all = packageDAO.getAll();
        Subject subject = sdao.getSubjectByID(sdao.getConnection(), subjectId);
        request.setAttribute("packages", packages);
        request.setAttribute("subjectId", subjectId);
        request.setAttribute("all", all);
        request.setAttribute("subject", subject);
        request.getRequestDispatcher("../../view/admin/subject/packages.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int subjectId = Integer.parseInt(request.getParameter("subjectId"));
        int packageId = Integer.parseInt(request.getParameter("packageId"));
        String action = request.getParameter("type");
        PricePackageDAO packageDAO = new PricePackageDAO();
        String alert = "";
        String message = "";
        if (action.equals("add")) {
            int add = packageDAO.addPricePackageSubject(packageId, subjectId);
            if (add != -1) {
                alert = "success";
                message = "Add package success";
            } else {
                alert = "danger";
                message = "Add package failed";
            }
        } else if(action.equals("edit")) {
            int oldId = Integer.parseInt(request.getParameter("oldId"));
            int edit = packageDAO.updatePricePackageSubject(packageId, subjectId, oldId);
            if (edit != -1) {
                alert = "success";
                message = "Edit package success";
            } else {
                alert = "danger";
                message = "Edit package failed";
            }
        }else{
            int delete = packageDAO.deletePricePackageSubject(packageId, subjectId);
            if (delete != -1) {
                alert = "success";
                message = "Delete package success";
            } else {
                alert = "danger";
                message = "Delete package failed";
            }
        }

        request.setAttribute("alert", alert);
        request.setAttribute("message", message);
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
