/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin.subject.detail;

import dao.DimensonDAO;
import entity.Dimension;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author xuant
 */
public class DimensionSubjectServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DimensonDAO dimensonDAO = new DimensonDAO();
        int subjectId = Integer.parseInt(request.getParameter("subjectId"));
        List<Dimension> dimensions = dimensonDAO.getDimensionBySubject(subjectId);
        List<Dimension> all = dimensonDAO.findAllDimension(null, null);
        request.setAttribute("dimensions", dimensions);
        request.setAttribute("subjectId", subjectId);
        request.setAttribute("all", all);
        request.getRequestDispatcher("../../view/admin/subject/dimensions.jsp").forward(request, response);
    }

  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int subjectId = Integer.parseInt(request.getParameter("subjectId"));
        int dimensionId = Integer.parseInt(request.getParameter("dimensionId"));
        String action = request.getParameter("type");
        DimensonDAO dimensonDAO = new DimensonDAO();
        String alert = "";
        String message = "";
        if (action.equals("add")) {
            int add = dimensonDAO.addDimensionSubject(dimensionId, subjectId);
            if (add != -1) {
                alert = "success";
                message = "Add dimension success";
            } else {
                alert = "danger";
                message = "Add dimension failed";
            }
        } else if(action.equals("edit")) {
            int oldId = Integer.parseInt(request.getParameter("oldId"));
            int edit = dimensonDAO.editDimensionSubject(oldId, subjectId, dimensionId);
            if (edit != -1) {
                alert = "success";
                message = "Edit dimension success";
            } else {
                alert = "danger";
                message = "Edit dimension failed";
            }
        }else{
            int delete = dimensonDAO.deleteDimensionSubject(dimensionId, subjectId);
            if (delete != -1) {
                alert = "success";
                message = "Delete dimension success";
            } else {
                alert = "danger";
                message = "Delete dimension failed";
            }
        }

        request.setAttribute("alert", alert);
        request.setAttribute("message", message);
        processRequest(request, response);
    }

    
  
}
