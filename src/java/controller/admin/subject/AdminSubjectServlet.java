/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin.subject;

import dao.CategoryDAO;
import dao.SubjectDAO;
import entity.Category;
import entity.Subject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author xuant
 */
@MultipartConfig()
public class AdminSubjectServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String keyWord = request.getParameter("keyWord");
        
        SubjectDAO sdbc = new SubjectDAO();
        String page = request.getParameter("page");
        if (page == null || page.trim().length() == 0) {
            page = "1";
        }
        int pagesize = 2;
        int pageindex = Integer.parseInt(page);
        CategoryDAO cdbc = new CategoryDAO();
        ArrayList<Category> categorys = (ArrayList<Category>) cdbc.getAll();
        request.setAttribute("categorys", categorys);
        String categoryID = request.getParameter("category");
        String featured = request.getParameter("featured");
        ArrayList<Subject> subjects = sdbc.getSubjects(pageindex, pagesize, keyWord, categoryID, featured,null,null);
       
        request.setAttribute("subjects", subjects);
        request.setAttribute("keyWord", keyWord);
        int numofrecords = sdbc.count(keyWord, categoryID, featured);
        int totalpage = (numofrecords % pagesize == 0) ? (numofrecords / pagesize)
                : (numofrecords / pagesize) + 1;
        request.setAttribute("totalpage", totalpage);
        request.setAttribute("pa1gesize", pagesize);
        request.setAttribute("pageindex", pageindex);
        request.setAttribute("categoryID", categoryID);
        request.setAttribute("featured", featured);
        request.getRequestDispatcher("/view/admin/subjects.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String keyWord = request.getParameter("keyWord");
        SubjectDAO sdbc = new SubjectDAO();
        String page = request.getParameter("page");
        if (page == null || page.trim().length() == 0) {
            page = "1";
        }
        int pagesize = 2;
        int pageindex = Integer.parseInt(page);
        CategoryDAO cdbc = new CategoryDAO();
        ArrayList<Category> categorys = (ArrayList<Category>) cdbc.getAll();
        request.setAttribute("categorys", categorys);
        String categoryID = request.getParameter("category");
        String featured = request.getParameter("featured");

        ArrayList<Subject> subjects = sdbc.getSubjects(pageindex, pagesize, keyWord, categoryID, featured,null,null);
        request.setAttribute("subjects", subjects);
        request.setAttribute("keyWord", keyWord);
        int numofrecords = sdbc.count(keyWord, categoryID, featured);
        int totalpage = (numofrecords % pagesize == 0) ? (numofrecords / pagesize)
                : (numofrecords / pagesize) + 1;
        request.setAttribute("totalpage", totalpage);
        request.setAttribute("pa1gesize", pagesize);
        request.setAttribute("pageindex", pageindex);
        request.setAttribute("categoryID", categoryID);
        request.setAttribute("featured", featured);
        request.getRequestDispatcher("/view/admin/subjects.jsp").forward(request, response);
    }

}
