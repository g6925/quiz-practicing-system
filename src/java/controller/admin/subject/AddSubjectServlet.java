/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin.subject;

import dao.CategoryDAO;
import dao.ExpertDAO;
import dao.SubjectDAO;
import entity.Category;
import entity.MyMethod;
import entity.Subject;
import entity.User;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author xuant
 */
@MultipartConfig
public class AddSubjectServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CategoryDAO cdao = new CategoryDAO();
        ArrayList<Category> categorys = (ArrayList<Category>) cdao.getAll();
        request.setAttribute("categorys", categorys);
        String message = request.getParameter("message");
        String alert = request.getParameter("alert");
        if (message != null && alert != null) {
            request.setAttribute("message", message);
            request.setAttribute("alert", alert);
        }
        ExpertDAO edao = new ExpertDAO();
        ArrayList<User> experts = (ArrayList<User>) edao.getAll();
        request.setAttribute("experts", experts);
        request.getRequestDispatcher("../../view/admin/subject/add.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        User created = (User) session.getAttribute("user");
        SubjectDAO sdbc = new SubjectDAO();
        String name = request.getParameter("subject_name");
        int categoryID = Integer.parseInt(request.getParameter("subject_category"));
        boolean featured;
        String row_featued = request.getParameter("featured");
        if (row_featued != null) {
            featured = true;
        } else {
            featured = false;
        }
        int row_status = Integer.parseInt(request.getParameter("status"));
        boolean status;
        if (row_status == 1) {
            status = true;
        } else {
            status = false;
        }
        int expertId =Integer.parseInt(request.getParameter("expert"));
        String tagLine = request.getParameter("tag_line");
        String description = request.getParameter("subject_description");
        Part file = request.getPart("image");
        double listedPrice = Double.valueOf(request.getParameter("listedPrice"));
        double salePrice = Double.valueOf(request.getParameter("salePrice"));
        String thumnail = MyMethod.upload(request, file, "assets/images/courses");
        Category c = new Category();
        c.setId(categoryID);
        User expert = new User();
        expert.setId(expertId);
        
        Subject s = new Subject();
        s.setThumbnail(thumnail);
        s.setTitle(name);
        s.setDescription(description);
        s.setTagline(tagLine);
        s.setStatus(status);
        s.setFeatured(featured);
        s.setTaught(expert);
        s.setCategory(c);
        s.setUpdateDate(MyMethod.getT_now());
        s.setSalePrice(salePrice);
        s.setListedPrice(listedPrice);
        s.setCreated(created);
        int test = sdbc.addSubject(s);
        
        if (test != 0) {
            response.sendRedirect("add?message=Create Success&alert=success");
        } else {
            response.sendRedirect("add?message=Create Failure&alert=danger");
        }
    }

}
