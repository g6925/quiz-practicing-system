/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin.lesson;

import dao.LessonDAO;
import dao.SubjectDAO;
import entity.Lesson;
import entity.Subject;
import entity.Topic;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Duong-PC
 */
public class LessonServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        LessonDAO lessonDAO = new LessonDAO();
        String subjectId = request.getParameter("subjectId");
        if (subjectId != null) {
            int sId = Integer.parseInt(subjectId);
            request.setAttribute("subjectId", subjectId);
            String action = request.getParameter("action");
            String name = request.getParameter("name");
            request.setAttribute("keyWord", name);
            String active = request.getParameter("active");
            String type = request.getParameter("type");
            request.setAttribute("typ", type);
            request.setAttribute("act", active);
            if (action != null) {
                request.setAttribute("action", action);
                if (action.equals("add") || action.equals("edit")) {
                    request.setAttribute("topics", lessonDAO.findAllTopicBySid(sId));
                    if (action.equals("edit")) {
                        String lessonId = request.getParameter("lessonId");
                        if (lessonId != null) {
                            int lId = Integer.parseInt(lessonId);
                            Lesson lesson = lessonDAO.findLessonById(lId);
                            request.setAttribute("lesson", lesson);
                        }
                        request.getRequestDispatcher("../view/admin/lesson/add-edit.jsp").forward(request, response);
                    }
                    if (action.equals("add")) {
                        request.getRequestDispatcher("../view/admin/lesson/add-edit.jsp").forward(request, response);
                    }
                    return;
                }
            }
            request.setAttribute("types", lessonDAO.findAllDistinctLessonType());
            request.setAttribute("lessons", lessonDAO.findAllLessonBySubjectId(sId, name, active, type));
        }
        request.getRequestDispatcher("../view/admin/lessons.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        LessonDAO lessonDAO = new LessonDAO();
        String action = request.getParameter("action");
        if (action != null) {
            if (action.equals("add") || action.equals("edit")) {
                String name = request.getParameter("name");
                String content = request.getParameter("content");
                String subjectId = request.getParameter("subjectId");
                String topicId = request.getParameter("topicId");
                String video = request.getParameter("video");
                String order = request.getParameter("order");
                int status = Integer.parseInt(request.getParameter("status"));
                int rtype = Integer.parseInt(request.getParameter("type"));
                Lesson lesson = new Lesson();
                lesson.setName(name);
                Subject subject = new Subject();
                subject.setId(Integer.parseInt(subjectId));
                Topic topic = new Topic();
                topic.setId(Integer.parseInt(topicId));
                topic.setSubject(subject);
                lesson.setTopic(topic);
                String type = "";
                if (rtype == 0) {
                    type = "Subject Topic";
                    video = "";
                    content = "";
                } else if (rtype == 1) {
                    type = "Lesson";
                } else {
                    type = "Quiz";
                    video = "";
                }
                lesson.setUrl(video);
                lesson.setDescription(content);
                lesson.setOrder(Integer.parseInt(order));
                lesson.setStatus(status == 1 ? true : false);
                lesson.setType(type);
                String alert = "";
                String message = "";
                if (action.equals("add")) {
                    if (lessonDAO.addLesson(lesson) != 0) {
                        alert = "success";
                        message = "Added Successfully";
                    } else {
                        alert = "danger";
                        message = "Added Failed";
                    }
                }
                if (action.equals("edit")) {
                    lesson.setId(Integer.parseInt(request.getParameter("lessonId")));
                    if (lessonDAO.updateLesson(lesson) != 0) {
                        alert = "success";
                        message = "Edited Successfully";
                    } else {
                        alert = "danger";
                        message = "Edited Failed";
                    }
                }
                request.setAttribute("alert", alert);
                request.setAttribute("message", message);
            }
            if (action.equals("changeStatus")) {
                int lessonId = Integer.parseInt(request.getParameter("lessonId"));
                boolean status = Boolean.parseBoolean(request.getParameter("status"));
                if (lessonDAO.changeStatusLesson(lessonId, status) != 0) {
                    response.getWriter().write("success");
                } else {
                    response.getWriter().write("failed");
                }
                return;
            }
        }
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
