/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.marketing.post;

import dao.CategoryDAO;
import dao.PostDAO;
import entity.Category;
import entity.MyMethod;
import entity.Post;
import entity.User;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author xuant
 */
@MultipartConfig
public class AddPostServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CategoryDAO cdao = new CategoryDAO();
        List<Category> categorys = cdao.getAll();
        request.setAttribute("categorys", categorys);
        String message = request.getParameter("message");
        String alert = request.getParameter("alert");
        if (message != null && alert != null) {
            request.setAttribute("message", message);
            request.setAttribute("alert", alert);
        }
        request.getRequestDispatcher("../../view/marketing/post/add.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        PostDAO pd = new PostDAO();
        String title = request.getParameter("title");
        int categoryID = Integer.parseInt(request.getParameter("category"));
        boolean status = Integer.parseInt(request.getParameter("status"))  == 1 ? true : false;
        String brief = request.getParameter("brief");
        String content = request.getParameter("content");
        Part file = request.getPart("thumbnail");
        String thumnail = MyMethod.upload(request, file, "assets/images/posts");
        Category c = new Category(categoryID);
        User u = new User(1);
        Post p = new Post();
        p.setTitle(title);
        p.setStatus(status);
        p.setDate(MyMethod.getT_now());
        p.setContent(content);
        p.setCategory(c);
        p.setUser(u);
        p.setThumbnail(thumnail);
        p.setBrief(brief);
        int test = pd.addPost(p);
        if (test != 0) {
            response.sendRedirect("add?message=Create Success&alert=success");
        } else {
            response.sendRedirect("add?message=Create Failure&alert=danger");
        }
    }
}
