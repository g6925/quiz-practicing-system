/*
 * and open the template in the editor.
 */
package controller.marketing.post;

import dao.CategoryDAO;
import dao.PostDAO;
import entity.Category;
import entity.MyMethod;
import entity.Post;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author xuant
 */
@MultipartConfig
public class EditPostServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        int pId = Integer.parseInt(request.getParameter("pId"));
        PostDAO pd = new PostDAO();
        Post post = pd.getPostByID(pd.getConnection(), pId);
        request.setAttribute("post", post);
        CategoryDAO cdao = new CategoryDAO();
        List<Category> categorys = cdao.getAll();
        request.setAttribute("categorys", categorys);
        request.getRequestDispatcher("../../view/marketing/post/edit.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        PostDAO pd = new PostDAO();
        int id = Integer.parseInt(request.getParameter("id"));
        Post p = pd.getPostByID(pd.getConnection(), id);
        String title = request.getParameter("title");
        int categoryID = Integer.parseInt(request.getParameter("category"));
        boolean status = Integer.parseInt(request.getParameter("status"))  == 1 ? true : false;
        String brief = request.getParameter("brief");
        String content = request.getParameter("content");
        Part file = request.getPart("thumbnail");
        if (file.getSize() != 0) {
            String thumnail = MyMethod.upload(request, file, "assets/images/posts");
            p.setThumbnail(thumnail);
        }
        Category c = new Category(categoryID);
        p.setTitle(title);
        p.setStatus(status);
        p.setContent(content);
        p.setCategory(c);
        p.setBrief(brief);
        pd.editPost(p);
        response.sendRedirect("../posts");
    }
}
