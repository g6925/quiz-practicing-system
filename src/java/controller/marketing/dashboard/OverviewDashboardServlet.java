/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.marketing.dashboard;

import com.google.gson.Gson;
import dao.CategoryDAO;
import dao.RegisterDAO;
import dao.SubjectDAO;
import entity.Apexcharts;
import entity.MyMethod;
import static entity.MyMethod.addCategoryPieDataCharts;
import static entity.MyMethod.addRevenueDataChartsByMonth;
import static entity.MyMethod.getDataCharts;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author dclon
 */
public class OverviewDashboardServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = request.getRequestURI();
        CategoryDAO cd = new CategoryDAO();
        String categoryID = request.getParameter("category");
        categoryID = categoryID == null ? "0" : categoryID;
        String duration = request.getParameter("duration");
        duration = duration == null ? "12 months" : duration;
        request.setAttribute("categoryID", categoryID);
        request.setAttribute("duration", duration);
        request.setAttribute("categories", cd.getAll());
        request.setAttribute("rd", new RegisterDAO());
        request.setAttribute("sd", new SubjectDAO());
        List<Apexcharts> listData = null;
        switch (duration) {
            case "12 months":
            case "6 months":
                listData = MyMethod.getDataCharts(Integer.parseInt(categoryID), MyMethod.getAmountCharts(duration), Calendar.getInstance(), (data, amount, calendar, cID) -> MyMethod.addRevenueDataChartsByMonth(data, amount, calendar, cID));
                break;
            case "2 months":
            case "1 month":
                listData = MyMethod.getDataCharts(Integer.parseInt(categoryID), MyMethod.getAmountCharts(duration), Calendar.getInstance(), (data, amount, calendar,cID) -> MyMethod.addRevenueDataChartsByWeek(data, amount, calendar, cID));
                break;
            case "1 week":
                listData = MyMethod.getDataCharts(Integer.parseInt(categoryID), MyMethod.getAmountCharts(duration), Calendar.getInstance(), (data, amount, calendar, cID) -> MyMethod.addRevenueDataChartsByDay(data, amount, calendar, cID));
                break;
        }
        Gson gson = new Gson();
        Apexcharts[] arr = listData.toArray(new Apexcharts[0]);
        String dataCharts = gson.toJson(arr);
        request.setAttribute("dataCharts", dataCharts);
        request.setAttribute("time", new Timestamp(Calendar.getInstance().getTimeInMillis()));
        if (url.contains("overview")) {
            request.getRequestDispatcher("../../view/marketing/dashboard.jsp").forward(request, response);
        } else if(url.contains("dashboard")) {
            request.getRequestDispatcher("../view/marketing/dashboard.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/view/marketing/dashboard.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
