/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.marketing.slider;

import dao.SliderDAO;
import entity.MyMethod;
import entity.Slider;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author taina
 */
@MultipartConfig
public class AddSliderServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddSliderServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddSliderServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String message = request.getParameter("message");
        String alert = request.getParameter("alert");
        if (message != null && alert != null) {
            request.setAttribute("message", message);
            request.setAttribute("alert", alert);
        }
//        String name = request.getParameter("slider_name");
//        String backlink = request.getParameter("backlink");
//        int status_raw = Integer.parseInt(request.getParameter("status"));
//        boolean status;
//        if (status_raw == 1) {
//            status = true;
//        } else {
//            status = false;
//        }
//        String content = request.getParameter("slider_content");
//        request.setAttribute("slider_name", name);
//        request.setAttribute("backlink", backlink);
//        request.setAttribute("status", status);
//        request.setAttribute("slider_content", content);
        request.getRequestDispatcher("../../view/marketing/slider/add.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession ss = request.getSession();
        User user = (User) ss.getAttribute("user");
        SliderDAO sd = new SliderDAO();
        String name = request.getParameter("slider_name");
        String backlink = request.getParameter("backlink");
        int status_raw = Integer.parseInt(request.getParameter("status"));
        boolean status;
        if (status_raw == 1) {
            status = true;
        } else {
            status = false;
        }
        String content = request.getParameter("slider_content");
        Part file = request.getPart("image");
        String thumnail = MyMethod.upload(request, file, "assets/images/slider");
        Slider s = new Slider(name, thumnail, content, status, backlink, user);
        int test = sd.addSlider(s);
        if (test != 0) {
            request.setAttribute("message", "New Slider Added!");
            request.setAttribute("alert", "Successful");
            request.getRequestDispatcher("../../view/marketing/slider/add.jsp").forward(request, response);
        } else {
            request.setAttribute("message", "New Slider Added Failed!");
            request.setAttribute("alert", "Failed");
            request.getRequestDispatcher("../../view/marketing/slider/add.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
