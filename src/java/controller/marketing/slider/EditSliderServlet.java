/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.marketing.slider;

import dao.SliderDAO;
import entity.MyMethod;
import entity.Slider;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author taina
 */
@MultipartConfig
public class EditSliderServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditSliderServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditSliderServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_raw = request.getParameter("id");
        int id = Integer.parseInt(id_raw);
        SliderDAO sd = new SliderDAO();
        Slider s = sd.getSliderByID(sd.getConnection(), id);
        request.setAttribute("slider", s);
        request.getRequestDispatcher("../../view/marketing/slider/edit.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        HttpSession ss = request.getSession();
        User user = (User) ss.getAttribute("user");
        SliderDAO sd = new SliderDAO();
        int id = Integer.parseInt(request.getParameter("slider_id"));
        String name = request.getParameter("slider_name");
        String backlink = request.getParameter("backlink");
        int status_raw = Integer.parseInt(request.getParameter("status"));
        boolean status;
        if (status_raw == 1) {
            status = true;
        } else {
            status = false;
        }
        String content = request.getParameter("slider_content");
        Part file = request.getPart("image");
        Slider s = sd.getSliderByID(sd.getConnection(), id);
        s.setTitle(name);
        if (file.getSize() != 0) {
            String image = MyMethod.upload(request, file, "assets/images/slider");
            s.setImage(image);
        }
        s.setContent(content);
        s.setStatus(status);
        s.setBacklink(backlink);
        s.setUser(user);
        sd.editSlider(s);
        response.sendRedirect("../sliders");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
