/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.marketing.slider;

import dao.SliderDAO;
import entity.Slider;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author taina
 */
public class SliderListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SliderListServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SliderListServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String keyWord = request.getParameter("keyWord");
        String status_raw = request.getParameter("status");
        String type_raw = request.getParameter("type");
        SliderDAO s = new SliderDAO();
        int status;
        if (status_raw == null || status_raw.equals("")) {
            status = 2;
        } else {
            status = Integer.parseInt(status_raw);
        }
        int type;
        if (type_raw == null || type_raw.equals("")) {
            type = 2;
        } else {
            type = Integer.parseInt(type_raw);
        }
//        String page = request.getParameter("page");
//        if (page == null || page.trim().length() == 0) {
//            page = "1";
//        }
//        int pagesize = 2;
//        int pageindex = Integer.parseInt(page);
//        ArrayList<Slider> slider = s.getSlider(pageindex, pagesize, keyWord, status);
//        int numofrecords = s.count(keyWord);
//        int totalpage = (numofrecords % pagesize == 0) ? (numofrecords / pagesize)
//                : (numofrecords / pagesize) + 1;
        List<Slider> sl = s.search(keyWord, status, type);
        int page, numperpage = 6, size = sl.size(), num = (size % 6 == 0 ? (size / 6) : ((size / 6)) + 1);
        String xpage = request.getParameter("page");
        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int start, end;
        start = (page - 1) * numperpage;
        end = Math.min(page * numperpage, size);
        List<Slider> slider = s.getListByPage(sl, start, end);
        request.setAttribute("slider", slider);
        request.setAttribute("keyWord", keyWord);
        request.setAttribute("stt", status);
        request.setAttribute("type", type);
        request.setAttribute("page", page);
        request.setAttribute("num", num);
        request.getRequestDispatcher("../view/marketing/sliders.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String keyWord = request.getParameter("keyWord");
        String status_raw = request.getParameter("status");
        String type_raw = request.getParameter("type");
        SliderDAO s = new SliderDAO();
        int status;
        if (status_raw == null || status_raw.equals("")) {
            status = 2;
        } else {
            status = Integer.parseInt(status_raw);
        }
        int type;
        if (type_raw == null || type_raw.equals("")) {
            type = 2;
        } else {
            type = Integer.parseInt(type_raw);
        }
//        String page = request.getParameter("page");
//        if (page == null || page.trim().length() == 0) {
//            page = "1";
//        }
//        int pagesize = 2;
//        int pageindex = Integer.parseInt(page);
//        ArrayList<Slider> slider = s.getSlider(pageindex, pagesize, keyWord, status);
//        int numofrecords = s.count(keyWord);
//        int totalpage = (numofrecords % pagesize == 0) ? (numofrecords / pagesize)
//                : (numofrecords / pagesize) + 1;
        List<Slider> sl = s.search(keyWord, status, type);
        int page, numperpage = 6, size = sl.size(), num = (size % 6 == 0 ? (size / 6) : ((size / 6)) + 1);
        String xpage = request.getParameter("page");
        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int start, end;
        start = (page - 1) * numperpage;
        end = Math.min(page * numperpage, size);
        List<Slider> slider = s.getListByPage(sl, start, end);
        request.setAttribute("slider", slider);
        request.setAttribute("keyWord", keyWord);
        request.setAttribute("stt", status);
        request.setAttribute("type", type);
        request.setAttribute("page", page);
        request.setAttribute("num", num);
        request.getRequestDispatcher("../view/marketing/sliders.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
