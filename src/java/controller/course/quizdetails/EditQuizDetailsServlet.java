/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.course.quizdetails;

import dao.DSQDAO;
import dao.DimensonDAO;
import dao.LevelDAO;
import dao.QuestionDAO;
import dao.QuizDAO;
import dao.QuizQuestionDAO;
import dao.SubjectDAO;
import dao.TopicDAO;
import entity.Dimension;
import entity.Level;
import entity.Question;
import entity.Quiz;
import entity.Subject;
import entity.Topic;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Jarnsaurus
 */
public class EditQuizDetailsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        QuizDAO qd = new QuizDAO();
        String id_raw = request.getParameter("id");
        int id = Integer.parseInt(id_raw);
        Quiz q = qd.getQuizByID(qd.getConnection(), id);
        SubjectDAO sd = new SubjectDAO();
        List<Subject> s = sd.getAll();
        QuizQuestionDAO qqd = new QuizQuestionDAO();
        int total = qqd.countQuizQuestionByID(qqd.getConnection(), id);
        request.setAttribute("total", total);
        request.setAttribute("subject", s);
        request.setAttribute("quiz", q);
        request.getRequestDispatcher("../../view/course/editquizdetails.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_raw = request.getParameter("id");
        int id = Integer.parseInt(id_raw);
        String name = request.getParameter("name");
        String subjectId_raw = request.getParameter("subjectId");
        String level = request.getParameter("level");
        String duration = request.getParameter("duration");
        float rate = Float.parseFloat(request.getParameter("rate"));
        String type = request.getParameter("type");
        String description = request.getParameter("description");
        int total = Integer.parseInt(request.getParameter("total"));
        String qType = request.getParameter("qType");
        String qqType[] = request.getParameterValues("qqType");
        QuestionDAO qd = new QuestionDAO();
        QuizDAO qzd = new QuizDAO();
        Quiz q = qzd.getQuizByID(qzd.getConnection(), id);
        SubjectDAO sd = new SubjectDAO();
        List<Subject> s = sd.getAll();
        request.setAttribute("quiz", q);
        request.setAttribute("subject", s);
        request.setAttribute("qttype", qType);
        request.setAttribute("total", total);
        String qTypeB[] = request.getParameterValues("qTypeB");
        PrintWriter out = response.getWriter();
//        out.write("qq: \n");
//        for (int i = 0; i < qqType.length; i++) {
//            out.write(qqType[i] + " ");
//        }
        if (qqType == null) {
            if (qType.equals("Topic")) {
                TopicDAO td = new TopicDAO();
                List<Topic> t = td.getListTopicsBySubjectID(Integer.parseInt(subjectId_raw));
                int count = t.size();
                request.setAttribute("qqType", t);
                request.setAttribute("qqTypeC", count);
            }
            if (qType.equals("Group")) {
                DimensonDAO dd = new DimensonDAO();
                List<Dimension> d = dd.getAllGroup();
                int count = d.size();
                request.setAttribute("qqType", d);
                request.setAttribute("qqTypeC", count);
            }
            if (qType.equals("Domain")) {
                DimensonDAO dd = new DimensonDAO();
                List<Dimension> d = dd.getAllDomain();
                int count = d.size();
                request.setAttribute("qqType", d);
                request.setAttribute("qqTypeC", count);
            }
        } else {
            Quiz qz = new Quiz();
            qz.setName(name);
            Subject subject = sd.getSubjectByID(sd.getConnection(), Integer.parseInt(subjectId_raw));
            qz.setSubject(subject);
            qz.setLevel(level);
            Time t = Time.valueOf(duration);
            qz.setDuration(t);
            qz.setPassRate(rate);
            qz.setDescription(description);
            qz.setType(type);
            if (qType.equals("Topic")) {
                int count = 0;
                List<Question> quest = new ArrayList<>();
                for (int i = 0; i < qqType.length; i++) {
                    quest = qd.getQByTopicId(Integer.parseInt(qqType[i]), subject.getId());
                    count += quest.size();
                }
                if (total > count) {
                    request.setAttribute("warning", "Total > Number Of Questions Existed!");
                    request.setAttribute("alert", "Failed");
                } else {
                    int qzId = qzd.addQuiz(qz);
                    List<Question> listQ = qd.getQByTopics(qqType);
                    QuizQuestionDAO qqd = new QuizQuestionDAO();
                    int totalMed = 0, totalEasy = 0, totalHard = 0;
                    if (level.equals("Easy")) {
                        totalMed = (total / 2) / 2;
                        totalHard = totalMed / 2;
                        totalEasy = total - (totalHard + totalMed);
                    }
                    if (level.equals("Medium")) {
                        totalMed = total / 2;
                        totalHard = totalMed / 2;
                        totalEasy = total - (totalHard + totalMed);
                    }
                    if (level.equals("Hard")) {
                        totalHard = total / 2;
                        totalEasy = totalHard / 2;
                        totalMed = total - (totalHard + totalEasy);
                    }
                    int countQ = 0;
                    for (Question question : listQ) {
                        if (question.getLevel() == 2) {
                            if (countQ == totalMed) {
                                countQ = 0;
                                break;
                            } else {
                                qqd.insert(question.getId(), qzId);
                                countQ++;
                            }
                        }
                    }
                    for (Question question : listQ) {
                        if (question.getLevel() == 3) {
                            if (countQ == totalHard) {
                                countQ = 0;
                                break;
                            } else {
                                qqd.insert(question.getId(), qzId);
                                countQ++;
                            }
                        }
                    }
                    for (Question question : listQ) {
                        if (question.getLevel() == 1) {
                            if (countQ == totalEasy) {
                                countQ = 0;
                                break;
                            } else {
                                qqd.insert(question.getId(), qzId);
                                countQ++;
                            }
                        }
                    }
                    request.setAttribute("message", "New Quiz Added Successful!");
                    request.setAttribute("alert", "Successful");
                    List<Question> questList = qqd.getAllQuestIdByQuizId(qzId);
                    request.setAttribute("list", questList);
                    request.getRequestDispatcher("/view/course/quizdetailslist.jsp").forward(request, response);
                }
            }
            if (qType.equals("Group")) {
                int count = 0;
                DSQDAO dsq = new DSQDAO();
                List<Question> quest = new ArrayList<>();
                for (int i = 0; i < qqType.length; i++) {
                    quest = qd.getQByGroup(Integer.parseInt(qqType[i]), subject.getId());
                    count += quest.size();
                }
                if (total > count) {
                    request.setAttribute("warning", "Total > Number Of Questions Existed!");
                    request.setAttribute("alert", "Failed");
                } else {
                    int qzId = qzd.addQuiz(qz);
                    List<Question> listQ = dsq.getQuestByDimension(qqType);
                    QuizQuestionDAO qqd = new QuizQuestionDAO();
                    int totalMed = 0, totalEasy = 0, totalHard = 0;
                    if (level.equals("Easy")) {
                        totalMed = (total / 2) / 2;
                        totalHard = totalMed / 2;
                        totalEasy = total - (totalHard + totalMed);
                    }
                    if (level.equals("Medium")) {
                        totalMed = total / 2;
                        totalHard = totalMed / 2;
                        totalEasy = total - (totalHard + totalMed);
                    }
                    if (level.equals("Hard")) {
                        totalHard = total / 2;
                        totalEasy = totalHard / 2;
                        totalMed = total - (totalHard + totalEasy);
                    }
                    for (Question question : listQ) {
                        qqd.insert(question.getId(), qzId);
                    }
                    int countQ = 0;
                    for (Question question : listQ) {
                        if (question.getLevel() == 2) {
                            if (countQ == totalMed) {
                                countQ = 0;
                                break;
                            } else {
                                qqd.insert(question.getId(), qzId);
                                countQ++;
                            }
                        }
                    }
                    for (Question question : listQ) {
                        if (question.getLevel() == 3) {
                            if (countQ == totalHard) {
                                countQ = 0;
                                break;
                            } else {
                                qqd.insert(question.getId(), qzId);
                                countQ++;
                            }
                        }
                    }
                    for (Question question : listQ) {
                        if (question.getLevel() == 1) {
                            if (countQ == totalEasy) {
                                countQ = 0;
                                break;
                            } else {
                                qqd.insert(question.getId(), qzId);
                                countQ++;
                            }
                        }
                    }
                    request.setAttribute("message", "New Quiz Added Successful!");
                    request.setAttribute("alert", "Successful");
                    List<Question> questList = qqd.getAllQuestIdByQuizId(qzId);
                    request.setAttribute("list", questList);
                    request.getRequestDispatcher("/view/course/quizdetailslist.jsp").forward(request, response);
                }
            }
            if (qType.equals("Domain")) {
                int quest = 0;
                DSQDAO dsq = new DSQDAO();
                for (int i = 0; i < qqType.length; i++) {
                    quest += dsq.getQByDomain(Integer.parseInt(qqType[i]), subject.getId());
                }
                if (total > quest) {
                    request.setAttribute("warning", "Total > Number Of Questions Existed!");
                    request.setAttribute("alert", "Failed");
                } else {
                    int qzId = qzd.addQuiz(qz);
                    List<Question> listQ = dsq.getQuestByDimension(qqType);
                    QuizQuestionDAO qqd = new QuizQuestionDAO();
                    int totalMed = 0, totalEasy = 0, totalHard = 0;
                    if (level.equals("Easy")) {
                        totalMed = (total / 2) / 2;
                        totalHard = totalMed / 2;
                        totalEasy = total - (totalHard + totalMed);
                    }
                    if (level.equals("Medium")) {
                        totalMed = total / 2;
                        totalHard = totalMed / 2;
                        totalEasy = total - (totalHard + totalMed);
                    }
                    if (level.equals("Hard")) {
                        totalHard = total / 2;
                        totalEasy = totalHard / 2;
                        totalMed = total - (totalHard + totalEasy);
                    }
                    for (Question question : listQ) {
                        qqd.insert(question.getId(), qzId);
                    }
                    int countQ = 0;
                    for (Question question : listQ) {
                        if (question.getLevel() == 2) {
                            if (countQ == totalMed) {
                                countQ = 0;
                                break;
                            } else {
                                qqd.insert(question.getId(), qzId);
                                countQ++;
                            }
                        }
                    }
                    for (Question question : listQ) {
                        if (question.getLevel() == 3) {
                            if (countQ == totalHard) {
                                countQ = 0;
                                break;
                            } else {
                                qqd.insert(question.getId(), qzId);
                                countQ++;
                            }
                        }
                    }
                    for (Question question : listQ) {
                        if (question.getLevel() == 1) {
                            if (countQ == totalEasy) {
                                countQ = 0;
                                break;
                            } else {
                                qqd.insert(question.getId(), qzId);
                                countQ++;
                            }
                        }
                    }
                    request.setAttribute("message", "New Quiz Added Successful!");
                    request.setAttribute("alert", "Successful");
                    List<Question> questList = qqd.getAllQuestIdByQuizId(qzId);
                    request.setAttribute("list", questList);
                    request.getRequestDispatcher("/view/course/quizdetailslist.jsp").forward(request, response);
                }
            }
        }
        request.setAttribute("qTypeB", qType);
        request.getRequestDispatcher("/view/course/editquizdetails.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
