/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.course.price;

import dao.PricePackageDAO;
import entity.PricePackage;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author taina
 */
public class AddPricePackageServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddPricePackageServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddPricePackageServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String message = request.getParameter("message");
        String alert = request.getParameter("alert");
        if (message != null && alert != null) {
            request.setAttribute("message", message);
            request.setAttribute("alert", alert);
        }
        request.getRequestDispatcher("../../view/course/price-package/add.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PricePackageDAO pp = new PricePackageDAO();
        String name = request.getParameter("name");
        int duration = Integer.parseInt(request.getParameter("duration"));
        int status_raw = Integer.parseInt(request.getParameter("status"));
        boolean status;
        if (status_raw == 1) {
            status = true;
        } else {
            status = false;
        }
        int priceRate = Integer.parseInt(request.getParameter("priceRate"));
        String description = request.getParameter("description");
        PricePackage p = new PricePackage(name, duration, priceRate, description, status);
        int test = pp.addPricePackage(p);
        if (test != 0) {
            request.setAttribute("message", "New Price Package Added!");
            request.setAttribute("alert", "Successful");
            request.getRequestDispatcher("../../view/course/price-package/add.jsp").forward(request, response);
//            response.sendRedirect("add?message=Create Success&alert=success");
        } else {
            request.setAttribute("message", "New Price Package Added Failed!");
            request.setAttribute("alert", "Failed");
            request.getRequestDispatcher("../../view/course/price-package/add.jsp").forward(request, response);
//            response.sendRedirect("add?message=Create Failure&alert=danger");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
