/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.quiz;

import dao.QuestionDAO;
import dao.QuizDAO;
import dao.QuizHandleDAO;
import entity.Quiz;
import entity.User;
import entity.UserQuiz;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author vietd
 */
public class QuizController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user != null) {
            String qId = request.getParameter("quizId");
            request.setAttribute("quizId", qId);
            if (qId == null) {
                // response error
                response.getWriter().write("error quiz id");
            } else {
                QuizDAO qd = new QuizDAO();
                int quizId = Integer.parseInt(qId);
                Quiz quiz = qd.getQuizByIdFirst(quizId);
                String group_raw = request.getParameter("group");
                String type_raw = request.getParameter("type");
                if (type_raw != null) {
                    if (type_raw.equals("1")) {
                        type_raw = "Subject Topic";
                    }
                    if (type_raw.equals("2")) {
                        type_raw = "Group";
                    }
                    if (type_raw.equals("3")) {
                        type_raw = "Domain";
                    }
                    request.setAttribute("setTypeGroup", "&type=" + type_raw + "&group=" + group_raw);
                }

                if (quiz == null) {
                    // response error
                    response.getWriter().write("error quiz id");
                } else {
                    QuizHandleDAO quizHandleDAO = new QuizHandleDAO();
                    boolean complete = quizHandleDAO.checkUserHasComplete(quizId, user.getId());
                    try {
                        if (type_raw == null) {
                            UserQuiz u = new QuizDAO().getUserQuiz(quizId, user.getId());
                            if (type_raw == null) {
                                type_raw = u.getFilterType();
                                group_raw = u.getFilterGroup() + "";
                            }
                        }
                    } catch (Exception ex) {

                    }
                    if (complete) {
                        request.setAttribute("hasDo", 1);
                    } else {
                        request.setAttribute("hasDo", 0);
                    }
                    String actionTake = request.getParameter("takeQuiz");
                    if (actionTake != null && actionTake.equalsIgnoreCase("true")) {
                        LocalDateTime currentTime = LocalDateTime.now();
                        if (complete) {
                            if (quiz.getType().equalsIgnoreCase("Practice")) {
                                response.getWriter().write("quiz has complete");
                                String actionRetake = request.getParameter("retake");
                                if (actionRetake != null && actionRetake.equalsIgnoreCase("true")) {
                                    Time timeDuration = quiz.getDuration();
                                    LocalDateTime endTime = currentTime.plusHours(timeDuration.getHours())
                                            .plusMinutes(timeDuration.getMinutes())
                                            .plusSeconds(timeDuration.getSeconds());
                                    quizHandleDAO.updateRetake(currentTime, endTime, user.getId(), quizId);
                                    try {
                                        new QuestionDAO().clearOldAnswer(user.getId(), quizId);
                                        request.getRequestDispatcher("/view/quiz/take-quiz.jsp").forward(request, response);
                                        return;
                                    } catch (Exception e) {
                                        response.getWriter().write("Cannot retake");
                                        return;
                                    }
                                }
                            } else {
                                response.getWriter().write("quiz has complete");
                            }
                        } else {
                            if (quiz.getType().equalsIgnoreCase("Practice")) {
                                if (!quizHandleDAO.checkUserDoQuiz(quizId, user.getId())) {
                                    UserQuiz userQuiz = new UserQuiz();
                                    userQuiz.setUserId(user.getId());
                                    userQuiz.setQuiz(quiz);
                                    userQuiz.setStartedOn(currentTime);
                                    userQuiz.setFilterType(type_raw);
                                    userQuiz.setFilterGroup(group_raw != null ? Integer.parseInt(group_raw) : 0);
                                    quizHandleDAO.insertOrUpdateUserQuiz(userQuiz);
                                } else {
                                }
                            } else {
                                if (!quizHandleDAO.checkUserDoQuiz(quizId, user.getId())) {
                                    Time timeDuration = quiz.getDuration();
                                    LocalDateTime endTime = currentTime.plusHours(timeDuration.getHours())
                                            .plusMinutes(timeDuration.getMinutes())
                                            .plusSeconds(timeDuration.getSeconds());
                                    UserQuiz userQuiz = new UserQuiz();
                                    userQuiz.setUserId(user.getId());
                                    userQuiz.setQuiz(quiz);
                                    userQuiz.setStartedOn(currentTime);
                                    userQuiz.setExpireTime(endTime);
                                    quizHandleDAO.insertOrUpdateUserQuiz(userQuiz);
                                } else {
                                    if (quizHandleDAO.checkExpired(quizId, user.getId(), currentTime) <= 0) {
                                        response.getWriter().write("quiz has complete because expiredTime");
                                        return;
                                    }
                                }
                            }
                            request.getRequestDispatcher("/view/quiz/take-quiz.jsp").forward(request, response);
                            return;
                        }
                    }
                    request.setAttribute("quiz", quiz);
                    request.getRequestDispatcher("/view/quiz/quiz-info.jsp").forward(request, response);
                }
            }
        } else {
            response.sendRedirect("../common/home");
        }

    }

}
