/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.quiz;

import com.google.gson.Gson;
import dao.CompleteQuestionDAO;
import dao.QuestionDAO;
import dao.QuizDAO;
import dao.QuizQuestionDAO;
import entity.Answer;
import entity.Question;
import entity.Quiz;
import entity.User;
import entity.UserQuiz;
import entity.UserQuizQuestion;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author vietd
 */
public class ReviewQuizController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");

        try {
            User account = (User) request.getSession().getAttribute("user");
            if (account == null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You need to loginfirst");
                return;
            }
            int userId = account.getId();
            int quizId = Integer.parseInt(request.getParameter("quizId"));
            UserQuiz userQuiz = new QuizDAO().getUserQuiz(quizId, userId);
            List<UserQuizQuestion> answers = new CompleteQuestionDAO().listAllQuizReview(userId, quizId);
            List<Question> totalQuestion = new QuestionDAO().getAllQuestionInQuiz(quizId, userId, true);
            // calculate score
            float scorePerQuestion = (float) 10 / totalQuestion.size();
            request.setAttribute("scorePerQuestion", scorePerQuestion);
            request.setAttribute("userQuiz", userQuiz);
            request.setAttribute("userAnswers", answers);
            request.setAttribute("questions", totalQuestion);
        } catch (Exception e) {

        }
        request.getRequestDispatcher("/view/quiz/review-quiz.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
