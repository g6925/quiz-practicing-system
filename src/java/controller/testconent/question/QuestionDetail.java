/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.testconent.question;

import dao.AnswerDAO;
import dao.DBContext;
import dao.DimensonDAO;
import dao.LevelDAO;
import dao.QuestionDAO;
import dao.SubjectDAO;
import dao.TopicDAO;
import entity.Answer;
import entity.Dimension;
import entity.Level;
import entity.Question;
import entity.Subject;
import entity.Topic;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hiepx
 */
public class QuestionDetail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuestionDetail</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuestionDetail at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("add") != null) {
            request.setAttribute("add", "add");
        }
        if (request.getAttribute("mess") != null) {
            request.setAttribute("add", "add");
            request.setAttribute("mess", request.getAttribute("mess"));
        }
        int questionID = 3;
        if (request.getParameter("questionID") != null) {
            questionID = Integer.parseInt(request.getParameter("questionID"));
        }
        if (request.getAttribute("questionID") != null) {
            questionID = (int) request.getAttribute("questionID");
        }
        QuestionDAO qdao = new QuestionDAO();
        Question q = qdao.getQuestionByQuestiontID(questionID);
        SubjectDAO sdao = new SubjectDAO();
        List<Subject> lsubject = sdao.getAll();
        TopicDAO tdao = new TopicDAO();
        List<Topic> ltopic = tdao.getListTopicsBySubjectID(q.getSubject().getId());
        DimensonDAO ddao = new DimensonDAO();
        List<Dimension> ldimension = ddao.getAll();
        LevelDAO ldao = new LevelDAO();
        List<Level> lLevel = ldao.getAll();
        request.setAttribute("dimensions", ldimension);
        request.setAttribute("subjectID", q.getSubject().getId());
        request.setAttribute("levels", lLevel);
        request.setAttribute("question", q);
        request.setAttribute("subjects", lsubject);
        request.setAttribute("topics", ltopic);
        request.setAttribute("filter", "filter");
        request.setAttribute("check", qdao.check(questionID));
        request.getRequestDispatcher("/view/admin/question/questiondetail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        int questionID = Integer.parseInt(request.getParameter("questionID"));
        out.print(request.getParameter("question_subject"));
        int subjectID = Integer.parseInt(request.getParameter("question_subject"));
        if (request.getParameter("singlebutton") == null) {
            QuestionDAO qdao = new QuestionDAO();
            Question q = qdao.getQuestionByQuestiontID(questionID);
            SubjectDAO sdao = new SubjectDAO();
            List<Subject> lsubject = sdao.getAll();
            TopicDAO tdao = new TopicDAO();
            List<Topic> ltopic = tdao.getListTopicsBySubjectID(subjectID);
            DimensonDAO ddao = new DimensonDAO();
            List<Dimension> ldimension = ddao.getAll();
            LevelDAO ldao = new LevelDAO();
            List<Level> lLevel = ldao.getAll();
            request.setAttribute("dimensions", ldimension);
            request.setAttribute("subjectID", subjectID);
            request.setAttribute("levels", lLevel);
            request.setAttribute("question", q);
            request.setAttribute("subjects", lsubject);
            request.setAttribute("topics", ltopic);
            request.setAttribute("filter", "!filter");
            request.getRequestDispatcher("/view/admin/question/questiondetail.jsp").forward(request, response);
        }

        QuestionDAO qdao = new QuestionDAO();
        Question q = qdao.getQuestionByQuestiontID(questionID);
        int topicID = Integer.parseInt(request.getParameter("question_topic"));
        TopicDAO tdao = new TopicDAO();
        Topic t = tdao.getTopicsByID(topicID);
        q.setTopic(t);
        String content = request.getParameter("question_content");
        q.setContent(content);
        DBContext db = new DBContext();
        Subject s = SubjectDAO.getSubjectByID(db.getConnection(), subjectID);
        q.setSubject(s);

        int levelID = Integer.parseInt(request.getParameter("question_level"));
        LevelDAO ldao = new LevelDAO();
        Level l = ldao.getLevelByID(levelID);
        q.setLeve(l);
        int dimensionGroup = Integer.parseInt(request.getParameter("question_group"));
        int dimensionDomain = Integer.parseInt(request.getParameter("question_domain"));

        DimensonDAO ddao = new DimensonDAO();
        List<Dimension> lDimension = new ArrayList<>();
        lDimension.add(ddao.getDimension(dimensionDomain));
        lDimension.add(ddao.getDimension(dimensionGroup));
        q.setDimension(lDimension);
        String status = request.getParameter("status");
        if (status.equals("0")) {
            q.setStatus(true);
        }
        if (status.equals("1")) {
            q.setStatus(false);
        }
        qdao.updateQuestion(q);
        qdao.updateQuestionDimension(questionID, dimensionGroup, dimensionDomain, q);
        doGet(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
