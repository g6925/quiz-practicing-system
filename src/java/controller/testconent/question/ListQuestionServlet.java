/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.testconent.question;

import dao.DimensonDAO;
import dao.LevelDAO;
import dao.QuestionDAO;
import dao.TopicDAO;
import entity.Dimension;
import entity.Level;
import entity.Question;
import entity.Topic;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hiepx
 */
public class ListQuestionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListQuestionServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListQuestionServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        QuestionDAO qdao = new QuestionDAO();
        List<Question> questions = qdao.getQuestionBySubjectID(1);
        TopicDAO tdao = new TopicDAO();
        List<Topic> ltopic = tdao.getListTopicsBySubjectID(1);
        LevelDAO ldao = new LevelDAO();
        List<Level> llevel = ldao.getAll();
        DimensonDAO ddao = new DimensonDAO();
        List<Dimension> ldimension = ddao.getAll();
        request.setAttribute("dimensions", ldimension);
        request.setAttribute("levels", llevel);
        request.setAttribute("levelDAO", ldao);
        request.setAttribute("topics", ltopic);
        request.setAttribute("filter", "0");

        int page, numPerPage = 12;
        int num = questions.size() % numPerPage == 0 ? (questions.size() / numPerPage) : ((questions.size() / numPerPage) + 1);
        String spage = request.getParameter("page");
        if (spage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(spage);
        }
        int start = (page - 1) * numPerPage;
        int end = Math.min(questions.size(), page * numPerPage);
        List<Question> listPaging = qdao.getListByPage(questions, start, end);
        request.setAttribute("listquestion", listPaging);
        request.setAttribute("pageindex", page);
        request.setAttribute("totalpage", num);
        request.getRequestDispatcher("../view/admin/question/questions.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        TopicDAO tdao = new TopicDAO();
        List<Topic> ltopic = tdao.getListTopicsBySubjectID(1);
        LevelDAO ldao = new LevelDAO();
        List<Level> llevel = ldao.getAll();
        DimensonDAO ddao = new DimensonDAO();
        List<Dimension> ldimension = ddao.getAll();
        request.setAttribute("dimensions", ldimension);
        request.setAttribute("levels", llevel);
        request.setAttribute("topics", ltopic);

        int topicID = 0;
        if (request.getParameter("topic") != null) {
            topicID = Integer.parseInt(request.getParameter("topic"));
        }
        request.setAttribute("topicID", topicID);
        String dimension = request.getParameter("dimension");
        request.setAttribute("dimension", dimension);
        int levelID = 0;
        if (request.getParameter("level") != null) {
            levelID = Integer.parseInt(request.getParameter("level"));
        }
        request.setAttribute("levelID", levelID);
        String status = request.getParameter("status");
        request.setAttribute("status", status);
        String keyWord = request.getParameter("keyWord");
        request.setAttribute("keyWord", keyWord);
        QuestionDAO qdao = new QuestionDAO();

        List<Question> questions = qdao.filter(1, topicID, dimension, levelID, status, keyWord);
        //Paging
        int page, numPerPage = 12;
        int num = questions.size() % numPerPage == 0 ? (questions.size() / numPerPage) : ((questions.size() / numPerPage) + 1);
        String spage = request.getParameter("page");
        if (spage.isEmpty()) {
            page = 1;
        } else {
            page = Integer.parseInt(spage);
        }
        int start = (page - 1) * numPerPage;
        int end = Math.min(questions.size(), page * numPerPage);
        List<Question> listPaging = qdao.getListByPage(questions, start, end);
        request.setAttribute("listquestion", listPaging); 
        request.setAttribute("pageindex", page);
        request.setAttribute("totalpage", num);
        request.setAttribute("filter", "1");
        request.getRequestDispatcher("../view/admin/question/questions.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
