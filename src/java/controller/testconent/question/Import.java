/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.testconent.question;

import com.sun.faces.util.CollectionsUtils;
import dao.AnswerDAO;
import dao.DBContext;
import dao.DSQDAO;
import dao.DimensonDAO;
import dao.LevelDAO;
import dao.QuestionDAO;
import dao.SubjectDAO;
import dao.TopicDAO;
import entity.Answer;
import entity.Dimension;
import entity.MyMethod;
import entity.Question;
import entity.Subject;
import entity.Topic;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author hiepx
 */
@MultipartConfig
public class Import extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Import</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Import at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    static List<Question> listQuestions = new ArrayList<>();
    static HashMap<String, List<Answer>> map = new HashMap<>();

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SubjectDAO subjectdao = new SubjectDAO();
        List<Subject> subjects = subjectdao.getAll();
        request.setAttribute("listSubject", subjects);
        request.getRequestDispatcher("../view/admin/importQuestion/importQuestion.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        int subjectID = Integer.parseInt(request.getParameter("subjectID"));
        TopicDAO topicdao = new TopicDAO();
        List<Topic> topics = topicdao.getListTopicsBySubjectID(subjectID);

        if (request.getParameter("submit") == null) {
            SubjectDAO subjectdao = new SubjectDAO();
            List<Subject> subjects = subjectdao.getAll();
            out.print(subjects.size());
            request.setAttribute("listTopic", topics);
            request.setAttribute("subjectID", subjectID);
            DBContext db = new DBContext();
            Connection connection = db.getConnection();
            DimensonDAO ddao = new DimensonDAO();
            List<Dimension> lDimension = ddao.getDimensionBySubject(subjectID);
            request.setAttribute("listSubject", subjects);
            request.setAttribute("dimensions", lDimension);
            request.setAttribute("subject", SubjectDAO.getSubjectByID(connection, subjectID));
            request.getRequestDispatcher("../view/admin/importQuestion/importQuestion.jsp").forward(request, response);
        } else {
            int topicID = Integer.parseInt(request.getParameter("topicID"));
            int dimensionGroup = Integer.parseInt(request.getParameter("dimension_group"));
            int dimensionDomain = Integer.parseInt(request.getParameter("dimension_domain"));

            if (request.getPart("tec").getSize() == 0) {
                response.getWriter().print(request.getPart("tec"));
                SubjectDAO subjectdao = new SubjectDAO();
                List<Subject> subjects = subjectdao.getAll();
                request.setAttribute("listTopic", topics);
                request.setAttribute("subjectID", subjectID);
                DBContext db = new DBContext();
                Connection connection = db.getConnection();
                DimensonDAO ddao = new DimensonDAO();
                List<Dimension> lDimension = ddao.getDimensionBySubject(subjectID);
                request.setAttribute("listSubject", subjects);
                request.setAttribute("dimensions", lDimension);
                request.setAttribute("subject", SubjectDAO.getSubjectByID(connection, subjectID));
                request.setAttribute("mess", "Upload file!");

                request.getRequestDispatcher("../view/admin/importQuestion/importQuestion.jsp").forward(request, response);
            }
            Part file = request.getPart("tec");

            String imageFileName = file.getSubmittedFileName();
            String realPath = request.getServletContext().getRealPath("/assets/questionExcel");
            //D:\Summer 2022\SWP391\QuizPracticingSystem\build\web\assets\images
            String uploadPath = null;
            if (imageFileName != "") {
                uploadPath = realPath.replace("build\\", "") + "\\" + imageFileName;

                try (FileOutputStream fos = new FileOutputStream(uploadPath)) {
                    InputStream is = file.getInputStream();
                    byte[] data = new byte[is.available()];
                    is.read(data);
                    fos.write(data);
                }
            }
            //out.println(uploadPath);
            readExcel(request, response, uploadPath, topicID, subjectID, dimensionGroup, dimensionDomain);

            //Insert into database
            QuestionDAO qdao = new QuestionDAO();
            AnswerDAO adao = new AnswerDAO();
            List<Question> lQuestion = qdao.getAll();
            for (Question q : listQuestions) {
                Boolean check = true;
                for (Question qst : lQuestion) {
                    if (q.getContent().equalsIgnoreCase(qst.getContent())) {
                        check = false;
                    }
                }
                if (check) {
                    qdao.insertQuestion(q, topicID, subjectID);
                    Question qs = qdao.getQuestionByContent(q.getContent());
                    DimensonDAO ddao = new DimensonDAO();
                    DSQDAO dsqdao = new DSQDAO();
                    dsqdao.insertDimensionSubjectQuestion(dimensionGroup, subjectID, qs.getId());
                    dsqdao.insertDimensionSubjectQuestion(dimensionDomain, subjectID, qs.getId());
                    out.print("success");
                    List<Answer> list = map.get(q.getContent());
                    for (Answer answer : list) {
                        answer.setQuestion(qdao.getQuestionByContent(q.getContent()));
                        adao.insertAnswer(answer);
                    }
                }
            }
            response.sendRedirect("questions");
        }
        

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static void readExcel(HttpServletRequest request, HttpServletResponse response, String excelFilePath, int topicID, int subjectID, int dimension_group, int dimension_domain) throws IOException, ServletException {

        // Get file
        InputStream inputStream = new FileInputStream(new File(excelFilePath));
        // Get workbook
        Workbook workbook = getWorkbook(inputStream, excelFilePath);

        // Get sheet
        Sheet sheet = workbook.getSheetAt(0);
        List<Answer> listAnswer = new ArrayList<>();

        // Get all rows
        Iterator<Row> iterator = sheet.iterator();
        String oldQuestion = "0";
        Question questionOld = new Question();
        String value = "";
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            if (nextRow.getRowNum() == 0) {
                // Ignore header
                continue;
            }

            // Get all cells
            Iterator<Cell> cellIterator = nextRow.cellIterator();
            // Read cells and set value for book object
            Question question = new Question();
            questionOld = question;

            Answer answer = new Answer();
            while (cellIterator.hasNext()) {
                //Read cell
                Cell cell = cellIterator.next();
                Object cellValue = getCellValue(cell);
                if (cellValue == null || cellValue.toString().isEmpty()) {
                    return;
                }
                // Set value for book object
                int columnIndex = cell.getColumnIndex();
                switch (columnIndex) {
                    case 0:
                        value = (String) cellValue;
                        if (!oldQuestion.toLowerCase().equals(value.toLowerCase())) {
                            oldQuestion = (String) cellValue;
                            question.setContent(oldQuestion);
                        }

                        if (question.getContent() != null) {
                            listAnswer = new ArrayList<>();
                        }
                        break;
                    case 1:
                        QuestionDAO qdao = new QuestionDAO();
                        List<Question> lquestion = qdao.getAll();
                        for (Question q : lquestion) {
                            if (q.getContent().equals((String) cellValue)) {
                                return;
                            }
                        }
                        answer.setContent((String) cellValue);
                        break;
                    case 2:
                        if (cellValue.equals("T") || cellValue.equals("t")) {
                            answer.setCorrect(Boolean.TRUE);
                        } else if (cellValue.equals("F") || cellValue.equals("f")) {
                            answer.setCorrect(Boolean.FALSE);
                        } else {
                            TopicDAO topicdao = new TopicDAO();
                            List<Topic> topics = topicdao.getListTopicsBySubjectID(subjectID);
                            request.setAttribute("listTopic", topics);
                            request.setAttribute("subjectID", subjectID);
                            DBContext db = new DBContext();
                            Connection connection = db.getConnection();
                            request.setAttribute("subject", SubjectDAO.getSubjectByID(connection, subjectID));
                            request.setAttribute("mess", "Something wrong in Answers column!");

                            request.getRequestDispatcher("./view/admin/importQuestion/importQuestion.jsp").forward(request, response);
                        }
                        listAnswer.add(answer);
                        break;
                    case 3: {
                        LevelDAO ldao = new LevelDAO();
                        String lev = (String) cellValue;
                        if (lev.equalsIgnoreCase("Easy")) {
                            question.setLeve(ldao.getLevelByName("Easy"));
                        } else if (lev.equalsIgnoreCase("Medium")) {
                            question.setLeve(ldao.getLevelByName("Medium"));
                        } else if (lev.equalsIgnoreCase("Hard")) {
                            question.setLeve(ldao.getLevelByName("Hard"));
                        } else {
                            return;
                        }
                    }
                    case 4: {
                        String explanation = (String) cellValue;
                        listAnswer.get(listAnswer.size() - 1).setExplanation(explanation);
                    }
                    default:
                        break;
                }
            }
            if (answer.getContent().isEmpty()) {
                TopicDAO topicdao = new TopicDAO();
                List<Topic> topics = topicdao.getListTopicsBySubjectID(subjectID);
                request.setAttribute("listTopic", topics);
                request.setAttribute("subjectID", subjectID);
                DBContext db = new DBContext();
                Connection connection = db.getConnection();
                request.setAttribute("subject", SubjectDAO.getSubjectByID(connection, subjectID));
                request.setAttribute("mess", "Something wrong in Option column!");
                request.getRequestDispatcher("./view/admin/importQuestion/importquestio.jsp").forward(request, response);
            } else if (question.getContent() != null) {
                listQuestions.add(question);
            }
            map.put(oldQuestion, listAnswer);
            QuestionDAO qdao = new QuestionDAO();

        }
        workbook.close();
        inputStream.close();
    }

    // Get Workbook
    private static Workbook getWorkbook(InputStream inputStream, String excelFilePath) throws IOException {
        Workbook workbook = null;
        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }

    // Get cell value
    private static Object getCellValue(Cell cell) {
        CellType cellType = cell.getCellType();
        Object cellValue = null;
        switch (cellType) {
            case BOOLEAN:
                cellValue = cell.getBooleanCellValue();
                break;
            case FORMULA:
                Workbook workbook = cell.getSheet().getWorkbook();
                FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
                cellValue = evaluator.evaluate(cell).getNumberValue();
                break;
            case NUMERIC:
                cellValue = cell.getNumericCellValue();
                break;
            case STRING:
                cellValue = cell.getStringCellValue();
                break;
            case _NONE:
            case BLANK:
            case ERROR:
                break;
            default:
                break;
        }

        return cellValue;
    }

}
