/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.tescontent.quiz;

import dao.QuizDAO;
import dao.SubjectDAO;
import entity.Quiz;
import entity.Subject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hiepx
 */
public class ListQuizServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListQuizServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListQuizServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        QuizDAO qdao = new QuizDAO();
        List<Quiz> lquiz = qdao.getAll();

        SubjectDAO sdao = new SubjectDAO();
        List<Subject> lsubject = sdao.getAll();
        //Paging
        int page, numPerPage = 6;
        int num = lquiz.size() % numPerPage == 0 ? (lquiz.size() / numPerPage) : ((lquiz.size() / numPerPage) + 1);
        String spage = request.getParameter("page");
        if (spage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(spage);
        }
        int start = (page - 1) * numPerPage;
        int end = Math.min(lquiz.size(), page * numPerPage);
        List<Quiz> listPaging = qdao.getListByPage(lquiz, start, end);
        request.setAttribute("pageindex", page);
        request.setAttribute("totalpage", num);
        request.setAttribute("listsubject", lsubject);
        request.setAttribute("listquizzes", listPaging);
        request.setAttribute("filter", "0");

        request.getRequestDispatcher("/view/admin/quiz/quizzes.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SubjectDAO sdao = new SubjectDAO();
        List<Subject> lsubject = sdao.getAll();
        request.setAttribute("listsubject", lsubject);
        QuizDAO qdao = new QuizDAO();
        int subjectID = 0;
        if (request.getParameter("subject") != null) {
            subjectID = Integer.parseInt(request.getParameter("subject"));
        }
        String type = request.getParameter("type");
        String keyWord = request.getParameter("keyWord");
        List<Quiz> lquiz = qdao.filter(subjectID, type, keyWord);
        request.setAttribute("listquizzes", lquiz);
        request.setAttribute("subjectID", subjectID);
        request.setAttribute("type", type);
        request.setAttribute("keyWord", keyWord);

        int page, numPerPage = 6;
        int num = lquiz.size() % numPerPage == 0 ? (lquiz.size() / numPerPage) : ((lquiz.size() / numPerPage) + 1);
        String spage = request.getParameter("page");
        if (spage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(spage);
        }
        int start = (page - 1) * numPerPage;
        int end = Math.min(lquiz.size(), page * numPerPage);
        List<Quiz> listPaging = qdao.getListByPage(lquiz, start, end);
        request.setAttribute("pageindex", page);
        request.setAttribute("totalpage", num);
        request.setAttribute("listsubject", lsubject);
        request.setAttribute("listquizzes", listPaging);
        request.setAttribute("filter", "1");

        request.getRequestDispatcher("/view/admin/quiz/quizzes.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
