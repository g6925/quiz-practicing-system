/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.common.subject;

import dao.PricePackageDAO;
import dao.RegisterDAO;
import dao.SubjectDAO;
import dao.UserDAO;
import entity.PricePackage;
import entity.Register;
import entity.Role;
import entity.Subject;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author xuant
 */
public class SubjectPackageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PricePackageDAO packageDAO = new PricePackageDAO();
       
        int sId = Integer.parseInt(request.getParameter("sId"));
        List<PricePackage> pricePackages = packageDAO.getAllBySubjectId(sId,1);
        int countPackage = pricePackages.size();
        SubjectDAO sdbc = new SubjectDAO();
        Subject subject = sdbc.getSubjectByID(sdbc.getConnection(), sId);
        request.setAttribute("subject", subject);
        request.setAttribute("pricePackages", pricePackages);
        request.setAttribute("size",12/countPackage);
        HttpSession s = request.getSession();
        User user = (User) s.getAttribute("user");
        
        RegisterDAO rdao = new RegisterDAO();
        if (user != null) {
            Register register = rdao.getRegister(rdao.getConnection(), user.getId(), sId);
            if (register != null) {
                request.getRequestDispatcher("../../view/common/subject/after-enroll.jsp").forward(request, response);
            }
        } 
        request.getRequestDispatcher("../../view/common/subject/pricePackage.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
