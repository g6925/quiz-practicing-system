/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.common.subject;

import dao.RegisterDAO;
import dao.SubjectDAO;
import dao.TopicDAO;
import entity.Register;
import entity.Subject;
import entity.Topic;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author xuant
 */
public class SubjectDetailsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int sId = Integer.parseInt(request.getParameter("sId"));
        SubjectDAO sdbc = new SubjectDAO();
        Subject subject = sdbc.getSubjectByID(sdbc.getConnection(), sId);
        TopicDAO tdao = new TopicDAO();
        List<Topic> topics = tdao.getListTopicsBySubjectID(sId);
        request.setAttribute("topics", topics);
        request.setAttribute("subject", subject);
        HttpSession s = request.getSession();
        User user = (User) s.getAttribute("user");

        RegisterDAO rdao = new RegisterDAO();
        if (user != null) {
            Register register = rdao.getRegister(rdao.getConnection(), user.getId(), sId);
            request.setAttribute("register", register);
        } else {
            request.setAttribute("register", null);
        }
        request.getRequestDispatcher("../view/common/subject/details.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
