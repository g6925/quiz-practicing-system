/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.common.user;

import dao.UserDAO;
import entity.MyMethod;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author taina
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginServlet at " + request.getContextPath() + "/view/common/login.jsp" + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        String subjectId = (request.getParameter("subjectId"));
        String pricePackageId = (request.getParameter("pricePackageId"));
        if (subjectId != null && pricePackageId != null) {
            request.setAttribute("subjectId", subjectId);
            request.setAttribute("pricePackageId", pricePackageId);
        }
        request.getRequestDispatcher("../../view/common/user/login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String u = request.getParameter("user");
        String p = request.getParameter("password");
        String r = request.getParameter("remember");
        UserDAO d = new UserDAO();
        User user = d.getUserByEmailAndPass(u, MyMethod.convertToMD5(p));
        if (user == null) {
            request.setAttribute("error", "Username/Password Invalid!!!");
            request.getRequestDispatcher("../../view/common/user/login.jsp").forward(request, response);
        } else if (user.isStatus() != true) {
            request.setAttribute("error", "Your Account Has Been Suspended");
            request.getRequestDispatcher("../../view/common/user/login.jsp").forward(request, response);
        } else {
            HttpSession s = request.getSession();
            s.setAttribute("user", user);
            MyMethod.removeAllCookies(request, response);
            if (r != null) {
                response.addCookie(MyMethod.createCooky("user", u, 4 * 24 * 60 * 60));
                response.addCookie(MyMethod.createCooky("password", MyMethod.convertToMD5(p), 4 * 24 * 60 * 60));
                response.addCookie(MyMethod.createCooky("remember", r, 4 * 24 * 60 * 60));
            }
            String subjectId = (request.getParameter("subjectId"));
            String pricePackageId = (request.getParameter("pricePackageId"));
            if (!subjectId.isEmpty() && !pricePackageId.isEmpty()) {

                response.sendRedirect("../subject/register?subjectId=" + subjectId + "&pricePackageId=" + pricePackageId);
                
            } else {
                response.sendRedirect("../home");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
