/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.common.user;

import dao.RoleDAO;
import dao.UserDAO;
import entity.MyMethod;
import entity.SendMail;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dclon
 */
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SignUpServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SignUpServlet at " + request.getContextPath() + "assets/images/user/user-default.jpg</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("../../view/common/user/register.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        boolean gender = request.getParameter("gender").equals("1");
        String phone = request.getParameter("phone");
        User u = new User();
        u.setEmail(email);
        u.setGender(gender);
        u.setName(name);
        u.setPass(MyMethod.convertToMD5(password.trim()));
        u.setPhone(phone);
        u.setStatus(true);
        u.setImage(request.getContextPath() + "/assets/images/user/user-default.jpg");
        u.setRole(RoleDAO.getRoleByID(new RoleDAO().getConnection(), 4));
        SendMail SE = new SendMail();
        UserDAO d = new UserDAO();
        if (d.getUserByEmail(email) != null) {
            request.setAttribute("error", "Username already available!!!");
            request.getRequestDispatcher("../../view/common/user/register.jsp").forward(request, response);
        } else {
            try {
//                d.insertUser(u);
//                request.setAttribute("status", "Register Acount Successfully!");
//                request.getRequestDispatcher("../../view/common/user/confirm.jsp").forward(request, response);
                SE.SendMailConfirmRegister(email);
                request.setAttribute("error", "Please Check Your Mail To Cofimn Account");
                request.getRequestDispatcher("../../view/common/user/register.jsp").forward(request, response);
                HttpSession s = request.getSession();
                s.setAttribute("newEmail", email);
                s.setAttribute("newUser", u);
                s.setMaxInactiveInterval(300);
            } 
            catch (MessagingException ex) {
                Logger.getLogger(ResetServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
