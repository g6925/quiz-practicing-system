/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.common.blog;

import dao.PostDAO;
import dao.CategoryDAO;
import entity.Post;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hiepx
 */
public class BlogCategoryDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BlogCategoryDetailServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BlogCategoryDetailServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int categoryId = Integer.parseInt(request.getParameter("categoryId"));
        
        PrintWriter out = response.getWriter();
        CategoryDAO category = new CategoryDAO();
        PostDAO p = new PostDAO();
        PostDAO dum = new PostDAO();
        List<Post> list = p.getAllWithCategoryID(categoryId);
        List<Post> listAll = p.getAllPostActive();
        int page, numPerPage = 6;
        int num = list.size() % numPerPage == 0 ? (list.size() / numPerPage) : ((list.size() / numPerPage) + 1);
        String spage = request.getParameter("page");
        
        if (spage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(spage);
        }
        int start = (page - 1) * numPerPage;
        int end = Math.min(list.size(), page * numPerPage);
        List<Post> listPaging = dum.getListByPage(list, start, end);
        List<Post> listRecent = dum.getListByPage(listAll, 0, 4);

        CategoryDAO tec = new CategoryDAO();
        request.setAttribute("listCategory", category.getAll());
        request.setAttribute("page", page);
        request.setAttribute("num", num);
        request.setAttribute("active", "active");
        request.setAttribute("listPaging", listPaging);
        request.setAttribute("listRecent", listRecent);
        request.setAttribute("categorydao", tec);
        request.setAttribute("categoryId", categoryId);
        
        request.getRequestDispatcher("../../view/common/blog/blogs-category.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
