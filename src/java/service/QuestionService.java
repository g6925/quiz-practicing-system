package service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dao.QuestionDAO;
import dao.QuizDAO;
import dao.QuizHandleDAO;
import entity.Question;
import entity.Quiz;
import entity.User;
import entity.UserQuiz;
import java.sql.SQLException;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import util.SessioUtil;

@Path("/question")
public class QuestionService {

    @Context
    private HttpServletRequest req;

    private final QuestionDAO questionDAO;
    private final Gson gson;

    public QuestionService() {
        questionDAO = new QuestionDAO();
        gson = new Gson();
    }

    @GET
    @Path("/sessionTime/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRemainTime(@PathParam("id") int id) {
        User user = SessioUtil.getAccount(req);
        if (user != null) {
            QuizDAO quizDAO = new QuizDAO();
            UserQuiz userQuiz = quizDAO.getUserQuiz(id, user.getId());
            Map<String, String> time = new HashMap<>();
            time.put("startTime", userQuiz.getStartedOn().format(DateTimeFormatter.ISO_DATE_TIME));
            Quiz quiz = new QuizDAO().getQuizById(id, user.getId());
            if (quiz.getType().equalsIgnoreCase("Exam")) {
                time.put("expiredTime", userQuiz.getExpireTime().format(DateTimeFormatter.ISO_DATE_TIME));
            } else {
                time.put("expiredTime", "null");
            }
            return Response.ok().entity(gson.toJson(time)).build();
        } else {
            return Response.status(401).build();
        }

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getQuestionByQuizId(@QueryParam("quizId") int id
    ) {
        User user = SessioUtil.getAccount(req);
        if (user != null) {
            List<Question> questions = questionDAO.getAllQuestionInQuiz(id, user.getId(), true);
            return Response.ok().entity(new Gson().toJson(questions)).build();
        } else {
            return Response.status(401).build();
        }

    }

    @POST
    @Path("/submit")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response submitAnswer(String json
    ) {
        User user = SessioUtil.getAccount(req);
        if (user != null) {
            JsonParser jsonParser = new JsonParser();
            JsonElement jsonElement = jsonParser.parse(json);
            JsonObject jsonObject = (JsonObject) jsonElement;
            int quizId = jsonObject.get("quizId").getAsInt();
            QuizHandleDAO quizHandleDAO = new QuizHandleDAO();
            LocalDateTime current = LocalDateTime.now();
            Quiz quiz = new QuizDAO().getQuizById(quizId, user.getId());
            if (quiz.getType().equalsIgnoreCase("Exam")) {
                if (quizHandleDAO.checkExpired(quizId, user.getId(), current) <= 0) {
                    return Response.status(403)
                            .entity("{\"message\": \"Expired session\"}")
                            .build();
                }
            }
            try {
                List<Question> questions = questionDAO.getAllQuestionInQuiz(quizId, user.getId(), false);
                float grade = questionDAO.saveAnswer(user.getId(), jsonElement, questions.size());
                
                QuizDAO quizDAO = new QuizDAO();
                UserQuiz userQuiz = quizDAO.getUserQuiz(quizId, user.getId());
                userQuiz.setCompletedOn(current);
                Time timeTaken = new Time(current.getHour() - userQuiz.getStartedOn().getHour(), current.getMinute() - userQuiz.getStartedOn().getMinute(), current.getSecond() - userQuiz.getStartedOn().getSecond());
                userQuiz.setTimeTaken(timeTaken);
                userQuiz.setGrade(grade);
                quizHandleDAO.insertOrUpdateUserQuiz(userQuiz);
                return Response.ok()
                        .entity("{\"message\": \"Submit successfuly score: " + grade + "/10 \"}")
                        .build();
            } catch (SQLException ex) {
                return Response.serverError().build();
            }
        } else {
            return Response.status(401).build();
        }
    }
}
