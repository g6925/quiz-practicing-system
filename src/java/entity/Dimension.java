/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author taina
 */
public class Dimension {

    private int id;
    private String name, type, description;

    public Dimension() {
    }

    public Dimension(int id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public Dimension(String name, String type, String description) {
        this.name = name;
        this.type = type;
        this.description = description;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
