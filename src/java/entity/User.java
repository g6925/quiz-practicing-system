/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.List;

/**
 *
 * @author taina
 */
public class User {

    private int id;
    private String email, pass, name;
    private Boolean gender;
    private String phone;
    private String image;
    private Role role;
    private Subject subject;
    private List<Subject> subjects; 
    private boolean status;
    private String Introduce;

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getIntroduce() {
        return Introduce;
    }

    public void setIntroduce(String Introduce) {
        this.Introduce = Introduce;
    }

    public User() {
    }

    public User(int id) {
        this.id = id;
    }
    
    public User(int id, String email, String pass, String name, Boolean gender, String phone, String image, Role role, Subject subject, List<Subject> subjects, boolean status) {
        this.id = id;
        this.email = email;
        this.pass = pass;
        this.name = name;
        this.gender = gender;
        this.phone = phone;
        this.image = image;
        this.role = role;
        this.subject = subject;
        this.subjects = subjects;
        this.status = status;
    }   

    public User(String email, String pass, String name, Boolean gender, String phone, String image, Role role, boolean status, String Introduce) {
        this.email = email;
        this.pass = pass;
        this.name = name;
        this.gender = gender;
        this.phone = phone;
        this.image = image;
        this.role = role;
        this.status = status;
        this.Introduce = Introduce;
    }

    
    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
