 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.sun.xml.wss.util.DateUtils;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.Normalizer;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dao.CategoryDAO;
import dao.RegisterDAO;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dclon
 */


public class MyMethod {
    
    @FunctionalInterface
    public interface DataApexcharts {
        List<Apexcharts> getData(List<Apexcharts> data, int amount, Calendar calendar, int categoryID);
    }

    public static String getFileType(String fileName) {
        int i = fileName.lastIndexOf(".");
        return fileName.substring(i, fileName.length());
    }

    public static String removeAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    public static String formatName(String name) {
        String s_temp = removeAccent(name);
        s_temp = s_temp.toLowerCase();
        s_temp = s_temp.replace(' ', '-');
        s_temp = s_temp.replace('đ', 'd');
        s_temp = s_temp.replace('ê', 'e');
        s_temp = s_temp.replace('ô', 'o');
        s_temp = s_temp.replace('â', 'a');
        return s_temp;
    }

    public static String upload(HttpServletRequest request, Part file, String pathAddressUpload) throws IOException {
        String imageFileName = file.getSubmittedFileName();
        String realPath = request.getServletContext().getRealPath(pathAddressUpload);
        //D:\Summer 2022\SWP391\QuizPracticingSystem\build\web\assets\images
        String uploadPath = null;
        String UUID = randomUUID();
        if (imageFileName != "") {
            uploadPath = realPath.replace("build\\", "") + "\\" + UUID + getFileType(imageFileName);

            try (FileOutputStream fos = new FileOutputStream(uploadPath)) {
                InputStream is = file.getInputStream();
                byte[] data = new byte[is.available()];
                is.read(data);
                fos.write(data);
            }
        }
        return request.getContextPath() + "/" + pathAddressUpload + "/" + UUID + getFileType(imageFileName);

//        String imageFileName = file.getSubmittedFileName();
//        String realPath = request.getServletContext().getRealPath(pathAddressUpload);
//        //D:\Summer 2022\SWP391\QuizPracticingSystem\build\web\assets\images
//        String uploadPath = null;
//        if (imageFileName != "") {
//            uploadPath = realPath.replace("build\\", "") + "\\" + imageFileName;
//
//            try (FileOutputStream fos = new FileOutputStream(uploadPath)) {
//                InputStream is = file.getInputStream();
//                byte[] data = new byte[is.available()];
//                is.read(data);
//                fos.write(data);
//            }
//        }
//        return request.getContextPath() + "/" + pathAddressUpload + "/" + imageFileName;
    }

    public static String formatNVarchar(String nvarchar) throws IOException {
        String byteString = nvarchar;
        byte ptextString[] = byteString.getBytes("ISO-8859-1");
        return new String(ptextString, "UTF-8");
    }

    public static void removeAllCookies(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cooky : cookies) {
                cooky.setValue(null);
                cooky.setMaxAge(0);
                response.addCookie(cooky);
            }
        }
    }

    public static void removeCooky(HttpServletRequest request, HttpServletResponse response, String c_name) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cooky : cookies) {
                if (cooky.getName().equals(c_name)) {
                    cooky.setValue(null);
                    cooky.setMaxAge(0);
                    response.addCookie(cooky);
                    break;
                }
            }
        }
    }

    public static String getValueCooky(HttpServletRequest request, HttpServletResponse response, String cooky_name) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cooky : cookies) {
                if (cooky.getName().equals(cooky_name)) {
                    return cooky.getValue();
                }
            }
        }
        return null;
    }

    public static Cookie createCooky(String cooky_name, String cooky_value, int maxAge) {
        Cookie cookie = new Cookie(cooky_name, cooky_value);
        cookie.setMaxAge(maxAge);
        return cookie;
    }

    public static Timestamp getT_now() {
        Date nowDate = new Date();
        Timestamp now = new Timestamp(nowDate.getTime());
        return now;
    }

    public static Timestamp addMonth(int month) {
        
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, month);
        java.util.Date dt = cal.getTime();
        Timestamp timestamp = new Timestamp(dt.getTime());
        return timestamp;
    }

    

    public static String getTimeAgo(Timestamp t_now, Timestamp t_lastOnline) {
        long durationInMillis = Math.abs(t_now.getTime() - t_lastOnline.getTime());
        int second = (int) (durationInMillis / 1000);
        int minute = second / 60;
        int hour = minute / 60;
        int day = hour / 24;
        int week = day / 7;
        int month = week / 4;
        int year = month / 12;
        if (year >= 1) {
            return String.format("%d year ago", year);
        } else if (month >= 1) {
            return String.format("%d month ago", month);
        } else if (week >= 1) {
            return String.format("%d week ago", week);
        } else if (day >= 1) {
            return String.format("%d day ago", day % 7);
        } else if (hour >= 1) {
            return String.format("%d hour ago", hour % 24);
        } else if (minute >= 1) {
            return String.format("%d minute ago", minute % 60);
        } else {
            return String.format("%d second ago", second % 60);
        }
    }

    public static String convertToMD5(String passwork) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(MyMethod.class.getName()).log(Level.SEVERE, null, ex);
        }
        md.update(passwork.getBytes());
        byte[] digest = md.digest();
        String myHashPasswork = DatatypeConverter.printHexBinary(digest).toUpperCase();
        return myHashPasswork;
    }

    public static String randomUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
    
    public static String formatDate(Date d) {
        String dateString = new SimpleDateFormat("MMM dd").format(d).toString();
        return dateString;
    }

    public static List<Apexcharts> addRevenueDataChartsByMonth(List<Apexcharts> data, int amount, Calendar calendar, int categoryID) {
        if (amount == 0) {
            return data;
        }
        calendar.add(Calendar.MONTH, -1);
        RegisterDAO r = new RegisterDAO();
        int value = r.getTotalRevenueInMonth(categoryID, new Timestamp(calendar.getTimeInMillis()));
        String label = formatDate(calendar.getTime());
        data.add(new Apexcharts(label, value));
        return addRevenueDataChartsByMonth(data, amount - 1, calendar, categoryID);
    }

    public static List<Apexcharts> addRevenueDataChartsByWeek(List<Apexcharts> data, int amount, Calendar calendar, int categoryID) {
        if (amount == 0) {
            return data;
        }
        calendar.add(Calendar.WEEK_OF_MONTH, -1);
        calendar.set(Calendar.DAY_OF_WEEK, 1);
        Date sunday = calendar.getTime();
        calendar.set(Calendar.DAY_OF_WEEK, 7);
        Date saturday = calendar.getTime();
        RegisterDAO r = new RegisterDAO();
        int value = r.getTotalRevenueInWeek(categoryID, new Timestamp(sunday.getTime()), new Timestamp(saturday.getTime()));
        String label = formatDate(saturday);
        data.add(new Apexcharts(label, value));
        return addRevenueDataChartsByWeek(data, amount - 1, calendar, categoryID);
    }

    public static List<Apexcharts> addRevenueDataChartsByDay(List<Apexcharts> data, int amount, Calendar calendar, int categoryID) {
        if (amount == 0) {
            return data;
        }
        calendar.add(Calendar.DAY_OF_WEEK, -1);
        RegisterDAO r = new RegisterDAO();
        int value = r.getTotalRevenueInDay(categoryID, new Timestamp(calendar.getTimeInMillis()));
        String label = formatDate(calendar.getTime());
        data.add(new Apexcharts(label, value));
        return addRevenueDataChartsByDay(data, amount - 1, calendar, categoryID);
    }

    public static List<Apexcharts> addCategoryPieDataCharts(List<Apexcharts> data, int amount, Calendar calendar) {
        if (amount == 0) {
            return data;
        }
        RegisterDAO r = new RegisterDAO();
        List<Category> categories = new CategoryDAO().getAll();
        Category c = categories.get(amount - 1);
        String label = c.getName();
        int total = r.getTotalEnrollmentWithSinceTimeAgo(new Timestamp(calendar.getTimeInMillis()));
        int value = total != 0 ? Math.round((float) (r.getTotalEnrollmentOfCategoryWithSinceTimeAgo(c.getId(), new Timestamp(calendar.getTimeInMillis())) * 100 / total)) : 0;
        data.add(new Apexcharts(label, value));
        return addCategoryPieDataCharts(data, amount - 1, calendar);
    }

    public static List<Apexcharts> getDataCharts(int categoryID, int amount, Calendar calendar, DataApexcharts dataApexcharts) {
        List<Apexcharts> data = new ArrayList<>();
        return dataApexcharts.getData(data, amount, calendar, categoryID);
    }

    public static int getAmountCharts(String duration) {
        switch (duration) {
            default:
            case "12 months":
                return 12;
            case "6 months":
                return 6;
            case "2 months":
                return 8;
            case "1 month":
                return 4;
            case "1 week":
                return 7;
        }
    }

    public static Calendar getCalendarWithSinceTimeAgo(String duration) {
        Calendar calendar = Calendar.getInstance();
        duration = duration == null ? "12 months" : duration;
        switch (duration) {
            default:
            case "12 months":
                calendar.add(Calendar.MONTH, -12);
                return calendar;
            case "6 months":
                calendar.add(Calendar.MONTH, -6);
                return calendar;
            case "2 months":
                calendar.add(Calendar.MONTH, -2);
                return calendar;
            case "1 month":
                calendar.add(Calendar.MONTH, -1);
                return calendar;
            case "1 week":
                calendar.add(Calendar.WEEK_OF_YEAR, -1);
                return calendar;
        }
    }

    public static void main(String[] args) {
        Gson gson = new Gson();
        List<Apexcharts> list = MyMethod.getDataCharts(0, new CategoryDAO().getAll().size(), MyMethod.getCalendarWithSinceTimeAgo("12 months"), (data, amount, calendar, cID) -> MyMethod.addCategoryPieDataCharts(data, amount, calendar));
        Apexcharts[] arr = list.toArray(new Apexcharts[0]);
        String rs = gson.toJson(arr);
        System.out.println(rs);
    }
}
