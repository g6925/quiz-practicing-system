/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Jarnsaurus
 */
public class Dimension_Subject_Question {

    private int dimensionID, subjectID, questId;

    public Dimension_Subject_Question() {
    }

    public Dimension_Subject_Question(int dimensionID, int subjectID, int questId) {
        this.dimensionID = dimensionID;
        this.subjectID = subjectID;
        this.questId = questId;
    }

    public int getDimensionID() {
        return dimensionID;
    }

    public void setDimensionID(int dimensionID) {
        this.dimensionID = dimensionID;
    }

    public int getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(int subjectID) {
        this.subjectID = subjectID;
    }

    public int getQuestId() {
        return questId;
    }

    public void setQuestId(int questId) {
        this.questId = questId;
    }
}
