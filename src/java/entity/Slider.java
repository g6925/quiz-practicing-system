/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author taina
 */
public class Slider {

    private int id;
    private String title, image, content;
    private boolean status;
    private String backlink;
    private User user;

    public Slider() {
    }

    public Slider(String title, String image, String content, boolean status, String backlink, User user) {
        this.title = title;
        this.image = image;
        this.content = content;
        this.status = status;
        this.backlink = backlink;
        this.user = user;
    }

    public Slider(int id, String title, String image, String content, boolean status, String backlink, User user) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.content = content;
        this.status = status;
        this.backlink = backlink;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getBacklink() {
        return backlink;
    }

    public void setBacklink(String backlink) {
        this.backlink = backlink;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
