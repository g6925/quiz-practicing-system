/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 *
 * @author taina
 */
public class Post {

    private int id;
    private String thumbnail, title, brief;
    private Timestamp date;
    private String content;
    private Category category;
    private User user;
    private boolean status;

    public Post() {
    }

    public Post(int id, String thumbnail, String title, String excerpt, Timestamp date, String content, String brief, Category cate, User user, boolean status) {
        this.id = id;
        this.thumbnail = thumbnail;
        this.title = title;
        this.brief = excerpt;
        this.date = date;
        this.content = content;
        this.category = cate;
        this.user = user;
        this.brief = brief;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExcerpt() {
        return brief;
    }

    public void setExcerpt(String excerpt) {
        this.brief = excerpt;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category cate) {
        this.category = cate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    public String getTimeAgo(){
        return MyMethod.getTimeAgo(MyMethod.getT_now(), date);
    }
}
