/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.sql.Time;
import java.util.List;

/**
 *
 * @author taina
 */
public class Quiz {

    private int id;
    private String name;
    private Time duration;
    private float passRate;
    private String description, type, level;
    private Subject subject;
    private UserQuiz userQuiz;
    private List<Question> Question;

    public List<Question> getQuestion() {
        return Question;
    }

    public void setQuestion(List<Question> Question) {
        this.Question = Question;
    }

    public Quiz() {
    }

    public Quiz(int id) {
        this.id = id;
    }

    public Quiz(int id, String name, Time duration, float passRate, String description, String type, String level, Subject subject, List<Question> Question) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.passRate = passRate;
        this.description = description;
        this.type = type;
        this.level = level;
        this.subject = subject;
        this.Question = Question;
    }

    public Quiz(String name, Time duration, float passRate, String description, String type, String level, Subject subject, List<Question> Question) {
        this.name = name;
        this.duration = duration;
        this.passRate = passRate;
        this.description = description;
        this.type = type;
        this.level = level;
        this.subject = subject;
        this.Question = Question;
    }

    public UserQuiz getUserQuiz() {
        return userQuiz;
    }

    public void setUserQuiz(UserQuiz userQuiz) {
        this.userQuiz = userQuiz;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Time getDuration() {
        return duration;
    }

    public void setDuration(Time duration) {
        this.duration = duration;
    }

    public float getPassRate() {
        return passRate;
    }

    public void setPassRate(float passRate) {
        this.passRate = passRate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
