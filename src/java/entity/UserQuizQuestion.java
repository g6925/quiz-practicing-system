/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author taina
 */
public class UserQuizQuestion {

    private UserQuiz userQuiz;
    private Question question;
    private int userAnswer;
    private Boolean marked;

    public UserQuizQuestion() {
    }

    public UserQuizQuestion(UserQuiz userQuiz, Question question, int userAnswer, Boolean marked) {
        this.userQuiz = userQuiz;
        this.question = question;
        this.userAnswer = userAnswer;
        this.marked = marked;
    }

    public UserQuiz getUserQuiz() {
        return userQuiz;
    }

    public void setUserQuiz(UserQuiz userQuiz) {
        this.userQuiz = userQuiz;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public int getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(int userAnswer) {
        this.userAnswer = userAnswer;
    }

    public Boolean getMarked() {
        return marked;
    }

    public void setMarked(Boolean marked) {
        this.marked = marked;
    }
}
