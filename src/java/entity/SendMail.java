/*
package testmail;
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author HP
 */
public class SendMail {

    public void SendMail(String Email) throws MessagingException {
        Properties pr = new Properties();
        System.out.println("Preparing to send main");
        pr.setProperty("mail.smtp.host", "smtp.gmail.com");
        pr.setProperty("mail.smtp.port", "587");
        pr.setProperty("mail.smtp.auth", "true");
        pr.setProperty("mail.smtp.starttls.enable", "true");
        String myaccount = "a32255@thanglong.edu.vn";
        String password = "123456789long";
        Session session = Session.getInstance(pr, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(myaccount, password);
            }
        });
            Message mess = prepareMessage(session, Email);
            Transport.send(mess);
            return;
    }

    private static Message prepareMessage(Session session, String account) {
        Message message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(account));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(account));
            message.setSubject("Reset Password Confirmation");
            message.setText("Please click on this link to reset your password"
                    + "\nhttp://localhost:8080/QuizPracticingSystem/view/common/resetpassword.jsp"
                    + "\n\n Please remember that this request only available for 5 minutes only!");
            return message;
        } catch (MessagingException ex) {
            Logger.getLogger(SendMail.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void SendMailConfirmRegister(String Email) throws MessagingException {
        Properties pr = new Properties();
        System.out.println("Preparing to send main");
        pr.setProperty("mail.smtp.host", "smtp.gmail.com");
        pr.setProperty("mail.smtp.port", "587");
        pr.setProperty("mail.smtp.auth", "true");
        pr.setProperty("mail.smtp.starttls.enable", "true");
        String myaccount = "a32255@thanglong.edu.vn";
        String password = "123456789long";
        Session session = Session.getInstance(pr, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(myaccount, password);
            }
        });
            Message mess = prepareMessageConfirmRegister(session, Email);
            Transport.send(mess);
            return;
    }
    
    private static Message prepareMessageConfirmRegister(Session session, String account) {
        Message message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(account));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(account));
            message.setSubject("Register Confirmation");
            message.setText("Please click on this link to confirm register account"
                    + "\nhttp://localhost:8080/quiz-practicing-system/common/user/confirm"
                    + "\n\n Please remember that this request only available for 5 minutes only!");
            return message;
        } catch (MessagingException ex) {
            Logger.getLogger(SendMail.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
