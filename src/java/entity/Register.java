/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.sql.Timestamp;

/**
 *
 * @author taina
 */
public class Register {

    private Timestamp dateStart, dateEnd;
    private User user;
    private Subject subject;
    private PricePackage pricePackage;
    private double SalePrice;
    private int PriceRate;
    private Boolean status;

    public Register() {
    }

    public Register(Timestamp dateStart, Timestamp dateEnd, User user, Subject subject, PricePackage pricePackage, double SalePrice, int PriceRate) {
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.user = user;
        this.subject = subject;
        this.pricePackage = pricePackage;
        this.SalePrice = SalePrice;
        this.PriceRate = PriceRate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
    
    public Timestamp getDateStart() {
        return dateStart;
    }

    public void setDateStart(Timestamp dateStart) {
        this.dateStart = dateStart;
    }
    
    public String getDateStartAgo() {
        return MyMethod.getTimeAgo(MyMethod.getT_now(), dateStart);
    }

    public Timestamp getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Timestamp dateEnd) {
        this.dateEnd = dateEnd;
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public PricePackage getPricePackage() {
        return pricePackage;
    }

    public void setPricePackage(PricePackage pricePackage) {
        this.pricePackage = pricePackage;
    }

    public double getSalePrice() {
        return SalePrice;
    }

    public void setSalePrice(double SalePrice) {
        this.SalePrice = SalePrice;
    }

    public int getPriceRate() {
        return PriceRate;
    }

    public void setPriceRate(int PriceRate) {
        this.PriceRate = PriceRate;
    }
    
    
    
}
