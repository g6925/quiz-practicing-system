/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author taina
 */
public class PricePackage {

    private int id;
    private String name;
    private int duration;
    private int priceRate;
    private String description;
    private boolean status;

    public PricePackage() {
    }

    public PricePackage(int id, String name, int duration, int priceRate, String description, boolean status) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.priceRate = priceRate;
        this.description = description;
        this.status = status;
    }

    public PricePackage(String name, int duration, int priceRate, String description, boolean status) {
        this.name = name;
        this.duration = duration;
        this.priceRate = priceRate;
        this.description = description;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getPriceRate() {
        return priceRate;
    }

    public void setPriceRate(int priceRate) {
        this.priceRate = priceRate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
