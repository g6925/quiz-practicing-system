/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Jarnsaurus
 */
public class QuizQuestion {

    private int quizID, questID;

    public QuizQuestion() {
    }

    public QuizQuestion(int quizID, int questID) {
        this.quizID = quizID;
        this.questID = questID;
    }

    public int getQuestID() {
        return questID;
    }

    public int getQuizID() {
        return quizID;
    }

    public void setQuestID(int questID) {
        this.questID = questID;
    }

    public void setQuizID(int quizID) {
        this.quizID = quizID;
    }
}
