/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 *
 * @author taina
 */
public class UserQuiz {

    private Time timeTaken;
    private LocalDateTime startedOn, completedOn, expireTime;
    private float grade;
    private int userId;
    private User user;
    private Quiz quiz;
    private String filterType;
    private int filterGroup;

    public UserQuiz() {
    }

    public UserQuiz(Time timeTaken, LocalDateTime startedOn, LocalDateTime completedOn, LocalDateTime expireTime, float grade, int userId, User user, Quiz quiz, String filterType, int filterGroup) {
        this.timeTaken = timeTaken;
        this.startedOn = startedOn;
        this.completedOn = completedOn;
        this.expireTime = expireTime;
        this.grade = grade;
        this.userId = userId;
        this.quiz = quiz;
        this.filterType = filterType;
        this.filterGroup = filterGroup;
    }

    public UserQuiz(Time timeTaken, LocalDateTime startedOn, LocalDateTime completedOn, float grade, User user, Quiz quiz, String filterType, int filterGroup) {
        this.timeTaken = timeTaken;
        this.startedOn = startedOn;
        this.completedOn = completedOn;
        this.grade = grade;
        this.user = user;
        this.quiz = quiz;
        this.filterType = filterType;
        this.filterGroup = filterGroup;
    }

    public int getFilterGroup() {
        return filterGroup;
    }

    public String getFilterType() {
        return filterType;
    }

    public void setFilterGroup(int filterGroup) {
        this.filterGroup = filterGroup;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(LocalDateTime expireTime) {
        this.expireTime = expireTime;
    }

    public Time getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(Time timeTaken) {
        this.timeTaken = timeTaken;
    }

    public LocalDateTime getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(LocalDateTime startedOn) {
        this.startedOn = startedOn;
    }

    public LocalDateTime getCompletedOn() {
        return completedOn;
    }

    public void setCompletedOn(LocalDateTime completedOn) {
        this.completedOn = completedOn;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

}
