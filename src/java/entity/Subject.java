/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author taina
 */
public class Subject {

    private int id;
    private String thumbnail, title, description, tagline;
    private Boolean status, featured;
    private Category category;
    private List<PricePackage> pricePackages;
    private User created, taught;
    private int numbersOfLesson;
    private Timestamp updateDate;
    private double listedPrice;
    private double salePrice;
    private int numbersOfEnrollments;

    public double getListedPrice() {
        return listedPrice;
    }

    public void setListedPrice(double listedPrice) {
        this.listedPrice = listedPrice;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public Subject() {
    }

    public Subject(String thumbnail, String title, String description, String tagline, Boolean status, Boolean featured, Category category, User taught, int numbersOfLesson, Timestamp updateDate) {
        this.thumbnail = thumbnail;
        this.title = title;
        this.description = description;
        this.tagline = tagline;
        this.status = status;
        this.featured = featured;
        this.category = category;
        this.taught = taught;
        this.numbersOfLesson = numbersOfLesson;
        this.updateDate = updateDate;
    }

    public Subject(int id, String thumbnail, String title, String description, String tagline, Boolean status, Boolean featured, Category category,List<PricePackage> pricePackage, User created, User taught, int numbersOfLesson, Timestamp updateDate) {
        this.id = id;
        this.thumbnail = thumbnail;
        this.title = title;
        this.description = description;
        this.tagline = tagline;
        this.status = status;
        this.featured = featured;
        this.category = category;
        this.pricePackages = pricePackages;
        this.created = created;
        this.taught = taught;
        this.numbersOfLesson = numbersOfLesson;
        this.updateDate = updateDate;
    }
    
    public Subject(String thumbnail, String title, String description, String tagline, Boolean status, Category cate, Boolean featured, User taught) {
        this.thumbnail = thumbnail;
        this.title = title;
        this.description = description;
        this.tagline = tagline;
        this.status = status;
        this.category = cate;
        this.featured = featured;
        this.taught = taught;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getFeatured() {
        return featured;
    }

    public Boolean isFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<PricePackage> getPricePackages() {
        return pricePackages;
    }

    public void setPricePackages(List<PricePackage> pricePackages) {
        this.pricePackages = pricePackages;
    }

    public User getCreated() {
        return created;
    }

    public void setCreated(User created) {
        this.created = created;
    }

    public User getTaught() {
        return taught;
    }

    public void setTaught(User taught) {
        this.taught = taught;
    }

    public int getNumbersOfLesson() {
        return numbersOfLesson;
    }

    public void setNumbersOfLesson(int numbersOfLesson) {
        this.numbersOfLesson = numbersOfLesson;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateTime) {
        this.updateDate = updateTime;
    }
    
    public int getNumbersOfEnrollments() {
        return numbersOfEnrollments;
    }

    public void setNumbersOfEnrollments(int numbersOfEnrollments) {
        this.numbersOfEnrollments = numbersOfEnrollments;
    }
}
