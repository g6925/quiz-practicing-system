/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.List;
import entity.Level;

/**
 *
 * @author taina
 */
public class Question {

    private int id;
    private String content,explanation;
    private Topic topic;
    private Subject subject;
    private int level;
    private boolean status;
    private DimensionSubject dimensionSubject;
    private Level leve;
    private List<Answer> answers;
    private List<Dimension> dimension;

    public Question() {
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public Question(int id, String content, Topic topic, Subject subject, int level, boolean status, DimensionSubject dimensionSubject, Level leve, List<Answer> answers, List<Dimension> dimension) {
        this.id = id;
        this.content = content;
        this.topic = topic;
        this.subject = subject;
        this.level = level;
        this.status = status;
        this.dimensionSubject = dimensionSubject;
        this.leve = leve;
        this.answers = answers;
        this.dimension = dimension;
    }

    public List<Dimension> getDimension() {
        return dimension;
    }

    public void setDimension(List<Dimension> dimension) {
        this.dimension = dimension;
    }

    

    public Question(int id, String content, Topic topic, Subject subject, int level, boolean status) {
        this.id = id;
        this.content = content;
        this.topic = topic;
        this.subject = subject;
        this.level = level;
        this.status = status;
    }

    public Question(String content, Topic topic, Subject subject, int level, boolean status) {
        this.content = content;
        this.topic = topic;
        this.subject = subject;
        this.level = level;
        this.status = status;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public Level getLeve() {
        return leve;
    }

    public void setLeve(Level leve) {
        this.leve = leve;
    }

    public DimensionSubject getDimensionSubject() {
        return dimensionSubject;
    }

    public void setDimensionSubject(DimensionSubject dimensionSubject) {
        this.dimensionSubject = dimensionSubject;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public int getLevel() {
        return level;
    }

    public boolean isStatus() {
        return status;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setLevel(Level level) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
