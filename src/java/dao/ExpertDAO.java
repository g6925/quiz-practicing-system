/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.UserDAO.getUser;
import entity.Role;
import entity.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author taina
 */
public class ExpertDAO extends DBContext{
    public static User getUser(ResultSet rs, String id, String email, String passwork,
            String fullname, String gender, String phone, String image, String status,
            Role role) throws SQLException {
        User u = new User();
        u.setId(rs.getInt(id));
        u.setEmail(rs.getString(email));
        u.setPass(rs.getString(passwork));
        u.setName(rs.getString(fullname));
        u.setGender(rs.getBoolean(gender));
        u.setPhone(rs.getString(phone));
        u.setImage(rs.getString(image));
        u.setStatus(rs.getBoolean(status));
        u.setRole(role);
        return u;
    }
    public List<User> getAll() {
        List<User> list = new ArrayList<>();
        String sql = "SELECT ID, Email, [PassWord], [FullName], Gender, Phone, [Image], [Status], [RoleID]\n"
                + "FROM [User] WHERE RoleID = 5";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Role role = RoleDAO.getRoleByID(connection, rs.getInt("RoleID"));
                list.add(getUser(rs, "ID", "Email", "PassWord", "FullName",
                        "Gender", "Phone", "Image", "Status", role));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    
    
    
    public static void main(String[] args) {
        ExpertDAO d = new ExpertDAO();
        List<User> users = d.getAll();
        for (User user : users) {
            System.out.println(user.getName());
        }
    }
   
}
