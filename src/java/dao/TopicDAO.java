/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.UserDAO.getUser;
import entity.Lesson;
import entity.Subject;
import entity.Topic;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hiepx
 */
public class TopicDAO extends DBContext {

    public static Topic getTopic(ResultSet rs, String id, String name, Subject s,List<Lesson> lessons) throws SQLException {
        Topic t = new Topic();
        t.setId(rs.getInt(id));
        t.setName(rs.getString(name));
        t.setSubject(s);
        t.setLessons(lessons);
        return t;
    }

    public List<Topic> getListTopicsBySubjectID(int subjectID) {
        List<Topic> list = new ArrayList<>();
        LessonDAO ldao = new LessonDAO();
        String sql = "select t.ID, t.Name \n"
                + "from Topic t where t.SubjectSID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subjectID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject subject = SubjectDAO.getSubjectByID(getConnection(), subjectID);
                List<Lesson> lessons = ldao.findLessonsByTopicID(rs.getInt("ID"));
                list.add(getTopic(rs, "ID", "Name", subject,lessons));
                
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    public static void main(String[] args) {
        TopicDAO db = new TopicDAO();
        System.out.println(db.getListTopicsBySubjectID(1).size());
    }

    public Topic getTopicsBySubjectID(int subjectID) {
        LessonDAO ldao = new LessonDAO();
        String sql = "select t.ID, t.Name \n"
                + "from Topic t where t.SubjectSID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subjectID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject subject = SubjectDAO.getSubjectByID(getConnection(), subjectID);
                List<Lesson> lessons = ldao.findLessonsByTopicID(rs.getInt("ID"));
                return getTopic(rs, "ID", "Name", subject,lessons);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public Topic getTopicsByID(int topicID) {
        Topic t = new Topic();
        LessonDAO ldao = new LessonDAO();
        String sql = "select t.ID, t.Name \n"
                + "from Topic t where t.ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, topicID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                t.setId(topicID);
                t.setName(rs.getString("Name"));
                List<Lesson> lessons = ldao.findLessonsByTopicID(topicID);
                t.setLessons(lessons);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return t;
    }
}
