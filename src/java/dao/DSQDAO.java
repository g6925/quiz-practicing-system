/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.QuestionDAO.getQuest;
import entity.Dimension;
import entity.Dimension_Subject_Question;
import entity.Question;
import entity.Subject;
import entity.Topic;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jarnsaurus
 */
public class DSQDAO extends DBContext {

    public static Dimension_Subject_Question getDSQ(ResultSet rs, String did, String sid, String qid) throws SQLException {
        Dimension_Subject_Question dsq = new Dimension_Subject_Question();
        dsq.setDimensionID(rs.getInt(did));
        dsq.setSubjectID(rs.getInt(sid));
        dsq.setQuestId(rs.getInt(qid));
        return dsq;
    }

    public List<Question> getQuestByDimension(String qqType[]) {
        List<Question> q = new ArrayList<>();
        String sql = "SELECT"
                + " [Dimension_SubjectDimensionID]\n"
                + "      ,[Dimension_SubjectSubjectID]\n"
                + "      ,[QuestionID]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Dimension_Subject_Question] where";
        sql += " Dimension_SubjectDimensionID = " + Integer.parseInt(qqType[0]);
        int i = 1;
        while (i < qqType.length) {
            sql += " or Dimension_SubjectDimensionID = " + Integer.parseInt(qqType[i]);
            i++;
        }
        sql += " ORDER BY NEWID()";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                QuestionDAO qd = new QuestionDAO();
                Question qq = qd.getQById(rs.getInt("QuestionID"));
                q.add(qq);
            }
        } catch (SQLException e) {
        }
        return q;
    }

    public List<Question> getQuestByDimension(List<Dimension> l, int total) {
        List<Question> q = new ArrayList<>();
        String sql = "SELECT Top " + total
                + " [Dimension_SubjectDimensionID]\n"
                + "      ,[Dimension_SubjectSubjectID]\n"
                + "      ,[QuestionID]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Dimension_Subject_Question] where 1=1";

        for (Dimension d : l) {
            sql += " OR Dimension_SubjectDimensionID = " + d.getId();
        }
        sql += " ORDER BY NEWID()";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                QuestionDAO qd = new QuestionDAO();
                Question qq = qd.getQById(rs.getInt("QuestionID"));
                q.add(qq);
            }
        } catch (SQLException e) {
        }
        return q;
    }

    public List<Question> getQuestByDimension(Dimension l, int total) {
        List<Question> q = new ArrayList<>();
        String sql = "SELECT Top " + total
                + " [Dimension_SubjectDimensionID]\n"
                + "      ,[Dimension_SubjectSubjectID]\n"
                + "      ,[QuestionID]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Dimension_Subject_Question] where";
        sql += " Dimension_SubjectDimensionID = " + l.getId();
        sql += " ORDER BY NEWID()";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                QuestionDAO qd = new QuestionDAO();
                Question qq = qd.getQById(rs.getInt("QuestionID"));
                q.add(qq);
            }
        } catch (SQLException e) {
        }
        return q;
    }

    public int getQByDomain(int sid, int did) {
        String sql = "SELECT COUNT([QuestionID]) as total\n"
                + " FROM [QuizPracticingSystem].[dbo].[Dimension_Subject_Question] where Dimension_SubjectSubjectID = ? and Dimension_SubjectDimensionID = ?";
        int count = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, sid);
            st.setInt(2, did);
            ResultSet rs = st.executeQuery();
            rs.next();
            count = rs.getInt(("total"));
        } catch (SQLException e) {
            System.out.println(e);
        }
        return count;
    }

    public void insertDimensionSubjectQuestion(int dimensionID,int subjectID,int questionID) {
        try {
            String sql = "Insert into Dimension_Subject_Question\n"
                    + "(Dimension_SubjectDimensionID,Dimension_SubjectSubjectID,QuestionID) values\n"
                    + "(?,?,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, dimensionID);
            statement.setInt(2, subjectID);
            statement.setInt(3, questionID);
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("insertDimensionSubjectQuestion err : " + ex.getMessage());
        }
    }
}
