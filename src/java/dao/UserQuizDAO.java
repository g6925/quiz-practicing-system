/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Quiz;
import entity.User;
import entity.UserQuiz;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jarnsaurus
 */
public class UserQuizDAO extends DBContext {

    public List<UserQuiz> getQuizTakenByUserID(int uid) {
        List<UserQuiz> ls = new ArrayList<>();
        String sql = "select uq.UserID, uq.QuizID, s.Title, q.Name, q.Level, q.Duration, q.PassRate, "
                + "q.Description, q.Type, uq.TimeTaken, uq.StartedOn, uq.CompletedOn, uq.Grade, uq.filterType, "
                + "uq.filterGroup from User_Quiz uq join Quiz q on uq.QuizID=q.ID join [Subject] s on q.SubjectSID=s.ID "
                + "where uq.UserID=? and q.Type='Practice'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, uid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz q = QuizDAO.getQuizByID(connection, rs.getInt("QuizID"));
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                UserQuiz uq = new UserQuiz();
                
                if(rs.getTimestamp("StartedOn") != null){
                    LocalDateTime startedTime = rs.getTimestamp("StartedOn").toLocalDateTime();
                    uq.setStartedOn(startedTime);
                }
                if(rs.getTimestamp("CompletedOn") != null){
                    LocalDateTime completedTime = rs.getTimestamp("CompletedOn").toLocalDateTime();
                    uq.setCompletedOn(completedTime);
                }
                uq.setTimeTaken(rs.getTime("TimeTaken"));
                uq.setGrade(rs.getFloat("Grade"));
                uq.setUser(u);
                uq.setQuiz(q);
                uq.setFilterGroup(rs.getInt("filterGroup"));
                uq.setFilterType(rs.getString("filterType"));
                //UserQuiz uq = new UserQuiz(rs.getTime("TimeTaken"), startedTime, completedTime, rs.getFloat("Grade"), u, q, rs.getString("filterType"), rs.getInt("filterGroup"));
                ls.add(uq);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return ls;
    }

    public List<UserQuiz> getQuizTakenByUserIDAndSubjectID(int uid, int sid) {
        List<UserQuiz> ls = new ArrayList<>();
        String sql = "select uq.UserID, uq.QuizID, s.Title, q.Name, q.Level, q.Duration, q.PassRate, "
                + "q.Description, q.Type, uq.TimeTaken, uq.StartedOn, uq.CompletedOn, uq.Grade, uq.filterType, uq.filterGroup from User_Quiz uq join "
                + "Quiz q on uq.QuizID=q.ID join [Subject] s on q.SubjectSID=s.ID where uq.UserID=? and s.ID=? and q.Type='Practice'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, uid);
            st.setInt(2, sid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz q = QuizDAO.getQuizByID(connection, rs.getInt("QuizID"));
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                LocalDateTime startedTime = rs.getTimestamp("StartedOn").toLocalDateTime();
                LocalDateTime completedTime = rs.getTimestamp("CompletedOn").toLocalDateTime();
                UserQuiz uq = new UserQuiz(rs.getTime("TimeTaken"), startedTime, completedTime, rs.getFloat("Grade"), u, q, rs.getString("filterType"), rs.getInt("filterGroup"));
                ls.add(uq);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return ls;
    }

    public UserQuiz getQuizTakenByUserIDandQuizID(int uid, int qid) {
        String sql = "select uq.UserID, uq.QuizID, s.Title, q.Name, q.Level, q.Duration, q.PassRate, "
                + "q.Description, q.Type, uq.TimeTaken, uq.StartedOn, uq.CompletedOn, uq.Grade, uq.filterType, uq.filterGroup from User_Quiz uq join "
                + "Quiz q on uq.QuizID=q.ID join [Subject] s on q.SubjectSID=s.ID where uq.UserID=? and uq.QuizID=? and q.Type='Practice'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, uid);
            st.setInt(2, qid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz q = QuizDAO.getQuizByID(connection, rs.getInt("QuizID"));
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                LocalDateTime startedTime = rs.getTimestamp("StartedOn").toLocalDateTime();
                LocalDateTime completedTime = rs.getTimestamp("CompletedOn").toLocalDateTime();
                UserQuiz uq = new UserQuiz(rs.getTime("TimeTaken"), startedTime, completedTime, rs.getFloat("Grade"), u, q, rs.getString("filterType"), rs.getInt("filterGroup"));
                return uq;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public static void main(String[] args) {
        UserQuizDAO uq = new UserQuizDAO();
        List<UserQuiz> l = uq.getQuizTakenByUserIDAndSubjectID(3, 1);
        for (UserQuiz userQuiz : l) {
            System.out.println(userQuiz.getQuiz().getSubject().getTitle());
        }
    }
}
