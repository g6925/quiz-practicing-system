/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Level;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jarnsaurus
 */
public class LevelDAO extends DBContext {

    public static Level getLevel(ResultSet rs, String id, String name) throws SQLException {
        Level l = new Level();
        l.setId(rs.getInt(id));
        l.setName(rs.getString(name));
        return l;
    }

    public List<Level> getAll() {
        List<Level> l = new ArrayList<>();
        String sql = "SELECT LevelID, Name\n"
                + "FROM Level";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                l.add(getLevel(rs, "LevelID", "Name"));
            }
        } catch (SQLException e) {
        }
        return l;
    }
       
    public Level getLevelByID(int levelID) {
        String sql = "SELECT [LevelID]\n"
                + "      ,[Name]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Level]\n"
                + "  where LevelID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, levelID);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
               return getLevel(rs, "LevelID", "Name");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }
    public Level getLevelByName(String name) {
        String sql = "SELECT [LevelID]\n"
                + "      ,[Name]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Level]\n"
                + "  where Name = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
               return getLevel(rs, "LevelID", "Name");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }
    
    public static void main(String[] args) {
        LevelDAO db = new LevelDAO();
        System.out.println(db.getAll().size());
    }
  }
