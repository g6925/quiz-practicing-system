/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Question;
import entity.Quiz;
import entity.User;
import entity.UserQuiz;
import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author vietd
 */
public class QuizHandleDAO extends DBContext {

    UserQuiz mapping(UserQuiz q, ResultSet rs) throws SQLException {
        q.setUserId(rs.getInt("UserID"));
        q.setQuiz(QuizDAO.getQuizByID(connection, rs.getInt("QuizID")));
        q.setTimeTaken(rs.getTime("TimeTaken"));
        q.setStartedOn(rs.getTimestamp("StartedOn").toLocalDateTime());
        q.setCompletedOn(rs.getTimestamp("CompletedOn").toLocalDateTime());
        q.setGrade(rs.getFloat("Grade"));
        return q;
    }

    public void updateRetake(LocalDateTime start, LocalDateTime expire, int userId, int quizId) {
        try {
            String sql = "update User_Quiz set TimeTaken = null, Grade = null, CompletedOn = null,\n"
                    + "StartedOn = ?, ExpireTime = ? where UserID = ? and QuizId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setTimestamp(1, java.sql.Timestamp.valueOf(start));
            stm.setTimestamp(2, java.sql.Timestamp.valueOf(expire));
            stm.setInt(3, userId);
            stm.setInt(4, quizId);
            stm.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void insertOrUpdateUserQuiz(UserQuiz userQuiz) {
        try {
            String sql = "SELECT * FROM User_Quiz where UserID = ? and QuizID = ?";
            PreparedStatement stm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            stm.setInt(1, userQuiz.getUserId());
            stm.setInt(2, userQuiz.getQuiz().getId());
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                rs.updateTime("TimeTaken", userQuiz.getTimeTaken());
                rs.updateTimestamp("CompletedOn", java.sql.Timestamp.valueOf(userQuiz.getCompletedOn()));
                rs.updateFloat("Grade", userQuiz.getGrade());
                rs.updateRow();
            } else {
                rs.moveToInsertRow();
                rs.updateInt("UserID", userQuiz.getUserId());
                rs.updateInt("QuizID", userQuiz.getQuiz().getId());
                rs.updateTimestamp("StartedOn", java.sql.Timestamp.valueOf(userQuiz.getStartedOn()));
                if (userQuiz.getQuiz().getType().equalsIgnoreCase("exam")) {
                    rs.updateTimestamp("ExpireTime", java.sql.Timestamp.valueOf(userQuiz.getExpireTime()));
                }
                rs.updateInt("filterGroup", userQuiz.getFilterGroup());
                rs.updateString("filterType", userQuiz.getFilterType());
                rs.insertRow();
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public boolean checkUserDoQuiz(int quizId, int userId) {
        try {
            String sql = "SELECT * FROM User_Quiz where UserID = ? and QuizID = ?";
            PreparedStatement stm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            stm.setInt(1, userId);
            stm.setInt(2, quizId);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                rs.close();
                return true;
            }
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

    public int checkExpired(int quizId, int userId, LocalDateTime currentTime) {
        try {
            String sql = "SELECT count(*) FROM User_Quiz where UserID = ? and QuizID = ? and ExpireTime >= ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userId);
            stm.setInt(2, quizId);
            stm.setTimestamp(3, java.sql.Timestamp.valueOf(currentTime));
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return -1;
    }

    public boolean checkUserHasComplete(int quizId, int userId) {
        try {
            String sql = "select uq.UserID from Quiz q\n"
                    + "left join User_Quiz uq\n"
                    + "on q.ID = uq.QuizID\n"
                    + "where q.ID = ? and uq.UserID = ? and uq.CompletedOn is not null";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, quizId);
            stm.setInt(2, userId);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                rs.close();
                return true;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

    public Time getDuration(int quizId) {
        try {
            String sql = "SELECT Duration from Quiz where QuizID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, quizId);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getTime("Duration");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

}
