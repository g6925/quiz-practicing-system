/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import entity.Answer;
import entity.Dimension;
import entity.DimensionSubject;
import entity.Question;
import entity.Subject;
import entity.Topic;
import entity.Level;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.apache.poi.ss.formula.functions.TDist;

/**
 *
 * @author hiepx
 */
public class QuestionDAO extends DBContext {

    public List<Question> getListByPage(List<Question> list, int start, int end) {
        List<Question> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public static Question getQuestion(ResultSet rs, String id, String content, Topic topic, String level, String status, DimensionSubject ds, List<Dimension> d) throws SQLException {
        Question c = new Question();
        c.setId(rs.getInt(id));
        c.setContent(rs.getString(content));
        c.setLevel(rs.getInt(level));
        c.setTopic(topic);
        c.setStatus(rs.getBoolean(status));
        c.setDimensionSubject(ds);
        c.setDimension(d);
        return c;
    }

    public void insertQuestion(Question q, int topicID, int subjectID) {
        try {
            String sql = "INSERT INTO Question\n"
                    + "  (Content, \n"
                    + "  TopicID, \n"
                    + "  TopicSubjectID, \n"
                    + "  LevelID,\n"
                    + "  status) \n"
                    + "VALUES (\n"
                    + "  ?, \n"
                    + "  ?, \n"
                    + "  ?, \n"
                    + "  ?, \n"
                    + "  1);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, q.getContent());
            statement.setInt(2, topicID);
            statement.setInt(3, subjectID);
            statement.setInt(4, q.getLeve().getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("insert question err: " + ex.getMessage());
        }
    }

    public static Question getQuest(ResultSet rs, String id, String content, Topic t, Subject s, String Level, String Status) throws SQLException {
        Question q = new Question();
        q.setId(rs.getInt(id));
        q.setContent(rs.getString(content));
        q.setTopic(t);
        q.setSubject(s);
        q.setLevel(rs.getInt(Level));
        q.setStatus(rs.getBoolean(Status));
        return q;
    }

    public Question getQuestionByContent(String content) {
        String sql = "SELECT [ID]\n"
                + "      ,[Content]\n"
                + "      ,[TopicID]\n"
                + "      ,[TopicSubjectID]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Question]\n"
                + "  where Content = ?";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setString(1, content);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Question q = new Question();
                q.setId(rs.getInt("ID"));
                q.setContent(rs.getString("Content"));
                return q;
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public List<Question> getAll() {
        List<Question> list = new ArrayList<>();
        String sql = "SELECT [ID]\n"
                + "      ,[Content]\n"
                + "      ,[TopicID]\n"
                + "      ,[TopicSubjectID]\n"
                + "      ,[LevelID]\n"
                + "      ,[status]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Question]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("TopicSubjectID"));
                TopicDAO td = new TopicDAO();
                Topic t = td.getTopicsBySubjectID(s.getId());
                list.add(getQuest(rs, "ID", "Content", t, s, "LevelID", "status"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Question> getQByTopics(List<Topic> l, int total) {
        List<Question> list = new ArrayList<>();
        String sql = "SELECT  Top " + total
                + " [ID]\n"
                + "      ,[Content]\n"
                + "      ,[TopicID]\n"
                + "      ,[TopicSubjectID]\n"
                + "      ,[LevelID]\n"
                + "      ,[status]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Question] where 1=1";
        for (Question question : list) {
            sql += " OR TopicSubjectID = " + question.getId() + " ";
        }
        sql += " ORDER BY NEWID()";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("TopicSubjectID"));
                TopicDAO td = new TopicDAO();
                Topic t = td.getTopicsByID(rs.getInt("TopicID"));
                list.add(getQuest(rs, "ID", "Content", t, s, "LevelID", "status"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

//    public static void main(String[] args) {
//        QuestionDAO db = new QuestionDAO();
//        TopicDAO td = new TopicDAO();
//        List<Topic> t = td.getListTopicsBySubjectID(1);
//        for (Question qByTopic : db.getQByTopics(t)) {
//            System.out.println(qByTopic.getId());
//        }
//    }
    public List<Question> getQByTopics(String qqType[]) {
        List<Question> list = new ArrayList<>();
        String sql = "SELECT "
                + " [ID]\n"
                + "      ,[Content]\n"
                + "      ,[TopicID]\n"
                + "      ,[TopicSubjectID]\n"
                + "      ,[LevelID]\n"
                + "      ,[status]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Question] where ";
        sql += " TopicSubjectID = " + Integer.parseInt(qqType[0]);
        int i = 1;
        while (i < qqType.length) {
            sql += " or TopicID = " + Integer.parseInt(qqType[i]);
            i++;
        }
        sql += " ORDER BY NEWID()";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("TopicSubjectID"));
                TopicDAO td = new TopicDAO();
                Topic t = td.getTopicsByID(rs.getInt("TopicID"));
                list.add(getQuest(rs, "ID", "Content", t, s, "LevelID", "status"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Question> getQByTopic(int tt, int total) {
        List<Question> list = new ArrayList<>();
        String sql = "SELECT Top " + total
                + " [ID]\n"
                + "      ,[Content]\n"
                + "      ,[TopicID]\n"
                + "      ,[TopicSubjectID]\n"
                + "      ,[LevelID]\n"
                + "      ,[status]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Question] where TopicSubjectID = ?";
        sql += " ORDER BY NEWID()";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, tt);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("TopicSubjectID"));
                TopicDAO td = new TopicDAO();
                Topic t = td.getTopicsByID(rs.getInt("TopicID"));
                list.add(getQuest(rs, "ID", "Content", t, s, "LevelID", "status"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Question> getQByLevels(String qqType[], int total) {
        List<Question> list = new ArrayList<>();
        String sql = "SELECT Top " + total
                + " [ID]\n"
                + "      ,[Content]\n"
                + "      ,[TopicID]\n"
                + "      ,[TopicSubjectID]\n"
                + "      ,[LevelID]\n"
                + "      ,[status]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Question] where ";
        sql += " LevelID = " + Integer.parseInt(qqType[0]);
        int i = 1;
        while (i < qqType.length) {
            sql += " or LevelID = " + Integer.parseInt(qqType[i]);
            i++;
        }
        sql += " ORDER BY NEWID()";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("TopicSubjectID"));
                TopicDAO td = new TopicDAO();
                Topic t = td.getTopicsByID(rs.getInt("TopicID"));
                list.add(getQuest(rs, "ID", "Content", t, s, "LevelID", "status"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Question> getQByGroup(int lid, int sid) {
        List<Question> list = new ArrayList<>();
        String sql = "SELECT * FROM [QuizPracticingSystem].[dbo].[Question] where TopicSubjectID = ? and LevelID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, sid);
            st.setInt(2, lid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = SubjectDAO.getSubjectByID(connection, sid);
                TopicDAO td = new TopicDAO();
                Topic t = td.getTopicsByID(rs.getInt("TopicID"));
                list.add(getQuest(rs, "ID", "Content", t, s, "LevelID", "status"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Question getQById(int id) {
        String sql = "SELECT * FROM [QuizPracticingSystem].[dbo].[Question] where ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("TopicSubjectID"));
                TopicDAO td = new TopicDAO();
                Topic t = td.getTopicsByID(rs.getInt("TopicID"));
                return getQuest(rs, "ID", "Content", t, s, "LevelID", "status");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Question> getQByTopicId(int tid, int sid) {
        List<Question> qs = new ArrayList<>();
        String sql = "SELECT * FROM [QuizPracticingSystem].[dbo].[Question] where TopicSubjectID = ? and TopicID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, sid);
            st.setInt(2, tid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question q = new Question();
                Subject s = SubjectDAO.getSubjectByID(connection, sid);
                q.setId(rs.getInt("ID"));
                q.setContent(rs.getString("Content"));
                LevelDAO ldao = new LevelDAO();
                q.setLevel(rs.getInt("LevelID"));
                q.setStatus(rs.getBoolean("Status"));
                q.setSubject(s);
                TopicDAO t = new TopicDAO();
                q.setTopic(t.getTopicsByID(tid));
                qs.add(q);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return qs;
    }

    public List<Question> getQuestionBySubjectID(int subjectID) {
        List<Question> questions = new ArrayList<Question>();
        String sql = "SELECT  q.Content,q.ID as qID,q.LevelID,q.Status,q.TopicID,q.TopicSubjectID,d.ID as dID,d.Name,d.Type\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Question] q join Dimension_Subject_Question dsq on q.ID = dsq.QuestionID and q.TopicSubjectID = dsq.Dimension_SubjectSubjectID\n"
                + "  join Dimension_Subject ds on ds.SubjectID = dsq.Dimension_SubjectSubjectID and ds.DimensionID = dsq.Dimension_SubjectDimensionID\n"
                + "  join Dimension d on d.ID = ds.DimensionID\n"
                + "  join Subject s on s.ID = ds.SubjectID\n"
                + " where s.ID = ?";
        sql += " order by q.ID";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subjectID);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                TopicDAO tdao = new TopicDAO();
                Topic c = tdao.getTopicsByID(rs.getInt("TopicID"));
                LevelDAO ldao = new LevelDAO();

                DimensionSubject ds = new DimensionSubject();
                DimensonDAO ddao = new DimensonDAO();
                List<Dimension> d = ddao.getDimensionByQuestionID(rs.getInt("qID"));

                questions.add(getQuestion(rs, "qID", "Content", c, "LevelID", "Status", ds, d));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return questions;
    }

    public List<Question> filter(int subjectID, int topicID, String dimensionType, int levelID, String status, String keyWord) {
        List<Question> questions = new ArrayList<>();
        String sql = "SELECT  q.Content,q.ID,q.LevelID,q.Status,q.TopicID,q.TopicSubjectID,d.ID as dID,d.Name,d.Type\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Question] q join Dimension_Subject_Question dsq on q.ID = dsq.QuestionID and q.TopicSubjectID = dsq.Dimension_SubjectSubjectID\n"
                + "  join Dimension_Subject ds on ds.SubjectID = dsq.Dimension_SubjectSubjectID and ds.DimensionID = dsq.Dimension_SubjectDimensionID\n"
                + "  join Dimension d on d.ID = ds.DimensionID\n"
                + "  join Subject s on s.ID = ds.SubjectID\n"
                + " where s.ID = ?";
        if (topicID > 0) {
            sql += " AND TopicID =" + topicID;
        }
        if (levelID > 0) {
            sql += " AND LevelID =" + levelID;
        }
        if (status != null) {
            sql += " AND q.Status = '" + status + "'";
        }
        if (dimensionType != null) {
            sql += " AND d.Name = '" + dimensionType + "'";
        }
        if (keyWord != null) {
            sql += " AND Content like '%" + keyWord + "%'";
        }
        sql += " order by q.ID";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, subjectID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question q = new Question();
                QuestionDAO qdao = new QuestionDAO();
                q = qdao.getQuestionByQuestiontID(rs.getInt("ID"));
                q.setContent(rs.getString("Content"));
                q.setId(rs.getInt("ID"));
                DimensionSubject ds = new DimensionSubject();
                DimensonDAO ddao = new DimensonDAO();
                ds.setDimension(ddao.getDimensionByName(rs.getString("Name")));
                q.setDimensionSubject(ds);
                LevelDAO ldao = new LevelDAO();
                q.setLevel(rs.getInt("LevelID"));
                q.setStatus(rs.getBoolean("Status"));
                TopicDAO tdao = new TopicDAO();
                q.setTopic(tdao.getTopicsByID(rs.getInt("TopicID")));
                questions.add(q);
            }
        } catch (Exception e) {
            System.out.println("filter question err :" + e);
        }
        return questions;
    }

    public ArrayList<Answer> getAllAnswers(int questionId, Connection c, boolean answer) {
        ArrayList<Answer> list = new ArrayList<>();
        String sql = "SELECT * FROM Answers where QuestionID = ?";
        try {
            PreparedStatement st = c.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Answer a = new Answer();
                a.setId(rs.getInt("ID"));
                a.setContent(rs.getString("Content"));
                if (answer) {
                    a.setCorrect(rs.getBoolean("Correct"));
                }
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<Question> getAllQuestionInQuiz(int quizId, int userId, boolean answer) {
        ArrayList<Question> questions = new ArrayList<>();
        try {
            String sql = "select q.* from Quiz AS quiz\n"
                    + "join User_Quiz AS userQuiz\n"
                    + "on quiz.ID = userQuiz.QuizID\n"
                    + "join Quiz_Question qq\n"
                    + "on qq.QuizID = quiz.ID\n"
                    + "join Question q\n"
                    + "on qq.QuestionID = q.ID\n"
                    + "where quiz.ID = ? and userQuiz.UserID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, quizId);
            stm.setInt(2, userId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                // Subject s = SubjectDAO.getSubjectByID(connection,
                // rs.getInt("TopicSubjectID"));
                // TopicDAO td = new TopicDAO();
                // Topic t = td.getTopicsByID(rs.getInt("TopicID"));
                Question question = new Question();
                question.setId(rs.getInt("ID"));
                question.setContent(rs.getString("Content"));
                question.setAnswers(getAllAnswers(question.getId(), connection, answer));
                questions.add(question);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return questions;
    }

    private List<Integer> getIdsOfTrueAnswerInQuizLesson(int quizId) {
        String sql = "select a.ID from Quiz_Question qq\n"
                + "join Question q\n"
                + "on qq.QuestionID = q.ID \n"
                + "join Answers a\n"
                + "on a.QuestionID = q.ID \n"
                + "where a.Correct = 1 and qq.QuizID = ?";
        List<Integer> ls = new ArrayList<>();
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, quizId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ls.add(rs.getInt(1));
            }
        } catch (SQLException ex) {
        }

        return ls;
    }

    public void clearOldAnswer(int accountId, int quizId) throws SQLException {
        String sql = "DELETE FROM User_Quiz_Question WHERE User_QuizUserID = ?\n"
                + "AND User_QuizQuizID = ? and QuestionID in (select q.ID from Question q\n"
                + "join Quiz_Question qq\n"
                + "on q.ID = qq.QuestionID\n"
                + "and qq.QuizID = ?)";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, accountId);
            stmt.setInt(2, quizId);
            stmt.setInt(3, quizId);
            stmt.executeLargeUpdate();
        }
    }

    public int getNumberOfTrueAnswerInQuiz(int questionId) {
        String sql = "SELECT COUNT(*) FROM Question AS q INNER JOIN Answers AS a ON\n"
                + "a.QuestionID = q.ID\n"
                + "WHERE q.ID = ? and a.Correct = 1";
        int count = 0;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, questionId);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException ex) {
        }
        return count;
    }

    /**
     * Json format: { "quizId":"4", "answers":[
     * {"questionId":118,"answerIds":["475"]},
     * {"questionId":109,"answerIds":["440","441"]} ] }
     *
     */
    public float saveAnswer(int userId, JsonElement jsonElement, int totalQuestion) throws SQLException {
        JsonObject json = (JsonObject) jsonElement;
        int quizId = json.get("quizId").getAsInt();
        float scorePerQuestion = (float) 10 / totalQuestion;
        float score = 0;
        PreparedStatement stmt = null;
        try {
            clearOldAnswer(userId, quizId);
            List<Integer> trueAnswerIds = getIdsOfTrueAnswerInQuizLesson(quizId);
            stmt = connection.prepareStatement(
                    "insert into User_Quiz_Question (User_QuizUserID, User_QuizQuizID, QuestionID, UserAnswer, Marked) values (?, ?, ?, ?, ?)");
            stmt.setInt(1, userId);
            stmt.setInt(2, quizId);
            for (JsonElement e : json.get("answers").getAsJsonArray()) {
                JsonObject jAnswer = (JsonObject) e;
                int questionId = jAnswer.get("questionId").getAsInt();
                stmt.setInt(3, questionId);
                int trueAnsOfQuestion = getNumberOfTrueAnswerInQuiz(questionId);

                int countLengthUserAnswer = 0;
                int countUserAnswerTrue = 0;
                for (JsonElement j : jAnswer.get("answerIds").getAsJsonArray()) {
                    int answerId = j.getAsInt();
                    stmt.setInt(4, answerId);
                    stmt.setBoolean(5, trueAnswerIds.contains(answerId));
                    if (trueAnswerIds.contains(answerId)) {
                        countUserAnswerTrue++;
                    }
                    countLengthUserAnswer++;
                    stmt.executeUpdate();
                }
                if (trueAnsOfQuestion == countLengthUserAnswer && countUserAnswerTrue == trueAnsOfQuestion) {
                    score += scorePerQuestion;
                }
                countUserAnswerTrue = 0;
                countLengthUserAnswer = 0;
            }
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return (float) Math.round(score * 100) / 100;
    }

    public Question getQuestionByQuestiontID(int questionID) {
        String sql = "SELECT q.Content,q.ID,q.LevelID,q.Status,q.TopicID,q.TopicSubjectID,d.ID as dID\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Question] q\n"
                + "  join Dimension_Subject_Question dsq on q.ID = dsq.QuestionID\n"
                + "  join Dimension_Subject ds on ds.DimensionID = dsq.Dimension_SubjectDimensionID and ds.SubjectID = dsq.Dimension_SubjectSubjectID\n"
                + "  join Dimension d on d.ID = ds.DimensionID\n"
                + "  where q.ID = ?";
        sql += " order by q.ID";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionID);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                TopicDAO tdao = new TopicDAO();
                Topic c = tdao.getTopicsByID(rs.getInt("TopicID"));
                LevelDAO ldao = new LevelDAO();
                Level l = ldao.getLevelByID(rs.getInt("LevelID"));
                DimensionSubject ds = new DimensionSubject();
                DimensonDAO ddao = new DimensonDAO();
                Dimension d = ddao.getDimension(rs.getInt("dID"));
                ds.setDimension(d);
                List<Dimension> ld = ddao.getDimensionByQuestionID(rs.getInt("ID"));
                Question q = getQuestion(rs, "ID", "Content", c, "LevelID", "Status", ds, ld);
                q.setSubject(SubjectDAO.getSubjectByID(connection, Integer.parseInt(rs.getString("TopicSubjectID"))));
                AnswerDAO adao = new AnswerDAO();
                q.setAnswers(adao.getAnswerByQuestionID(questionID));
                q.setLeve(l);
                return q;
            }
        } catch (Exception e) {
            System.out.println("getQuestionByQuestiontID err : " + e);
        }
        return null;
    }

    public List<Question> getQuestionsByQuestiontID(int questionID) {
        List<Question> lQuestion = new ArrayList<>();
        String sql = "SELECT q.Content,q.ID,q.LevelID,q.Status,q.TopicID,q.TopicSubjectID,d.ID as dID\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Question] q\n"
                + "  join Dimension_Subject_Question dsq on q.ID = dsq.QuestionID\n"
                + "  join Dimension_Subject ds on ds.DimensionID = dsq.Dimension_SubjectDimensionID and ds.SubjectID = dsq.Dimension_SubjectSubjectID\n"
                + "  join Dimension d on d.ID = ds.DimensionID\n"
                + "  where q.ID = ?";
        sql += " order by q.ID";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionID);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                TopicDAO tdao = new TopicDAO();
                Topic c = tdao.getTopicsByID(rs.getInt("TopicID"));
                LevelDAO ldao = new LevelDAO();
                Level l = ldao.getLevelByID(rs.getInt("LevelID"));
                DimensionSubject ds = new DimensionSubject();
                DimensonDAO ddao = new DimensonDAO();
                Dimension d = ddao.getDimension(rs.getInt("dID"));
                ds.setDimension(d);
                List<Dimension> ld = ddao.getDimensionByQuestionID(rs.getInt("ID"));
                Question q = getQuestion(rs, "ID", "Content", c, "LevelID", "Status", ds, ld);
                q.setSubject(SubjectDAO.getSubjectByID(connection, Integer.parseInt(rs.getString("TopicSubjectID"))));
                AnswerDAO adao = new AnswerDAO();
                q.setAnswers(adao.getAnswerByQuestionID(questionID));
                q.setLeve(l);
                lQuestion.add(q);
            }
        } catch (Exception e) {
            System.out.println("getQuestionByQuestiontID err : " + e);
        }
        return lQuestion;
    }

    public void updateQuestion(Question q) {
        try {
            String sql = "Update Question set \n"
                    + "Content = ?,\n"
                    + "TopicID = ?,\n"
                    + "LevelID = ?,\n"
                    + "Status = ?,\n"
                    + "TopicSubjectID = ?\n"
                    + "where ID = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, q.getContent());
            statement.setInt(2, q.getTopic().getId());
            statement.setInt(3, q.getLeve().getId());
            statement.setBoolean(4, q.isStatus());
            statement.setInt(5, q.getSubject().getId());
            statement.setInt(6, q.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    public void updateQuestionDimension(int questionID, int group, int domain, Question q) {
        QuestionDAO qdao = new QuestionDAO();
        Question question = qdao.getQuestionByQuestiontID(questionID);
        int count = 0;
        for (Dimension d : question.getDimension()) {
            try {
                String sql = "Update Dimension_Subject_Question set	\n"
                        + "Dimension_SubjectDimensionID = ?,\n"
                        + "Dimension_SubjectSubjectID = ?,\n"
                        + "QuestionID =?\n"
                        + "WHERE Dimension_SubjectDimensionID = ? and Dimension_SubjectSubjectID = ? and QuestionID = ?";
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setInt(1, q.getDimension().get(count).getId());
                count++;
                statement.setInt(2, q.getSubject().getId());
                statement.setInt(3, q.getId());
                statement.setInt(4, d.getId());
                statement.setInt(5, question.getSubject().getId());
                statement.setInt(6, question.getId());
                statement.executeUpdate();
            } catch (SQLException ex) {
                System.out.println(ex);
            }
        }

    }

    public Boolean check(int questionID) {
        String sql = "SELECT  [User_QuizUserID]\n"
                + "      ,[User_QuizQuizID]\n"
                + "      ,[QuestionID]\n"
                + "      ,[UserAnswer]\n"
                + "      ,[Marked]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[User_Quiz_Question]\n"
                + "  where QuestionID = ?";
        try (
                PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, questionID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return false;
            }
        } catch (SQLException ex) {
            System.out.println("Check err: " + ex.getMessage());
        }
        return true;
    }
}
