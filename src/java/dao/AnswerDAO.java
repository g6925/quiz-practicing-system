/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Answer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hiepx
 */
public class AnswerDAO extends DBContext {

    public void insertAnswer(Answer a) {
        try {
            String sql = "INSERT INTO Answers\n"
                    + "  (\n"
                    + "  QuestionID, \n"
                    + "  Content, \n"
                    + "  Correct) \n"
                    + "VALUES \n"
                    + "  (\n"
                    + "  ?, \n"
                    + "  ?, \n"
                    + "  ?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, a.getQuestion().getId());
            statement.setString(2, a.getContent());
            statement.setBoolean(3, a.getCorrect());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void insertAnswer2(Answer a) {
        try {
            String sql = "INSERT INTO Answers\n"
                    + "  (\n"
                    + "  QuestionID, \n"
                    + "  Content, \n"
                    + "  Explanation, \n"
                    + "  Correct) \n"
                    + "VALUES \n"
                    + "  (\n"
                    + "  ?, \n"
                    + "  ?, \n"
                    + "  ?, \n"
                    + "  ?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, a.getQuestion().getId());
            statement.setString(2, a.getContent());
            statement.setString(3, a.getExplanation());
            statement.setBoolean(4, a.getCorrect());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Answer> getAnswerByQuestionID(int questionID) {
        List<Answer> lAnswer = new ArrayList<>();
        try {
            String sql = "SELECT q.ID,q.LevelID,q.Status,q.TopicID,q.TopicSubjectID,a.ID as aID,a.Content,a.Correct,a.Explanation\n"
                    + "FROM [QuizPracticingSystem].[dbo].[Question] q\n"
                    + "join Answers a on a.QuestionID = q.ID\n"
                    + "where q.ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Answer a = new Answer();
                a.setContent(rs.getString("Content"));
                a.setCorrect(rs.getBoolean("Correct"));
                a.setId(rs.getInt("aID"));
                a.setExplanation(rs.getString("Explanation"));
                lAnswer.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lAnswer;
    }

    public void updateAnswer(Answer a) {
        try {
            String sql = "UPDATE Answers set\n"
                    + "Content = ?,\n"
                    + "Correct = ?,\n"
                    + "Explanation = ?\n"
                    + "where ID = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, a.getContent());
            statement.setBoolean(2, a.getCorrect());
            statement.setString(3, a.getExplanation());
            statement.setInt(4, a.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteAnswer(int id) {
        try {
            String sql = "Delete from Answers where ID = " + id;
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("deleteAnswer err: " + ex.getMessage());
        }
    }

}
