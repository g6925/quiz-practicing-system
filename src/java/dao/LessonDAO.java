/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Lesson;
import entity.Subject;
import entity.Topic;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author Duong-PC
 */
public class LessonDAO extends DBContext {

    PreparedStatement stm;
    ResultSet rs;
    
    public List<Lesson> findAll() {
        List<Lesson> lessons = new ArrayList<>();
        try {
            String sql = "select ID, [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], TopicSubjectID\n"
                    + "from Lesson";
            System.out.println(sql);
            stm = connection.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("ID"));
                lesson.setName(rs.getString("Name"));
                lesson.setUrl(rs.getString("URL"));
                lesson.setDescription(rs.getString("Description"));
                lesson.setOrder(rs.getInt("Order"));
                lesson.setStatus(rs.getBoolean("Status"));
                Topic topic = new Topic();
                topic.setId(rs.getInt("TopicID"));
                Subject subject = new Subject();
                subject.setId(rs.getInt("TopicSubjectID"));
                topic.setSubject(subject);
                lesson.setTopic(topic);
                lesson.setType(rs.getString("Type"));
                lessons.add(lesson);
            }
        } catch (SQLException e) {
        }
        return lessons;
    }

    public List<Lesson> findAllWithState(String state) {
        List<Lesson> lessons = new ArrayList<>();
        try {
            String sql = "select Lesson.ID, [Name], [URL], Lesson.[Description], [Order], Lesson.[Status], [TopicID], [Type], TopicSubjectID\n"
                    + "from Lesson join [Subject] on Lesson.TopicSubjectID = Subject.ID";
            if (state.equalsIgnoreCase("new")) {
                sql += "WHERE [Subject].UpdateDate > ?\n";
            }
            System.out.println(sql);
            stm = connection.prepareStatement(sql);
            if (state.equalsIgnoreCase("new")) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, -14);
                stm.setTimestamp(1, new Timestamp(calendar.getTimeInMillis()));
            }
            rs = stm.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("ID"));
                lesson.setName(rs.getString("Name"));
                lesson.setUrl(rs.getString("URL"));
                lesson.setDescription(rs.getString("Description"));
                lesson.setOrder(rs.getInt("Order"));
                lesson.setStatus(rs.getBoolean("Status"));
                Topic topic = new Topic();
                topic.setId(rs.getInt("TopicID"));
                Subject subject = new Subject();
                subject.setId(rs.getInt("TopicSubjectID"));
                topic.setSubject(subject);
                lesson.setTopic(topic);
                lesson.setType(rs.getString("Type"));
                lessons.add(lesson);
            }
        } catch (SQLException e) {
        }
        return lessons;
    }

    public List<String> findAllDistinctLessonType() {
        List<String> types = new ArrayList<>();
        try {
            String sql = "select distinct(UPPER(l.Type)) from Lesson l";
            stm = connection.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                types.add(rs.getString(1));
            }
        } catch (Exception e) {
        }
        return types;
    }

    public List<Lesson> findAllLessonBySubjectId(int sid, String name, String status, String type) {
        List<Lesson> lessons = new ArrayList<>();
        try {
            String sql = "select l.*, t.Name as TopicName from Topic t\n" + "join Lesson l\n" + "on t.ID = l.TopicID\n" + "and t.SubjectSID = ?";
            if (name != null && !name.isEmpty()) {
                sql += " and l.Name like '%" + name + "%' ";
            }
            if (status != null) {
                if (status.equals("1")) {
                    sql += " and l.Status = 1 ";
                }
                if (status.equals("0")) {
                    sql += " and l.Status = 0 ";
                }
            }
            if (type != null && !type.isEmpty()) {
                sql += " and l.[Type] like '%" + type + "%' ";
            }
            sql += " order by l.[Order] ASC";
            System.out.println(sql);
            stm = connection.prepareStatement(sql);
            stm.setInt(1, sid);
            rs = stm.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("ID"));
                lesson.setName(rs.getString("Name"));
                lesson.setUrl(rs.getString("URL"));
                lesson.setDescription(rs.getString("Description"));
                lesson.setOrder(rs.getInt("Order"));
                lesson.setStatus(rs.getBoolean("Status"));
                lesson.setType(rs.getString("Type"));
                Topic topic = new Topic();
                topic.setId(rs.getInt("TopicID"));
                topic.setName(rs.getString("TopicName"));
                lesson.setTopic(topic);
                lessons.add(lesson);
            }
        } catch (SQLException e) {
        }
        return lessons;
    }

    public int updateLesson(Lesson lesson) {
        try {
            String sql = "update Lesson set [Name] = ?, [Description] = ?,  [URL] = ?, [Order] = ?, [Status] = ?, [TopicID] = ?, [Type] = ?, [TopicSubjectID] =? where ID = ?";
            stm = connection.prepareStatement(sql);
            stm.setNString(1, lesson.getName());
            stm.setNString(2, lesson.getDescription());
            stm.setString(3, lesson.getUrl());
            stm.setInt(4, lesson.getOrder());
            stm.setBoolean(5, lesson.getStatus());
            stm.setInt(6, lesson.getTopic().getId());
            stm.setString(7, lesson.getType());
            stm.setInt(8, lesson.getTopic().getSubject().getId());
            stm.setInt(9, lesson.getId());
            return stm.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        }
    }

    public Lesson findLessonById(int id) {

        try {
            String sql = "select * from Lesson where ID = ?";
            stm = connection.prepareStatement(sql);
            stm.setInt(1, id);
            rs = stm.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("ID"));
                lesson.setName(rs.getString("Name"));
                lesson.setUrl(rs.getString("URL"));
                lesson.setDescription(rs.getString("Description"));
                lesson.setOrder(rs.getInt("Order"));
                lesson.setStatus(rs.getBoolean("Status"));
                Topic topic = new Topic();
                topic.setId(rs.getInt("TopicID"));
                Subject subject = new Subject();
                subject.setId(rs.getInt("TopicSubjectID"));
                topic.setSubject(subject);
                lesson.setTopic(topic);
                lesson.setType(rs.getString("Type"));
                return lesson;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public List<Topic> findAllTopicBySid(int sid) {
        List<Topic> topics = new ArrayList<>();
        try {
            String sql = "select * from Topic where SubjectSID = ?";
            stm = connection.prepareStatement(sql);
            stm.setInt(1, sid);
            rs = stm.executeQuery();
            while (rs.next()) {
                Topic topic = new Topic();
                topic.setId(rs.getInt("ID"));
                topic.setName(rs.getString("Name"));
                Subject subject = new Subject();
                subject.setId(rs.getInt("SubjectSID"));
                topic.setSubject(subject);
                topics.add(topic);
            }
        } catch (SQLException e) {
        }
        return topics;
    }

    public List<Lesson> findLessonsByTopicID(int tid) {
        List<Lesson> lessons = new ArrayList<>();
        try {
            String sql = "SELECT TOP (1000) [ID]\n"
                    + "      ,[Name]\n"
                    + "      ,[URL]\n"
                    + "      ,[Description]\n"
                    + "      ,[Order]\n"
                    + "      ,[Status]\n"
                    + "      ,[TopicID]\n"
                    + "      ,[Type]\n"
                    + "      ,[TopicSubjectID]\n"
                    + "  FROM [QuizPracticingSystem].[dbo].[Lesson] \n"
                    + "  WHERE TopicID = ?";
            stm = connection.prepareStatement(sql);
            stm.setInt(1, tid);
            rs = stm.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("ID"));
                lesson.setName(rs.getString("Name"));
                lesson.setUrl(rs.getString("URL"));
                lesson.setDescription(rs.getString("Description"));
                lesson.setOrder(rs.getInt("Order"));
                lesson.setStatus(rs.getBoolean("Status"));
                Topic topic = new Topic();
                topic.setId(rs.getInt("TopicID"));
                Subject subject = new Subject();
                subject.setId(rs.getInt("TopicSubjectID"));
                topic.setSubject(subject);
                lesson.setTopic(topic);
                lesson.setType(rs.getString("Type"));
                lessons.add(lesson);
            }
        } catch (SQLException e) {
        }
        return lessons;
    }

    public int addLesson(Lesson lesson) {
        try {
            String sql = "insert into Lesson ([Name], [URL], [Description], [Order], [Status], [TopicID], [Type], TopicSubjectID) values (?, ?, ?, ?, ?, ?, ?, ?)";
            stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, lesson.getName());
            stm.setString(2, lesson.getUrl());
            stm.setString(3, lesson.getDescription());
            stm.setInt(4, lesson.getOrder());
            stm.setBoolean(5, lesson.getStatus());
            stm.setInt(6, lesson.getTopic().getId());
            stm.setString(7, lesson.getType());
            stm.setInt(8, lesson.getTopic().getSubject().getId());
            return stm.executeUpdate();
        } catch (SQLException e) {
            return 0;
        }
    }

    public int changeStatusLesson(int id, boolean status) {
        try {
            String sql = "update Lesson set Status = ? where ID = ?";
            stm = connection.prepareStatement(sql);
            stm.setBoolean(1, status);
            stm.setInt(2, id);
            return stm.executeUpdate();
        } catch (SQLException e) {
            return 0;
        }
    }
    public static void main(String[] args) {
        LessonDAO db = new LessonDAO();
        List<Lesson> m = db.findLessonsByTopicID(1);
        for (Lesson lesson : m) {
            System.out.println(lesson.getName());
        }
        
        
    }
}
