/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.PricePackage;
import entity.Register;
import entity.Subject;
import entity.User;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author xuant
 */
public class RegisterDAO extends DBContext {

    public static Register getRegister(ResultSet rs, User user, Subject subject, String dateStart,
            String dateEnd, PricePackage pricePackage, String salePrice, String priceRate) throws SQLException {
        Register r = new Register();
        r.setUser(user);
        r.setSubject(subject);
        r.setDateStart(rs.getTimestamp(dateStart));
        r.setDateEnd(rs.getTimestamp(dateEnd));
        r.setPricePackage(pricePackage);
        r.setSalePrice(rs.getDouble(salePrice));
        r.setPriceRate(rs.getInt(priceRate));
        return r;
    }

    public static Register getRegister(ResultSet rs, User user, Subject subject, String dateStart,
            String dateEnd, PricePackage pricePackage, String status) throws SQLException {
        Register r = new Register();
        r.setUser(user);
        r.setSubject(subject);
        r.setDateStart(rs.getTimestamp(dateStart));
        r.setDateEnd(rs.getTimestamp(dateEnd));
        r.setPricePackage(pricePackage);
        r.setStatus(rs.getBoolean(status));
        return r;
    }

    public static Register getRegister(Connection connection, int userId, int subjectId) {
        String sql = "SELECT [UserID]\n"
                + "      ,[SubjectID]\n"
                + "      ,[DateStart]\n"
                + "      ,[DateEnd]\n"
                + "      ,[PricePackageID]\n"
                + "      ,[SalePrice]\n"
                + "      ,[PriceRate]\n"
                + "  FROM [dbo].[Register]"
                + "  WHERE [UserID] = ? AND [SubjectID] = ?";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, userId);
            st.setInt(2, subjectId);
            ResultSet rs = st.executeQuery();
            rs.next();
            User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
            Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("SubjectID"));
            PricePackage pp = PricePackageDAO.getPricePackageByID(connection, rs.getInt("PricePackageID"));
            return getRegister(rs, u, s, "DateStart", "DateEnd", pp, "SalePrice", "PriceRate");
        } catch (SQLException e) {
        }
        return null;
    }

    public List<Register> getRegisterByUserID(Connection connection, int userId) {
        List<Register> list = new ArrayList<>();
        String sql = "SELECT [UserID]\n"
                + "      ,[SubjectID]\n"
                + "      ,[DateStart]\n"
                + "      ,[DateEnd]\n"
                + "      ,[PricePackageID]\n"
                + "  FROM [dbo].[Register]"
                + "  WHERE [UserID] = ?";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("SubjectID"));
                PricePackage pp = PricePackageDAO.getPricePackageByID(connection, rs.getInt("PricePackageID"));
                list.add(getRegister(rs, u, s, "DateStart", "DateEnd", pp, "SalePrice", "PriceRate"));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public void addRegister(Register r) {
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "INSERT INTO [dbo].[Register]\n"
                + "           ([UserID]\n"
                + "           ,[SubjectID]\n"
                + "           ,[DateStart]\n"
                + "           ,[DateEnd]\n"
                + "           ,[PricePackageID]\n"
                + "           ,[PriceRate]\n"
                + "           ,[SalePrice])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?,?)\n";
              

        try {
            st = connection.prepareStatement(sql);
            st.setInt(1, r.getUser().getId());
            st.setInt(2, r.getSubject().getId());
            st.setTimestamp(3, r.getDateStart());
            st.setTimestamp(4, r.getDateEnd());
            st.setInt(5, r.getPricePackage().getId());
            st.setDouble(6, r.getSalePrice());
            st.setInt(7, r.getPriceRate());
            st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RegisterDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (st != null) {
                    st.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }

    }

    public List<Register> getAll() {
        List<Register> lr = new ArrayList<>();
        String sql = "SELECT [UserID]\n"
                + "      ,[SubjectID]\n"
                + "      ,[DateStart]\n"
                + "      ,[DateEnd]\n"
                + "      ,[PricePackageID]\n"
                + "      ,[SalePrice]\n"
                + "      ,[PriceRate]\n"
                + "      ,IIF((GETDATE() <= DateEnd AND GETDATE() >= DateStart) OR DateEnd is null , '1', '0') AS [Status]\n"
                + "  FROM [dbo].[Register]";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("SubjectID"));
                PricePackage pp = PricePackageDAO.getPricePackageByID(connection, rs.getInt("PricePackageID"));
                lr.add(getRegister(rs, u, s, "DateStart", "DateEnd", pp, "SalePrice", "PriceRate"));
            }
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return lr;
    }

    public List<Register> getRegistrationsByUserId(int userId) {
        List<Register> lr = new ArrayList<>();
        String sql = "SELECT [UserID]\n"
                + "      ,[SubjectID]\n"
                + "      ,[DateStart]\n"
                + "      ,[DateEnd]\n"
                + "      ,[PricePackageID]\n"
                + "      ,[SalePrice]\n"
                + "      ,[PriceRate]\n"
                + "      ,IIF((GETDATE() <= DateEnd AND GETDATE() >= DateStart) OR DateEnd is null , '1', '0') AS [Status]\n"
                + "  FROM [dbo].[Register]"
                + "  WHERE [UserID] = ?";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("SubjectID"));
                PricePackage pp = PricePackageDAO.getPricePackageByID(connection, rs.getInt("PricePackageID"));
                lr.add(getRegister(rs, u, s, "DateStart", "DateEnd", pp, "SalePrice", "PriceRate"));
            }
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return lr;
    }

    public double getTotalRevenue(int categoryID) {
        String sql = "SELECT SUM(Register.SalePrice * PricePackage.Duration * Register.PriceRate / 100)\n"
                + "FROM Register JOIN Subject ON Register.SubjectID = Subject.ID\n"
                + "	JOIN PricePackage ON PricePackage.ID = Register.PricePackageID ";
        if (categoryID != 0) {
            sql += "WHERE CategoryID = " + categoryID;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getDouble(1);
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public double getTotalRevenueWithSinceTimeAgo(Timestamp time) {
        String sql = "SELECT SUM(Register.SalePrice * PricePackage.Duration * Register.PriceRate / 100)\n"
                + "FROM Register JOIN Subject ON Register.SubjectID = Subject.ID\n"
                + "	JOIN PricePackage ON PricePackage.ID = Register.PricePackageID \n"
                + "WHERE Register.DateStart > ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setTimestamp(1, time);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getDouble(1);
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public List<Register> getAllEnrollment(int categoryID) {
        List<Register> lr = new ArrayList<>();
        String sql = "SELECT Register.UserID, SubjectID, DateStart, DateEnd, PricePackageID, Register.SalePrice, Register.PriceRate\n"
                + "FROM Register JOIN [Subject] ON Register.SubjectID = [Subject].ID\n";
        if (categoryID != 0) {
            sql += "WHERE CategoryID = " + categoryID;
        }
        try {
            PreparedStatement st = connection.prepareCall(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("SubjectID"));
                PricePackage pp = PricePackageDAO.getPricePackageByID(connection, rs.getInt("PricePackageID"));
                lr.add(getRegister(rs, u, s, "DateStart", "DateEnd", pp, "SalePrice", "PriceRate"));
            }
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return lr;
    }

    public int getTotalEnrollment(String state) {
        String sql = "SELECT COUNT(*)\n"
                + "FROM Register JOIN Subject ON Register.SubjectID = Subject.ID\n";
        if (state.equalsIgnoreCase("new")) {
            sql += "WHERE [Subject].UpdateDate > ? ";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (state.equalsIgnoreCase("new")) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, -14);
                st.setTimestamp(1, new Timestamp(calendar.getTimeInMillis()));
            }
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalEnrollment(int categoryID) {
        String sql = "SELECT COUNT(*)\n"
                + "FROM Register JOIN Subject ON Register.SubjectID = Subject.ID\n";
        if (categoryID != 0) {
            sql += "WHERE CategoryID = " + categoryID;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalEnrollmentInMonth(int categoryID, Timestamp time) {
        String sql = "SELECT COUNT( *)\n"
                + "FROM Register JOIN Subject ON Register.SubjectID = Subject.ID\n"
                + "WHERE MONTH(Register.DateStart) = MONTH(?) AND YEAR(Register.DateStart) = YEAR(?) ";
        if (categoryID != 0) {
            sql += "AND CategoryID = " + categoryID;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setTimestamp(1, time);
            st.setTimestamp(2, time);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalEnrollmentOfCategory(int categoryID) {
        String sql = "SELECT COUNT( *)\n"
                + "FROM Register JOIN Subject ON Register.SubjectID = Subject.ID\n"
                + "WHERE Subject.CategoryID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, categoryID);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalCustomerWithState(String state) {
        String sql = "SELECT COUNT(DISTINCT Register.UserID)\n"
                + "FROM Register JOIN [Subject] on Register.SubjectID = Subject.ID\n";
        if (state.equalsIgnoreCase("new")) {
            sql += "WHERE [Subject].UpdateDate > ?\n";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (state.equalsIgnoreCase("new")) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, -14);
                st.setTimestamp(1, new Timestamp(calendar.getTimeInMillis()));
            }
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalCustomerOfSubject(int subjectID) {
        String sql = "SELECT COUNT(DISTINCT Register.UserID)\n"
                + "FROM Register JOIN Subject ON Register.SubjectID = Subject.ID\n"
                + "WHERE Subject.ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subjectID);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalRevenueInMonth(int categoryID, Timestamp time) {
        String sql = "SELECT SUM(Register.SalePrice * PricePackage.Duration * Register.PriceRate / 100)\n"
                + "FROM Register JOIN Subject ON Register.SubjectID = Subject.ID\n"
                + "	JOIN PricePackage ON PricePackage.ID = Register.PricePackageID\n"
                + "WHERE MONTH(Register.DateStart) = MONTH(?) AND YEAR(Register.DateStart) = YEAR(?) ";
        if (categoryID != 0) {
            sql += "AND CategoryID = " + categoryID;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setTimestamp(1, time);
            st.setTimestamp(2, time);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalRevenueInWeek(int categoryID, Timestamp sunday, Timestamp saturday) {
        String sql = "SELECT SUM(Register.SalePrice * PricePackage.Duration * Register.PriceRate / 100)\n"
                + "FROM Register JOIN Subject ON Register.SubjectID = Subject.ID\n"
                + "	JOIN PricePackage ON PricePackage.ID = Register.PricePackageID\n"
                + "WHERE Register.DateStart BETWEEN ? AND ? ";
        if (categoryID != 0) {
            sql += "AND CategoryID = " + categoryID;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setTimestamp(1, sunday);
            st.setTimestamp(2, saturday);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalRevenueInDay(int categoryID, Timestamp time) {
        String sql = "SELECT SUM(Register.SalePrice * PricePackage.Duration * Register.PriceRate / 100)\n"
                + "FROM Register JOIN Subject ON Register.SubjectID = Subject.ID\n"
                + "	JOIN PricePackage ON PricePackage.ID = Register.PricePackageID\n"
                + "WHERE Register.DateStart = ? ";
        if (categoryID != 0) {
            sql += "AND CategoryID = " + categoryID;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setTimestamp(1, time);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalEnrollmentWithSinceTimeAgo(Timestamp time) {
        String sql = "SELECT COUNT(*)\n"
                + "FROM Register\n"
                + "WHERE Register.DateStart > ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setTimestamp(1, time);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalEnrollmentOfCategoryWithSinceTimeAgo(int categoryID, Timestamp time) {
        String sql = "SELECT COUNT(*)\n"
                + "FROM Register JOIN Subject ON Register.SubjectID = Subject.ID\n"
                + "WHERE Subject.CategoryID = ? AND Register.DateStart > ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, categoryID);
            st.setTimestamp(2, time);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public static void main(String[] args) {
        RegisterDAO r = new RegisterDAO();
//        Timestamp time = new Timestamp(MyMethod.getCalendarWithSinceTimeAgo("6 months").getTimeInMillis());
//        double value = r.getTotalRevenueWithSinceTimeAgo(time);
        List<Register> v = r.getRegistrationsByUserId(4);
        System.out.println(v.size());

    }
//    public static void main(String[] args) {
//        RegisterDAO r = new RegisterDAO();
////        Timestamp time = new Timestamp(MyMethod.getCalendarWithSinceTimeAgo("6 months").getTimeInMillis());
////        double value = r.getTotalRevenueWithSinceTimeAgo(time);
//        int v = r.getTotalEnrollment(0);
//        System.out.println(v);
//    }
}
