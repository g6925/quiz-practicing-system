/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Category;
import entity.Dimension;
import entity.Subject;
import entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dclon
 */
public class SubjectDAO extends DBContext {

    public static Subject getSubject(ResultSet rs, String id, String thumbnail, String status, String title, String tagLine, String description, String featured, Category c, User taught, String listedPrice, String salePrice) throws SQLException {
        Subject s = new Subject();
        s.setId(rs.getInt(id));
        s.setThumbnail(rs.getString(thumbnail));
        s.setStatus(rs.getBoolean(status));
        s.setTitle(rs.getString(title));
        s.setTagline(rs.getString(tagLine));
        s.setDescription(rs.getString(description));
        s.setFeatured(rs.getBoolean(featured));
        s.setCategory(c);
        s.setTaught(taught);
        s.setListedPrice(rs.getDouble(listedPrice));
        s.setSalePrice(rs.getDouble(salePrice));
        return s;
    }

    public static Subject getSubjectByID(Connection connection, int id) {
        String sql = "WITH l AS ( SELECT COUNT(l.TopicSubjectID) AS slot,l.TopicSubjectID  \n"
                + "               FROM [Lesson] l inner join Subject s ON l.TopicSubjectID = s.ID\n"
                + "               GROUP BY l.TopicSubjectID)\n"
                + "SELECT ID, Thumbnail, Status, Title, TagLine, Description, CategoryID, Featured ,t.UserID as Taught,ListedPrice,SalePrice,l.slot as Lectures\n"
                + "FROM Subject s inner join Teach t on s.ID = t.SubjectID\n"
                + "INNER JOIN l on l.TopicSubjectID  =ID \n"
                + "WHERE ID = ?";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            rs.next();
            Category c = CategoryDAO.getCategoryByID(connection, rs.getInt("CategoryID"));
            User taught = UserDAO.getUserByID(connection, rs.getInt("Taught"));
            Subject s = getSubject(rs, "ID", "Thumbnail", "Status", "Title", "TagLine", "Description", "Featured", c, taught, "ListedPrice", "SalePrice");
            s.setNumbersOfLesson(rs.getInt("Lectures"));
            return s;
        } catch (SQLException e) {
        }
        return null;
    }

    public List<Subject> getAll() {
        List<Subject> ls = new ArrayList<>();
        String sql = "WITH l AS ( SELECT COUNT(l.TopicSubjectID) AS slot,l.TopicSubjectID  \n"
                + "               FROM [Lesson] l inner join Subject s ON l.TopicSubjectID = s.ID\n"
                + "               GROUP BY l.TopicSubjectID)\n"
                + "SELECT ID, Thumbnail, Status, Title, TagLine, Description, CategoryID, Featured,t.UserID as Taught,ListedPrice,SalePrice ,l.slot as Lectures\n"
                + "FROM Subject s inner join Teach t on s.ID = t.SubjectID \n"
                + "INNER JOIN l on l.TopicSubjectID  =ID \n";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = CategoryDAO.getCategoryByID(connection, rs.getInt("CategoryID"));
                User taught = UserDAO.getUserByID(connection, rs.getInt("Taught"));
                Subject s = getSubject(rs, "ID", "Thumbnail", "Status", "Title", "TagLine", "Description", "Featured", c, taught, "ListedPrice", "SalePrice");
                s.setNumbersOfLesson(rs.getInt("Lectures"));
                ls.add(s);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return ls;
    }

    public List<Subject> getAllWithCategoryID(int categoryID) {
        List<Subject> ls = new ArrayList<>();
        String sql = "WITH l AS ( SELECT COUNT(l.TopicSubjectID) AS slot,l.TopicSubjectID  \n"
                + "               FROM [Lesson] l inner join Subject s ON l.TopicSubjectID = s.ID\n"
                + "               GROUP BY l.TopicSubjectID)\n"
                + "SELECT ID, Thumbnail, Status, Title, TagLine, Description, CategoryID, Featured,t.UserID as Taught,ListedPrice,SalePrice ,l.slot as Lectures\n"
                + "FROM Subject s inner join Teach t on s.ID = t.SubjectID \n"
                + "INNER JOIN l on l.TopicSubjectID  =ID \n"
                + "WHERE CategoryID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, categoryID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = CategoryDAO.getCategoryByID(connection, rs.getInt("CategoryID"));
                User taught = UserDAO.getUserByID(connection, rs.getInt("Taught"));
                Subject s = getSubject(rs, "ID", "Thumbnail", "Status", "Title", "TagLine", "Description", "Featured", c, taught, "ListedPrice", "SalePrice");
                s.setNumbersOfLesson(rs.getInt("Lectures"));
                ls.add(s);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return ls;
    }

    public int addSubject(Subject subject) {
        PreparedStatement stm = null;
        PreparedStatement stm1 = null;
        ResultSet rs = null;
        String sql = "INSERT INTO Subject (Thumbnail, Status, Title, TagLine, Description, CategoryID,  Featured,ListedPrice,SalePrice,UpdateDate,UserID) \n"
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?);";
        int subjectId = 0;
        try {

            stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, subject.getThumbnail());
            stm.setBoolean(2, subject.getStatus());
            stm.setString(3, subject.getTitle());
            stm.setString(4, subject.getTagline());
            stm.setString(5, subject.getDescription());
            stm.setInt(6, subject.getCategory().getId());
            stm.setBoolean(7, subject.isFeatured());
            stm.setDouble(8, subject.getListedPrice());
            stm.setDouble(9, subject.getSalePrice());
            stm.setTimestamp(10, subject.getUpdateDate());
            stm.setInt(11, subject.getCreated().getId());
            stm.executeUpdate();
            rs = stm.getGeneratedKeys();
            if (rs.next()) {
                subjectId = rs.getInt(1);
            }

            String s = "INSERT INTO [Teach] ([UserID],[SubjectID]) VALUES (?,?)";
            stm1 = connection.prepareStatement(s);
            stm1.setInt(1, subject.getTaught().getId());
            stm1.setInt(2, subjectId);
            stm1.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (stm != null) {
                    stm.close();
                }

                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }
        return subjectId;
    }

    public void editSubject(Subject subject, int oldExpert) {
        PreparedStatement stm = null;
        PreparedStatement stm1 = null;
        ResultSet rs = null;
        String sql = "UPDATE [dbo].[Subject]\n"
                + "   SET [Thumbnail] = ?\n"
                + "      ,[Status] = ?\n"
                + "      ,[Title] = ? \n"
                + "      ,[TagLine] = ?\n"
                + "      ,[Description] = ?\n"
                + "      ,[CategoryID] = ?\n"
                + "      ,[Featured] = ?\n"
                + "      ,[UpdateDate] = ?\n"
                + "      ,[UserID] = ?\n"
                + "      ,[ListedPrice] = ?\n"
                + "      ,[SalePrice] = ?\n"
                + " WHERE ID = ?";

        try {
            stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, subject.getThumbnail());
            stm.setBoolean(2, subject.getStatus());
            stm.setString(3, subject.getTitle());
            stm.setString(4, subject.getTagline());
            stm.setString(5, subject.getDescription());
            stm.setInt(6, subject.getCategory().getId());
            stm.setBoolean(7, subject.isFeatured());
            stm.setTimestamp(8, subject.getUpdateDate());
            stm.setInt(9, subject.getCreated().getId());
            stm.setDouble(10, subject.getListedPrice());
            stm.setDouble(11, subject.getSalePrice());
            stm.setInt(12, subject.getId());
            stm.executeUpdate();
            rs = stm.getGeneratedKeys();

            String s = "UPDATE [dbo].[Teach] SET [UserID] = ? \n"
                    + "WHERE [UserID] = ? and [SubjectID] = ?";
            stm1 = connection.prepareStatement(s);
            stm1.setInt(1, subject.getTaught().getId());
            stm1.setInt(2, oldExpert);
            stm1.setInt(3, subject.getId());

            stm1.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (stm != null) {
                    stm.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }

    }

    public ArrayList<Subject> getSubjects(int pageindex, int pagesize, String keyWord, String categoryID, String featured, String status, String orderByDate) {
        ArrayList<Subject> subjects = new ArrayList<>();
        try {
            String sql = "with l as(\n"
                    + "SELECT count(l.TopicSubjectID)as slot,l.TopicSubjectID  FROM [Lesson] l inner join Subject s on l.TopicSubjectID = s.ID\n"
                    + "group by l.TopicSubjectID \n"
                    + ")\n"
                    + "SELECT* FROM  (SELECT s1.Description,s1.Thumbnail,s1.TagLine,s1.ID,s1.Featured,s1.Title,c.Name as Category,s1.Status,u.FullName as [Owner] ,l.slot as [Number of lessons],s1.ListedPrice,s1.SalePrice,s1.UpdateDate,ROW_NUMBER() OVER (ORDER BY s1.ID ASC) as row_index FROM Subject s1\n"
                    + "                         inner join Category c on c.ID = s1.CategoryID"
                    + "                         full outer join Teach t on t.SubjectID = s1.ID \n"
                    + "				inner join [User] u on u.ID = t.UserID\n"
                    + "				full outer join l on l.TopicSubjectID = s1.ID"
                    + " WHERE 1 = 1 ";
            if (keyWord != null) {
                sql += " AND s1.Title like ?";
            }
            if (categoryID != null) {
                sql += " AND CategoryID =  " + categoryID;
            }
            if (featured != null) {
                sql += " AND Featured = " + featured;
            }
            if (status != null) {
                sql += " AND s1.Status = " + status;
            }

            sql += " ) g";
            sql += " WHERE row_index >= (? -1)* ? +1 AND row_index <= ? * ?";
            if ("asc".equals(orderByDate)) {
                sql += " order by UpdateDate ASC";
            }
            if ("desc".equals(orderByDate)) {
                sql += " order by UpdateDate DESC";
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            if (keyWord != null) {
                stm.setString(1, "%" + keyWord + "%");
                stm.setInt(2, pageindex);
                stm.setInt(3, pagesize);
                stm.setInt(4, pageindex);
                stm.setInt(5, pagesize);
            } else {
                stm.setInt(1, pageindex);
                stm.setInt(2, pagesize);
                stm.setInt(3, pageindex);
                stm.setInt(4, pagesize);
            }
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Subject s = new Subject();
                s.setId(rs.getInt("ID"));
                s.setThumbnail(rs.getString("Thumbnail"));
                s.setStatus(rs.getBoolean("Status"));
                s.setTitle(rs.getString("Title"));
                s.setTagline(rs.getString("TagLine"));
                s.setDescription(rs.getString("Description"));
                Category c = new Category();
                c.setName(rs.getString("Category"));
                s.setCategory(c);
                s.setFeatured(rs.getBoolean("Featured"));
                User u = new User();
                u.setName(rs.getString("Owner"));
                s.setTaught(u);
                s.setListedPrice(rs.getDouble("ListedPrice"));
                s.setSalePrice(rs.getDouble("SalePrice"));
                s.setNumbersOfLesson(rs.getInt("Number of lessons"));
                subjects.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return subjects;
    }

    public int count(String keyWord, String categoryID, String featured) {
        try {
            String sql = "SELECT count(*) as Total FROM Subject WHERE 1=1 ";
            if ((keyWord) != null) {
                sql += " AND Title like ? ";
            }
            if ((categoryID) != null) {
                sql += " AND CategoryID =  " + categoryID;
            }
            if ((featured) != null) {
                sql += " AND Featured = " + featured;
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            if ((keyWord) != null) {
                stm.setString(1, "%" + keyWord + "%");
            }
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int getTotalSubjects(int categoryID) {
        String sql = "SELECT COUNT(*)\n"
                + "FROM [Subject] ";
        if (categoryID != 0) {
            sql += "WHERE CategoryID = " + categoryID;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalSubjectsInMonth(int categoryID, Timestamp time) {
        String sql = "SELECT COUNT(*)\n"
                + "FROM [Subject] "
                + "WHERE MONTH([UpdateDate]) = MONTH(?) AND YEAR([UpdateDate]) = YEAR(?) ";
        if (categoryID != 0) {
            sql += "AND CategoryID = " + categoryID;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setTimestamp(1, time);
            st.setTimestamp(2, time);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalSubjectsWithSinceTimeAgo(Timestamp time) {
        String sql = "SELECT COUNT(*)\n"
                + "FROM [Subject] \n"
                + "WHERE [Subject].UpdateDate > ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setTimestamp(1, time);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public List<Subject> getTopEnrollmentWithStateSubjects(int amount, String state) {
        List<Subject> ls = new ArrayList<>();
        String sql = "WITH t AS (\n"
                + "	SELECT [Subject].ID AS SubjectID, COUNT(DISTINCT Register.UserID) AS NumbersOfEnrollments\n"
                + "	FROM [Subject] LEFT JOIN Register ON [Subject].ID = Register.SubjectID\n"
                + "	GROUP BY [Subject].ID\n"
                + ")\n"
                + "SELECT TOP " + amount + " ID, Thumbnail, Status, Title, TagLine, Description, CategoryID, Featured ,[Teach].UserID AS Taught,ListedPrice,SalePrice, NumbersOfEnrollments\n"
                + "FROM [Subject] JOIN [t] ON [Subject].ID = [t].SubjectID\n"
                + "	JOIN [Teach] ON [Subject].ID = [Teach].SubjectID\n";
        if (state.equalsIgnoreCase("new")) {
            sql += "WHERE [Subject].UpdateDate > ? \n";
        }
        sql += "ORDER BY NumbersOfEnrollments DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (state.equalsIgnoreCase("new")) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, -14);
                st.setTimestamp(1, new Timestamp(calendar.getTimeInMillis()));
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = CategoryDAO.getCategoryByID(connection, rs.getInt("CategoryID"));
                User taught = UserDAO.getUserByID(connection, rs.getInt("Taught"));
                Subject s = getSubject(rs, "ID", "Thumbnail", "Status", "Title", "TagLine", "Description", "Featured", c, taught, "ListedPrice", "SalePrice");
                s.setNumbersOfEnrollments(rs.getInt("NumbersOfEnrollments"));
                ls.add(s);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return ls;
    }

    public List<Subject> getTopEnrollmentWithTimeAgoSubjects(int amount, Timestamp timestamp) {
        List<Subject> ls = new ArrayList<>();
        String sql = "WITH t AS (\n"
                + "	SELECT [Subject].ID AS SubjectID, COUNT(DISTINCT Register.UserID & Register.SubjectID) AS NumbersOfEnrollments\n"
                + "	FROM [Subject] LEFT JOIN Register ON [Subject].ID = Register.SubjectID\n"
                + "     WHERE Register.DateStart > ?"
                + "	GROUP BY [Subject].ID\n"
                + ")\n"
                + "SELECT TOP " + amount + " ID, Thumbnail, Status, Title, TagLine, Description, CategoryID, Featured ,[Teach].UserID AS Taught,ListedPrice,SalePrice, NumbersOfEnrollments\n"
                + "FROM [Subject] JOIN [t] ON [Subject].ID = [t].SubjectID\n"
                + "	JOIN [Teach] ON [Subject].ID = [Teach].SubjectID\n"
                + "ORDER BY NumbersOfEnrollments DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setTimestamp(1, timestamp);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = CategoryDAO.getCategoryByID(connection, rs.getInt("CategoryID"));
                User taught = UserDAO.getUserByID(connection, rs.getInt("Taught"));
                Subject s = getSubject(rs, "ID", "Thumbnail", "Status", "Title", "TagLine", "Description", "Featured", c, taught, "ListedPrice", "SalePrice");
                s.setNumbersOfEnrollments(rs.getInt("NumbersOfEnrollments"));
                ls.add(s);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return ls;
    }

    public static void main(String[] args) {
        SubjectDAO sd = new SubjectDAO();
        List<Subject> l = sd.getTopEnrollmentWithStateSubjects(6, "new");
        System.out.println(l.size());
    }
}
