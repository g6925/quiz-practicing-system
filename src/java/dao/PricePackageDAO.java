/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.PricePackage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author taina
 */
public class PricePackageDAO extends DBContext {

    public static PricePackage getPricePackage(ResultSet rs, String id, String name, String duration, String status, String priceRate, String description) throws SQLException {
        PricePackage pp = new PricePackage();
        pp.setId(rs.getInt(id));
        pp.setName(rs.getString(name));
        pp.setDuration(rs.getInt(duration));
        pp.setStatus(rs.getBoolean(status));
        pp.setPriceRate(rs.getInt(priceRate));
        pp.setDescription(rs.getString(description));
        return pp;
    }

    public static PricePackage getPricePackageByID(Connection connection, int id) {
        String sql = "select ID, [Name], Duration, [Status], PriceRate, Description from PricePackage where ID = ?";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            rs.next();
            return getPricePackage(rs, "ID", "Name", "Duration", "Status", "PriceRate", "Description");
        } catch (SQLException e) {
        }
        return null;
    }

    public List<PricePackage> getAll() {
        List<PricePackage> ls = new ArrayList<>();
        String sql = "select ID, [Name], Duration, [Status], PriceRate, Description from PricePackage";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ls.add(getPricePackage(rs, "ID", "Name", "Duration", "Status", "PriceRate", "Description"));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return ls;
    }

    public List<PricePackage> getAllBySubjectId(int subjectId) {
        List<PricePackage> ls = new ArrayList<>();
        String sql = "select ID, [Name], Duration, sp.[Status], PriceRate, Description from PricePackage pp\n"
                + "inner join Subject_PricePackage sp on sp.PricePackageID = pp.ID\n"
                + "where sp.SubjectID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subjectId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ls.add(getPricePackage(rs, "ID", "Name", "Duration", "Status", "PriceRate", "Description"));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return ls;
    }
    
    public List<PricePackage> getAllBySubjectId(int subjectId,int status) {
        List<PricePackage> ls = new ArrayList<>();
        String sql = "select ID, [Name], Duration, sp.[Status], PriceRate, Description from PricePackage pp\n"
                + "inner join Subject_PricePackage sp on sp.PricePackageID = pp.ID\n"
                + "where sp.SubjectID = ? and sp.[Status] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subjectId);
            st.setInt(2, status);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ls.add(getPricePackage(rs, "ID", "Name", "Duration", "Status", "PriceRate", "Description"));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return ls;
    }
    
    public void editPricePackage(PricePackage p) {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = "UPDATE PricePackage SET [Name] = ?, Duration = ?, [Status] = ?, PriceRate = ?, Description = ? WHERE ID = ?";
        try {
            stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, p.getName());
            stm.setInt(2, p.getDuration());
            stm.setBoolean(3, p.isStatus());
            stm.setInt(4, p.getPriceRate());
            stm.setString(5, p.getDescription());
            stm.setInt(6, p.getId());
            stm.executeUpdate();
            rs = stm.getGeneratedKeys();
        } catch (SQLException ex) {
            Logger.getLogger(PricePackageDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (stm != null) {
                    stm.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }
    }

    public int addPricePackage(PricePackage p) {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = "INSERT INTO PricePackage ([Name], Duration, [Status], PriceRate, Description) VALUES (?,?,?,?,?)";
        int ppId = 0;
        try {
            stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, p.getName());
            stm.setInt(2, p.getDuration());
            stm.setBoolean(3, p.isStatus());
            stm.setInt(4, p.getPriceRate());
            stm.setString(5, p.getDescription());
            stm.executeUpdate();
            rs = stm.getGeneratedKeys();
            if (rs.next()) {
                ppId = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PricePackageDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (stm != null) {
                    stm.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }
        return ppId;
    }

    public void statusPP(int stt, int id) {
        String sql = "UPDATE PricePackage SET [Status] = ? WHERE ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, stt);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public void changeStatusOfPackageSubject(int subjectId, int packageId, int status) {
        String sql = "UPDATE [Subject_PricePackage] SET [Status] = ? WHERE [SubjectID] = ? AND [PricePackageID] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            st.setInt(2, subjectId);
            st.setInt(3, packageId);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public int addPricePackageSubject(int pricePackageId, int subjectId) {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = "INSERT INTO [dbo].[Subject_PricePackage]\n"
                + "           ([SubjectID]\n"
                + "           ,[PricePackageID]) VALUES (?,?)";

        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, subjectId);
            stm.setInt(2, pricePackageId);

            return stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PricePackageDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (stm != null) {
                    stm.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }
        return -1;
    }

    public int deletePricePackageSubject(int pricePackageId, int subjectId) {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = "DELETE FROM [dbo].[Subject_PricePackage]\n"
                + "      WHERE SubjectID = ? AND PricePackageID = ?";

        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, subjectId);
            stm.setInt(2, pricePackageId);

            return stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PricePackageDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (stm != null) {
                    stm.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }
        return -1;
    }

    public int updatePricePackageSubject(int newPricePackageId, int subjectId, int oldPricePackageId) {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = "UPDATE [dbo].[Subject_PricePackage]\n"
                + "   SET [PricePackageID] = ?\n"
                + " WHERE SubjectID = ? AND PricePackageID = ?";

        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, newPricePackageId);
            stm.setInt(2, subjectId);
            stm.setInt(3, oldPricePackageId);

            return stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PricePackageDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (stm != null) {
                    stm.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }
        return -1;
    }
}
