/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Question;
import entity.Quiz;
import entity.QuizQuestion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jarnsaurus
 */
public class QuizQuestionDAO extends DBContext {

    public static QuizQuestion getQQ(ResultSet rs, String qz, String qt) throws SQLException {
        QuizQuestion q = new QuizQuestion();
        q.setQuizID(rs.getInt(qz));
        q.setQuestID(rs.getInt(qt));
        return q;
    }

    public void insert(int qt, int qz) {
        try {
            String sql = "INSERT INTO Quiz_Question ([QuizID], [QuestionID]) VALUES (?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, qz);
            statement.setInt(2, qt);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void main(String[] args) {
        QuizQuestionDAO db = new QuizQuestionDAO();
        db.insert(2, 38);
    }
    public List<Question> getAllQuestIdByQuizId(int qid) {
        List<QuizQuestion> q = new ArrayList<>();
        List<Question> list = new ArrayList<>();
        String sql = "SELECT [QuestionID], [QuizID]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Quiz_Question] where QuizID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, qid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                QuizQuestion qq = new QuizQuestion();
                qq.setQuizID(rs.getInt("QuizID"));
                qq.setQuestID(rs.getInt("QuestionID"));
                q.add(qq);
            }
            QuestionDAO qd = new QuestionDAO();
            for (QuizQuestion item : q) {
                Question quest = qd.getQById(item.getQuestID());
                list.add(quest);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    

    public static int countQuizQuestionByID(Connection connection, int ID) {
        String sql = "SELECT * FROM Quiz_Question WHERE QuizID=?";
        List<QuizQuestion> qq = new ArrayList<QuizQuestion>();
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                qq.add(getQQ(rs, "QuizID", "QuestionID"));
            }
            return qq.size();
        } catch (SQLException e) {
        }
        return -1;
    }

    public void deleteQQ(int id) {
        String sql = "delete from Quiz_Question where QuizID= ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
}
