/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Category;
import entity.Post;
import entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dclon
 */
public class PostDAO extends DBContext {

    public static Post getPost(ResultSet rs, String id, String thumbnail, String title,
            String brief, String date, String content, String status, Category category, User user) throws SQLException {
        Post p = new Post();
        p.setId(rs.getInt(id));
        p.setThumbnail(rs.getString(thumbnail));
        p.setTitle(rs.getString(title));
        p.setExcerpt(rs.getString(brief));
        p.setDate(rs.getTimestamp(date));
        p.setContent(rs.getString(content));
        p.setStatus(rs.getBoolean(status));
        p.setCategory(category);
        p.setUser(user);
        return p;
    }

    public static Post getPostByID(Connection connection, int ID) {
        String sql = "SELECT ID, Title, [Date], Content, CategoryID, UserID, Thumbnail, Brief, Status \n"
                + "  FROM Post\n"
                + "WHERE ID = ?";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            rs.next();
            Category c = CategoryDAO.getCategoryByID(connection, rs.getInt("CategoryID"));
            User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
            return getPost(rs, "ID", "Thumbnail", "Title", "Brief", "Date", "Content", "Status", c, u);
        } catch (SQLException e) {
        }
        return null;
    }
    
    public List<Post> getAll() {
        List<Post> lp = new ArrayList<>();
        String sql = "Select ID, Thumbnail, Title, Brief, Date, Content, CategoryID, UserID, Status\n"
                + "From Post";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = CategoryDAO.getCategoryByID(connection, rs.getInt("CategoryID"));
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                lp.add(getPost(rs, "ID", "Thumbnail", "Title", "Brief", "Date", "Content", "Status", c, u));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return lp;
    }

    public List<Post> getAllPostActive() {
        List<Post> lp = new ArrayList<>();
        String sql = "SELECT ID, Thumbnail, Title, Brief, Date, Content, CategoryID, UserID, Status\n"
                + "FROM Post \n"
                + "WHERE Status = 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = CategoryDAO.getCategoryByID(connection, rs.getInt("CategoryID"));
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                lp.add(getPost(rs, "ID", "Thumbnail", "Title", "Brief", "Date", "Content", "Status", c, u));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return lp;
    }
    
    

    public List<Post> getTopList(int size) {
        List<Post> lp = new ArrayList<>();
        String sql = "Select TOP " + String.valueOf(size) + " ID, Thumbnail, Title, Brief, Date, Content, CategoryID, UserID, Status\n"
                + "From Post\n"
                + "WHERE Status = 1\n"
                + "ORDER BY Date desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = CategoryDAO.getCategoryByID(connection, rs.getInt("CategoryID"));
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                lp.add(getPost(rs, "ID", "Thumbnail", "Title", "Brief", "Date", "Content", "Status", c, u));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return lp;
    }

    public List<Post> getAllWithCategoryID(int categoryID) {
        List<Post> lp = new ArrayList<>();
        String sql = "SELECT ID, Title, [Date], Content, CategoryID, UserID, Thumbnail, Brief, Status \n"
                + "  FROM Post\n"
                + "  WHERE CategoryID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, categoryID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = CategoryDAO.getCategoryByID(connection, rs.getInt("CategoryID"));
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                lp.add(getPost(rs, "ID", "Thumbnail", "Title", "Brief", "Date", "Content", "Status", c, u));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return lp;
    }

    //Phan trang cua Hiep start
    public List<Post> getListByPage(List<Post> list, int start, int end) {
        List<Post> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public List<Post> getListByContentSearch(String key, int categoryId) {
        List<Post> list = new ArrayList<>();
        String sql = "SELECT [ID]\n"
                + "      ,[Title]\n"
                + "      ,[Date]\n"
                + "      ,[Content]\n"
                + "      ,[CategoryID]\n"
                + "      ,[UserID]\n"
                + "      ,[Thumbnail]\n"
                + "      ,[Brief]\n"
                + "      ,[Status]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Post]\n"
                + "  where (Content like ? or Title like ?)";
        if (categoryId != 0) {
            sql += " and CategoryID = ?";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + key + "%");
            st.setString(2, "%" + key + "%");
            if (categoryId != 0) {
                st.setInt(3, categoryId);
            }

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Post p = new Post();
                Category c = CategoryDAO.getCategoryByID(connection, categoryId);
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                p.setId(rs.getInt("ID"));
                p.setTitle(rs.getString("Title"));
                p.setDate(rs.getTimestamp("Date"));
                p.setContent(rs.getString("Content"));
                p.setThumbnail(rs.getString("Thumbnail"));
                p.setBrief(rs.getString("Brief"));
                p.setStatus(rs.getBoolean("Status"));
                p.setCategory(c);
                p.setUser(u);
                list.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Post> getPosts(int pageindex, int pagesize, String keyWord, String categoryID) {
        List<Post> posts = new ArrayList<>();
        try {
            String sql = "SELECT* FROM  (SELECT ID, Title, [Date], Content, CategoryID, UserID, Thumbnail, Brief, Status, ROW_NUMBER() OVER (ORDER BY ID ASC) as row_index\n"
                    + "				FROM Post\n"
                    + "				WHERE 1 = 1";
            if (keyWord != null) {
                sql += " AND Title like ?";
            }
            if (categoryID != null) {
                sql += " AND CategoryID =  " + categoryID;
            }
            sql += " ) g";
            sql += " WHERE row_index >= (? -1)* ? +1 AND row_index <= ? * ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            if (keyWord != null) {
                stm.setString(1, "%" + keyWord + "%");
                stm.setInt(2, pageindex);
                stm.setInt(3, pagesize);
                stm.setInt(4, pageindex);
                stm.setInt(5, pagesize);
            } else {
                stm.setInt(1, pageindex);
                stm.setInt(2, pagesize);
                stm.setInt(3, pageindex);
                stm.setInt(4, pagesize);
            }
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Category c = CategoryDAO.getCategoryByID(connection, rs.getInt("CategoryID"));
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                posts.add(getPost(rs, "ID", "Thumbnail", "Title", "Brief", "Date", "Content", "Status", c, u));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return posts;
    }

    public int count(String keyWord, String categoryID) {
        try {
            String sql = "SELECT count(*) as Total FROM Post WHERE 1=1 ";
            if ((keyWord) != null) {
                sql += " AND Title like ? ";
            }
            if ((categoryID) != null) {
                sql += " AND CategoryID =  " + categoryID;
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            if ((keyWord) != null) {
                stm.setString(1, "%" + keyWord + "%");
            }
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public void editPost(Post p) {
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "UPDATE Post SET \n"
                + "  Title = ?, \n"
                + "  [Date] = ?, \n"
                + "  Content = ?, \n"
                + "  CategoryID = ?, \n"
                + "  UserID = ?, \n"
                + "  Thumbnail = ?, \n"
                + "  Brief = ?, \n"
                + "  Status = ? \n"
                + "WHERE\n"
                + "  ID = ?;";
        try {
            st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, p.getTitle());
            st.setTimestamp(2, p.getDate());
            st.setString(3, p.getContent());
            st.setInt(4, p.getCategory().getId());
            st.setInt(5, p.getUser().getId());
            st.setString(6, p.getThumbnail());
            st.setString(7, p.getBrief());
            st.setBoolean(8, p.isStatus());
            st.setInt(9, p.getId());
            st.executeUpdate();
            rs = st.getGeneratedKeys();

        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (st != null) {
                    st.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }
    }

    public int addPost(Post p) {
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "INSERT INTO Post\n"
                + "  (Title, \n"
                + "  [Date], \n"
                + "  Content, \n"
                + "  CategoryID, \n"
                + "  UserID, \n"
                + "  Thumbnail, \n"
                + "  Brief, \n"
                + "  Status) \n"
                + "VALUES \n"
                + "  (?, \n"
                + "  ?, \n"
                + "  ?, \n"
                + "  ?, \n"
                + "  ?, \n"
                + "  ?, \n"
                + "  ?, \n"
                + "  ?);";
        int pId = 0;
        try {
            st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, p.getTitle());
            st.setTimestamp(2, p.getDate());
            st.setString(3, p.getContent());
            st.setInt(4, p.getCategory().getId());
            st.setInt(5, p.getUser().getId());
            st.setString(6, p.getThumbnail());
            st.setString(7, p.getBrief());
            st.setBoolean(8, p.isStatus());
            st.executeUpdate();
            rs = st.getGeneratedKeys();
            if (rs.next()) {
                pId = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (st != null) {
                    st.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }
        return pId;
    }
}
