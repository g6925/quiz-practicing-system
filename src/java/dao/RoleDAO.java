/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Role;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dclon
 */
public class RoleDAO extends DBContext{
    
    public static Role getRole(ResultSet rs, String id, String name) throws SQLException {
        Role r = new Role();
        r.setId(rs.getInt(id));
        r.setName(rs.getString(name));
        return r;
    }

    public static Role getRoleByID(Connection connection, int id) {
        String sql = "SELECT ID, Name\n"
                + "FROM Role\n"
                + "WHERE ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return (getRole(rs, "ID", "Name"));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public List<Role> getAll() {
        List<Role> lr = new ArrayList<>();
        String sql = "SELECT ID, Name\n"
                + "FROM Role";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                lr.add(getRole(rs, "ID", "Name"));
            }
        } catch (SQLException e) {
        }
        return lr;
    }
}
