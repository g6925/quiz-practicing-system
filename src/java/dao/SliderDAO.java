/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Slider;
import entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author taina
 */
public class SliderDAO extends DBContext {

    public static Slider getSlider(ResultSet rs, String id, String title, String image, String content, String status, String backlink, User u) throws SQLException {
        Slider s = new Slider();
        s.setId(rs.getInt(id));
        s.setTitle(rs.getString(title));
        s.setImage(rs.getString(image));
        s.setContent(rs.getString(content));
        s.setStatus(rs.getBoolean(status));
        s.setBacklink(rs.getString(backlink));
        s.setUser(u);
        return s;
    }

    public static Slider getSliderByID(Connection connection, int id) {
        String sql = "Select ID, Title, Image, Content, [Status], Backlink, UserID from Slider where ID = ?";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            rs.next();
            User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
            return getSlider(rs, "ID", "Title", "Image", "Content", "Status", "Backlink", u);
        } catch (SQLException e) {
        }
        return null;
    }

    public List<Slider> getAll() {
        List<Slider> ls = new ArrayList<>();
        String sql = "Select ID, Title, Image, Content, [Status], Backlink, UserID from Slider";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                ls.add(getSlider(rs, "ID", "Title", "Image", "Content", "Status", "Backlink", u));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return ls;
    }

    public List<Slider> search(String keyWord, int s, int t) {
        List<Slider> list = new ArrayList<>();
        String sql = "Select ID, Title, Image, Content, [Status], Backlink, UserID from Slider where 1=1";
        if (t == 1) {
            if (keyWord != null || keyWord != "" || !keyWord.isEmpty()) {
                sql += " and Title like ?";
            }
        }
        if (t == 0) {
            if (keyWord != null || keyWord != "" || !keyWord.isEmpty()) {
                sql += " and Backlink like ?";
            }
        }
        if (t == 2) {
            if (keyWord != null || keyWord != "" || !keyWord.isEmpty()) {
                sql += "";
            }
        }
        if (s == 1) {
            sql += " and [Status] = 1 ";
        }
        if (s == 0) {
            sql += " and [Status] = 0 ";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (keyWord != null && t != 2) {
                st.setString(1, "%" + keyWord + "%");
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = UserDAO.getUserByID(connection, rs.getInt("UserID"));
                list.add(getSlider(rs, "ID", "Title", "Image", "Content", "Status", "Backlink", u));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void editSlider(Slider s) {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = "UPDATE Slider SET [Image] = ?, [Status] = ?, Title = ?, Content = ?, Backlink = ?, UserID = ? WHERE ID = ?";
        try {
            stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, s.getImage());
            stm.setBoolean(2, s.isStatus());
            stm.setString(3, s.getTitle());
            stm.setString(4, s.getContent());
            stm.setString(5, s.getBacklink());
            stm.setInt(6, s.getUser().getId());
            stm.setInt(7, s.getId());
            stm.executeUpdate();
            rs = stm.getGeneratedKeys();
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (stm != null) {
                    stm.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }
    }

    public int addSlider(Slider s) {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = "INSERT INTO Slider (Title, [Image], Content, [Status], Backlink, UserID) VALUES (?,?,?,?,?,?)";
        int sliderId = 0;
        try {
            stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, s.getTitle());
            stm.setString(2, s.getImage());
            stm.setString(3, s.getContent());
            stm.setBoolean(4, s.isStatus());
            stm.setString(5, s.getBacklink());
            stm.setInt(6, s.getUser().getId());
            stm.executeUpdate();
            rs = stm.getGeneratedKeys();
            if (rs.next()) {
                sliderId = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (stm != null) {
                    stm.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }
        return sliderId;
    }

    public List<Slider> getListByPage(List<Slider> list, int start, int end) {
        ArrayList<Slider> a = new ArrayList<>();
        for (int i = start; i < end; i++) {
            a.add(list.get(i));
        }
        return a;
    }

    public void statusSlider(int stt, int id) {
        String sql = "UPDATE Slider SET [Status] = ? WHERE ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, stt);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
}
