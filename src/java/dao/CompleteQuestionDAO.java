/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Question;
import entity.UserQuizQuestion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vietd
 */
public class CompleteQuestionDAO extends DBContext {

    public List<UserQuizQuestion> listAllQuizReview(int userId, int quizId) {
        List<UserQuizQuestion> list = new ArrayList<>();
        try {
            String sql = "select c.UserAnswer, c.Marked,\n"
                    + "q.* from User_Quiz_Question c\n"
                    + "join Question q\n"
                    + "on q.ID = c.QuestionID\n"
                    + "where c.User_QuizUserID = ? and c.User_QuizQuizID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userId);
            stm.setInt(2, quizId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                //create Question add to CompletedQuestion
                UserQuizQuestion userQuizQuestion = new UserQuizQuestion();
                userQuizQuestion.setMarked(rs.getBoolean("Marked"));
                userQuizQuestion.setUserAnswer(rs.getInt("UserAnswer"));
                userQuizQuestion.setUserQuiz(new QuizDAO().getUserQuiz(quizId, userId));
                list.add(userQuizQuestion);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CompleteQuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<UserQuizQuestion> getUserAnswerByQuestionId(int userId, int quesId, int quizId) {
        String sql = "select * from User_Quiz_Question u\n"
                + "where u.QuestionID = ? and u.User_QuizQuizID = ? and u.User_QuizUserID = ?";
        List<UserQuizQuestion> userQuizQuestions = new ArrayList<>();
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, quesId);
            stm.setInt(2, quizId);
            stm.setInt(3, userId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                UserQuizQuestion quizQuestion = new UserQuizQuestion();
                quizQuestion.setMarked(rs.getBoolean("Marked"));
                quizQuestion.setUserAnswer(rs.getInt("UserAnswer"));
                userQuizQuestions.add(quizQuestion);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return userQuizQuestions;
    }

    public static void main(String[] args) {
        System.out.println(new CompleteQuestionDAO().listAllQuizReview(3, 1));
    }
}
