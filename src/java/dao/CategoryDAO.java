/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Category;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dclon
 */
public class CategoryDAO extends DBContext {

    public static Category getCategory(ResultSet rs, String id, String name) throws SQLException {
        Category c = new Category();
        c.setId(rs.getInt(id));
        c.setName(rs.getString(name));
        return c;
    }

    public static Category getCategoryByID(Connection connection, int id) {
        String sql = "SELECT ID, Name\n"
                + "FROM Category\n"
                + "WHERE ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return (getCategory(rs, "ID", "Name"));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<Category> getAll() {
        List<Category> lc = new ArrayList<>();
        String sql = "SELECT ID, Name\n"
                + "FROM Category";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                lc.add(getCategory(rs, "ID", "Name"));
            }
        } catch (Exception e) {
        }
        return lc;
    }
    
    //cua hiep
    public int countCategory(int id) {
        int count = 0;
        ArrayList<Category> list = new ArrayList<>();
        String sql = "select COUNT(c.Name) as count\n"
                + "from Post p join Category c on p.CategoryID = c.ID\n"
                + "where c.ID = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                count = rs.getInt("count");
                return count;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return count;
    }
}
