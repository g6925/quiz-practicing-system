/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Dimension;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author xuant
 */
public class DimensonDAO extends DBContext {

    public List<Dimension> findAllDimension(String name, String type) {
        List<Dimension> dimensions = new ArrayList<>();
        try {
            String sql = "SELECT * FROM Dimension";
            if (name != null && !name.isEmpty()) {
                sql += sql.contains("WHERE") ? " AND " : " WHERE ";
                sql += " Name like '%" + name + "%'";
            }
            if (type != null && !type.isEmpty()) {
                sql += sql.contains("WHERE") ? " AND " : " WHERE ";
                sql += " Type like '%" + type + "%'";
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Dimension dimension = new Dimension();
                dimension.setId(rs.getInt("ID"));
                dimension.setName(rs.getString("Name"));
                dimension.setDescription(rs.getString("Description"));
                dimension.setType(rs.getString("Type"));
                dimensions.add(dimension);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DimensonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dimensions;
    }

    public List<Dimension> getDimensionBySubject(int subjectId) {
        List<Dimension> dimensions = new ArrayList<>();
        try {
            String sql = "SELECT D.ID,D.Name,D.Type\n"
                    + "  FROM [QuizPracticingSystem].[dbo].[Dimension] D\n"
                    + "  INNER JOIN Dimension_Subject DS ON D.ID = DS.DimensionID \n"
                    + "  INNER JOIN Subject S ON S.ID = DS.SubjectID\n"
                    + "  WHERE S.ID = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, subjectId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Dimension dimension = new Dimension();
                dimension.setId(rs.getInt("ID"));
                dimension.setName(rs.getString("Name"));
                dimension.setType(rs.getString("Type"));
                dimensions.add(dimension);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DimensonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dimensions;
    }

    public List<Dimension> getAllGroup() {
        List<Dimension> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM Dimension where Type='Group'";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Dimension dimensions = new Dimension();
                dimensions.setId(rs.getInt("ID"));
                dimensions.setName(rs.getString("Name"));
                dimensions.setDescription(rs.getString("Description"));
                dimensions.setType(rs.getString("Type"));
                list.add(dimensions);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DimensonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<Dimension> getAllDomain() {
        List<Dimension> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM Dimension where Type='Domain'";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Dimension dimensions = new Dimension();
                dimensions.setId(rs.getInt("ID"));
                dimensions.setName(rs.getString("Name"));
                dimensions.setDescription(rs.getString("Description"));
                dimensions.setType(rs.getString("Type"));
                list.add(dimensions);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DimensonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<Dimension> getAll() {
        List<Dimension> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM Dimension";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Dimension dimensions = new Dimension();
                dimensions.setId(rs.getInt("ID"));
                dimensions.setName(rs.getString("Name"));
                dimensions.setDescription(rs.getString("Description"));
                dimensions.setType(rs.getString("Type"));
                list.add(dimensions);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DimensonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public Dimension getDimension(int dimensionID) {
        try {
            String sql = "SELECT * FROM Dimension WHERE ID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, dimensionID);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Dimension dimension = new Dimension();
                dimension.setId(rs.getInt("ID"));
                dimension.setName(rs.getString("Name"));
                dimension.setDescription(rs.getString("Description"));
                dimension.setType(rs.getString("Type"));
                return dimension;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DimensonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int addDimension(Dimension dimension) {
        try {
            String sql = "INSERT INTO Dimension(Name,Description,Type) VALUES(?,?,?)";
            PreparedStatement stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, dimension.getName());
            stm.setString(2, dimension.getDescription());
            stm.setString(3, dimension.getType());
            return stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DimensonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int addDimensionSubject(int dimensionId, int subjectId) {

        try {
            String s = "INSERT INTO [dbo].[Dimension_Subject]\n"
                    + "           ([DimensionID]\n"
                    + "           ,[SubjectID])\n"
                    + "             VALUES\n"
                    + "             (?,?)";
            PreparedStatement stm1 = connection.prepareStatement(s);
            stm1.setInt(1, dimensionId);
            stm1.setInt(2, subjectId);
            return stm1.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DimensonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int updateDimension(Dimension dimension) {
        try {
            String sql = "UPDATE Dimension SET Name = ?,Description = ?,Type = ? WHERE ID = ?";
            PreparedStatement stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, dimension.getName());
            stm.setString(2, dimension.getDescription());
            stm.setString(3, dimension.getType());
            stm.setInt(4, dimension.getId());
            return stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DimensonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int deleteDimensionSubject(int dimensionId, int subjectId) {
        try {
            String sql = "DELETE FROM [dbo].[Dimension_Subject]\n"
                    + "      WHERE DimensionID = ? and SubjectID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, dimensionId);
            stm.setInt(2, subjectId);
            return stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DimensonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int editDimensionSubject(int oldDimensionId, int subjectId, int newDimensionId) {
        try {
            String sql = "UPDATE [dbo].[Dimension_Subject]\n"
                    + "   SET [DimensionID] = ?\n"
                    + " WHERE [SubjectID] = ? and [DimensionID] = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, newDimensionId);
            stm.setInt(2, subjectId);
            stm.setInt(3, oldDimensionId);
            return stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DimensonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public Dimension getDimensionByName(String name) {
        try {
            String sql = "SELECT [ID]\n"
                    + "      ,[Name]\n"
                    + "      ,[Type]\n"
                    + "      ,[Description]\n"
                    + "  FROM [QuizPracticingSystem].[dbo].[Dimension]\n"
                    + "  WHERE	Name = ?";
            PreparedStatement st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, name);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Dimension d = new Dimension();
                d.setType(rs.getString("Type"));
                d.setId(rs.getInt("ID"));
                d.setName(rs.getString("Name"));
                d.setDescription(rs.getString("Description"));
                return d;
            }
        } catch (Exception e) {
            System.out.println("getDimensionByType err : " + e);
        }
        return null;
    }
    
    public List<Dimension> getDimensionByQuestionID(int questionID) {
        List<Dimension> ldimension = new ArrayList<>();
        try {
            String sql = "SELECT q.Content,q.ID,q.LevelID,q.Status,q.TopicID,q.TopicSubjectID,d.ID as dID,d.Type,d.Name,d.Description\n"
                    + "  FROM [QuizPracticingSystem].[dbo].[Question] q\n"
                    + "  join Dimension_Subject_Question dsq on q.ID = dsq.QuestionID\n"
                    + "  join Dimension_Subject ds on ds.DimensionID = dsq.Dimension_SubjectDimensionID and ds.SubjectID = dsq.Dimension_SubjectSubjectID\n"
                    + "  join Dimension d on d.ID = ds.DimensionID\n"
                    + "  where q.ID = ?\n"
                    + "  order by q.ID";
            PreparedStatement st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            st.setInt(1, questionID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Dimension d = new Dimension();
                d.setType(rs.getString("Type"));
                d.setId(rs.getInt("dID"));
                d.setName(rs.getString("Name"));
                d.setDescription(rs.getString("Description"));
                ldimension.add(d);
            }
        } catch (Exception e) {
            System.out.println("getDimensionByQuestion err : " + e);
        }
        return ldimension;
    }
    
}
