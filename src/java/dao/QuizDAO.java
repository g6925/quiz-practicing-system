/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Quiz;
import entity.Subject;
import entity.UserQuiz;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jarnsaurus
 */
public class QuizDAO extends DBContext {

    public List<Quiz> getListByPage(List<Quiz> list, int start, int end) {
        List<Quiz> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public static Quiz getQuiz(ResultSet rs, String id, String Name, Subject s, String Level, String Duration, String Passrate, String Description, String Type) throws SQLException {
        Quiz q = new Quiz();
        q.setId(rs.getInt(id));
        q.setName(rs.getString(Name));
        q.setSubject(s);
        q.setLevel(rs.getString(Level));
        q.setDuration(rs.getTime(Duration));
        q.setPassRate(rs.getFloat(Passrate));
        q.setDescription(rs.getString(Description));
        q.setType(rs.getString(Type));
        return q;
    }

    public List<Quiz> getAll() {
        List<Quiz> list = new ArrayList<>();
        String sql = "SELECT ID, SubjectSID, Level, Duration, PassRate, Description, Type,Name\n"
                + "FROM Quiz";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("SubjectSID"));
                list.add(getQuiz(rs, "ID", "Name", s, "Level", "Duration", "PassRate",
                        "Description", "Type"));
            }
        } catch (SQLException e) {
            System.out.println("get all err: " + e);
        }
        return list;
    }

    public static Quiz getQuizByID(Connection connection, int ID) {
        String sql = "SELECT ID, Name, SubjectSID, Level, Duration, PassRate, Description, Type\n"
                + "FROM Quiz WHERE ID=?";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            rs.next();
            Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("SubjectSID"));
            return getQuiz(rs, "ID", "Name", s, "Level", "Duration", "PassRate",
                    "Description", "Type");
        } catch (SQLException e) {
        }
        return null;
    }

    public int addQuiz(Quiz q) {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = "INSERT INTO Quiz (Name, SubjectSID, Level, Duration, Passrate, Description, [Type]) VALUES (?,?,?,?,?,?,?)";
        int Id = 0;
        try {
            stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, q.getName());
            stm.setInt(2, q.getSubject().getId());
            stm.setString(3, q.getLevel());
            stm.setTime(4, q.getDuration());
            stm.setFloat(5, q.getPassRate());
            stm.setString(6, q.getDescription());
            stm.setString(7, q.getType());
            stm.executeUpdate();
            rs = stm.getGeneratedKeys();
            if (rs.next()) {
                Id = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QuizDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (stm != null) {
                    stm.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }
        return Id;
    }

    public int editQuiz(Quiz q) {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = "UPDATE Quiz SET [Name] = ?, [SubjectSID] = ?, Level = ?, Duration = ?, Passrate = ?, Description = ?, Type = ? WHERE ID = ?";
        int Id = 0;
        try {
            stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, q.getName());
            stm.setInt(2, q.getSubject().getId());
            stm.setString(3, q.getLevel());
            stm.setTime(4, q.getDuration());
            stm.setFloat(5, q.getPassRate());
            stm.setString(6, q.getDescription());
            stm.setString(7, q.getType());
            stm.setInt(8, q.getId());
            stm.executeUpdate();
            rs = stm.getGeneratedKeys();
            if (rs.next()) {
                Id = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Close Result Set Object
                if (rs != null) {
                    rs.close();
                }
                // Close Prepared Statement Object      
                if (stm != null) {
                    stm.close();
                }
                // Close Connection Object      
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqlException) {
            }
        }
        return Id;
    }

    public List<Quiz> filter(int subjectId, String type, String keyWord) {
        List<Quiz> list = new ArrayList<>();
        String sql = "SELECT TOP (1000) [ID]\n"
                + "      ,[SubjectSID]\n"
                + "      ,[Level]\n"
                + "      ,[Duration]\n"
                + "      ,[PassRate]\n"
                + "      ,[Description]\n"
                + "      ,[Type]\n"
                + "      ,[CreateBy]\n"
                + "      ,[Name]\n"
                + "  FROM [QuizPracticingSystem].[dbo].[Quiz]\n"
                + "  where 1=1 ";
        if (subjectId != 0) {
            sql += "AND SubjectSID = " + subjectId;
        }
        if (type != null) {
            sql += "AND type = '" + type + "'";
        }
        if (keyWord != null) {
            sql += "AND Name like '%" + keyWord + "%'";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("SubjectSID"));
                list.add(getQuiz(rs, "ID", "Name", s, "Level", "Duration", "PassRate",
                        "Description", "Type"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Quiz getQuizById(int quizId, int userId) {
        String sql = "select q.* from Quiz q\n"
                + "join User_Quiz uq\n"
                + "on q.ID = uq.QuizID\n"
                + "where q.ID = ? and uq.UserID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            st.setInt(2, userId);
            ResultSet rs = st.executeQuery();
            rs.next();
            Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("SubjectSID"));
            Quiz q = getQuiz(rs, "ID", "Name", s, "Level", "Duration", "PassRate",
                    "Description", "Type");
            return q;
        } catch (SQLException e) {
        }
        return null;
    }

    public Quiz getQuizByIdFirst(int quizId) {
        String sql = "select * from Quiz where ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            ResultSet rs = st.executeQuery();
            rs.next();
            Subject s = SubjectDAO.getSubjectByID(connection, rs.getInt("SubjectSID"));
            Quiz q = getQuiz(rs, "ID", "Name", s, "Level", "Duration", "PassRate",
                    "Description", "Type");
            return q;
        } catch (SQLException e) {
        }
        return null;
    }

    public UserQuiz getUserQuiz(int quizId, int userId) {
        String sql = "select * from User_Quiz where UserID = ? and QuizID = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userId);
            stm.setInt(2, quizId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Quiz quiz = getQuizById(quizId, userId);
                UserQuiz userQuiz = new UserQuiz();
                userQuiz.setUserId(userId);
                userQuiz.setQuiz(quiz);
                userQuiz.setStartedOn(rs.getTimestamp("StartedOn").toLocalDateTime());
                if(rs.getTimestamp("ExpireTime") != null){
                    userQuiz.setExpireTime(rs.getTimestamp("ExpireTime").toLocalDateTime());
                }
                userQuiz.setFilterType(rs.getString("filterType"));
                userQuiz.setFilterGroup(rs.getInt("filterGroup"));
                if (rs.getTime("TimeTaken") != null) {
                    userQuiz.setTimeTaken(rs.getTime("TimeTaken"));
                }
                if (rs.getTimestamp("CompletedOn") != null) {
                    userQuiz.setCompletedOn(rs.getTimestamp("CompletedOn").toLocalDateTime());
                }
                userQuiz.setGrade(rs.getFloat("Grade"));
                return userQuiz;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

}
