/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Role;
import entity.Subject;
import entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author taina
 */
public class UserDAO extends DBContext {

    public static User getUser(ResultSet rs, String id, String email, String passwork,
            String fullname, String gender, String phone, String image, String status, String introduce,
            Role role) throws SQLException {
        User u = new User();
        u.setId(rs.getInt(id));
        u.setEmail(rs.getString(email));
        u.setPass(rs.getString(passwork));
        u.setName(rs.getString(fullname));
        u.setGender(rs.getBoolean(gender));
        u.setPhone(rs.getString(phone));
        u.setImage(rs.getString(image));
        u.setStatus(rs.getBoolean(status));
        u.setIntroduce(rs.getString(introduce));
        u.setRole(role);
        return u;
    }

    public static User getUserByID(Connection connection, int ID) {
        String sql = "SELECT ID, Email, [PassWord], [FullName], Gender, Phone, [Image], [Status],Introduce, [RoleID]\n"
                + "FROM [User]\n"
                + "WHERE [User].ID = ?";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            rs.next();
            Role r = RoleDAO.getRoleByID(connection, rs.getInt("RoleID"));
            return getUser(rs, "ID", "Email", "PassWord", "FullName",
                    "Gender", "Phone", "Image", "Status", "Introduce", r);
        } catch (SQLException e) {
        }
        return null;
    }

    public int updateUserProfile(User user) {
        try {
            String sql = "update [user]\n"
                    + "set FullName = ?, \n"
                    + "Gender = ? ,\n"
                    + "Phone = ? ,\n"
                    + "[Image] = ?  \n"
                    + "where id = ?";
            PreparedStatement stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setNString(1, user.getName());
            stm.setBoolean(2, user.getGender());
            stm.setString(3, user.getPhone());
            stm.setString(4, user.getImage());
            stm.setInt(5, user.getId());
            return stm.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return -1;
    }

    public List<User> getAll() {
        List<User> list = new ArrayList<>();
        String sql = "SELECT ID, Email, [PassWord], [FullName], Gender, Phone, [Image], [Status],Introduce, [RoleID]\n"
                + "FROM [User]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Role role = RoleDAO.getRoleByID(connection, rs.getInt("RoleID"));
                list.add(getUser(rs, "ID", "Email", "PassWord", "FullName",
                        "Gender", "Phone", "Image", "Status", "Introduce", role));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public User getUserByEmailAndPass(String user, String pass) {
        String sql = "SELECT ID as UserID, Email, [PassWord], [FullName], "
                + "Gender, Phone, [Image], [Status],Introduce, [RoleID]\n"
                + "FROM [User]\n"
                + "WHERE Email = ? and PassWord = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Role role = RoleDAO.getRoleByID(connection, rs.getInt("RoleID"));
                return getUser(rs, "UserID", "Email", "PassWord", "FullName",
                        "Gender", "Phone", "Image", "Status", "Introduce", role);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public User getUserByEmail(String user) {
        String sql = "SELECT ID, Email, [PassWord], [FullName], "
                + "Gender, Phone, [Image], [Status],Introduce, [RoleID]\n"
                + "FROM [User]\n"
                + "WHERE Email = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Role role = RoleDAO.getRoleByID(connection, rs.getInt("RoleID"));
                return getUser(rs, "ID", "Email", "PassWord", "FullName",
                        "Gender", "Phone", "Image", "Status", "Introduce", role);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void updatePass(String user, String pass) {
        String sql = "UPDATE [User]\n"
                + "SET PassWord = ?\n"
                + "WHERE Email = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pass);
            st.setString(2, user);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public int insertUser(User u) {
        ResultSet rs = null;
        int userId = 0;
        try {
            String sql = "INSERT INTO [User] (Email, [PassWord], FullName, Gender, Phone, [Image], [Status], RoleID) \n"
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, u.getEmail());
            st.setString(2, u.getPass());
            st.setString(3, u.getName());
            st.setBoolean(4, u.getGender());
            st.setString(5, u.getPhone());
            st.setString(6, u.getImage());
            st.setBoolean(7, u.isStatus());
            st.setInt(8, u.getRole().getId());
            st.executeUpdate();
            rs = st.getGeneratedKeys();
            if (rs.next()) {
                userId = rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return userId;
    }

    public List<Subject> getSubjectRegister(int userId) {
        List<Subject> subjects = new ArrayList<>();
        try {
            String sql = "select s.Title, s.ID from Register r\n"
                    + "join [Subject] s\n"
                    + "on r.SubjectID = s.ID\n"
                    + "where UserID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setId(rs.getInt("ID"));
                s.setTitle(rs.getString("Title"));
                subjects.add(s);
            }
        } catch (Exception e) {
        }
        return subjects;
    }

    public int updateRoleAndStatus(int userId, int roleId, boolean status) {
        try {
            String sql = "Update [User] set  RoleID = ?,\n"
                    + "status = ? where ID = ?";
            PreparedStatement stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setInt(1, roleId);
            stm.setBoolean(2, status);
            stm.setInt(3, userId);
            return stm.executeUpdate();
        } catch (Exception e) {
        }
        return -1;

    }

    public User login(String user, String pass) {
        String sql = "SELECT ID, Email, [PassWord], [FullName], Gender, Phone, [Image], [Status],Introduce, [RoleID]\n"
                + "FROM [User] WHERE Email = ? AND PassWord = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Role role = RoleDAO.getRoleByID(connection, rs.getInt("RoleID"));
                return getUser(rs, "UserID", "Email", "PassWord", "FullName",
                        "Gender", "Phone", "Image", "Status", "Introduce", role);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void changePass(String user, String pass) {
        String sql = "UPDATE [User]\n"
                + "SET PassWord = ?\n"
                + "WHERE Email = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pass);
            st.setString(2, user);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public User getUserByEmailAndPassword(String user, String pass) {
        String sql = "SELECT ID, Email, [PassWord], [FullName], Gender, Phone, [Image], [Status],Introduce, [RoleID]\n"
                + "FROM [User] WHERE Email = ? AND PassWord = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Role role = RoleDAO.getRoleByID(connection, rs.getInt("RoleID"));
                return getUser(rs, "UserID", "Email", "PassWord", "FullName",
                        "Gender", "Phone", "Image", "Status", "Introduce", role);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public static void main(String[] args) {
        UserDAO us = new UserDAO();
        System.out.println(us.getUserByEmailAndPass("Admin@gmail.com", "Admin123").getName());
    }
}
