USE [master]
GO
/****** Object:  Database [QuizPracticingSystem]    Script Date: 20/07/2022 9:31:56 SA ******/
CREATE DATABASE [QuizPracticingSystem]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QuizPracticingSystem', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.TRINHNX151\MSSQL\DATA\QuizPracticingSystem.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'QuizPracticingSystem_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.TRINHNX151\MSSQL\DATA\QuizPracticingSystem_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [QuizPracticingSystem] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QuizPracticingSystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QuizPracticingSystem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET ARITHABORT OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [QuizPracticingSystem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QuizPracticingSystem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QuizPracticingSystem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET  ENABLE_BROKER 
GO
ALTER DATABASE [QuizPracticingSystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QuizPracticingSystem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QuizPracticingSystem] SET  MULTI_USER 
GO
ALTER DATABASE [QuizPracticingSystem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QuizPracticingSystem] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QuizPracticingSystem] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [QuizPracticingSystem] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [QuizPracticingSystem] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [QuizPracticingSystem] SET QUERY_STORE = OFF
GO
USE [QuizPracticingSystem]
GO
/****** Object:  Table [dbo].[Answers]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuestionID] [int] NOT NULL,
	[Content] [varchar](255) NOT NULL,
	[Correct] [bit] NOT NULL,
	[Explanation] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dimension]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dimension](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Type] [varchar](30) NOT NULL,
	[Description] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dimension_Subject]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dimension_Subject](
	[DimensionID] [int] NOT NULL,
	[SubjectID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DimensionID] ASC,
	[SubjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dimension_Subject_Question]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dimension_Subject_Question](
	[Dimension_SubjectDimensionID] [int] NOT NULL,
	[Dimension_SubjectSubjectID] [int] NOT NULL,
	[QuestionID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Dimension_SubjectDimensionID] ASC,
	[Dimension_SubjectSubjectID] ASC,
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lesson]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lesson](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[URL] [varchar](max) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[Order] [int] NOT NULL,
	[Status] [bit] NOT NULL,
	[TopicID] [int] NOT NULL,
	[Type] [varchar](20) NOT NULL,
	[TopicSubjectID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Level]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Level](
	[LevelID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[LevelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Post]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Post](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](max) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Content] [varchar](max) NOT NULL,
	[CategoryID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[Thumbnail] [varchar](max) NOT NULL,
	[Brief] [varchar](max) NOT NULL,
	[Status] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PricePackage]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PricePackage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Duration] [int] NOT NULL,
	[Status] [bit] NOT NULL,
	[PriceRate] [int] NOT NULL,
	[Description] [varchar](max) NULL,
 CONSTRAINT [PK__PricePac__3214EC27C850ABB4] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Question]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Content] [varchar](512) NOT NULL,
	[TopicID] [int] NOT NULL,
	[TopicSubjectID] [int] NOT NULL,
	[LevelID] [int] NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Quiz]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quiz](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SubjectSID] [int] NOT NULL,
	[Level] [varchar](30) NOT NULL,
	[Duration] [time](7) NULL,
	[PassRate] [int] NULL,
	[Description] [varchar](max) NULL,
	[Type] [varchar](30) NOT NULL,
	[CreateBy] [int] NULL,
	[Name] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Quiz_Question]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quiz_Question](
	[QuizID] [int] NOT NULL,
	[QuestionID] [int] NOT NULL,
	[Explanation] [varchar](max) NULL,
 CONSTRAINT [PK_Quiz_Question] PRIMARY KEY CLUSTERED 
(
	[QuizID] ASC,
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Register]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Register](
	[UserID] [int] NOT NULL,
	[SubjectID] [int] NOT NULL,
	[DateStart] [datetime] NOT NULL,
	[DateEnd] [datetime] NULL,
	[PricePackageID] [int] NULL,
	[PriceRate] [int] NULL,
	[SalePrice] [money] NULL,
 CONSTRAINT [PK__Register__8D497694D31A3299] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[SubjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Slider]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slider](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[Image] [varchar](max) NOT NULL,
	[Content] [varchar](255) NOT NULL,
	[UserID] [int] NOT NULL,
	[Backlink] [varchar](max) NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subject]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subject](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Thumbnail] [varchar](max) NOT NULL,
	[Status] [bit] NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[TagLine] [varchar](255) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[CategoryID] [int] NOT NULL,
	[Featured] [bit] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UserID] [int] NULL,
	[ListedPrice] [money] NULL,
	[SalePrice] [money] NULL,
 CONSTRAINT [PK__Subject__3214EC27D14BB702] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subject_PricePackage]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subject_PricePackage](
	[SubjectID] [int] NOT NULL,
	[PricePackageID] [int] NOT NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[SubjectID] ASC,
	[PricePackageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Teach]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teach](
	[UserID] [int] NOT NULL,
	[SubjectID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[SubjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Topic]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Topic](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SubjectSID] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[SubjectSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](64) NOT NULL,
	[PassWord] [varchar](32) NOT NULL,
	[FullName] [varchar](50) NOT NULL,
	[Gender] [bit] NOT NULL,
	[Phone] [varchar](16) NOT NULL,
	[Image] [varchar](max) NULL,
	[RoleID] [int] NOT NULL,
	[Status] [bit] NOT NULL,
	[Introduce] [varchar](max) NULL,
 CONSTRAINT [PK__User__3214EC27CFFF6473] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User_Quiz]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Quiz](
	[UserID] [int] NOT NULL,
	[QuizID] [int] NOT NULL,
	[TimeTaken] [time](7) NULL,
	[StartedOn] [datetime] NOT NULL,
	[CompletedOn] [datetime] NULL,
	[Grade] [real] NULL,
	[filterType] [nvarchar](50) NULL,
	[filterGroup] [int] NULL,
	[ExpireTime] [datetime] NULL,
 CONSTRAINT [PK__User_Qui__EF3CE64AA769BA70] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[QuizID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User_Quiz_Question]    Script Date: 20/07/2022 9:31:56 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Quiz_Question](
	[User_QuizUserID] [int] NOT NULL,
	[User_QuizQuizID] [int] NOT NULL,
	[QuestionID] [int] NOT NULL,
	[UserAnswer] [int] NOT NULL,
	[Marked] [bit] NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Answers] ON 

INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (1, 1, N'LowerCase letters', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (2, 1, N'UpperCase letters', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (3, 1, N'CamelCase letters', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (4, 1, N'Record 5 on Event 2', 0, N'')
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (5, 2, N'signed short', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (6, 2, N'unsigned short', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (7, 2, N'long', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (8, 2, N'int', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (9, 3, N'Basic datatype of C', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (10, 3, N'Qualifier', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (11, 3, N'short is the qualifier and int is the basic datatype', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (12, 3, N'All of the mentioned', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (13, 4, N'char > int > float', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (14, 4, N'int > char > float', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (15, 4, N'char < int < double', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (16, 4, N'double > char > int', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (17, 5, N'structure', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (18, 5, N'long', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (19, 5, N'string', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (20, 5, N'float', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (21, 6, N'exp1', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (22, 6, N'exp2', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (23, 6, N'exp3', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (24, 6, N'All of the mentioned', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (25, 7, N'Ranked', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (26, 7, N'Redundant', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (27, 7, N'Consistent', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (28, 7, N'Verifiable', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (29, 8, N'Stable conditions', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (30, 8, N'Distributed and conflicting knowledge sources', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (31, 8, N'Difficult access to sources', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (32, 8, N'Tacit knowledge and hidden needs', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (33, 9, N'Stakeholders', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (34, 9, N'The operational environment', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (35, 9, N'Application domain specialists', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (36, 9, N'The organizational environment', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (37, 10, N'A document listing the time it takes to execute the existing manual processes', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (38, 10, N'A document detailing software requirements and specifications', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (39, 10, N'A document which features instructions for how to install new software and test it for errors', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (40, 10, N'A document used while testing the software code for validity', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (41, 11, N'Background study', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (42, 11, N'Data Collection', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (43, 11, N'Group sessions', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (44, 11, N'Questionnaires', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (45, 12, N'Verifiability', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (46, 12, N'Modifiability', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (47, 12, N'Efficiency', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (48, 12, N'Availability', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (49, 13, N'A reference', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (50, 13, N'A class', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (51, 13, N'An interface', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (52, 13, N'A variable of primitive type', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (53, 14, N'Transient methods may not be overridden', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (54, 14, N'Transient methods must be overridden.', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (55, 14, N'Transient classes may not be serialized.', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (56, 14, N'Transient variables must be static', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (57, 14, N'Transient variables are not serialized.', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (58, 15, N'True', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (59, 15, N'False', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (60, 16, N'Pipeline', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (61, 16, N'Superscalar', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (62, 16, N'Multicore', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (63, 16, N'None of the other choices', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (64, 17, N'PSW', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (65, 17, N'PSW & PC', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (66, 17, N'PSW and Contents of processor registers', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (67, 17, N'None of the other choices', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (68, 18, N'A part of the program where the shared memory is accessed', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (69, 18, N'A part of shared data', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (70, 18, N'A part of shared memory', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (71, 18, N'None of the other choices', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (72, 19, N'Disable Interrupts', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (73, 19, N'Monitors', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (74, 19, N'Semaphore', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (75, 19, N'None of the other choices', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (76, 20, N'Interactive', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (77, 20, N'Real time', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (78, 20, N'Batch', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (79, 20, N'Multiuser', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (80, 21, N'Memory demand', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (81, 21, N'Virtual demand', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (82, 21, N'Virtual paging', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (83, 21, N'Virtual memory', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (84, 22, N'1 MB', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (85, 22, N'2 MB', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (86, 22, N'4 MB', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (87, 22, N'8 MB', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (88, 23, N'There are several linear address spaces', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (89, 23, N'The total address space can be more than the size of physical memory', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (90, 23, N'Sharing of procedures between different users can be facilitated', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (91, 23, N'None of the other choices', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (92, 24, N'The length of the exam', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (93, 24, N'How to schedule your exam', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (94, 24, N'Time and date of your exam', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (95, 24, N'Practice questions', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (96, 25, N'Instructions on how to schedule your exam', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (97, 25, N'Domains and objectives covered in the exam', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (98, 25, N'Expectations of personal conduct such as cheating', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (99, 25, N'List of subject matter experts that contributed to the job task analysis', 0, NULL)
GO
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (100, 26, N'A dangerous machine intelligence put in a digital prison', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (101, 26, N'The challenge of understanding the inner workings of opaque systems', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (102, 26, N'Not being able to know how something crashed or failed', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (103, 26, N'Machine intelligence making something illusory, like pulling a rabbit from a hat', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (104, 27, N'Upon delivery, with appropriate warranties where necessary', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (105, 27, N'During periodic reviews, with ongoing customer feedback solicited', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (106, 27, N'Once an ethical issue has received negative feedback in public media', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (107, 27, N'From its inception, through maintenance, to applying foresight regarding its decommissioning.', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (108, 28, N'Superintelligence', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (109, 28, N'Perceptrons', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (110, 28, N'Narrow AI', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (111, 28, N'Strong AI', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (112, 29, N'Taking ownership of an assigned task', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (113, 29, N'The legal responsibility for one actions', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (114, 29, N'The moral duty one has to take action.', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (115, 29, N'Answering for ones actions to an authority figure', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (116, 30, N'Service-level agreement (SLA)', 1, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (117, 30, N'Software as a Service (SaaS)', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (118, 30, N'End-user license agreement (EULA)', 0, NULL)
INSERT [dbo].[Answers] ([ID], [QuestionID], [Content], [Correct], [Explanation]) VALUES (119, 30, N'Terms of Service (ToS)', 0, NULL)
SET IDENTITY_INSERT [dbo].[Answers] OFF
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([ID], [Name]) VALUES (1, N'Front-end')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (2, N'Back-end')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (3, N'Boostrap')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (4, N'Framework')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (5, N'Marketing')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (6, N'.NET')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (7, N'Java')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (8, N'Development')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (9, N'OOP')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (10, N'Operating system')
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Dimension] ON 

INSERT [dbo].[Dimension] ([ID], [Name], [Type], [Description]) VALUES (1, N'Business', N'Domain', N'Khong biet description nhu the nao')
INSERT [dbo].[Dimension] ([ID], [Name], [Type], [Description]) VALUES (2, N'Process', N'Domain', N'Khong biet description nhu the nao')
INSERT [dbo].[Dimension] ([ID], [Name], [Type], [Description]) VALUES (3, N'People', N'Domain', N'Khong biet description nhu the nao')
INSERT [dbo].[Dimension] ([ID], [Name], [Type], [Description]) VALUES (4, N'Initiating', N'Group', N'Khong biet description nhu the nao')
INSERT [dbo].[Dimension] ([ID], [Name], [Type], [Description]) VALUES (5, N'Planning', N'Group', N'Khong biet description nhu the nao')
INSERT [dbo].[Dimension] ([ID], [Name], [Type], [Description]) VALUES (6, N'Executing', N'Group', N'Khong biet description nhu the nao')
SET IDENTITY_INSERT [dbo].[Dimension] OFF
GO
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 1)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 2)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 3)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 4)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 5)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 6)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 7)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 8)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 9)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 10)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 11)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 12)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 13)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 14)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 15)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (1, 18)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 1)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 2)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 3)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 4)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 5)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 6)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 7)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 8)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 9)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 10)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 11)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 12)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 13)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 14)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 15)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (2, 18)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 1)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 2)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 3)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 4)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 5)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 6)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 7)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 8)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 9)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 10)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 11)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 12)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 13)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 14)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (3, 15)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 1)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 2)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 3)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 4)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 5)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 6)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 7)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 8)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 9)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 10)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 11)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 12)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 13)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 14)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (4, 15)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 1)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 2)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 3)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 4)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 5)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 6)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 7)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 8)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 9)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 10)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 11)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 12)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 13)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 14)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (5, 15)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 1)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 2)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 3)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 4)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 5)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 6)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 7)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 8)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 9)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 10)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 11)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 12)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 13)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 14)
INSERT [dbo].[Dimension_Subject] ([DimensionID], [SubjectID]) VALUES (6, 15)
GO
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (1, 1, 1)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (1, 1, 2)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (1, 1, 3)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (2, 1, 4)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (2, 1, 5)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (2, 1, 6)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (3, 1, 7)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (3, 1, 8)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (3, 1, 9)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (4, 1, 1)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (4, 1, 4)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (4, 1, 7)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (5, 1, 2)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (5, 1, 5)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (5, 1, 8)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (6, 1, 3)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (6, 1, 6)
INSERT [dbo].[Dimension_Subject_Question] ([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID], [QuestionID]) VALUES (6, 1, 9)
GO
SET IDENTITY_INSERT [dbo].[Lesson] ON 

INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (71, N'Course Structure and Projects', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 1, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (72, N'WACC and Capital Structure: What You Need to Know
', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 2, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (73, N'Adapting the Organization to Handle Continuous Change
', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 3, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (74, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 4, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (75, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 5, N'Lesson', 2)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (76, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 6, N'Lesson', 2)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (77, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 7, N'Lesson', 3)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (78, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 8, N'Lesson', 3)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (79, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 9, N'Lesson', 4)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (80, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 10, N'Lesson', 5)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (81, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 11, N'Lesson', 6)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (82, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 12, N'Lesson', 7)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (83, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 13, N'Lesson', 7)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (84, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 14, N'Lesson', 8)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (85, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 15, N'Lesson', 8)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (86, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 16, N'Lesson', 8)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (87, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 17, N'Lesson', 8)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (88, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 18, N'Lesson', 9)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (89, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 19, N'Lesson', 9)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (90, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 20, N'Lesson', 9)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (91, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 21, N'Lesson', 10)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (92, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 22, N'Lesson', 10)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (93, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 23, N'Lesson', 10)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (94, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 24, N'Lesson', 10)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (95, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 25, N'Lesson', 10)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (96, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 26, N'Lesson', 11)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (97, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 27, N'Lesson', 11)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (98, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 28, N'Lesson', 11)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (99, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 29, N'Lesson', 11)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (100, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 30, N'Lesson', 12)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (101, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 31, N'Lesson', 12)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (102, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 32, N'Lesson', 12)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (103, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 33, N'Lesson', 12)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (104, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 34, N'Lesson', 13)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (105, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 35, N'Lesson', 13)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (106, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 36, N'Lesson', 13)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (107, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 37, N'Lesson', 13)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (108, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 38, N'Lesson', 14)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (109, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 39, N'Lesson', 15)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (110, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 40, N'Lesson', 15)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (111, N'Welcome to Introduction to HTML5', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 41, N'Lesson', 15)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (115, N'A High-Level Overview of Web Development
', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 1, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (117, N'Setting Up Our Code Editor
', N'https://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Descripton', 2, 1, 1, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (121, N'Your Very First Webpage!
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 1, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (122, N'Downloading Course Material
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 1, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (123, N'Watch Before You Start!
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 1, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (124, N'Section Intro
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 2, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (125, N'Introduction to HTML', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 2, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (128, N'HTML Document Structure', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 2, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (129, N'Text Elements
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 2, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (132, N'Section Intro
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 3, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (134, N'Inline, Internal and External CSS
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 3, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (135, N'Combining Selectors
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 3, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (136, N'Class and ID Selectors
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 3, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (138, N'Pseudo-classes
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 3, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (139, N'The 3 Ways of Building Layouts
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 4, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (140, N'Using Floats
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 4, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (141, N'Clearing Floats
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 4, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (142, N'Building a Simple Float Layout
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 4, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (143, N'Overview of Web Design and Website Personalities
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 43, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (144, N'Implementing Typography
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 43, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (146, N'Web Design Rules #2: Colors
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 43, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (148, N'Web Design Rules #3: Images and Illustrations
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 43, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (150, N'Web Design Rules #10 - Part 1: Elements and Components
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 45, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (151, N'Building an Accordion Component - Part 1
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 45, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (153, N'Building an Accordion Component - Part 2
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 45, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (154, N'CHALLENGE #1: Building a Pagination Component
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 45, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (155, N'Web Design Rules #10 - Part 2: Layout Patterns
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 45, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (156, N'Where to Go from Here
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 46, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (157, N'My Other Courses + Updates
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 46, N'Lesson', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (158, N'xuantrinh151', N'', N'<p>gdsfsfs</p>', 44, 1, 1, N'Quiz', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (159, N'test', N'', N'', 432, 1, 1, N'Subject Topic', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (160, N'test', N'', N'', 3, 1, 1, N'Subject Topic', 1)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (167, N'My Other Courses + Updates
', N'dsahttps://www.coursera.org/lecture/html/01-01-welcome-to-introduction-to-html5-Ilm91', N'Description', 1, 1, 48, N'Lesson', 18)
INSERT [dbo].[Lesson] ([ID], [Name], [URL], [Description], [Order], [Status], [TopicID], [Type], [TopicSubjectID]) VALUES (168, N'abc', N'', N'', 1, 1, 48, N'Subject Topic', 18)
SET IDENTITY_INSERT [dbo].[Lesson] OFF
GO
SET IDENTITY_INSERT [dbo].[Level] ON 

INSERT [dbo].[Level] ([LevelID], [Name]) VALUES (1, N'Easy')
INSERT [dbo].[Level] ([LevelID], [Name]) VALUES (2, N'Medium')
INSERT [dbo].[Level] ([LevelID], [Name]) VALUES (3, N'Hard')
SET IDENTITY_INSERT [dbo].[Level] OFF
GO
SET IDENTITY_INSERT [dbo].[Post] ON 

INSERT [dbo].[Post] ([ID], [Title], [Date], [Content], [CategoryID], [UserID], [Thumbnail], [Brief], [Status]) VALUES (1, N'12 Useful CSS Tricks for Front-end Developers (Part 1)', CAST(N'2022-06-14T15:16:19.347' AS DateTime), N'<p><strong>Table of Contents</strong></p><p>CSS is in quite a good state right now. The new features being introduced are helping to solidify CSS as a true scripting language. We know that <a href="https://www.w3.org/TR/css-conditional-5/">a proposal draft</a> has been made to introduce @when &amp; @else statements. Although not available right now, it does set a precedent for future potential to write conditional logic using CSS.</p><p>Michelle Barker wrote an article for <a href="https://www.smashingmagazine.com/2022/03/new-css-features-2022/">Smashing Magazine</a> discussing the upcoming CSS features. Check it out if you hadn’t had the time to catch up yet!</p><p>In my experience, it’s quite easy to overlook existing features unless you constantly check for updates. Properties like is() and where() but also attr() have been around for a while, yet are easily overshadowed by <a href="https://stackdiary.com/front-end-frameworks/">the potential of modern frameworks</a>.</p><h2>Hacking WordPress with CSS</h2><p>My inspiration for this article comes directly from my experience of working with WordPress on a daily basis. I have been using WordPress for more than 10 years. And during that time, I must have written 10,000+ lines of CSS to customize various theme designs.</p><p>But, more specifically, I use CSS to overcome the need for plugins. The way WordPress works is that you need to use a plugin for almost everything. Unless you know a bit of CSS, of course. Want to show a tooltip? Get a plugin. Want to add an icon to a button? Get a plugin.</p><p>You get the idea.</p><h2>How to use these CSS tricks</h2><p>The only requirement is that you know a bit of CSS &amp; HTML. I have provided sample templates that you can import directly into your projects.</p><p>You can use this template and save it as index.html:</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>&lt;</strong>html<strong>&gt;</strong></p><p><strong>&lt;</strong>head<strong>&gt;</strong></p><p><strong>&lt;</strong>title<strong>&gt;</strong>CSS Tricks &amp; Tips<strong>title&gt;</strong></p><p><strong>&lt;meta charset="UTF-8" /&gt;</strong></p><p><strong>&lt;style&gt;</strong></p><p>&nbsp;</p><p><strong>style&gt;</strong></p><p><strong>head&gt;</strong></p><p><strong>&nbsp;</strong></p><p><strong>&lt;body&gt;</strong></p><p>&nbsp;</p><p><strong>body&gt;</strong></p><p><strong>&nbsp;</strong></p><p><strong>html&gt;</strong></p><h2><strong>Typing effect for text</strong></h2><p><strong><img src="https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/03/Typing-effect-for-text.gif" alt="Typing effect for text"></strong></p><p><strong>Web designs are getting more creative by the minute. And with the help of CSS animation features, you can make your web pages feel alive. In this example, we are using the animation and @keyframes properties to achieve a typewriter effect.</strong></p><p><strong>Specifically, for this demo, we implement the steps() property to segment our text animation. First, you have to specify the number of steps() which in our case is the character length for the text we wish to animate.</strong></p><p><strong>And, second, we use @keyframes to declare when the animation is going to start. For example, if you wrote another word after </strong><i><strong>“Typing effect for text”</strong></i><strong> the animation wouldn’t work unless you change the number of steps() in the CSS snippet.</strong></p><p><strong>That said, this effect isn’t particularly new. However, most developers flock to JavaScript libraries despite the fact that the same result can be achieved using CSS.</strong></p><p><strong>HTML</strong></p><p><strong>&nbsp;</strong></p><p><strong>&lt;div class="typing"&gt;</strong></p><p><strong>&lt;div class="typing-effect"&gt;</strong></p><p><strong>Typing effect for text</strong></p><p><strong>div&gt;</strong></p><p><strong>div&gt;</strong></p><p><strong>CSS</strong></p><p><strong>&nbsp;</strong></p><p><strong>.typing {</strong></p><p><strong>height: 80vh;</strong></p><p><strong>display: flex;</strong></p><p><strong>align-items: center;</strong></p><p><strong>justify-content: center;</strong></p><p><strong>}</strong></p><p><strong>&nbsp;</strong></p><p><strong>.typing-effect {</strong></p><p><strong>width: 22ch;</strong></p><p><strong>animation: typing 2s steps(22), effect .5s step-end infinite alternate;</strong></p><p><strong>white-space: nowrap;</strong></p><p><strong>overflow: hidden;</strong></p><p><strong>border-right: 3px solid;</strong></p><p><strong>font-family: monospace;</strong></p><p><strong>font-size: 2em;</strong></p><p><strong>}</strong></p><p><strong>&nbsp;</strong></p><p><strong>@keyframes typing {</strong></p><p><strong>from {</strong></p><p><strong>width: 0</strong></p><p><strong>}</strong></p><p><strong>}</strong></p><p><strong>&nbsp;</strong></p><p><strong>@keyframes effect {</strong></p><p><strong>50% {</strong></p><p><strong>border-color: transparent</strong></p><p><strong>}</strong></p><p><strong>}</strong></p>', 1, 1, N'/quiz-practicing-system/assets/images/posts/40a23312-e273-4829-96a5-d38a758c5841.jpg', N'There are around 200 total CSS properties, depending on where you look. And, many of those properties interact with one another in their own unique ways. Keeping track of everything is practically impossible. So, this article is all about showcasing nifty CSS tricks that are useful for both developers as well as designers.
', 0)
INSERT [dbo].[Post] ([ID], [Title], [Date], [Content], [CategoryID], [UserID], [Thumbnail], [Brief], [Status]) VALUES (2, N'9 Skills You Need to Become a Back End Developer in 2022 (Part 1)', CAST(N'2022-06-14T15:23:06.147' AS DateTime), N'<h2>9 Skills You Need to Become a Back End Developer in 2022</h2><p>Back end developers are in hot demand, and there are some essential skills you’ll need to learn if you want to become one. But never fear — if you’ve ever peeked under the hood of your car for the fun of it or snuck backstage to get a firsthand look at what goes on behind a show’s curtain, you already have the curiosity that drives back end developers. With a little effort and training, you may just be able to use your natural inquisitiveness to build a rewarding career, but it ultimately depends on acquiring the essential back end development skills.&nbsp;</p><p><strong>The 9 Skills You Need to Become a Back End Developer</strong></p><ol><li>Python</li><li>Java</li><li>PHP</li><li>SQL</li><li>Git</li><li>HTML</li><li>CSS</li><li>JavaScript</li><li>Communication</li></ol><h2><strong>What Back End Developers Do (and How It Relates to Front End Development)</strong></h2><p>Before we dig into these skills, let’s first discuss back end development in its wider context. While front end developers manage the visible parts of a website, such as the design or functionality of its display, back end programmers dedicate their time to designing, fixing, and altering the aspects of a software application or information system that a typical user never sees. They serve as the wizards to the tech sector’s Oz, crafting the core features and components of the programs that drive consumers’ digital experiences.&nbsp;</p><p>When a user makes a request to the system through a front end (i.e., consumer-side) framework, it’s a <strong>back end developer’s responsibility</strong> to make sure that a program can deliver any requested data or information. These <a href="https://www.techopedia.com/definition/29568/back-end-developer"><strong>developers have other duties</strong></a>, too. They maintain core databases, manage application program interfaces (APIs), as well as test and debug back end processes to ensure that a program functions smoothly and effectively at all times.</p><p>Sound complicated? It is. But if the prospect of picking apart the nuts and bolts of an application and learning how to make it run <i>better</i> thrills you, there’s no better time to take on the challenge. With the world’s growing social and economic reliance on digital channels, employers have more need than ever for talented back end developers.</p><p><i>Interested in learning both front end and back end development skills?&nbsp;Columbia Engineering Coding Boot Camp‘s full stack curriculum has you covered.&nbsp;</i></p><h2><strong>GET BOOT CAMP INFO</strong></h2><p><strong>Choose Boot Camp</strong></p><p>Choose Boot Camp Choose Boot CampCodingCybersecurityDataDigital MarketingFinTechProduct ManagementTech Project MgmtUX/UI</p><p><strong>NEXT</strong></p><p>0%</p><p>&nbsp;</p><p><a href="https://www.hubspot.com/marketing-statistics"><strong>Statistics compiled by the marketing software giant HubSpot</strong></a> indicate that the digital world is thriving; researchers predict that by 2021, global e-commerce retail sales will top $4.5 trillion. Today, nearly half of Americans pick up their smartphones before they even roll out of bed! We’re looking at the news, chatting with friends, checking our email, and visiting our favorite social media and brand websites. With all of this interest in online shopping and brand exploration, companies need to have functional, well-optimized websites and applications — and the developers to craft them.</p><p>Given these numbers, it’s not all that surprising that web developers have an excellent job outlook. In 2018, the Bureau of Labor and Statistics (BLS) projected that <a href="https://www.bls.gov/ooh/computer-and-information-technology/web-developers.htm"><strong>the profession would grow a remarkable 13 percent by 2028</strong></a> — much faster, they write, than the average for all other occupations, which stands at just 5 percent. Predictably, BLS researchers attributed the speedy growth to the skyrocketing popularity of mobile devices and e-commerce.</p><p><img src="https://cdn.columbiauniversitybootcamp.com/wp-content/uploads/sites/108/2020/05/developer-employment-change.jpg" alt="Web developers leading other occupations in change of employment" srcset="https://cdn.columbiauniversitybootcamp.com/wp-content/uploads/sites/108/2020/05/developer-employment-change.jpg 1000w, https://cdn.columbiauniversitybootcamp.com/wp-content/uploads/sites/108/2020/05/developer-employment-change-300x156.jpg 300w, https://cdn.columbiauniversitybootcamp.com/wp-content/uploads/sites/108/2020/05/developer-employment-change-768x399.jpg 768w, https://cdn.columbiauniversitybootcamp.com/wp-content/uploads/sites/108/2020/05/developer-employment-change-200x104.jpg 200w, https://cdn.columbiauniversitybootcamp.com/wp-content/uploads/sites/108/2020/05/developer-employment-change-674x350.jpg 674w" sizes="100vw" width="750"></p><p>The tech sector’s need for talented developers won’t fade anytime soon. If you’re curious, tech-savvy, and want to understand how to make systems work from behind the metaphorical curtain, learning the essential skills for back end development can unlock an exciting career for you.</p>', 2, 1, N'/quiz-practicing-system/assets/images/posts/67fa88e0-6d7b-4977-aa60-051809a1817a.png', N'Back end developers are in hot demand, and there are some essential skills you''ll need to learn. With the world''s growing social and economic reliance on digital channels, employers have more need than ever for back end developers. Back end programmers are the wizards to the tech sector''s Oz. Web developers have an excellent job outlook. Bureau of Labor and Statistics projected that the profession would grow a remarkable 13 percent by 2028.

Global e-commerce retail sales will top $4.5 trillion by 2021, according to HubSpot''s research. The tech sector''s need for talented developers won''t fade anytime soon.', 0)
INSERT [dbo].[Post] ([ID], [Title], [Date], [Content], [CategoryID], [UserID], [Thumbnail], [Brief], [Status]) VALUES (3, N'What Skills Do You Need for Marketing in 2020?', CAST(N'2022-06-14T15:26:35.627' AS DateTime), N'<p>If you''re considering a <a href="https://online.champlain.edu/blog/top-reasons-to-consider-career-in-marketing">career in marketing</a>, you may be wondering if your skillset and professional strengths are a good fit for the field.</p><p>Modern marketing is a very different world from the marketing of even just a few years ago, with digital strategies playing a significant role and traditional methods (such as print) becoming less relevant and less effective. That means that while certain skills associated with the field (such as creativity and communication) are certainly still important and applicable, there are a whole host of other, newer skills that employers are now looking for in their <a href="https://online.champlain.edu/blog/what-can-you-do-with-a-degree-in-marketing">marketing hires</a>.</p><p>&nbsp;</p><h2><strong>What Skills Do You Need for Marketing?</strong></h2><p>Key marketing skills can be broken down into two categories - <a href="https://online.champlain.edu/blog/top-soft-skills-employers-are-looking-for">soft skills</a> and hard skills. Soft skills are broader and more widely applicable, and are useful (or necessary) in many different career paths. Hard skills are more focused on technical abilities and familiarity with specific tools and strategies. Given that marketing is a very broad field, the hard skills you''ll need to get hired will vary tremendously from role to role, but for the purposes of this article, we''re focusing on some of the most common skills employers are looking for when hiring an entry-level marketing generalist (typically a coordinator or assistant role).</p><h3><strong>Top Marketing Soft Skills</strong></h3><p>&nbsp;</p><p><strong>Communication</strong></p><p>At its core, marketing is about communicating to an audience, so it''s no surprise that communication is the top skill those in the field need to have! Being able to express yourself and convey concepts to others in a clear, engaging way will be essential to your work as a marketer.</p><p><strong>Creativity and Problem-Solving</strong></p><p>Marketing is all about cutting through the noise and delivering a message that resonates with your target customer. Creativity and thinking outside the box to find new ways of doing things is one of the trademarks of a <a href="https://online.champlain.edu/blog/switching-to-a-marketing-career">successful marketing professional</a>. Even if you''re not someone who thinks of themselves as creative (for example, if you''re more of a data person), marketing still requires the ability to tackle problems from new angles and come up with innovative solutions to rising challenges.</p><p><strong>Attention to Detail</strong></p><p>As a marketer, your work will be seen by many eyes, whether it''s a blog post, a social media graphic, or a printed piece of promotional material. Accuracy is essential - both to ensure your company''s image is maintained, and so that your customers are getting the right information.</p><p><strong>Interpersonal Skills</strong></p><p>Working in marketing often means working closely with a broader marketing team, colleagues in other departments, clients, and/or vendors. Given that you''ll be interacting frequently with all kinds of different people, it''s important that you have good interpersonal skills and can build strong working relationships with others.</p><p><strong>Leadership</strong></p><p>While your first marketing role probably won''t be in management, <a href="https://online.champlain.edu/blog/top-qualities-of-a-great-leader">leadership</a> is still an important skill to build and develop over time, and can be put into use at any point in your career. Depending on your role, this could mean taking charge on a specific project, acting as a point person for a vendor or client, or helping junior members of the team in their work.</p><p><strong>Adaptability</strong></p><p>Marketing is a fast-changing field, with new best practices, tools, and standards emerging constantly. Additionally, <a href="https://online.champlain.edu/online/keep-your-marcom-team-succesful-in-age-of-convergence">marketing teams</a> often have to work under tight deadlines and may be assigned last-minute projects or find priorities shifting with little notice. A successful marketer will enjoy this kind of fast-paced environment, and will be able to adapt to changing circumstances with ease.</p><h3><strong>Top Marketing Hard Skills</strong></h3><p>&nbsp;</p><p><strong>Writing</strong></p><p>Writing is a highly sought-after skill no matter what field you''re in, but it''s particularly important in marketing, where (as noted above) communication is a critical part of your day-to-day work.</p><p><strong>Data Analysis &amp; Analytics</strong></p><p>Marketing is nothing without measurement: you need to be able to calculate the success and ROI of your marketing efforts. Being comfortable working with data from a variety of sources and campaigns, understanding what''s relevant and what''s not, and using your analysis to inform future actions will be a key part of your role as a marketer.</p><p><strong>Project Management</strong></p><p>Project management is an essential piece of every marketer''s world: whether you''re juggling multiple campaigns, clients, or projects, you''ll need to have a strong set of <a href="https://online.champlain.edu/blog/essential-project-management-skills">project management skills </a>to keep track of everything that''s going on and to get things done in an efficient way. This means being able to work to deadlines, prioritize your work, keep track of others'' contributions - all while delivering an excellent end product!</p><p><strong>Research</strong></p><p>As a marketer, you''ll often need to do research to build out campaigns, inform strategy, and create content. This research could take a variety of forms, such as looking into competitors'' marketing campaigns, learning more about a topic relevant to the services or products your organization provides, or building out a list of contacts in a specific industry. Knowing how to conduct effective research from reputable sources is an underrated but important skill that will increase the quality of your work.</p><p><strong>SEO/SEM</strong></p><p>Search engines are some of the most prominent marketing tools used today - knowing how to use them to your organization''s advantage, whether through paid or organic strategies is critical. The world of search engine marketing is huge and ever-changing, and many marketers work solely in this space. As a marketing generalist, you won''t be expected to know the ins and outs of <a href="https://blog.hubspot.com/insiders/seo-sem-faqs">paid search and search engine optimization,</a> but knowing - and being able to execute - the basics is important.</p><p><strong>Social Media Marketing</strong></p><p>Social media continues to be one of the most effective ways for organizations to reach their customers - both more established platforms, such as Facebook and <a href="https://online.champlain.edu/blog/optimize-linkedin-profile-best-practices">LinkedIn</a>, and newer, emerging platforms like TikTok. Knowing how to adapt a brand''s voice and messaging to social media and connect authentically with your audience is an essential part of modern marketing.</p><p><strong>Email Marketing</strong></p><p>Although other forms of marketing have grown tremendously in recent years, email remains an important mode of communication. All marketers should understand the basics of <a href="https://mailchimp.com/email-marketing/">email marketing</a>, including voice and tone, engaging subject lines, and strong calls to action.</p><p><strong>Visual Marketing</strong></p><p>Design is a scary term for those who don''t think of themselves as artistic - you by no means need to be an expert graphic designer to be a successful marketer! Typically, marketing departments will have a dedicated designer on-staff, or you''ll outsource bigger design projects to an agency or contractor. However, you should be comfortable with the basics of visual marketing - selecting images for a website or to accompany a social media post, for example, or taking pictures at an event. It can also be really useful to know the basics of the <a href="https://www.adobe.com/creativecloud.html">Adobe Creative Suite</a> (particularly Photoshop and InDesign) and free design tools such as <a href="https://spark.adobe.com/">Adobe Spark</a> or <a href="https://www.canva.com/">Canva</a> to help you do your work more efficiently - it''s nice to be able to create a quick graphic on your own!</p><p><strong>Website Management</strong></p><p>Finally, being comfortable working in the back end of a website is an important marketing skill. Again, you don''t have to worry about your capabilities here - you don''t need to be a developer or web designer if that''s not what you''re interested in! Most marketers, however, will need to know how to make updates to pages, build out landing pages, and other basic web functions. (Keep in mind that every website will be different, so this is less about knowing the nitty-gritty of every content management system out there and more being comfortable diving in and learning if your organization uses a platform you''re not familiar with).</p><h3><strong>How to Build Marketing Skills</strong></h3><p>You might be looking at the list above and worrying that you don''t have what it takes to be a marketer. But not to worry - all of these skills can be built through dedicated training and education. A <a href="https://online.champlain.edu/blog/top-soft-skills-employers-are-looking-for">marketing degree</a> is a great way to learn the most important marketing skills today''s employers are looking for, both the core soft skills and the key technical skills. You''ll emerge from a degree program with a well-rounded, up-to-date marketing skillset that will poise you for success in your first role in the field.</p>', 5, 1, N'/quiz-practicing-system/assets/images/posts/7e103b4f-6e9a-46cd-a200-177c84df5007.jpg', N'Key marketing skills can be broken down into two categories - soft and hard. Hard skills are more focused on technical abilities and familiarity with specific tools and strategies. Communication, problem-solving and attention to detail are some of the most common skills employers are looking for in a marketing professional. Leadership is an important skill to build and develop over time. A successful marketer will be able to adapt to changing circumstances. Writing is a highly sought-after skill no matter what field you''re in.', 1)
INSERT [dbo].[Post] ([ID], [Title], [Date], [Content], [CategoryID], [UserID], [Thumbnail], [Brief], [Status]) VALUES (4, N'Guide to Internationalization in Spring Boot', CAST(N'2022-06-14T15:31:02.230' AS DateTime), N'<h3><strong>Get started with Spring 5 and Spring Boot 2, through the </strong><i><strong>Learn Spring</strong></i><strong> course:</strong></h3><p><a href="https://www.baeldung.com/ls-course-start"><strong>&gt;&gt; CHECK OUT THE COURSE</strong></a></p><h2><strong>1. Overview</strong></h2><p>In this quick tutorial, we''re going to take a look at how we can <strong>add internationalization to a Spring Boot application</strong>.</p><h2><strong>2. Maven Dependencies</strong></h2><p>For development, we need the following dependency:</p><p>&lt;<strong>dependency</strong>&gt;
 &nbsp; &nbsp;&lt;<strong>groupId</strong>&gt;org.springframework.boot&lt;/<strong>groupId</strong>&gt;
 &nbsp; &nbsp;&lt;<strong>artifactId</strong>&gt;spring-boot-starter-thymeleaf&lt;/<strong>artifactId</strong>&gt;
 &nbsp; &nbsp;&lt;<strong>version</strong>&gt;1.5.2.RELEASE&lt;/<strong>version</strong>&gt;
&lt;/<strong>dependency</strong>&gt;</p><p>The latest version of <a href="https://search.maven.org/classic/#search%7Cga%7C1%7Ca%3A%22spring-boot-starter-thymeleaf%22">spring-boot-starter-thymeleaf </a>can be downloaded from Maven Central.</p><h2><strong>3. </strong><i><strong>LocaleResolver</strong></i></h2><p>In order for our application to be able to determine which locale is currently being used, we need to add a <i>LocaleResolver</i> bean:</p><p>@Bean
<strong>public</strong> LocaleResolver <strong>localeResolver</strong>() {
 &nbsp; &nbsp;<strong>SessionLocaleResolver</strong> slr = <strong>new</strong> <strong>SessionLocaleResolver</strong>();
 &nbsp; &nbsp;slr.setDefaultLocale(Locale.US);
 &nbsp; &nbsp;<strong>return</strong> slr;
}</p><p>The <i>LocaleResolver</i> interface has implementations that determine the current locale based on the session, cookies, the <i>Accept-Language</i> header, or a fixed value.</p><p>In our example, we have used the session based resolver <i>SessionLocaleResolver</i> and set a default locale with value <i>US</i>.</p><h2><strong>4. </strong><i><strong>LocaleChangeInterceptor</strong></i></h2><p>Next, we need to add an interceptor bean that will switch to a new locale based on the value of the <i>lang</i> parameter appended to a request:</p><p>@Bean
<strong>public</strong> LocaleChangeInterceptor <strong>localeChangeInterceptor</strong>() {
 &nbsp; &nbsp;<strong>LocaleChangeInterceptor</strong> lci = <strong>new</strong> <strong>LocaleChangeInterceptor</strong>();
 &nbsp; &nbsp;lci.setParamName("lang");
 &nbsp; &nbsp;<strong>return</strong> lci;
}</p><p><strong>In order to take effect, this bean needs to be added to the application''s interceptor registry.</strong></p><p>To achieve this, our <i>@Configuration</i> class has to implement the <i>WebMvcConfigurer</i>&nbsp;interface and override the <i>addInterceptors()</i> method:</p><p>@Override
<strong>public</strong> <strong>void</strong> <strong>addInterceptors</strong>(InterceptorRegistry registry) {
 &nbsp; &nbsp;registry.addInterceptor(localeChangeInterceptor());
}</p><h2><strong>5. Defining the Message Sources</strong></h2><p>By default, a Spring Boot application will look for message files containing internationalization keys and values in the <i>src/main/resources</i> folder.</p><p>The file for the default locale will have the name <i>messages.properties</i>, and files for each locale will be named <i>messages_XX.properties</i>, where <i>XX</i> is the locale code.</p><p>The keys for the values that will be localized have to be the same in every file, with values appropriate to the language they correspond to.</p><p><strong>If a key does not exist in a certain requested locale, then the application will fall back to the default locale value.</strong></p><p>Let''s define a default message file for the English language called <i>messages.properties</i>:</p><p>greeting=Hello! Welcome to our website!
lang.change=Change the language
lang.eng=English
lang.fr=French</p><p>Next, let''s create a file called <i>messages_fr.properties</i> for the French language with the same keys:</p><p>greeting=Bonjour! Bienvenue sur notre site!
lang.change=Changez la langue
lang.eng=Anglais
lang.fr=Francais</p><h2><strong>6. Controller and HTML Page</strong></h2><p>Let''s create a controller mapping that will return a simple HTML page called<i> international.html</i> that we want to see in two different languages:</p><p>@Controller
<strong>public</strong> <strong>class</strong> <strong>PageController</strong> {

 &nbsp; &nbsp;@GetMapping("/international")
 &nbsp; &nbsp;<strong>public</strong> String <strong>getInternationalPage</strong>() {
 &nbsp; &nbsp; &nbsp; &nbsp;<strong>return</strong> "international";
 &nbsp; &nbsp;}
}</p><p>Since we are using thymeleaf to display the HTML page, the locale-specific values will be accessed using the keys with the syntax <i>#{key}</i>:</p><p>&lt;<strong>h1</strong> th:text="#{greeting}"&gt;&lt;/<strong>h1</strong>&gt;</p><p>If using JSP files, the syntax is:</p><p>&lt;<strong>h1</strong>&gt;&lt;<strong>spring:message</strong> code="greeting" text="default"/&gt;&lt;/<strong>h1</strong>&gt;</p><p>If we want to access the page with the two different locales we have to add the parameter <i>lang</i> to the URL in the form:<i> /international?lang=fr</i></p><p>If no <i>lang</i> parameter is present on the URL, the application will use the default locale, in our case <i>US </i>locale.</p><p>Let''s add a drop-down to our HTML page with the two locales whose names are also localized in our properties files:</p><p>&lt;<strong>span</strong> th:text="#{lang.change}"&gt;&lt;/<strong>span</strong>&gt;:
&lt;<strong>select</strong> id="locales"&gt;
 &nbsp; &nbsp;&lt;<strong>option</strong> value=""&gt;&lt;/<strong>option</strong>&gt;
 &nbsp; &nbsp;&lt;<strong>option</strong> value="en" th:text="#{lang.eng}"&gt;&lt;/<strong>option</strong>&gt;
 &nbsp; &nbsp;&lt;<strong>option</strong> value="fr" th:text="#{lang.fr}"&gt;&lt;/<strong>option</strong>&gt;
&lt;/<strong>select</strong>&gt;</p><p>Then we can add a jQuery script that will call the<i> /international</i> URL with the respective <i>lang</i> parameter depending on which drop-down option is selected:</p><p>&lt;<strong>script</strong> src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"&gt;
&lt;/<strong>script</strong>&gt;
&lt;<strong>script</strong> type="text/javascript"&gt;
$(document).<strong>ready</strong>(<strong>function</strong>() {
 &nbsp; &nbsp;$("#locales").<strong>change</strong>(<strong>function</strong> () {
 &nbsp; &nbsp; &nbsp; &nbsp;<strong>var</strong> selectedOption = $(''#locales'').<strong>val</strong>();
 &nbsp; &nbsp; &nbsp; &nbsp;<strong>if</strong> (selectedOption != ''''){
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;window.location.<strong>replace</strong>(''international?lang='' + selectedOption);
 &nbsp; &nbsp; &nbsp; &nbsp;}
 &nbsp; &nbsp;});
});
&lt;/<strong>script</strong>&gt;</p><h2><strong>7. Running the Application</strong></h2><p>In order to initialize our application, we have to add the main class annotated with <i>@SpringBootApplication</i>:</p><p>@SpringBootApplication
<strong>public</strong> <strong>class</strong> <strong>InternationalizationApp</strong> {
 &nbsp; &nbsp;
 &nbsp; &nbsp;<strong>public</strong> <strong>static</strong> <strong>void</strong> <strong>main</strong>(String[] args) {
 &nbsp; &nbsp; &nbsp; &nbsp;SpringApplication.run(InternationalizationApp.class, args);
 &nbsp; &nbsp;}
}</p><p>Depending on the selected locale, we will view the page in either English or French when running the application.</p><p>Let''s see the English version:</p><p><a href="https://www.baeldung.com/wp-content/uploads/2017/03/piceng.png"><img src="https://www.baeldung.com/wp-content/uploads/2017/03/piceng.png" alt="screen shot in English"></a></p><p>And now let''s see the French version:</p><p><a href="https://www.baeldung.com/wp-content/uploads/2017/03/picfr.png"><img src="https://www.baeldung.com/wp-content/uploads/2017/03/picfr.png" alt="screen shot in French" srcset="https://www.baeldung.com/wp-content/uploads/2017/03/picfr.png 546w, https://www.baeldung.com/wp-content/uploads/2017/03/picfr-300x82.png 300w" sizes="100vw" width="546"></a></p><h2><strong>8. Conclusion</strong></h2><p>In this tutorial, we have shown how we can use the support for internationalization in a Spring Boot application.</p><p>The full source code for the example can be found <a href="https://github.com/eugenp/tutorials/tree/master/spring-boot-modules/spring-boot-mvc">over on GitHub</a>.</p><h3><strong>Get started with Spring 5 and Spring Boot 2, through the </strong><i><strong>Learn Spring</strong></i><strong> course:</strong></h3><p><a href="https://www.baeldung.com/ls-course-end"><strong>&gt;&gt; CHECK OUT THE COURSE</strong></a></p><p><img src="https://www.baeldung.com/wp-content/uploads/2016/05/baeldung-rest-post-footer-main-1.2.0.jpg" alt="Build your API with SPRING - book cover"></p>', 7, 1, N'/quiz-practicing-system/assets/images/posts/4228c9ba-2009-497c-b5d1-dadb08f00cf4.png', N'The LocaleResolver interface has implementations that determine the current locale based on the session, cookies, the Accept-Language header, or a fixed value. For development, we need the following dependency:. org.springframework.boot    spring-boot-starter-thymeleaf. A Spring Boot application will look for files containing internationalization keys and values in the src/main/resources folder. Let''s define a default message file for the English language called messages.properties. In French, let''s create a file called messages_fr.properties with the same keys.', 1)
INSERT [dbo].[Post] ([ID], [Title], [Date], [Content], [CategoryID], [UserID], [Thumbnail], [Brief], [Status]) VALUES (5, N'12 Useful CSS Tricks for Front-end Developers (Part 2)', CAST(N'2022-06-14T15:32:47.770' AS DateTime), N'<h2>Shadow for transparent images</h2><p><img src="https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/03/Shadow-for-transparent-images-1024x373.png" alt="Shadow for transparent images" srcset="https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/03/Shadow-for-transparent-images-1024x373.png 1024w, https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/03/Shadow-for-transparent-images-300x109.png 300w, https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/03/Shadow-for-transparent-images-768x279.png 768w, https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/03/Shadow-for-transparent-images-350x127.png 350w, https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/03/Shadow-for-transparent-images-830x302.png 830w, https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/03/Shadow-for-transparent-images-1200x437.png 1200w, https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/03/Shadow-for-transparent-images.png 1253w" sizes="100vw" width="1024"></p><p>Have you ever tried adding a box-shadow to a transparent image only for it to look like you have added a border? I think we’ve all been there. The solution to adding shadow effects for transparent images is to use drop-shadow.</p><p>The way it works is that the drop-shadow property follows the alpha channels of the given image. As such, the shadow is based on the shape inside the image rather than being displayed outside of it.</p><p>HTML</p><p>&nbsp;</p><p><strong>&lt;</strong>div class="transparent-shadow"<strong>&gt;</strong></p><p><strong>&lt;</strong>div class="margin-right"<strong>&gt;</strong></p><p><strong>&lt;</strong>div class="margin-bottom align-center"<strong>&gt;</strong></p><p>box-shadow</p><p><strong>&lt;/</strong>div<strong>&gt;</strong></p><p>&nbsp;</p><p><strong>&lt;</strong>img class="box-shadow" src="https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/02/logo.png" alt="box-shadow example (transparent)"<strong>&gt;</strong></p><p><strong>&lt;/</strong>div<strong>&gt;</strong></p><p>&nbsp;</p><p><strong>&lt;</strong>div<strong>&gt;</strong></p><p><strong>&lt;</strong>div class="margin-bottom align-center"<strong>&gt;</strong></p><p>drop-shadow</p><p><strong>&lt;/</strong>div<strong>&gt;</strong></p><p>&nbsp;</p><p><strong>&lt;</strong>img class="drop-shadow" src="https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/02/logo.png" alt="drop-shadow example (transparent)"<strong>&gt;</strong></p><p><strong>&lt;/</strong>div<strong>&gt;</strong></p><p><strong>&lt;/</strong>div<strong>&gt;</strong></p><p>CSS</p><p>&nbsp;</p><p>.transparent-shadow <strong>{</strong></p><p>height: 80vh;</p><p>display: flex;</p><p>align-items: center;</p><p>justify-content: center;</p><p><strong>}</strong></p><p>&nbsp;</p><p>.margin-right <strong>{</strong></p><p>margin-right: 2em;</p><p><strong>}</strong></p><p>&nbsp;</p><p>.margin-bottom <strong>{</strong></p><p>margin-bottom: 1em;</p><p><strong>}</strong></p><p>&nbsp;</p><p>.align-center <strong>{</strong></p><p>text-align: center;</p><p><strong>}</strong></p><p>&nbsp;</p><p>.box-shadow <strong>{</strong></p><p>box-shadow: 2px 4px 8px #3723a1;</p><p><strong>}</strong></p><p>&nbsp;</p><p>.drop-shadow <strong>{</strong></p><p>filter: drop-shadow<strong>(</strong>2px 4px 8px #3723a1<strong>)</strong>;</p><p><strong>}</strong></p><h2>Set a custom cursor</h2><p><img src="https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/03/Set-a-custom-cursor-with-CSS.gif" alt="Set a custom cursor with CSS"></p><p>It’s unlikely that you’ll ever need to force your visitors into a unique cursor. At least, not for general UX purposes. Though, one thing to note about the cursor property is that it lets you display images. This is the equivalent of displaying a tooltip but in a photo format.</p><p>Some use cases include being able to compare two different photos without needing to render those photos in the viewport. E.g. The cursor property can be used to save real estate in your designs. Since you can lock the custom cursor to a specific div element, it won’t interfere with elements outside of it.</p><p>HTML</p><p>&nbsp;</p><p><strong>&lt;</strong>div class="custom-cursor"<strong>&gt;</strong></p><p><strong>&lt;</strong>div class="card"<strong>&gt;</strong></p><p>Default</p><p><strong>&lt;/</strong>div<strong>&gt;</strong></p><p>&nbsp;</p><p><strong>&lt;</strong>div class="card card-image-cursor"<strong>&gt;</strong></p><p>Image</p><p><strong>&lt;/</strong>div<strong>&gt;</strong></p><p>&nbsp;</p><p><strong>&lt;</strong>div class="card card-emoji-cursor"<strong>&gt;</strong></p><p>Emoji</p><p><strong>&lt;/</strong>div<strong>&gt;</strong></p><p><strong>&lt;/</strong>div<strong>&gt;</strong></p><p>CSS</p><p>&nbsp;</p><p>.custom-cursor <strong>{</strong></p><p>display: flex;</p><p>height: 80vh;</p><p>align-items: center;</p><p>justify-content: center;</p><p>background: #f3f3f3;</p><p>padding: 0 10px;</p><p><strong>}</strong></p><p>&nbsp;</p><p>.card <strong>{</strong></p><p>width: 200px;</p><p>height: 200px;display: flex;</p><p>align-items: center;</p><p>justify-content: center;</p><p>background-color: #D29A5A;</p><p>margin-right: 10px;color: #fff;</p><p>font-size: 1.4em;</p><p>text-align: center;</p><p><strong>}</strong></p><p>&nbsp;</p><p>.card-image-cursor <strong>{</strong></p><p>background-color: #D11A5A;</p><p>cursor: url(https://stackdiary.com/tools/assets/img/tools/html-beautifier.svg), auto;</p><p><strong>}</strong></p><p>&nbsp;</p><p>.card-emoji-cursor <strong>{</strong></p><p>background-color: #D29B22;</p><p>cursor: url("data:image/svg+xml;utf8,&lt;svg xmlns=''http://www.w3.org/2000/svg'' width=''48'' height=''48'' viewport=''0 0 100 100'' style=''fill:black;font-size:24px;''&gt;&lt;text y=''50%''&gt;??&lt;/text&gt;&lt;/svg&gt;"), auto;</p><p><strong>}</strong></p><h2>Simple tooltip using <i>attr()</i></h2><p><img src="https://c6c8j7x5.rocketcdn.me/wp-content/uploads/2022/03/CSS-tooltip-using-attr-property.gif" alt="CSS tooltip using attr property"></p><p>The attr() property is one of my favorite recent discoveries. I wanted to add a tooltip function to my WordPress blog, but doing so would require using a plugin that adds unnecessary bloat to my site. Thankfully, that can be circumvented using attr().</p><p>The way it works is quite simple, let me explain the code below:</p><ul><li>We use the tooltip class to specify which element is going to be the tooltip. You can style this however you like, but for sake of the demo we use a dotted border-bottom.</li><li>Next, we create a :before pseudo-element that will contain a content attr() function and its specification. In this case, we call it tooltip-data.</li><li>And finally, we create a :hover pseudo-class that will set the opacity to 1 whenever someone hovers over the tooltip itself.</li></ul><p>Additionally, you have to include custom styling. Depending on your tooltip data, you might need to adjust the width but also the margin. And once you do set it all up, you can reuse the tooltip-data attr() class in any part of your design.</p><p>HTML</p><p>&nbsp;</p><p><strong>&lt;</strong>h1<strong>&gt;</strong></p><p>HTML/CSS tooltip</p><p><strong>&lt;/</strong>h1<strong>&gt;</strong></p><p><strong>&lt;</strong>p<strong>&gt;</strong></p><p>Hover <strong>&lt;</strong>span class="tooltip" tooltip-data="Tooltip Content"<strong>&gt;</strong>Here<strong>&lt;/</strong>span<strong>&gt;</strong> to see the tooltip.</p><p><strong>&lt;/</strong>p<strong>&gt;</strong></p><p><strong>&lt;</strong>p<strong>&gt;</strong></p><p>You can also hover <strong>&lt;</strong>span class="tooltip" tooltip-data="This is another Tooltip Content"<strong>&gt;</strong>here<strong>&lt;/</strong>span<strong>&gt;</strong> to see another example.</p><p><strong>&lt;/</strong>p<strong>&gt;</strong></p><p>CSS</p><p>&nbsp;</p><p>.tooltip <strong>{</strong></p><p>position: relative;</p><p>border-bottom: 1px dotted black;</p><p><strong>}</strong></p><p>&nbsp;</p><p>.tooltip:before <strong>{</strong></p><p>content: attr<strong>(</strong>tooltip-data<strong>)</strong>;</p><p>position: absolute;</p><p>width: 250px;</p><p>background-color: #efba93;</p><p>color: #fff;</p><p>text-align: center;</p><p>padding: 15px;</p><p>line-height: 1.1;</p><p>border-radius: 5px;</p><p>z-index: 1;</p><p>opacity: 0;</p><p>transition: opacity .5s;</p><p>bottom: 125%;</p><p>left: 50%;</p><p>margin-left: -60px;</p><p>font-size: 0.70em;</p><p>visibility: hidden;</p>', 1, 1, N'/quiz-practicing-system/assets/images/posts/40a23312-e273-4829-96a5-d38a758c5841.jpg', N'There are around 200 total CSS properties, depending on where you look. And, many of those properties interact with one another in their own unique ways. Keeping track of everything is practically impossible. So, this article is all about showcasing nifty CSS tricks that are useful for both developers as well as designers.
', 1)
INSERT [dbo].[Post] ([ID], [Title], [Date], [Content], [CategoryID], [UserID], [Thumbnail], [Brief], [Status]) VALUES (6, N'BS in Human Resource Management: Career Outlook', CAST(N'2022-06-14T15:36:42.383' AS DateTime), N'<h2><strong>How to Become a Human Resource Manager</strong></h2><p>When you think of everything a human resources office does on a daily basis - from onboarding a new staff member to ensuring everyone’s direct deposit goes through - you can see just how versatile a field it is.</p><p>In this blog post, we’ll give an overview of what an HR manager does, the career outlook for the field, and provide insight on how to become a human resource manager.</p><h3><strong>What Does a Human Resource Manager Do?</strong></h3><p>A human resource manager can work as a generalist or specialist. For example, they might oversee an entire HR operation or manage a specific function within the department, such as serving as payroll manager or benefits coordinator.</p><p>Some of the more traditional tasks of a human resource manager include:</p><ul><li>Overseeing the recruitment, selection, and hiring process</li><li>Planning and managing employee compensation and benefit programs</li><li>Administering and processing payroll</li><li>Consulting with managers and executives on talent development and management</li><li>Ensuring compliance with local, state, federal, and industry regulations</li><li>Developing and directing training programs</li><li>Assessing employee performance</li><li>Supervising other HR staff members</li></ul><p>Today, human resource managers and related roles are becoming more diverse in scope and, in some cases, more specialized as needs arise within an organization. For example, many HR managers have a bigger role in strategic planning. Others work heavily with data, such as managing employee information systems.</p><p>As you think about these various roles and responsibilities, you can begin to set your own human resource manager career goals.</p><h3><strong>Human Resource Manager Job Outlook</strong></h3><p>Demand for human resources managers and other HR professionals remains strong: it’s a field that’s consistently looking for qualified professionals to fill roles in companies of all sizes. The U.S. Department of Labor Bureau of Labor Statistics (BLS) provides career outlook stats for a range of <a href="https://www.bls.gov/ooh/management/human-resources-managers.htm#tab-8">human resource-related occupations</a>. Let’s take a look at the expected growth between 2020 and 2030 for some of these positions:</p><ul><li>Human resource specialists: 10%</li><li>Human resource managers: 9%</li><li>Training and development specialists: 11% (faster than average)</li><li>Training and development managers: 11% (faster than average)</li><li>Compensation, benefits, and job analysis specialists: 10%</li><li>Business operation specialists: 9%</li></ul><p>The BLS data tracks overarching occupations; however, the data doesn’t always match up with the more specific job titles we see today - and the HR field is one that’s seeing a range of new roles and specialties such as compliance, workplace safety, inclusion, operations, and risk management.</p><h4>Demand for New (and Future) Human Resource Roles</h4><p>It’s an exciting time to become a human resource manager. The field of human resources is always evolving, but pandemic-related organizational changes within companies will likely increase demand - as well as create entirely new roles and functions for HR professionals.</p><p>For example, <a href="https://hbr.org/2020/08/21-hr-jobs-of-the-future">The Harvard Business Review (HBR)</a> reports that in a day where employee retention is more critical than ever, we might see emerging titles such as “director of well-being” become more widespread. Additionally, with more workplaces going remote, companies might need an HR manager fully dedicated to this contingent of employees; HBR refers to it as a “work from home facilitator.”</p><h3><strong>What are the Most Important Skills for a Human Resource Manager?</strong></h3><p>When you look at the roles highlighted above, it’s clear there is a wide range of skills employers seek in HR professionals.</p><h4>People Skills</h4><p>Collaboration, communication, and conflict-management skills are key when working with people at all levels of an organization. You’ll be conducting interviews, giving presentations, negotiating benefits plans, and handling workplace grievances - among many other duties that involve interacting with others.</p><h4>Business Skills</h4><p>Depending on your specific role in human resource management, budgeting and accounting skills might come in handy. Additionally, you might need knowledge of internal business processes and best practices specific to your organization or industry.</p><h4>Technology Skills</h4><p>Today, as companies and organizations rely more heavily on data and technology, digital literacy and data analytics experience will become standard for many human resource manager positions. This is key for many reasons, including online recruitment, remote employee engagement, and performance analysis.</p><p>As noted above, with the evolution of positions in HR management, the need for new skills and competencies will always arise.</p><h3><strong>Putting it All Together: How to Become a Human Resource Manager</strong></h3><p>The field of human resource management is broad, which means there are many paths you can take to break into your first HR job. Regardless of your journey - whether you’re new to the industry or are already in HR and looking to advance your career - there are two sure-fire things that will help: the right education and experience.</p><h4>Education for HR Managers</h4><p>As you scan HR manager job descriptions, you’ll see a bachelor’s degree is usually required for entry-level positions. An undergraduate business degree will certainly provide a good foundation; however, a program that offers a <a href="https://online.champlain.edu/degrees-certificates/undergraduate-certificate-human-resource-management">concentration</a> or <a href="https://online.champlain.edu/degrees-certificates/bachelors-human-resource-management">degree</a> in human resources management will give you an edge.</p><h4>Experience for HR Managers</h4><p>In addition to a human resource management degree or concentration, many positions require related work experience. If you’re changing careers, your previous professional will most likely count, especially if you’ve managed people or teams before - so use your work history as an advantage when creating your resume. If you’re brand new to the business world, you can gain experience in entry-level HR-related positions as you work your way into a human resource management job.</p><h4>Expanding Skills and Knowledge in HR</h4><p>Human resources is a field with plenty of growth opportunities, but advancing in your career and boosting your earning potential might require additional education or professional certification. Organizations like the Society of Human Resource Management (SHRM) offer continuing education and certification programs. You might also find opportunities to expand your knowledge of hiring, training, and managing people through industry-specific organizations, such as in healthcare, education, or consumer goods.</p><p>Some senior and executive positions might also prefer candidates with a master’s degree. If you see yourself moving up the ladder within your organization or pursuing a higher-level role at another company, a long-term goal for you might include earning a <a href="https://online.champlain.edu/degrees-certificates/all?type=1181">master’s degree in a business-related area</a> such as organization development, leadership, employment law, or even data science.</p><h3><strong>Getting an Online Degree in Human Resource Management</strong></h3><p>If you’re considering changing careers to become a human resource manager, you likely already have a full-time job. Attending a flexible, online program is a more convenient way to earn your degree in human resource management.</p><p>Champlain College Online’s new bachelor’s degree in HR management was designed with the working professional in mind. Whether you already have entry-level expertise in HR that you want to round out or you’re entering the field for the first time, Champlain’s online B.S. in human resource management will equip you with the skills needed to work in this growing industry.</p><p>Coursework covers talent acquisition, development, and retention; compensation and benefits; and workplace laws and regulation. Additionally, you’ll learn how to use HR data to inform decision-making and solve complex HR-related issues to successfully fulfill your organization’s talent vision. This program will shape you into a strategic partner with a systems-thinking mindset to attain organizational goals.</p><p>To learn more about earning your bachelor’s degree in HR management online at Champlain, <a href="https://online.champlain.edu/degrees-certificates/bachelors-human-resource-management">visit our program page</a>.</p>', 5, 1, N'/quiz-practicing-system/assets/images/posts/d513ede2-0378-484b-a81d-d7736e22fe22.jpg', N'What Does a Human Resource Manager Do? A human resource manager can work as a generalist or specialist. They might oversee an entire HR operation or manage a specific function within the department. Some HR managers have a bigger role in strategic planning, while others work heavily with data. The U.S. Department of Labor Bureau of Labor Statistics (BLS) provides career outlook stats for a range of human resource-related occupations.', 1)
INSERT [dbo].[Post] ([ID], [Title], [Date], [Content], [CategoryID], [UserID], [Thumbnail], [Brief], [Status]) VALUES (7, N'9 Skills You Need to Become a Back End Developer in 2022 (Part 2)', CAST(N'2022-06-14T15:37:47.537' AS DateTime), N'<h2><strong>The 9 Essential Skills of Back End Development and How to Learn Them</strong></h2><p>The majority of back end developers have at least some formal training, and many have bachelor’s degrees in computer science or advanced mathematics. As <a href="https://www.careerexplorer.com/careers/back-end-developer/how-to-become/"><strong>one writer for CareerExplorer explains</strong></a>, “Compared to front end developers, back end developers may have to do more abstraction — they could be data mining, writing abstract algorithms, and so forth.”&nbsp;</p><p>Though requirements vary between jobs, back end developers will need to have a passing familiarity with, if not command of, several technical languages and programs. These essential back end developer skills include but are not limited to: Python, Java, SQL, NoSQL, and Git.&nbsp;</p><p>There are countless routes an aspiring development professional can take to hone their skill set. As mentioned above, a four-year degree is a common option. However, if you’re unable to commit the time or funds necessary to complete an undergraduate study — or you already hold a degree in another field — you might want to consider alternative education solutions, such as a&nbsp;<a href="https://bootcamp.cvn.columbia.edu/coding/"><strong>coding boot camp to learn web development</strong></a>, which can teach the technical skills that back end developers need in a short period and at a reasonable cost.&nbsp;</p><p>At the end of the day, whichever educational route you take is less important than the technical and non-technical skills you acquire to build a career in back end development. Let’s now look at the 9 back end development skills in more depth.</p><h3><strong>1. Python</strong></h3><p>If you’re going to be a back end developer, you need to develop your Python skills. While it has nothing to do with the snake that inspired its name, Python is one of the premier languages used in development today. <a href="https://insights.stackoverflow.com/survey/2019"><strong>In 2019, a study conducted by Stack Overflow</strong></a> deemed Python the “fastest-growing major programming language” in the world above Java, and second only to Rust in likeability rankings. A full 41.7 percent of the report’s 90,000 respondents said that they used Python in 2018.&nbsp;</p><p>The language’s popularity is well-earned. It handles simple and complex web projects with equal ease and has been deployed across a variety of fields, from healthcare to finance to travel. Well-known industry giants such as Spotify, Instagram, Disqus, and Dropbox have all built their applications <a href="https://djangostars.com/blog/top-seven-apps-built-python/"><strong>using Python’s syntax</strong></a>.&nbsp;</p><p><a href="https://djangostars.com/blog/python-web-development/"><strong>The reason behind Python’s popularity is simple</strong></a>: It supports multiple programming styles and provides excellent data visualizations. With Python, developers can use procedural, functional, and object-oriented programming approaches with equal ease. The language’s support for expansive data libraries makes visualization easy and development speedy. Plus, Python is relatively easy to learn.</p><p><a href="https://djangostars.com/blog/python-web-development/"><strong>As a tech writer for Django Stars noted on the subject</strong></a>: “The simplicity of the syntax allows you to deal with intricate systems and ensure that all the elements have a clear relationship with each other. Thanks to this, more newbie coders can learn the language.”</p><p>Of all the skills that back end developers must have, Python tops the list.&nbsp;</p><h3><strong>2. Java</strong></h3><p>First, let’s get one thing out of the way: Java is <i>not</i> JavaScript. While the two have similar names — and did, during the Netscape era, briefly intersect — they are incredibly different. In fact, to <a href="https://www.seguetech.com/java-vs-javascript-difference/"><strong>borrow a quote from tech journalist David Diehl</strong></a>, “The evolution of the two languages took such wildly different paths from [Netscape] that the common joke is that Java is to JavaScript as ham is to a hamster.”</p><p>Simply put, <a href="https://www.geeksforgeeks.org/frontend-vs-backend/"><strong>Java is a general-purpose programming language</strong></a> for application development, while JavaScript is applied primarily to incorporate animation and interactivity into websites. The two do have similarities; both can run on a browser or server, for example. However, they are vastly different in their capabilities and execution.&nbsp;</p><p>Java is primarily intended for back end development. A more robust system, Java is typically written in an Integrated Development Environment (IDE) before being compiled in bytecode, or low-level code that can be read by a software interpreter, rather than human developers. JavaScript, in contrast, can typically be executed in its original syntax via a JavaScript engine. Generally, Java is capable of handling more robust programming tasks than its front end counterpart.&nbsp;</p><p>Java is an incredibly useful skill for back end developers; it’s a <a href="https://www.devteam.space/blog/why-should-you-use-java-for-your-backend-infrastructure/"><strong>high-performance language</strong></a> that supports object-oriented programming and can run in any system that supports a Java Virtual machine.</p><h3><strong>3. PHP</strong></h3><p>PHP, or Hypertext Preprocessor, is one of the most common and usable server-side languages in the development sector. Unlike Python or Java, <a href="https://www.guru99.com/what-is-php-first-php-program.html"><strong>PHP is a </strong><i><strong>scripting</strong></i><strong> language,</strong></a> which means that it interprets scripts — i.e., programming instructions — at runtime to automate routine processes or improve performance for an application.&nbsp;</p><p>According to statistics shared by Guru99, <a href="https://www.guru99.com/what-is-php-first-php-program.html"><strong>over 20 million websites and applications have been developed via PHP</strong></a>. This is for good reason: PHP is open-source, has a gentle learning curve, and is cost-effective because most web hosting servers already support the language by default. As a bonus, it also offers built-in support for the popular relational database MySQL (see below).&nbsp;</p><p>While PHP might not be a critical skill for back end developers if they know other major programming languages, having familiarity with it would certainly make an aspiring programmer more marketable.&nbsp;</p><h3><strong>4. SQL</strong></h3><p>Technology might get outmoded quickly, but SQL seems to be an exception to the rule. Since being <a href="https://www.whoishostingthis.com/resources/ansi-sql-standards/"><strong>ruled an industry-standard language by the American National Standards Institute (ANSI) in 1986</strong></a>, SQL, or Structured Query Language, has empowered back end developers to access and manipulate relational databases as needed. SQL allows programmers to insert and delete records easily, file queries against a database, create new tables and store procedures in a database, and even establish permissions on those tables and procedures. If you ever need to deal with relational databases — and if you’re a back end developer, there’s little doubt that you will — you need to know SQL.&nbsp;</p><p>If you want to familiarize yourself with the language, try an open-source platform like <a href="https://dev.mysql.com/doc/refman/8.0/en/what-is-mysql.html"><strong>MySQL</strong></a>. Named for creator Mondy Widenius’s daughter My and offered by Oracle, MySQL provides free access to SQL database source code. It’s also reasonably easy to use, as it can be installed on desktops and servers. It also runs on platforms including but not limited to Linux, Windows, and UNIX.&nbsp;</p><p>That said, SQL isn’t the only type of language used to manipulate databases. In 1998, developer Carl Strozz introduced the concept of a <a href="https://books.google.com/books/about/Strozzi_Nosql_RDBMS.html?id=HCJlLwEACAAJ"><strong>NoSQL language</strong></a>. NoSQL’s name is a point of contention for programmers. While some believe that the term stands for non-SQL, others insist that it means <i>not only </i>SQL. In any case, a NoSQL language communicates with databases that store information through means other than a relational table.&nbsp;</p><p>The decision to use SQL or NoSQL depends entirely on a developer’s needs. Both work well; <a href="https://www.mongodb.com/nosql-explained"><strong>as programmers for MongoDB explain in an overview</strong></a> on the subject, “A common misconception is that NoSQL databases or non-relational databases don’t store relationship data well. NoSQL databases can store relationship data — they just store it differently than relational databases do.”&nbsp;</p><p>As such, back end developers would do well to have both skills in their repertoire.&nbsp;</p><h3><strong>5. Git&nbsp;</strong></h3><p>If you’re looking for a widely used modern version control system to fit your needs, <a href="https://www.atlassian.com/git/tutorials/what-is-git"><strong>Git</strong></a> is a great option. It is actively maintained and open-sourced, created by the same founder of the Linux operating system. Each developer with a working copy of the code can easily access the full history of what changes have been made, making it easy to edit and restore the code. Among any back end developer skills list, Git is one of the most high-performing, flexible, and secure.</p><h3><strong>6, 7, 8: The Front End Trifecta</strong></h3><p>This list of back end developer skills would be incomplete without three very important programming languages: HTML, CSS, and JavaScript. While these languages are more often used on the front end, they’re still useful skills for back end developers. After all, the three determine everything a visitor sees, from a page’s text to images to scrolling drop-down menus.</p><p>So, what are these languages?</p><ul><li><a href="https://developer.mozilla.org/en-US/docs/Web/HTML"><i><strong>HTML</strong></i></a><i>: HTML is the most fundamental building block of the Internet. It determines the structure of web pages when working in conjunction with the other two languages.&nbsp;</i></li><li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS"><strong>CSS</strong></a>: This language determines how elements will be rendered on a webpage and standardizes display across all browsers.&nbsp;</li><li><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript"><strong>JavaScript</strong></a>: This language is most commonly used as a scripting language for web pages, but can also be used in non-browser elements, like Adobe Acrobat.&nbsp;</li></ul><p>The three languages work together for <a href="https://bootcamp.cvn.columbia.edu/blog/become-a-front-end-developer/"><strong>front end development</strong></a> and form the design of a website from the front end perspective. They provide direction and information on the style and content of a website, as well as how users interact with it.</p><p>You may be wondering why these front end skills are essential for a back end developer. The truth is, there isn’t a clear distinction between the two career paths. Even when developers focus on front or back end development exclusively, there will be some overlap in the <a href="https://bootcamp.cvn.columbia.edu/blog/how-to-become-web-developer/"><strong>skills required for both web development specialties</strong></a>. Moreover, companies <i>like</i> to hire developers who have multidisciplinary skills; that versatility is the major reason that cross-functional “full stack” developers can be so attractive in the hiring pool.&nbsp;</p><p>That said, not all companies will need multi-specialty programmers. While full stack developers can be useful for companies who have thin resources and can’t justify separate back and front end teams, many larger companies do have separate divisions for their developers and have a need for back end-specific professionals.&nbsp;</p><h3><strong>9. Communication&nbsp;</strong></h3><p>While there are many technical skills a back end developer needs, there are some useful non-technical ones as well, and communication ranks high on that list. Establishing well-honed communication skills means you’ll find it easier to collaborate, whether with other back end developers or with the front end developers who are working on the same projects. Being able to communicate clearly also helps when working with business leaders and other professionals who do not have the same background in development and programming.</p><h2><strong>A Final Note on Becoming a Back End Developer</strong></h2><p>A career in back end development is an excellent option for those who are interested in programming and are passionate about technology. As you work toward deciding what career path you want to take, you’ll also be deciding what your educational path looks like.</p><p>Remember, there are a variety of ways you can gain the back end developer skills you need to be successful. You can choose a formal degree program or opt for a <a href="https://bootcamp.cvn.columbia.edu/coding/"><strong>coding boot camp to learn web development</strong></a>. The best educational path depends on what your needs are, whether you’re looking for a variety of topics or want more specialized training that you can complete in a shorter period.</p>', 2, 1, N'/quiz-practicing-system/assets/images/posts/67fa88e0-6d7b-4977-aa60-051809a1817a.png', N'Back end developers are in hot demand, and there are some essential skills you''ll need to learn. With the world''s growing social and economic reliance on digital channels, employers have more need than ever for back end developers. Back end programmers are the wizards to the tech sector''s Oz. Web developers have an excellent job outlook. Bureau of Labor and Statistics projected that the profession would grow a remarkable 13 percent by 2028.

Global e-commerce retail sales will top $4.5 trillion by 2021, according to HubSpot''s research. The tech sector''s need for talented developers won''t fade anytime soon.', 1)
INSERT [dbo].[Post] ([ID], [Title], [Date], [Content], [CategoryID], [UserID], [Thumbnail], [Brief], [Status]) VALUES (8, N'Getting Started with CSS Grid', CAST(N'2022-06-17T09:26:08.137' AS DateTime), N'<p>Why is CSS Grid a big deal and why should we care?</p><p>Well, CSS Grid is the first real layout system for the web. It’s designed for organizing content both into columns and rows and it finally gives developers almost God-like control of the screens before us. That means that we can finally ditch decades of hacks and workarounds for setting elements on a web page – ultimately it means that complex layouts and beautifully typeset pages are now not only possible but <i>easy</i> and <i>maintainable</i>.</p><p>With CSS Grid, the web is about to become a much more beautiful place than what we’re accustomed to.</p><p>OK, but how does Grid work? There are a lot of complex tutorials out there that go into an awful lot of detail but I think we should start with the very basics. What we’ll be making here is a relatively simple type specimen, with a bunch of characters from an alphabet laid out on a page.</p><p>First up we’ll style those letters to use the right font-size and color and then we’ll center those letters in the divs with flexbox properties like align-items and justify-content. And yes, that’s right! CSS Grid doesn’t replace flexbox properties as much as it compliments what they already do. We can even use many of these properties in conjunction with CSS Grid. But for now let’s return to the demo:</p><p><img src="https://i0.wp.com/css-tricks.com/wp-content/uploads/2017/03/Screenshot-2017-03-12-00.31.26.png?ssl=1" alt=""></p><p>In the example above we have two simple divs sitting on top of one another because they’re default is display: block. Next up we’ll set our parent element to use Grid layout:</p><p>.wrapper { display: grid; }</p><p>Which will then lead to this:</p><p>Now you might see that nothing really happened. And you’d be right! Unlike setting display: inline-block; or display: inline;, it’s not entirely clear what happens when we set display to grid. In fact, to get our grid to actually <i>do</i> something we first need to feed it a certain number of columns or rows. In this example we’ll just align the letters next to each other into two columns:</p><p>.wrapper { display: grid; &nbsp;grid-template-columns: 1fr 1fr; &nbsp;grid-column-gap: 1px; &nbsp;background-color: black; }</p><p>Let’s break these new lines of code down. First we create two columns of our grid with grid-template-columns. That 1fr value might seem super weird if you’ve never seen it before but it’s a valid CSS unit that tells each column to be one fraction of our grid. In this instance, that means there will be two columns of equal width.</p><p>This will end up looking something like this:</p><p>Hooray! It works. But see that curious gap between the two columns? That’s the background of the wrapper peaking through each letter div and that’s because we’ve set the grid-column-gap property to 1px. Usually, we’d want to give a larger column-gap then that, especially if we’re aligning text blocks next to each other. But in this instance, a single pixel is good enough for us.</p><p>So what happens if we add two new letters to our markup? How will that change the layout?</p><p>&nbsp;</p><p>Well, technically it won’t change the grid at all – we’ve already told the grid to have two columns so those two letter divs are going to sit in place directly beneath the others and be exactly 1fr wide:</p><p>&nbsp;</p><p>Now here’s the weird thing – why isn’t there a 1px gap between letters A and C as well as between B and D? Well, grid-column-gap is only for columns and what we’ve effectively done here is create a new <i>row</i> in our grid. We’ll have to use grid-row-gap to see that change take effect:</p><p>.wrapper { &nbsp;grid-column-gap: 1px; &nbsp;grid-row-gap: 1px; &nbsp;<i>/* other styles go here */</i> &nbsp;<i>/* we could have also used the shorthand `grid-gap` */</i> }</p><p>And here’s what that looks like:</p><p>&nbsp;</p><p>We’ve created our very first grid. We’ve made a row and a column and all we’ve really had to do is change the markup. But let’s just explore our columns a little more. What would happen if we add another value to the grid-template-columns property? Like this:</p><p>.wrapper { grid-template-columns: 1fr 1fr 1fr; }</p><p>Well, we’d create another column of course! And notice how we can clearly see the background of the wrapper element now because there aren’t any children to fill that space:</p><p>&nbsp;</p><p>And if we change the value of a fr in that property then that would effectively create what’s known as an asymmetric grid. Let’s say that we wanted our first column in our grid to take up three times the amount of space as the other two columns:</p><p>.wrapper { grid-template-columns: 3fr 1fr 1fr; }</p><p>That would lead to the columns with <i>A</i> and <i>D</i> to be larger than the other two columns, just as we’d expect:</p><p>&nbsp;</p><p>Isn’t that powerful? No longer do we have to worry about negative margins or the perfect % value of a grid column to align things properly. We can make super complex grids without having to do any of the math that we would’ve been forced to do in the past. Now we just need to add a new value to the grid-template-columns property and voilá, a new grid column appears like magic!</p><p>But what about responsive grids, you might ask? Well that’s really just as simple as changing that property within a media query. Let’s say that we want 2 columns as our default grid size then at 500px we want 3 columns and finally, on larger screens, we’ll shift all that content into 4 columns. All we’d need to write is this:</p><p>.wrapper { &nbsp;display: grid; &nbsp;grid-template-columns: 1fr 1fr; } @media screen and (min-width: 500px) { &nbsp;.wrapper { &nbsp;grid-template-columns: 1fr 1fr 1fr; &nbsp;} } @media screen and (min-width: 800px) { &nbsp;.wrapper { grid-template-columns: 1fr 1fr 1fr 1fr; &nbsp;} }</p><p>Make sure to <a href="http://codepen.io/robinrendle/pen/Npjzyz?editors=1100">open up this demo</a> in a new tab and change the size of the viewport to see the responsive magic happen!</p><p><img src="https://i0.wp.com/css-tricks.com/wp-content/uploads/2017/03/media-query-grid.gif?ssl=1" alt=""></p><p>So the grid-template-columns property is a lot more complicated than what I’ve shown here but this is a great starting point. Next up we ought to learn about the real, life-changing property in the CSS Grid spec: grid-template-rows.</p><p>Ok, let’s go into it blind. In the small bit of code below, and with what we’ve learned so far about Grid, let’s figure out what this new property might do:</p><p>.wrapper { display: grid; &nbsp;grid-template-columns: 3fr 1fr 1fr; grid-template-rows: 1fr 3fr; }</p><p>Instead of setting the width of columns and their relationship with one another, we’re now going to set the height of rows and their relationship. So if we have two rows like in our previous demo and the last unit is set to 3fr then that means the second row will always be three times the height of the first:</p><p>&nbsp;</p><p>This might look pretty simple yet previously we’ve never really been able to do this. We’ve always had to write gross hacks like setting a min-height on a specific element or changing a class name. But we’ve never been able to create <i>relationships</i> between rows like this before; that’s what makes CSS Grid so powerful.</p><p>With this tiny bit of knowledge and a handful of new properties, we can create fabulously complex layouts – asymmetric and responsive grids being just one small part of them. And so far this has only been a glimpse into the monstrous CSS Grid spec, as there’s an awful lot to cover. But I think that Jen Simmons described it best when <a href="http://jensimmons.com/post/feb-28-2017/benefits-learning-how-code-layouts-css">she wrote about Grid</a>:</p><blockquote><p><i>We need to explore CSS Grid until we understand what it wants to do, what it can be forced into doing, and what it refuses to do. Many designers may not ever learn to code CSS, but you need to understand CSS well enough to understand our artistic medium.</i></p></blockquote><p>And sure, all the code above looks very strange at first. But what it means is that we don’t have to use giant CSS frameworks and also a whole bunch of layout hacks are now completely irrelevant. But what really excites me most about Grid is that it compels us to see the space inside a browser in a completely new way.</p><p>We’ll have to not only learn a bunch of new properties, but we’ll also have to entirely rethink what we’ve learned in the past. So CSS Grid is not just a spec but a strange philosophy unto itself.</p><p>Let’s figure it out together!</p>', 1, 1, N'/quiz-practicing-system/assets/images/posts/f340d050-c6dc-40f0-bfe5-8dc271608242.jpg', N'With CSS Grid, the web is about to become a much more beautiful place. The first real layout system for the web, it''s designed for organizing content into columns and rows. It finally gives developers almost God-like control of the screens before us. And it means complex layouts and beautifully typeset pages are now possible.', 1)
INSERT [dbo].[Post] ([ID], [Title], [Date], [Content], [CategoryID], [UserID], [Thumbnail], [Brief], [Status]) VALUES (9, N'What Is Skim Pricing? And Is it Right For Your Business?', CAST(N'2022-06-17T09:34:58.330' AS DateTime), N'<p>Discover skim pricing, how it works, and how to decide if it’s the right pricing strategy for your business.</p><p><img src="https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/https://images.ctfassets.net/wp1lcwdav1p1/2vdMnuAlkiEnprgAMbv68X/b93fd62060707d43ca962f312d658609/SI3Oykm4.png?w=1500&amp;h=680&amp;q=60&amp;fit=fill&amp;f=faces&amp;fm=jpg&amp;fl=progressive&amp;auto=format%2Ccompress&amp;dpr=1&amp;w=1000&amp;h=" alt="[Featured Image] A woman is presenting pie charts. " srcset="https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/https://images.ctfassets.net/wp1lcwdav1p1/2vdMnuAlkiEnprgAMbv68X/b93fd62060707d43ca962f312d658609/SI3Oykm4.png?w=1500&amp;h=680&amp;q=60&amp;fit=fill&amp;f=faces&amp;fm=jpg&amp;fl=progressive&amp;auto=format%2Ccompress&amp;dpr=2&amp;w=1000&amp;h= 2x, https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/https://images.ctfassets.net/wp1lcwdav1p1/2vdMnuAlkiEnprgAMbv68X/b93fd62060707d43ca962f312d658609/SI3Oykm4.png?w=1500&amp;h=680&amp;q=60&amp;fit=fill&amp;f=faces&amp;fm=jpg&amp;fl=progressive&amp;auto=format%2Ccompress&amp;dpr=3&amp;w=1000&amp;h= 3x" sizes="100vw" width="100%"></p><h2><strong>What is skim pricing?</strong></h2><p>Skim pricing, also known as price skimming, is a pricing strategy that sets new product prices high and subsequently lowers them as competitors enter the market. Skim pricing is the opposite of penetration pricing, which prices newly launched products low to build a big customer base at the outset.</p><p>Businesses adopt a skim pricing model for several purposes, including:</p><p>Generating high short-term profit</p><p>Attracting early adopters</p><p><a href="https://www.coursera.org/articles/customer-segmentation">Segmenting customers</a> as the price drops, according to what they are willing to pay for a new product</p><p>Big brands like Apple and Nike tend to do well with price skimming and provide excellent price skimming examples to examine:</p><p>Apple periodically introduces new iPhones with the latest features at a high price, attracts price-insensitive customers who value having the latest device to hit stores, and then sells them at lower prices to price-sensitive customers as newer versions are introduced.</p><p>Nike, an athletic apparel market leader, regularly introduces new designs at higher prices, relying on early adopters and loyal customers to purchase products at the introductory price. These prices can last for several months before Nike lowers the cost to sell remaining inventory to price-sensitive customers.&nbsp;&nbsp;</p><h2><strong>Advantages and disadvantages of skim pricing</strong></h2><p>As you consider skim pricing for your business, consider this strategy’s advantages and disadvantages:</p><p>&nbsp;</p><figure class="table"><table><thead><tr><th><strong>Potential advantages</strong></th><th><strong>Potential disadvantages</strong></th></tr></thead><tbody><tr><td>Attracting early adopters whoe value having the latet products</td><td>Difficulty justifying initial high price</td></tr><tr><td>Generating revenue quickly</td><td>Difficulty entering a crowded market</td></tr><tr><td>Reaching the break-even point with fewer sales</td><td>Attracting competitors who offer similiar products at a lower price</td></tr><tr><td>Associating high-priced products with quality</td><td>Alienating early adopters who see others purchasing a product at a lower price than they paid</td></tr><tr><td>Offering retailers a higher initial profit margin</td><td>Being seen by consumers as pricing products unethically</td></tr><tr><td>Earning maximum profits from different customer segments as price drops</td><td>Creating a higher customer churn rate</td></tr></tbody></table></figure><p>&nbsp;</p><h2><strong>4 signs price skimming is right for your business&nbsp;</strong></h2><p>In addition to knowing the potential advantages and disadvantages of skim pricing, it’s helpful to evaluate your products and look for the four signs below before committing to a price skimming strategy.</p><p>&nbsp;</p><h3><strong>1. Your market is not (yet) crowded with competitors.</strong></h3><p>&nbsp;</p><p>Price skimming is generally not a viable strategy in a crowded market, as consumers will have their pick of comparable products at competitive prices. Examine your industry and the market segments you are targeting for opportunities to introduce new products at an initial high price.&nbsp;</p><p>Is your product among the first (if not <i>the </i>first) of its kind?</p><p>What do similar brands offer?</p><p>How might you market your products to price-insensitive early adopters?</p><p>&nbsp;</p><h3><strong>2. You are launching an innovative product.</strong></h3><h3><strong>&nbsp;</strong></h3><p>As with the Apple and Nike examples we explored earlier, you may be able to succeed with a price skimming model if you are launching a product that consumers perceive as innovative and an indispensable must-have.</p><p>What are your product’s unique features?</p><p>What makes it one-of-a-kind and the result of careful research and development?</p><p>How can customers use it in ways that truly make a difference in their lives?</p><p>How can you ensure that the product’s quality surpasses what’s currently available?</p><p>What makes it difficult for competitors to emulate?</p><h3>&nbsp;</h3><h3><strong>3. Consumers in your target market are willing to pay a higher price.</strong></h3><p>Conduct market research and review your current customer base to learn more about potential price-insensitive early adopters in your market segment. You may be able to leverage their must-have mentality.</p><p>Have segments of your current customer base become repeat buyers and thus loyal to your brand?</p><p>Do they perceive your brand as offering higher value than other brands?</p><p>Do these consumers behave like early adopters and take pride in being the first to get the latest products on the market?</p><p>Do they tend to expect higher prices?</p><p>&nbsp;</p><h3><strong>4. Your demand curve is inelastic.</strong></h3><h3><strong>&nbsp;</strong></h3><p>When price changes do not affect the demand for a product, it’s called an inelastic demand curve. In other words, the need for your product would stay the same whether you lower or raise its price. Examples of such products include gasoline or toilet paper.</p><p>&nbsp;</p><p>Here are some factors that may help you determine your product’s demand curve:</p><p><strong>Consumers’ budgets:</strong> If buying your product would consume a large portion of a consumer’s budget, price changes would have a greater effect on demand. (Elastic)</p><p><strong>Competition:</strong> If consumers have fewer products like yours to choose from and no viable substitutes, price changes would have less effect on demand. (Inelastic)</p><p>&nbsp;</p><p><strong>Necessity: </strong>If consumers view a product as necessary, such as a lifesaving medication, price changes will likely not affect demand. (Inelastic)</p><p>&nbsp;</p><h2><strong>Key takeaways</strong></h2><p>Skim pricing (or price skimming) can be highly effective for businesses that offer innovative products and have a system to attract price insensitive customers. It’s critical to research the market and consumer demand as you explore pricing strategies and choose the one that makes the most sense for your business goals.</p>', 5, 1, N'/quiz-practicing-system/assets/images/posts/328e7b00-3e9e-4f33-aa90-7db3aeda2670.jpeg', N'Skim pricing, also known as price skimming, is a pricing strategy that sets new product prices high and subsequently lowers them as competitors enter the market. Businesses adopt a skim pricing model for several purposes, including:Generating high short-term profit. Attracting early adopters and loyal customers. In addition to knowing the potential advantages and disadvantages of price skimming, it''s helpful to evaluate your products and look for the four signs below. Price skimming may be a viable strategy if you are launching an innovative product that consumers perceive as a must-have.', 1)
SET IDENTITY_INSERT [dbo].[Post] OFF
GO
SET IDENTITY_INSERT [dbo].[PricePackage] ON 

INSERT [dbo].[PricePackage] ([ID], [Name], [Duration], [Status], [PriceRate], [Description]) VALUES (2, N'1 month', 1, 0, 100, N'1 month')
INSERT [dbo].[PricePackage] ([ID], [Name], [Duration], [Status], [PriceRate], [Description]) VALUES (3, N'3 month', 3, 1, 85, N'3 months')
INSERT [dbo].[PricePackage] ([ID], [Name], [Duration], [Status], [PriceRate], [Description]) VALUES (7, N'6 months', 6, 1, 75, N'6 months')
INSERT [dbo].[PricePackage] ([ID], [Name], [Duration], [Status], [PriceRate], [Description]) VALUES (8, N'Lifetime', 18, 1, 65, N'For Life')
SET IDENTITY_INSERT [dbo].[PricePackage] OFF
GO
SET IDENTITY_INSERT [dbo].[Question] ON 

INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (1, N'1.	All keywords in C are in', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (2, N'2.	Which data type is most suitable for storing a number 65000 in a 32-bit system?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (3, N'3.	What is short int in C programming?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (4, N'4.	Which is correct with respect to size of the datatypes?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (5, N'5.	Relational operators cannot be used on:', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (6, N'6.	Which expression has to be present in the following?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (7, N'7.	Which of the following is NOT a good characteristic well written of a software requirements specification?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (8, N'8.	Which is not an obstacle to effective knowledge acquisition?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (9, N'9.	As requirements are elicited, what source is most likely to impose previously unidentified user ', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (10, N'10.	What is a software requirements specification (SRS) document?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (11, N'11.	Which is not an artefact-driven elicitation technique?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (12, N'12.	Which is NOT the type of internal quality?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (13, N'13.	Which of the following may appear on the left-hand side of an instanceof operator?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (14, N'14.	Which of the following statements is true?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (15, N'15.	True or false: If class Y extends class X, the two classes are in different packages, and class X has a protected method called abby(), then any instance of Y may call the abby() method of any other instance of Y', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (16, N'16.	A CPU may have multiple execution units, so that can carry out multiple instructions in the same ', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (17, N'17.	Information that must be saved prior to the processor transferring control to the interrupt handler ', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (18, N'18.	Critical Region (Section) concept used in interprocess communication is:', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (19, N'19.	Which of the following statements is true about hardware solution to the critical region problem?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (20, N'20.	The first-come, first-served (FCFS) algorithm is fine for most ____ systems', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (21, N'21.	One of the most important innovations of demand paging was that it made ____ feasible', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (22, N'22.	A system with 32 bit virtual address. If the page size is 4 KB and each table entry occupies 4 bytes, what is the size of the page table?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (23, N'23.	Which of the following statements about segmentation is false?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (24, N'24.	Which of the following are included in an exam blueprint?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (25, N'25.	Which of the following are included in the CertNexus Candidate Agreement?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (26, N'26.	Which of the following defines the AI black box problem?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (27, N'27.	At what point should ethical consideration ideally be applied to emerging technologies?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (28, N'28.	Which of the following is the generally agreed upon current state of the art of AI?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (29, N'29.	Which of the following describes the concept of liability?', 1, 1, 1, 1)
INSERT [dbo].[Question] ([ID], [Content], [TopicID], [TopicSubjectID], [LevelID], [status]) VALUES (30, N'30.	Which of the following is a type of technology contract that establishes the goals of both parties and describes how those goals will be achieved?', 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Question] OFF
GO
SET IDENTITY_INSERT [dbo].[Quiz] ON 

INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (1, 4, N'Easy', CAST(N'00:40:00' AS Time), 80, N'', N'Practice', NULL, N'ok')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (2, 4, N'Medium', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, NULL)
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (3, 4, N'Hard', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, NULL)
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (4, 4, N'Medium', CAST(N'00:30:00' AS Time), 80, N'', N'Exam', NULL, NULL)
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (5, 4, N'Easy', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, NULL)
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (6, 4, N'Medium', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, NULL)
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (7, 4, N'Hard', CAST(N'00:20:00' AS Time), 90, N'', N'Exam', NULL, NULL)
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (8, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (9, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (10, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (11, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (12, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (13, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (14, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (15, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (16, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (17, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (18, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (19, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (20, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (21, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (22, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (23, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (24, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (25, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (26, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (27, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (28, 1, N'Medium', CAST(N'00:14:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (29, 1, N'Medium', CAST(N'00:55:00' AS Time), 60, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (30, 1, N'Medium', CAST(N'00:55:00' AS Time), 60, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (31, 1, N'Medium', CAST(N'00:55:00' AS Time), 60, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (32, 1, N'Medium', CAST(N'00:55:00' AS Time), 60, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (33, 1, N'Medium', CAST(N'00:55:00' AS Time), 50, N'ok', N'Practice', NULL, N'test')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (34, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (35, 2, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (36, 2, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (37, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (38, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (39, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (40, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (41, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (42, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (43, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (44, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (45, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (46, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (47, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (48, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (49, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (50, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
INSERT [dbo].[Quiz] ([ID], [SubjectSID], [Level], [Duration], [PassRate], [Description], [Type], [CreateBy], [Name]) VALUES (51, 1, N'', CAST(N'00:00:00' AS Time), 0, N'', N'Practice', NULL, N'')
SET IDENTITY_INSERT [dbo].[Quiz] OFF
GO
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 1, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 2, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 3, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 5, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 11, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 12, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 21, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 23, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 25, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 28, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 29, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (1, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (2, 38, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (8, 1, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (8, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (8, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (8, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (8, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (8, 36, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (8, 39, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (8, 52, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (8, 53, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (8, 55, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (9, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (9, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (9, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (9, 11, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (9, 12, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (9, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (9, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (9, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (9, 28, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (9, 32, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (10, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (10, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (10, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (10, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (10, 36, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (10, 37, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (10, 41, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (10, 46, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (10, 56, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (10, 62, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 1, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 2, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 3, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 5, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 11, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 12, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 21, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 23, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 25, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 28, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 29, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 32, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 42, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 43, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 50, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 53, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 57, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (11, 59, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 1, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 2, NULL)
GO
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 3, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 5, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 11, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 12, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 21, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 23, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 25, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 28, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 29, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 33, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 35, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 38, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 61, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (12, 62, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (13, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (13, 31, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (13, 32, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (13, 33, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (13, 37, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (13, 47, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (13, 52, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (13, 55, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (13, 58, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (13, 60, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (14, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (14, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (14, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (14, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (14, 33, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (14, 52, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (14, 53, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (14, 56, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (14, 59, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (14, 61, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (15, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (15, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (15, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (15, 25, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (15, 32, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (15, 35, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (15, 45, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (15, 50, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (15, 57, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (15, 59, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (16, 2, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (16, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (16, 5, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (16, 12, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (16, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (16, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (16, 33, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (16, 34, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (16, 44, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (16, 63, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (17, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (17, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (17, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (17, 37, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (17, 42, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (17, 45, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (17, 49, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (17, 58, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (17, 61, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (17, 63, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (18, 2, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (18, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (18, 11, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (18, 23, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (18, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (18, 32, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (18, 34, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (18, 42, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (18, 45, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (18, 59, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 1, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 2, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 5, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 12, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 13, NULL)
GO
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 28, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 31, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 32, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 34, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 43, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 49, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 55, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (19, 60, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 3, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 5, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 12, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 23, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 34, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 37, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 57, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (20, 63, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 2, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 21, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 25, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 28, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 49, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (21, 59, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 1, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 2, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 3, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 23, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 28, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 29, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 32, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 38, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 39, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 41, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 53, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 56, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (22, 60, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (23, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (23, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (23, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (23, 31, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (23, 32, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (23, 37, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (23, 43, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (23, 60, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (23, 61, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (23, 62, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (24, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (24, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (24, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (24, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (24, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (24, 37, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (24, 51, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (24, 52, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (24, 53, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (24, 54, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (25, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (25, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (25, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (25, 29, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (25, 32, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (25, 34, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (25, 40, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (25, 44, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (25, 45, NULL)
GO
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (25, 52, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (26, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (26, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (26, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (26, 31, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (26, 41, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (26, 49, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (26, 54, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (26, 56, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (26, 60, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (26, 63, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (27, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (27, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (27, 21, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (27, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (27, 31, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (27, 43, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (27, 45, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (27, 49, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (27, 56, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (27, 63, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (28, 2, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (28, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (28, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (28, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (28, 21, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (28, 23, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (28, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (28, 35, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (28, 44, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (28, 49, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 5, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 11, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 12, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 21, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 23, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 25, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 28, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 29, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 31, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 32, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 33, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 34, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 35, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 36, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 37, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 38, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 39, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 40, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 41, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 42, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 43, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 44, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 45, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 46, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 47, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 48, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 49, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 50, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 51, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 52, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 53, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 54, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 55, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 56, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 57, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 58, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 59, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 60, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 61, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 62, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (30, 63, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 5, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 11, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 12, NULL)
GO
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 21, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 23, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 25, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 28, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 29, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 31, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 32, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 33, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 34, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 35, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 36, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 37, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 38, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 39, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 40, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 41, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 42, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 43, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 44, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 45, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 46, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 47, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 48, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 49, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 50, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 51, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 52, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 53, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 54, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 55, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 56, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 57, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 58, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 59, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 60, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 61, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 62, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (31, 63, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 1, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 3, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 11, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 25, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 29, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 34, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 39, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 55, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 56, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (32, 63, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (33, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (33, 11, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (33, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (33, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (33, 32, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (33, 36, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (33, 47, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (33, 61, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (38, 1, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (38, 2, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 1, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 2, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 3, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 5, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 11, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 12, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 18, NULL)
GO
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 21, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 23, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 25, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 28, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 29, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (40, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (42, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (42, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (42, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (42, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (42, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 1, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 2, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 3, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 5, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 11, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 12, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 21, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 23, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 25, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 28, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 29, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (45, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (46, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (46, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (46, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (46, 21, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (46, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (47, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (47, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (47, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (47, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (48, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (48, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (48, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (48, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (49, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (49, 12, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (49, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (49, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 1, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 2, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 3, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 4, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 5, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 7, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 8, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 10, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 11, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 12, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 13, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 14, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 15, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 16, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 17, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 19, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 20, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 21, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 22, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 23, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 24, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 25, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 26, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 27, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 28, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 29, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (50, 30, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (51, 6, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (51, 9, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (51, 18, NULL)
INSERT [dbo].[Quiz_Question] ([QuizID], [QuestionID], [Explanation]) VALUES (51, 27, NULL)
GO
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (1, 1, CAST(N'2022-07-11T09:29:44.453' AS DateTime), CAST(N'2023-01-11T09:29:44.453' AS DateTime), 7, 75, 80.0000)
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (1, 3, CAST(N'2022-07-16T14:55:49.207' AS DateTime), NULL, 8, 65, 120.0000)
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (4, 1, CAST(N'2021-05-23T00:00:00.000' AS DateTime), CAST(N'2021-08-23T00:00:00.000' AS DateTime), 3, 85, 80.0000)
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (4, 2, CAST(N'2021-06-30T00:00:00.000' AS DateTime), CAST(N'2021-09-30T00:00:00.000' AS DateTime), 3, 85, 100.0000)
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (4, 3, CAST(N'2022-07-19T16:46:26.450' AS DateTime), CAST(N'2022-10-19T16:46:26.450' AS DateTime), 3, 85, 250.0000)
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (4, 4, CAST(N'2022-07-29T00:00:00.000' AS DateTime), CAST(N'2024-07-29T00:00:00.000' AS DateTime), 7, 75, 100000.0000)
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (5, 3, CAST(N'2021-11-20T00:00:00.000' AS DateTime), CAST(N'2022-02-20T00:00:00.000' AS DateTime), 3, 85, 120.0000)
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (5, 5, CAST(N'2022-05-01T00:00:00.000' AS DateTime), CAST(N'2021-08-01T00:00:00.000' AS DateTime), 3, 85, 70.0000)
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (5, 6, CAST(N'2021-01-10T00:00:00.000' AS DateTime), CAST(N'2021-04-10T00:00:00.000' AS DateTime), 3, 85, 150.0000)
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (6, 1, CAST(N'2021-03-23T00:00:00.000' AS DateTime), CAST(N'2021-06-23T00:00:00.000' AS DateTime), 3, 85, 80.0000)
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (6, 2, CAST(N'2021-03-23T00:00:00.000' AS DateTime), CAST(N'2021-06-23T00:00:00.000' AS DateTime), 3, 85, 100.0000)
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (10, 1, CAST(N'2022-07-13T10:33:59.293' AS DateTime), CAST(N'2023-01-13T10:33:59.293' AS DateTime), 7, 75, 80.0000)
INSERT [dbo].[Register] ([UserID], [SubjectID], [DateStart], [DateEnd], [PricePackageID], [PriceRate], [SalePrice]) VALUES (10, 2, CAST(N'2022-07-13T10:34:28.637' AS DateTime), CAST(N'2023-01-13T10:34:28.637' AS DateTime), 7, 75, 100.0000)
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([ID], [Name]) VALUES (1, N'Admin')
INSERT [dbo].[Role] ([ID], [Name]) VALUES (2, N'Sale')
INSERT [dbo].[Role] ([ID], [Name]) VALUES (3, N'Marketing')
INSERT [dbo].[Role] ([ID], [Name]) VALUES (4, N'Customer')
INSERT [dbo].[Role] ([ID], [Name]) VALUES (5, N'Expert')
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Slider] ON 

INSERT [dbo].[Slider] ([ID], [Title], [Image], [Content], [UserID], [Backlink], [Status]) VALUES (1, N'New subjects', N'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQUatiJzhc1Zy328PbD8-HMMGEXGcmGELthAw&usqp=CAU', N'<p>New subject with many quiz for practice</p>', 1, N'https://gitlab.com/g6925/quiz-practicing-system', 1)
INSERT [dbo].[Slider] ([ID], [Title], [Image], [Content], [UserID], [Backlink], [Status]) VALUES (2, N'Sale', N'http://icdn.dantri.com.vn/zoom/1200_630/2019/04/20/sales-hay-su-lua-doi-ngot-ngao-1-1555721239970.jpg', N'Sale up to 40% all subject', 3, NULL, 1)
INSERT [dbo].[Slider] ([ID], [Title], [Image], [Content], [UserID], [Backlink], [Status]) VALUES (3, N'Java', N'https://timviec365.com/pictures/news/2021/06/15/lls1623752136.jpg', N'Java update new version', 3, NULL, 1)
SET IDENTITY_INSERT [dbo].[Slider] OFF
GO
SET IDENTITY_INSERT [dbo].[Subject] ON 

INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (1, N'https://bizflyportal.mediacdn.vn/thumb_wm/1000,100/bizflyportal/images/htm16157919239459.jpg', 1, N'Build Responsive Real-World Websites with HTML and CSS', N'HTML', N'<p><i><strong>*** The #1 bestselling HTML and CSS course on Udemy! ***</strong></i></p><p><i><strong>*** Completely re-built from scratch in July 2021 (35+ hours video) ***</strong></i></p><p><i>"Having gone through other related courses on other platforms, I can say this course is the most practical and readily applicable course on web design and development I have taken." — Bernie Pacis</i></p><p>&nbsp;</p><p>Open a new browser tab, type in <i><strong>www.omnifood.dev</strong></i>, and take a look around. I will wait here...</p><p>...</p><p>Amazing, right? What if you knew exactly how to design and build a website like that, completely from scratch? How amazing would that be?</p><p>Well, I''m here to teach you<strong> HTML, CSS, and web design, all by building the stunning website that you just saw</strong>, step-by-step.</p><p>So, after finishing this course, you will know exactly how to build a beautiful, professional, and ready-to-launch website just like Omnifood, by following a 7-step process. And it will even look great on any computer, tablet, and smartphone.</p><p>But what if you want to build a completely different website? Well, no problem! I designed the course curriculum with exactly this goal: to <strong>enable you to design and build any website that you can think of, not just copy the course project</strong>.</p><p>&nbsp;</p><p><strong>So, in order to become a confident and independent developer, capable of building your own websites in the future, you will learn:</strong></p><p>The fundamentals of modern and semantic HTML, CSS, and building layouts in a small separate project, which will prepare you for the main course project (<i>www.omnifood.dev</i>). This includes modern flexbox and CSS Grid!</p><p>How to design beautiful websites, by learning a web design framework I created just for this course. It consists of easy-to-use guidelines for design aspects like typography, colors, images, spacing, and more (this is like a small standalone course!).</p><p>How to use well-established website components and layout patterns in order to come up with professional-looking designs</p><p>How to make any website work on any mobile device, no matter the design and layout (responsive design)</p><p>How to use the 7 steps of building a professional website in practice: planning, sketching, designing, building, testing, optimizing, and launching</p><p>How to find and use free design assets such as images, fonts, and icons</p><p>Important developer skills such as reading documentation, fixing code errors on your own, and using professional web development tools</p><p><strong>Does this sound like fun? Then join me and 200,000+ other developers and start building websites today!</strong></p><p><i>Or are you not sold yet and need to know more? No problem, just keep reading...</i></p><p>&nbsp;</p><p><strong>[01] Why should you learn HTML and CSS in the first place?</strong></p><p>Building websites allows you to do <strong>fun and creative work, from anywhere in the world, and it even pays well</strong>. Web development is one of the <strong>most future-proof and highest-paying industries</strong> in the world. And HTML and CSS is the entry point to this world!</p><p>But you already know all this, that''s why you want to learn HTML and CSS too. Well, you came to the right place!</p><p>This is the best and most complete course for starting your web development journey that you will find on Udemy. It''s an <strong>all-in-one package</strong> that takes you from knowing nothing about HTML and CSS, to building beautiful websites using tools and technologies that professional web developers use every single day.</p><p>&nbsp;</p><p><strong>[02] Why is this course so unique and popular?</strong></p><p><i><strong>Reason #1: The course is completely project-based</strong></i></p><p>Simple demos are boring, and therefore you''re gonna learn everything by <strong>building actual projects</strong>! In the final project (<i>www.omnifood.dev</i>), together we hand-code a beautiful and responsive landing page for a <strong>fictional company</strong> that I made up just for the course.</p><p>&nbsp;</p><p><i><strong>Reason #2: You will not just learn how to code</strong></i></p><p>Coding is great, but it''s not everything! That''s why we will go through the entire <strong>7-step process</strong> of building and launching our website project.</p><p>So the huge Omnifood project will teach you <strong>real-world skills to build real-world HTML and CSS websites</strong>: how to plan projects and page layouts, how to implement designs using HTML and CSS techniques, how to write clean and organized code, how to optimize websites for good speed performance, and many more.</p><p>On top of that, this course has a <strong>huge focus on beautiful design</strong>. In fact, this is the only course on the market that focuses on both coding and designing, together.</p><p>&nbsp;</p><p><i><strong>Reason #3: I''m the right teacher for you</strong></i></p><p>With the right guidance and a well-structured curriculum, building websites can be <strong>quite easy and fun to learn</strong>. With a bunch of random tutorials and YouTube videos? Not so much. And that''s where I come in.</p><p>My name is Jonas, I''m an experienced web developer and designer, and one of Udemy''s top instructors. I have been teaching this bestselling course since 2015 to over 200,000 developers, always listening to feedback and understanding exactly how students actually learn.</p><p>Using that feedback, I recently rebuilt this course from scratch and <strong>designed the ideal course curriculum</strong> for every type of student. It''s a <strong>unique blend</strong> of projects, deep explanations, theory lectures, and challenges. I''m sure you''re gonna love it!</p><p>&nbsp;</p><p><strong>[03] Why is this course so long?</strong></p><p><strong>Reason #1:</strong> I take time to <strong>explain every single concept</strong> that I teach, so that you actually learn, and not just copy code from the screen (<strong>this is a course</strong>, not a tutorial)</p><p><strong>Reason #2:</strong> I go into topics that other HTML and CSS courses shy away from: professional web design, component and layout patterns, some CSS theory, website planning in theory and practice, and developer skills. I believe you need all of this to be successful!</p><p><strong>Reason #3:</strong> There is a lot of repetition, so that you actually <strong>assimilate and practice</strong> what you learn. Practice is the single most important ingredient to learning, and therefore I provide plenty of opportunities for you to sharpen your skills</p><p>&nbsp;</p><p><strong>[04] Here is what''s also included in the package:</strong></p><p>Up-to-date HD-quality videos, that are easy to search and reference (great for Udemy Business students)</p><p>Professional English captions (not the&nbsp;auto-generated ones)</p><p>Downloadable design assets + starter code and final code for each section</p><p>Downloadable slides for 20+ theory videos (not boring, I promise!)</p><p>Access to countless free design and development resources that I curated over many years</p><p>Free support in the course Q&amp;A</p><p>10+ coding challenges to practice your new skills (solutions included)</p><p>&nbsp;</p><p><strong>[05] This course is for you if...</strong></p><p>... you are a complete beginner with no idea of how to build a website.</p><p>... you already know a bit of HTML and CSS from some tutorials or videos, but struggle to put together a good-looking, complete website.</p><p>... you are a designer and want to expand your skills into HTML and CSS, because all your designer friends are learning how to code (they are smart!).</p><p>Basically, if you are an HTML and CSS master, and build the most beautiful websites in the world, then DON''T take the course. Everyone else: you should take the course, today.</p><p>&nbsp;</p><p><i><strong>So, does all of this sound great? Then join me and 200,000+ other developers and start your web development journey today!</strong></i></p>', 1, 0, CAST(N'2022-05-01T00:00:00.000' AS DateTime), 1, 100.0000, 80.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (2, N'https://tmarketing.vn/wp-content/uploads/2021/09/css.jpeg', 1, N'CSS - The Complete Guide 2022 (incl. Flexbox, Grid & Sass)', N'CSS', N'CSS is the language we use to style a Web page.', 1, 1, CAST(N'2022-05-05T00:00:00.000' AS DateTime), 1, 123.0000, 100.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (3, N'https://blog.vinahost.vn/wp-content/uploads/2021/07/php-Extensions.png', 1, N'PHP Full Stack Web Developer Bootcamp', N'PHP', N'PHP code is executed on the server.', 2, 0, CAST(N'2022-05-02T00:00:00.000' AS DateTime), 1, 140.0000, 120.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (4, N'https://hotmart.com/media/2020/03/BLOG_web-design-1.png', 1, N'Web Design for Everybody: Basics of Web Development & Coding', N'Web', N'This Specialization covers how to write syntactically correct HTML5 and CSS3, and how to create interactive web experiences with JavaScript. Mastering this range of technologies will allow you to develop high quality web sites that, work seamlessly on mobile, tablet, and large screen browsers accessible.', 8, 0, CAST(N'2022-05-04T00:00:00.000' AS DateTime), 1, 80.0000, 50.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (5, N'https://nordiccoder.com/app/uploads/2019/08/26-nodejs.png', 1, N'Modern Application Development with Node.js on AWS', N'Web', N'This specialization is designed to help you master the skills of designing and building cloud-native applications on AWS', 4, 1, CAST(N'2022-05-03T00:00:00.000' AS DateTime), 1, 99.0000, 70.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (6, N'https://huongvnq.github.io/2021/08/22/crud-nodejs/nodejs.png', 1, N'Build a Node Server backend with Express', N'Web', N'By the end of this project, you will Build a Node Server backend with Express that will fetch data from a MongoDB database.', 6, 1, CAST(N'2022-06-01T00:00:00.000' AS DateTime), 1, 200.0000, 150.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (7, N'https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/https://s3.amazonaws.com/coursera-course-photos/78/7409f0136711e8825e05325ee34918/icon-mkt.png?auto=formathttps://vn4u.vn/wp-content/uploads/2021/12/digital-marketing-la-gi-1.jpg', 1, N'The Complete Digital Marketing Course - 12 Courses in 1', N'Marketing', N'Aprenda a desenvolver a estratégia de marketing digital para a sua empresa ou startup, nesse curso você irá aprender sobre os principais pontos do Marketing como ROI, SEO, SEM, Testes ', 5, 1, CAST(N'2022-05-12T00:00:00.000' AS DateTime), 1, 170.0000, 130.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (8, N'https://skillking.fpt.edu.vn/wp-content/uploads/2021/03/digital-marketing-la-gi-tong-quan-kien-thuc-tu-a-z-ve-digital-marketing.jpg', 1, N'Mega Digital Marketing Course A-Z: 12 Courses in 1 + Updates', N'System', N'This specialization takes a critical look at digital advertising tactics for small business. Students will learn how to generate and launch ad campaigns on small budgets with limited-to-no design skills.', 9, 0, CAST(N'2022-07-02T00:00:00.000' AS DateTime), 1, 193.0000, 150.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (9, N'https://skillking.fpt.edu.vn/wp-content/uploads/2021/03/tinh-thuan-tien.png', 1, N'Digital Marketing Masterclass - 23 Marketing Courses in 1', N'Marketing', N'In this specialization, students will explore the continually evolving platforms and channels of digital marketing and learn how to define and build an audience through social media, search engine marketing, and website optimization.', 5, 1, CAST(N'2022-04-01T00:00:00.000' AS DateTime), 1, 100.0000, 70.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (10, N'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATcAAACiCAMAAAATIHpEAAACcFBMVEX///8AoYIAN3IAJloAKV4Aru8AK2EAJFcAOXQAM2wAMmoALmTs7OwAP34AOnfl5eUARIYAtPYjMFUAAABYXngAIVb29/jMzMvtICTV1dUBY08AnHsAHFMAAEXg4ODo6Oi3uLsrOWeVlZOBiKMAL223x8MAWkQAm9gAUDe62tAAWYri7usAqOdBpoxVVVUAf7sAH2IAEUsAEWFespsAKmYAhcEAN300NDSioqIAAFTRICYAda+BhJkATocAAE9auvE5OTnliyQAZ6SVl6kpKSlCV4QAgLQARDMAToMuRoDlPiUAod8AjcYAYIgARGcAOV5+fn8/VXHEw8pnZ2fn5hUAP3OztMDX7PrlhSVDTHG63/cANyoAf2UAIl8dHR0ALXIAdaVhYmNMTU3kICQfV0n/4QAAqE+N0fLlcyTlUSPllyPmuR58xvNZd44ATXgAOmN1jp8wAAByEBK4GRybFBcAEw8AIBkcAADI5PkAcbPmyxv89qdHAAAAclvleSPlYyMbQzewyt4/kL5mp9CSvNiWqLdVboc4dJiQtc5ymrVGjbm5yddPb5FodYgCU3UAFjoAAB//zcsAARDupaPsjonpWVbpEAYAADeYfHjZtrboeHNca5BYCQuNExVsDxE/UEwdLzuSNE5YNmFQUmSsl0xEf229qEh5cV6PhVEAX2SAPV7AKzYArk2pM0rMsjoAjV4pTFubV0TVTDP6+IPUy4dKRy6Zd13w6mbY0VIfIw56dRPOUjt0RhZiJhCnoQBSThPx6gBlPjLGciSzTkOLqqCZPiuVV1HXWlbmOwmdT0mwgX5vV1Y0GxoKLCq3aWath4gGt10qAAAgAElEQVR4nO2di38T153oh4cA2zK2c7DGAlsehBZxHUW2uLYkbIxlWdhgyUaRQAZjSZbc4ADGQMC8EkhaAllK7L150VIgtNnm9u62d7elu3e37bbbLpuGpJvkX7q/35mRNCPNWzLb/Xz2h9G8z8z5zu915nGG2agq64qyXiobxNJQIfUKUqcha9VEa+O6OvmdVh5eg+Twy6pWqrMKGOZ5YjOPTC89hR3XHhxwa+3QkH0d+2Rkj17ZvXsP/HuOsgf3p+/Y9u2Rq5oWkFbk1vqC7b/FmLzQSrkxI3tNyY7/+mKm2iNMgduWjnY90toqDFpb02LpReF/NcW1iqLvCITDFQSrghWiFdNFwbelxK3dSFBQjwkG4oHeSGA8SugLDiqxQSU0tGtwW2VstYBmkJwquOfK7c8AmxI5w9mIXnAa3KrF1ksdH/2vw+sZ8XwApdKzlbgUypTYRe8GYVZviRDUTKghOjj6o0fQv7W3anMzh21DnztmUvp0S7esDNROwkXpKsm7zERXlxI3M+omMYoNsRbLc5SW5ydDDPxocjPt3NIDzxXc85MhBn4UuCmom6GY0Dv4n13D1RFd3ExaKRVXXdSoyg3JzVsY0lij5iLdZflCZW4m1E0u/6hP90nATTHMgsWywDCMx+Khv5YrDC+42EbHbAuesm1wnnDEVK7geGFDuukUXwAut8HWHtwIa77Ar7xQWNXmKZZJSyyWMU03nuIXT/Oz+AmGnw1L5w1wU1Q3PdhA0mExuHmG+XZfFA/rOzHgNtUd80wLB/6dWBS52ZDd21HRuS1ULCqecntE3EZjyAlK5Jd/Bxbi8CaUCDPu9rk9RW5Tfe4Crrt9URE3WMuCBwTbCqcPptyFk3nT7YGS3u6LanCrhboJ6afEycHO7Yt9eLbPdUFdbk3OIsSRMyArE244RFvr2WWAF3YXtoCjvjsxOYLHHqXbpw5NskjfM7Rw5xbDJJb37l2ZdUORL0MBUKaNGZmNzsOAWYYZsEFiZdTTMnTnzJlzUMjLMEHVz8EwiwB64eYZGBvZu/f2yige3OQsPSFN7ywuQwnfdgvcrLNRWDoyEdPLrWpsKO6SygGZs6NYuVTXtxnmNhwmcFs+NLmyApXHpRtXuug6nqKCOs5OriwCIccA1afU5Mo7UMEut8U9ew4x4qbREjc7nJnYFHOXYfauxCi3SSgsOrtytgnO1qGVUWriVihwGRhZYgfssCnsf6IPuR2aRcViFmHGGRjCMcFBAdlb3chtUoObtroZwVbXW3JyWL/vMra7jP3sFGM7tELtdOTMd2/ejIGFALEtd67gAc5Gi+ufm1wZdbuxMjGe24AAw+O+iZOzMdhU4AaYAchkHx2MiLh53H1wnuxn+YJBh1+GTYG9Jdp3hWphDIyacgNdZe5OrszG+qwMc2YUD4oDcBNDfIH6uOlVN1VsAC4scnDLdxjHLcbms8FRQlUF/2aLFuMC85dnC9w8NlQGnMARqguJ238J43vRoFC9UpOjhfPBc7sNf3cY27voBUrceNe4MkktDTY7txHO0SJOFJDzTuQQ7zgmcQaeQOo8EuA6Ut/V5lZjdQPp6S45uJEUcxcMbJJ3b0VuAzE8tUwnnGXmFrqeIjd6tDgyES3EBfuhgp+ScoPC1tmYW7CHd3GRmBsUcGeSboVrLZ5FN4HsEU8lt1E+0qOpA7eNnQzzrgFuZtWt/NKEKBuB6tiZO2Al4OrfxcObRuV5+eWXwUmhnX7v7G2o4TtCXEBlWKaVAlignZSbzXFnsqg4Um5XGFu7nUk0MXfKucFu7p5doWrsKcTLFC1ZhhtfKO4bHAlwO3sH5tWAmzFtqxdnv1OMw8q8swhGCt4fjwPjwuTKxMTEaFSIC+DSz40K/g0rsejmIyDPLbXuLASKCao4FdzAZ66/hbF08TYPrMANhrZFHnYpmemkhlrODQq1NowKp4rndmhgChMkDW41VjdpawvN0ra46MCUiXoR5LYIjt8dpXGhvStmK+gYr5/MuW4a48ClRfl4OjE7y9uxDDfr2Xf4oqXcYN67Zxf5KE0N8eVFtHzBHMXcqLdIhaNYZOLQBLXTQ6OUdbXcDGEra90vUO808W2mEDULee8874MSdlSEF4QjpIGEsWLCdxddGuU2i4gtstxsTNOhCRo2MFc5VORWzJGhvcDHmpXFu+j2ozLcaKMDd2mbnKTOA7jRXF2dm3xUMBAUpNrWV9ZCZWg8WCjmoAVub9NEgM/dhbiHEn1bmHd2ks/POB6UPDe05lko8ewKJImdZ2exoQDcosVmga0Pbd0Ky92wqAkNVRxPHcjNc5N3gPYVGkf4E+DRz60G6la/qaJhv3DnHAKDwSF6GJD2n0O5DagW3r41MnJuYnJyZbbY0oLsNpU6t4KRwGNpWbhze7KQE1voJE1ksRTK9eYZSFDcE5MrsYWb52AU1jgDG8CEINAswJUQFOyYuj5+Uzw0ujUEjm7Y5ci7wmEIc4fwuA1zM4dN9kJSbGIFEEVHaQsBNWp2hQqqWGxiko6OukvrR0cncB7v0jyjK0VVlEy6J2hxUPioJzo7ARWmo3QNGMQmVgQB1emboEBgEdV49+yK4E75TWAJv/oEH52EgvjjVuRWQ3WTv3AZjWFubnFjmk/rXrieHqXLIHMXFhThuGN9sJSf56brFaU4CSvhVlAANgLcMB7ly3EXChbEze9aOBJcQ9jUUtgaS6PrS+d6ivuqBTcVbL3P90L58xM5blWqmwjbJs9zvOT/XAW4VdxfqJ261fCu0p+ZfJeZHRjQw82Muknvcdbx9z1dxWWlpYWbx5t1i7BBsQBXqVgXv9vy+6ulG7W0EqXJ9fSHVljljmnp0ZjC8yG+dim32qmb7B35TUXRptNYFO11S+Vq3s2XHnCZGogqKq5/kQplJHe/XoubAXUzj61RVkyDU+NWr4ubFJxubnrVrU6DWyW2SibyzLTZ1VzhzHEzY6ZGsAGBTfUNGzf+FZWN9Y06oKmT0w3OjMJpcTNmpuasFOpY/1f/i1AJeXO5nPcyeV8nNTV0ei21akPVy62W6rZ5c91GZJZzcvbC5UPmvUfvtzboxqZIrsbgzHCDFfm4m06343OcwiOdOh4CVXn8ai0q2vskEuesAjDre+998OFH9+49fP977WsNcFMgpwyupoZaya2kbuval0dqLi8Qcv77998DWB98+IOPPmq7R6Vtru38Un7Lg2ztwK2iwqlzW9fAOWsrwWQIzLO/TYB1r60k/W2XM6GcM2UMnCy5miqckqEqc1vvS8WttRQb6yV5jiXn2yplrn+ORHJBu3PEXzU4U5Zq1FDV7DRo72wyJfZkxaxOK5snXtZms3HkjUpu/f39JJALOjjOoMKpgTNkqXoUTo1byUy3Ox3msHWy3vItrfYcybN88OTIw0p1679AQpSbQX2TJVdLhVNwcNrcHCakUzrZ1OkkEbZ4fT9IPq5Qt7mLJJ8MNpnhZgBc7QxVk5u1+heZGDZDgsVEDcRJXitXt7lHJJfkzHGrBKdL4UxEBh3c1gvcxPU1KU6S65TOSZJHZd6t7bWlXJztNMdNP7iaObhybutqzq0zQrjyebYcuShRt/62h5EquCmCM+DhjDm4VefGkXxTxUyrLU8uiLH1t4F7c9pNc6sAV63CaTk4Hdz+ohqJk3gFNRtmc4EiOMQ2d4F4k0GHeW6mwFVhqErc1pe4je00L0TGRvksuCmDDQdB2/rbPia5OPe8uRk3VCPcvrXVtJCMvcJEC+JYAnA8NTTTQDLONlXBTQmc2YhqjNu6WnLbSUKdStRAWELm2nhsYKaQvdkrufnFspn+wY+84M2ZtcW7NNKX4ARo0tszpRs0xZs0JTLSOzJS2biq3HYSr02ZGrZXyWXeu0EWgtmbo5Kb/rfbKl93E42HeXBHVN+E0y9HWleR206Sk2KzVTT0OWjjC9E0AmYqw62lNq+qddMbjXWDNSmspaVLkdt6cVzYZUIgJOSk1Do5Z9zJdYrp2YLQxgdwbf3ADfdWwc1iqVY3QNwlbpYBg9vK3HH26OTW1GlcwHmJtc1ma4oTEglFCJFcm7I5oY0/xzcWOKsctxY116/k50oODr1/b3eJm2e3q9zHiQQ9f6/Ku7v01nOHWyc3M2IV+zYbxoCMk+O4BBcnkSYxuDht45NQMm63yXJrriKm8hHVJebWozsTUWox+GS4rZPj9j9MSCRkE1GzciTHBXOBSCjOcYGAROOwqXqBPH4ctGpy00Yoy62ujJuQjbiyoI31oHwGMzjd3LYZljWXIlZxLHAAtiT54Zk7t0mE44hT4uO85OLDpbG3WJsit+ZmRNbsb/QXfnWCk9e3tdt/9Alg++sf57J1rpXrK67V4rbGqIwRhziA2nIRwHZnHuWHETBVaVgNEXJ87NMmRoFbc/PBn879qLl5/7VXfuxvXjxx4roKODmFK+e2HfLFT+q2v/LKK3/tOvuzEz+brDNiqKvIbSvB6+ElMHbiDJJb07wQJ0dYCbdOsjQ29qmtkhvoGeX2o7n+uYP+/w0VPeD/+xMnTui3VFlun0AE/5HrJ1Dc/8n+DRT3NwoK97y4zew8funS2PU1MyQoTdWChPPmC+4u5+VIULKYA3Xb+RJTzg2ACdz+Frg1+38MFW32z/zsxAk1P6eD29qfzvVvr8tee+WV7a66Ez87Uafg4ExwW2+Y2zZowj98dOHia2SJJKXYbMk8oCpwc4YSEQk3W+TS2K6Z/1nOrZkK5db8t//3k2YA93dbwLnN/P1itdw2fXIEAkPWi4EhO5k1GFBrym0rOV+4EHk5InVf4PlBxThFfQuSsbGt28q4NTeLufHi96MGwq+ancqAq+AmpCKuykzkOXPbdql0+fYisZdzA32LOAtZCfo60Rp2B7k0NjZTxq1ZjpsgPNequFV1Kal23K6Th3PFi7flzg3ESSCczvPyc8LlAjYRtiRVtzUSbs2a3Ezp21rkVs6LJ1TWLRcPa5300hEK9stYyW2dKW5j4nsF5/MV2KCx4OQyPxSwBYMiM7WzNCjMrJFwaxZzk15KqhRsNvlVHuRxudJiboOVcuDAAf73QGFSVY5Ea8PtOH/dtm0Ooh5YqaMCm9XqzXDBDPn57Z+TTJAj+RJQllrp9W3K3Czu6sVT4laLyyuWmnC7dHmudLn7goyVolqREMc5c/mcMxEkmQLZTpZtyoGVorqJuTVLuNWgoi2WEreolng8+IdDXuiIZI2acLt0fo5Se3SeEHLxDRkrReEyJI6t+qCX5AtBoYll7UHBSlW4hauXmMhO96V7SpIuk/K+GEGEAS909Hu18G9L9OGi/o8JuXT8+HFSEUuLGhcXnkkNFq6GOFgQwEatVMxNEgQq4kJjZWDYLCeiOwrSuKB+k8FcPDXM7dJlpAa5LtR/bNdOEpdXN9QuO+d0Btmi97Oz6NyWaCw1wk1HTC0PqOrcDN+aUeG2Xie34+jbLmKzHLzU9ZkxokSN92edIv1DbIGCc/uz4SagWmVuY3g/6iFGRKS2baas3aksTWijQkzYZoabAM7v3wx2O9jY6HIpvVmjya0IbkNvurf+wIGurgNH6tPt6zasGretkIBcpCa6aytWnwTKrNQWlLdb6tog4S06N1Pc/IMDseIrqtG+cGOBnRluDb29XdhhlCdK+0awuLsPpFvXrwq3GXKhjVc2Sg0wEk6CyeYk0kuUIhtlHXEJNqN26h/ss1hiA4ON/ixktpsHw93RFnd4M33/wTi3DekDsZbowIH6NB8300e6uj2WviPt62vPbRu52H8Z675TqDzxxiUX1mxxcuER4WRttAKbMW7+cNTSPej3Z/3Ng5hoDDZmXdnNYXdLN4Izyq0hHfZ4wvW+tG/TYHige+BqeLAeUo76AUu0SyBXQ25LH18UKRs2Ujl7TpSH2JKk/yKJs2XtB2qjbBPFtlVUsgFujYNRSxigha9GF+anr8xfuTI9P9R3tTmbbcRMraKBqsGt94AnesCX7g33Dc1PT03B3xSU190F6Los7iOtteV2/PJrNIwWVeZS3s7a86VmlpfMPSJOsEkxtU6Wx0Z929aZNWa4+ftaBlz+5qtD0/ytH6EPjal5dzjranRbBl1GuNX39rV0pXsG+xaE7kWEUpnpoat1Pt9Ay4BvfQ25gTMjY6JwCN6OAyBcJMPntZ2hy3OvkSBNN8qVjXV4K7AZ4OZxN/ub+xamChf0pot3zqYtYZcr3DKQ1c+tYZMn1ptudM8Xb1peKd2IW+je4GuIuiE+1IrbDLQPdpZsFPUP1I3lOC5Dr1o6IufbPgbDRUr2MmVj7YFyI9XPrXGwZcDv715QuKs7HR10NXpiLr3cegdbwun0wIJ8aczU0FWfb9RSv65W3JbI2FZJ1SF3o9g4vNxhsy+90fbGEidgEodRmExkUFPLsOnk1hhuCfub3fNTChVlFhauulxut0sfN8A2mN4UnZ5XKm5+OtYKtioBVwW3b5Gt14txVNC3iMAtSHIseW3ufIRlxdwEE2UdQRpNrpcXqo8bqId/cMjGKCiIDeZP90nAqXJDbFicTQHc9BQz5an3hS0N62rCbdvMzM6Sa+NnLUU4TgBHHs2RvL3AzSGixjpyaKO7KrDp5EaxMUKHq0rG5c663H0uHdw2UWzKJTF0L0N1vgFPqy5umu36bde3zpTNniECuDhi85ZQdZZG0UaPi6OJMW6NngF/s1o9BXB9rrWW8FpNbunoQLpRuzjGs94Xi/lqwm3NtoqZL/0CL05yXBLyYZK3CfktqhhbHIvjBZCxCuQ6ufn73P5mi3Y9mStXXYMtjVrcervd6To9xdncvnZLV2tNuJXLT568efQUyXNcjly4QPIkaCvhKvKLiJsXxrkNtjT73TrqCe580DUguDhlbkda6tJuXU9bXbnqO9DSvgrcPkVq1375D8Sbo40EsFXOLqVmF5RNzkal3BoVuPmjA/6r84qhT6IhHpfLElbnlnYPpMNKCYgU2/zCEV+su73m3J7euzF87fAvX3/9HwiZQ2xorUEJOAijfEBQwKZ8P6s4GvZQ5zavrSLT01PdrrCnV5XboCXdOwSJhjY2SHrcvoaW1hpz+3R8/CiP7fV/vEweEieNDnnCiahxIfLo/CVFZdPDzR8N+7sV8zaxYPeKC41Zz+BaufunArZeULerU0LI1D4Tg+2xgVZFbmbuO/+/tpPDR68d/qfXX/9l4HzbeZLkk5FgCZyDzZOH/f2PyC5lbNr3nQdb/M26zIoKKBz1cIrc1rbU9+qIpQVx+w5YfLXk9hSxnUZs/0ygkfCrX1MzDTqdwRARqOXIGxfoPcLKbFeBm8xzDo3dff6rEnWzTanpypBrbUs5N/FzDgOxdFjORoXM0HblimT2/JEOy4F1teP2m/Hx4eFThw8jtodt5391+Pf/AoYapL0QRDIiapRbeeNKkVvlczV+T9gflVZxasFjWViYXhC+ziCV6XAvXsdU4gYL0zEZbAsez8LCkCU6JMXGTF1t7+5urRm3J4htGLD98nfQtiL/ePjwtaO/JYXuGzJ5cHPQeqBvxEDAGNupm1tjhb6BmVbEUtu8cKFcRm/AUKHRoMQt3bJWzkz5T1Z4hipPRLTjQLS9VtzGMSSAc3v9n373PjQS/vXw4cOnj44/XOI7vXB6Cd59OE6AWlvb3GtLBvStUfzcII2mUX94Yb4y/NHPb3jKqw8renoGPS7559/W0mg6uDBdkdXQb4gslFOzQXFD7est2vqm73lL0LZXaUz4/e+wkfBrwHZ4+M3xP5CMMxiMg6phC37XGBEeVLpkjFvpOdWCe5P1Z1Me0RdQRLKADk6eG7i3sDsdlgvO8soLMn3EZzlCsVTNDXzbmxgTDr/ya2wkUGygbk+Oj5FILiPci946s5N2ewFmelx/XChKkVus2y/njxDcgmwuPN+ctjQqchvoSw/IJjXTCgndlS5ftKsm3J6Oj5+kMeH0b7GR8LvfH0bvBuoGTSle1ehd1TVL9Cmv/suXiveYjXHj75S6r2b1tbGKAMJpyOAUuLm6u9N9hoqbCvtitI1aLbefPBNC6akStsOnjo5/hhfXZnaiqtHGwXVCH7x5jVReqzTELZwt92LqckWd20A6ZqhPCuQWVuQm+56RbG1feoYx4dTha8PD1351nvwdxXZ6+OQ4j2cGodEN+Z4bLmArS0XdtLjRdz8OGpLim7vl78vQiyHw16MhPom0rm/nw6V+bnJvxEAovXH0FPgziAvX3v+3w7xzexWsFFoFovV2XqZWina7Ve0VGy1uWrJZRlS4aT1VI31gVSTr9HN7UUZ+8AxC6WlQNowLh4dP89hutD19nAw6ROv9Be19oP9yJpeLs3IFFaVKbnLwNmm/12b4aSQNbuL3dmXkvXsQSk+dOkod3LXho7xvg1iaSzodovVsS9gxTf/5JcSm1YSW52ZO1VDWCiLlJkOsQsHKaK0TyUb93Crf9mMRG+gabStcO3rjNMIbPjo+7n2c/HfxivnLRWz/XlmMRJS4yTzIbFT6RNw2SZYcMSENerlVOiOagQy/SpPe00ffHEYbHb4xjs7tuti57cJY2n+ZvJWMBzVeIVTybzV+T3x1nouW7wejIp4+odhOjiO2U0dP3kB40E4Y/0XZ/b3r6NwukKWxt+Jck8ZVPAVunpqIpcDNUpvizHF7ithAu45CTDh1Y/zVU4dPoY2Of3ZcmqHNYApCH775VrBJ6+qnAjeFlxY2F95NUH9zodgxudD/eZ3wkeny56GVH42mUtY5eWt7q65+fspq/LQNfBto16tHIZ6+On7y1DU6iQmvpEGwDTsr+xix7fq0k9HgpmSn+kUhCXne/fyIuL0kkV+0vXnq1XH0cKfBtUGb4fTw0ZNU29761ox4xaU32ubOk6/egtkAhVnzkqpUy80MtlXtV8oqTR8+vHdjGDEBr1PI6+ipYaQ4/sdcUppp5M+ja8tBYsJaGZvaDXYhC5F9T1xGstksrOPH383FcSDlyuKA/zXIraenob4eX2zopW83bMCmRHpDGn9M92PmsIvE8fkz1LFxygtcHCS/PMU/PfbGOfGKgctzH5OA15uTzFYVuf5DKuVgKpF44M+mQLIPUqnEjs3ZHYnUQZdrRyJxC35h1gODHQ72pFLp+t69sOUx2Bx+sdwHHSOJxIjPdH+D4v4+mz46yVODpimoGY2rOPWE8imt3WnNX75IlvKQtgVZI52L6uC2n0klmIO7maZEqmcHk0jsyKaYVBOzP5tgRhj77hSTYB+4DKlb7wOGedDbk2AS9mMjLNNpP4blLu+zNt1ibqXN9m9ZkhftFNkXT6Bpimr2BRgrxtHxp14vWKNoZauXnCehHFU2A9R0cnswgtxS+/dndzDQet/M3NrdwyR2J6z7Eg7gtt+ouqVTdkeipydh9W3vTXcwI3vSO5gtW7bvsya+Z0tVz+3FD+4Bo994f4PYoEn1JxjeoK7tcS4pUStrjpBIkro2gz3Z6uJmYxLZ3eARHbt3wO+D/cwO/+4mx+4ETOzP3sLftXqwlczUNpKyATfYMr2hlUn1tGK5yx2QBFjLPm1dxKabWye4tvFnT/70+CnggoD69PGT8TdvFG1UrFadgC0ZzxlWNr3c9gOp3aAWu/2gb6BwUNPdVN/2MyM9Kdue3S5D6tZ7DMPSFtC3nu0bGjby3Dr2pUHflpktrfLqptZftJjbix88e/bsyZfJ3BeobW+OfwH8TmIg/QJtNCFWq84gyQXjRj2bXm6bqX97AP7t7l2wUzBT9G8O9G/WPQ6007t3d7iMqFs6xezpYVL7ErZ9GxoaBH1LJPZ2WBMdTCptnFtPkGMTICzL3v/oHlADHfo+hoSTED7/OD7+Jo2jlYCg8vGkk7PrxOZgEyzdCe7KqdnP9sFEIiXEU9eDBKYchXgKcSKVpfHUJY9NPudNj9zq6RlJ9exN9UDetp7b27v+AccBt9RIx0jQODffMYed9vb8IlAb//z7uXzeCz7t6I3xJ6FQHhJfcHahUCgXdwbLJe7lZwpLKleQLOSwF3ShZ2n7Ds3+yf1Z/2a+Ada4mX9Wy8Xnby76l8261hpRNzDUNMDrbehN04QXxhpoQ2wD/VNwb8rfrfAtW/keyYDaky+SuRBwA2xgo599FQk8wYAAIxFnMg6StDtxEAdNQ3FydG68if7GHfyADdJBkJXMFQacVegAzcAnGGSvum1SdG5mOos28N0KnlvHXiu1ovsffvCHr75aIsSbj0TGT0IT4SEhSxcRG4wEvKEISD6ej2QikZAzFIBBJBiBdSOZICkOMjDIhEIwcAZwYSYeyuAgmcvQLSORACv0vW/V1jgVaqv6gRkd3DpGrIL3secyVJKZrzASPLlMIku/RWz/wcMMJSORXC4SSSI+GIQigXgm4vWCKmbowniALgS0kWQgkoMFsFUe5oRgK9gCF4KECu5QHziFa7w1wWb++zL7blkLDieO6hAIBMA2obFwkUCOEcD0g4QyGVyQdOLSQIjL4DoZLh+BQcTpxEEgD3NhkEngMpibxEHAG6RFRrgIP9eLBUTyVsHJWUd6TFLT/+WxatVN/vtZ+1KdRWyREC9vPTl5cvy3hGQiX4Fzu7i0RJLJQAj+BdgI/IQywThORrx0MpBhc7gskuSgANA4NoSDQDwYCOFsFostTPI7COQK56ozlVX9sJESNdPYasOt1ZcohjcndT4gCOvJf5BMKPPVZxgRljKZHOAAbURzS+aohWacEWqaXt5gM3HejANQCkziWgGYxLVyMIm2Go+UJBMvgGtSBqfIzAi2VfnOXWt7okCNDWaoCUUCX0Hr6jPUsVDkD+PPPl7KE2q78SAaWiASzPEmx/KTOBvHgkl+6OAHOcE+YW08HRk2EKHl013QEFIA50hghmEAmRa2WqtbJbf27XbI2/CyTgFbIBn86smz8T8sYUAMwOg3jwnFBsYYEaotVD9ToCAZZuz5wqQjQ4d5K+4s/y4AAAPoSURBVJ+8cRx/YuIOfpq1Ys6IO2cPamEyj636qFDJzXfMXrjixrJe+gW6nN375N7TxxgSYfqLZ186M4E8XRKHNXRInnPSIWzBOnHDXN6epHNCTXF+WigoZ+0sXIyzH1TqT1uDmja2GphpOTfAJr6SKNjrR+P3haaTw34PRuOcsMiurzHl4D9Ug5sXilQaimS/AXDq2Eyqm4qZlnHrWJYcuaB3bR9iqh9kkeRHPLbisspuB5SF1XvlVzhrD7LawMqpGcdmSt2k3PbJYbv/5D5gc3K01vdhXBjlxQg3Y9gA3A594IxhM6duatw69sph+8DOQeM7wbO6DxMibBW9NahKp0GFs9v36gCnRW1VsIm5HRspe6dKEA7iXvE1K5bjxMsMcbPKFq8m9hEtcJuqxFYDbuWvohW5cQnRlHicNWSmJrhpgdukjW111E3MTQGbarWMcTOzh5QyOD3UNGJC9dy2m+HGGRTje2DtSgnwJhPYqrZSOX1LBU1VbBWF41IjD/RAU6JmEJt+dZPG0w7fxhdePmZS9tdaDuIzCy4d0BSplWMzoG5GuEF7QeFDF8XP+4jvJ24ok8JXMxpc9fhRA1cdfm/D5So/diouxary/QkIA21kRrBpOTcD6iZ7/U18m2Gd8mfsyz4sXv5F9vKPi8t+ll0QWRxU1HhpUDOqbYrYTHJTBFe+3yrAqZBTF5UitbAZt1Jd3IwrXFXgzJBTK67CNZi2Ujl1M86tKoVTJ2cQnWpRVWHTVjc1bquhcBrg9KPTKEYbm1l1a/X52lt1cnue4HSw0y6hOmwq6tbacWxkZHmjT5WbCUutDTgVeHo2rdxlFdgk6ta6kcOXA5uWO/Rxq0LhZMDpJCfiZ2QDU9h0WCly87FN91GsW3xq3HQo3OqDMygyezOGTUXd2pc7P/j88/u/+eC+vUMfN/0Kpw/cqpFbNWzIrYP75v6X9h/cu3+x85gqNzMKJwPuuamc3BmSORw1bGo5iC/49Td2MFP2gmOLTm61B1d7cjqpmVa3jT7nl998Hfz83jdPmzT0TY/CmQZXY3Krjg38G/f1T/u/7v/6y6C6fzOpcHLgVpucbPGGsWm0sHxB7umFCxf+GNSIp7UEJ1uxWpGTL7tm2ArcWluDbPCbIGAT528+uZcIK0TxFUT+FUWFT9nKiZ53II2IgV1rvE+pWHnfvmPLy1vafe0iOx3Za1p2/NcVs1UeKXAz1A3Jf4vNRrlBi1Vd9inLHhXZvRv+/nMFD0DtEPeoVE2NSOtG5KYo63SFBpnYIBtVFeOD0SihVgruRU9IUIsJ0t4vFESNmzikrj44PfC0tpffrWlsKmT+PxDeQsRiJZxAAAAAAElFTkSuQmCC', 1, N'Optimizing a Website for Google Search Full', N'Google', N'Learn the ins and outs of optimizing a website, from conducting an initial audit to presenting your findings and recommendations', 7, 1, CAST(N'2022-04-12T00:00:00.000' AS DateTime), 1, 98.0000, 70.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (11, N'https://toiyeuit.com/wp-content/uploads/2022/03/java-spring-framework.jpg', 1, N'Spring Framework Master Class - Java Spring the Modern Way', N'Framework', N'This Specialization explains high level patterns used in Microservice architectures and the motivation to move towards these architectures and away from monolithic development of applications.', 4, 1, CAST(N'2022-05-06T00:00:00.000' AS DateTime), 1, 300.0000, 250.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (12, N'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBISEhgUEhUSGBUZFRUVGhocGBkYGR4YGhgZGhkaGh0cIS8lHCMrIRgaJjgnKy8xNTU1HCU7QDszQC40NTQBDAwMEA8QHhISHzQrJCw2NDY6NDQxNDc0MTE0MTQ0NDQ0NjQ2NDU0NDQ0NDQ0NDQ0NDQxNDE0NDQ0NDE0NDQ0NP/AABEIAK4BIgMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAABgcEBQEDCAL/xABIEAACAQIDAwUJDgQFBQAAAAABAgADEQQSIQUGMQcTQVFhIjI0cXOBkZOyFBYXQlJTVHJ0krGz0tMjocHRFSQzYqIlNWOC8P/EABkBAQADAQEAAAAAAAAAAAAAAAABAgMEBf/EACkRAQEAAgEDBAECBwAAAAAAAAABAhEDEiExEzJBUQQUYSIjQpGh4fH/2gAMAwEAAhEDEQA/AIrERPTbEREBERAREQEREBERAREluyNwMZXUPUyUVIuM1y/Yco4ecg9krlnMfNRvSJRLB+C+p9KT1J/XOfgvqfS19Sf3JX1sPtHVFexLC+C6p9LX1J/cj4L6n0tfUn9yPWw+zqivYlhfBdU+lr6k/uR8F1T6WvqT+5HrYfZ1RXsSwvguqfS19Sf3I+C6p9LX1J/cj1sPs6or2JYPwXVPpa+pP7k1G2NxMXh1LpkrIBc5LhwOk5DxH1ST2ROXC+KbiKxETRYiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiBOeTLYi1ajYmoLrSYKgPDnCMxY9qgrbta/QJa0hXJWP8k3l39lJNZwcuVudZZeXMREzQREQEREBERATicxAqPlK2ItCstemAFq5swHAVBqSPrA38YPXIXLV5WB/laX2gfl1JVU7uHK3GbaY+CIiarEREBERAREQEREBERAREQEREBERAREQEREBERAtvks8Bby7+ykmkhfJZ4C3l39lJNJ5/J7qypMTadc06FSotsyU3cX4XVSRfs0mXMDb3glfyFb2GlJ5QrMcpON+bw33X/XHwk435GG+6/65CSdPNLawvJ7gXpqx5+7KrHuxxIB6p2Zzjw8xpdRG/hJxvyMN91/1zuw/KZiQf4lGiw/2lkPpJb8JI/g4wH/AJ/vj+00G9O4SYeg9bDPUIQZ2VrHuB3xVgBwGtjfhKTLit1pG8Uv3c3sw+O7lLpVAuabWvYcSpGhH8+ySKedsFi3oVErUzZkYOPNxHiIuPPPQ1J8ygjgQD6dZnzccwvbwjKadkREyVQTlY8FpfaB+XUlVS1eVjwWl9oH5dSVVO38f2tMfBERNliIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgW3yWeAt5d/ZSTSQvks8Bby7+ykmk8/k91ZUmBt7wSv5Ct7DTPmBt/wSv5Ct7DSk8oefW4eaeidnn+En1E9kTzuJtV2jtAAAVsbawtZ6trdFtZ28uHVru0s2vuR7fXaVOhgqocjNUpvSRelmZSug6he57BKm/xLaHz2O+/W/vNfiqlRnvVaoz24uWLW8bazPHg1d2omL72fgXxFVKKAlnYKOwHix7ALk+Keg6ShVAHAAD0aSqdw9vYPDsEqUslR+558tmGp4EEfw1vbhcdctm8rz5W5a0jKvqIiYKoJyseC0vtA/LqSqpavKx4LS+0D8upKqnb+P7WmPgiImyxERAREQEREBERAREQEREBERAREQEREBERAREQLb5LPAW8u/spJpIXyWeAt5d/ZSTSefye6sqTX7f8ExH2et7DTYTA294JX8hW9hpSeUPPrcPN/Seidn/6SfUT2RPOzcPNPQ+z3HNJqO8Tp/2idP5HiL5MqYe0tnUcShp1kV1I4EajtB4g9omVnHWPTNftXbeGwqlq1RVsNFuCxPUq8SZzze+yijNrYL3PXqUSb5HZb9YB7kntIsZcG4GNatgKZc3ZM1InrCGy/wDHLKe2njGr1qlZhYu7NbqudB5hYeaW3ycYVqez0LC2dnqD6pNlPnCg+edXN7JvyvfCWRETkUQTlY8FpfaB+XUlVS1eVjwWl9oH5dSVVO38f2tMfBERNliIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgW5yWeAt5d/ZSTOQvks8Bby7+ykmk8/k91ZVzNdt0E4SuACSaFUADU3yNNjOJSIedxga3zVb7j/wBp8/4dV+Zqerf+09FWi06P1F+lup51/wAOqfM1PVv/AGmRhtiYpzZMPXPipsB6SLT0FaLR+ov0dSrN3eT2q7B8ZZEGvNggs3YxGijxXPiloU0CqFAAAAAA0AA4AT7iY5Z3K90W7cxESqEE5WPBKX2gfl1JVUtXlY8EpfaB+XUlVTt/H9rTHwRETZYiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiIFlclO0VyVcOT3QbnlHWpCq1vEQPvSxZ53weLehUWrSYq6m6kf/agjQjtllbI5R6DKBikam/Syguh7QB3Q8Vj45yc3Fd7ilifRIx7/ADZnz7eqrfoj3+bM+fb1Vb9Ex6Mvqq6qTxIx7/NmfPt6qt+iPf5sz59vVVv0R0ZfVNVJ4kY9/mzPn29VW/RHv82Z8+3qq36I6Mvqmqk8SMe/zZnz7eqrfoj3+bM+fb1Vb9EdGX1TVSeJF/f7s359vVVv0TT7Z5SKKqVwis7kaMwyIO2x7pvFYeOTOPK/BqsXlW2ipNLDqbkE1W7Lgql/Hdj5hK6nbisQ9V2qVCWdiSzHiT/TxdE6p28eHRjppJqEREukiIgIndhMJUrNlpI7tYtlVSxsLXNh0aj0zKrbDxaKWfD11VQWZijAADiSSNBI3Br4iJIREQEREBERAREAQETnKeo+icHTjpAREQEREBERAREQEREBERAREQEREBERAREQEREBESb8nG73PVPdVVf4dM2pg8GqD43aF/HxSuecxm6i3SXbjbve46GZx/GqAM/Wo+Knmvr2k9Qmy3t8AxP2er7BmBjNt5tpUMHSbRc9StbptTfInpKsfEvbM/e3wDE+Qq+wZw3dzlvyp8qIiInoNCIiAiIgIiICWJyRjusT9XD/AI1ZXcsTki77E/Vw/wCNWZ83squXhttrb+0MNXeg1GqzI2UkZbHuQdLm/TO3Yu+OEx9TmDSdWYGwdUKtYEkaE62BOo6Jrdu7gVcTialda6KHYMFKMSO5Uam/ZMndjcX3JXFepVDsoOUKpUXIKkkkm+hOk5v5fT+6O2kT5QNiU8JiENEZUqKzZehSpAIHUDcG3RrIrJTyh7SqVsVlem9NaalUDDVgTdn0uLGwtY9Hjks2HsnDbLwfurEoDWyhmJAZlLWy00vwOoBPSb62m85OnCb71O9RVjUnUZmVgOsqQPSZ8yysHyk03qZK9DJSY2z5s1gellKjTrtNfyh7tU6AGJw6hFZ8tRB3oJ71lHQDaxA0uR2yZy3q1lNG/tBSZ9Gk4XMVbL15Tb08JZe5+wsPhcJ7uxSqWKGqCwzBE4rlHyiLG/HUATrpcpqGpZ8OwpE2zZwXA6ytrHxAyPUtt6ZvR1fStpOeTjYuGxS1zXpq5RqYFydLhr8D2CZe/wBu7Rah7twyhe9Zwuisr2AcAcCCRe3EEnokl3N3hGOR7Uub5vIvfBr3B10AtwlM+S5Ybn/EW9lQ7ZppTxVZEACpWqoo6lV2AHoExVUk2AJPUNTJvvjvGuKZsGKQQpicnOZgb5WZL2yjje/GSzatalsjCBsPh8wzBTbToJz1HAJ6OPWRwl/Usk7d6nqU69NlNmVlPUQQf5ztwCK1akjaq1WkhHWGdQR6DLM2Zvlg8dTenjUpUxYaOwZGBvwJAIIt/MazQ7A21R2fiqlCkq16dStSVHDjRSbDoOYjPY/Vk+plZZ09zbI5Rth4XC0qTUKa02aoykgnUZSbanrkBLDrEu7e7eBcBTRmpc5ncrbMFtZSb6g9U0HJpUFV8ZUygZ6quBobZjUa17dsz4+TLHDdiJeyskQsbKCx6gCT6BOG046S18dvBgdlM1CnTd6hY1KmXLe7kt3TE6nXRRwFuEwN0zgBzuNrvRFV6taoqu6ZkTOxFlJ7466+K0v6t1vXZPUrrmXtmyPl68pt6Z8AyxhynDnLHDHmyQL853dj05ctr9l/PPjlP2RRprTxCKFZqnNvYWDXVmDED4wyWv037BJnJeqTKa2bV3mHWJ9pTZgSqsQOJAJA8duEuHc/DU6myqa1VUqyVA99O5zve58U0TcpFJHy0cN/AU5QQ4U5R0qgWw7AT6JHq5W2YzwdSuQYlp727Iw+OwZxmHAzinzoYCxZBq6uOlgAe0EWlVky/HyTOJl22OwtkvjK6UU0vqzdCqO+Y/0HSSBLe2zjqWy8EMgAyqKdJPlNbS/X0knxzo3H3eGDw+ZwOeqWZz1D4qDxX9JM0G+OwNo47EZlROZS60wXUafGcjoLEegCc+Wczz1b2ilu60fJ9WZ9pq7klmWszE8SxFyZZe9vgGJ+z1fYMh25e6eMwuMWrWVAgRwSHUm5FhoJN94MK1bCVqSAF3pOigmwuykDXoleTKXOWFvdQUTe7T3SxmGpNWrKgRctyGVj3TBRoO1hNFOyZS+GhERJCIiAiIgJYnJH32J+rh/xrSu5MuTvbmGwjVziKmTOKOXuXa+U1M3eg274ceuZ8stwsiMvDXb44uqu0MQFqVAA4sA7ADuE4AHSdO7u18UmKpZKlVs1VEKF2ZWDMAQVJtwJ16JN8VtTd+q7VKgpM7G7MaVW5NgLnueoCc4fb2wsMecoKgcDQrRfN5iyj8Zl1fw66b/ZXfbwcqtBPc9KqwGZKwW/TlZWLD/iDO/lPRmwCsmqrWpu1vklXUHxZmWQfe/edse6hVK0UuVUm7FjoWa2l7aADhr1zfbr740Pc4wmOHchMgcgsrJawVwNQQNL9Q6JXoyxkuvCNWK+MtzfUFNj5H7/ACYdP/cMl/wMwcLR2DhnFdalNmU5lXnHqZTxBVdTfx3tIxvnvScc6qistBCSoPfM1rZ2HRpcAdp69L23kymp2ifNTXegGpsS9PUczh30+Sppsx8wF/NKkMnG5m+NOhT9zYsE0hcI4GaynirLxK6m1r8bWm0p4Xd9H54VKRscwTOzKD5Pj5rW7JGFuG5ZSdmdWU09gkVND7jC2PQWFkHj7pRNfySd7ifrUvweabfbe8YtRQoBhRDBmYixYjvdOhRx11JtwtPjk/3io4N6i1yVSoEIYAtZlzcQNbEN0DokdGXRe3e9zV00e3KbPjq6oCWbE1VAHEsajAAdt5NcHvpi8IirtDC1SLZVe2VmsOlWFie248U1u92I2Zbn8EwOKNZahI5y3SxbK3cjuspm8XeTZu0aCpjbIwsSGzKA1rFkccB4yO0S2V3jN49v8l7zwyMDiNkbVJTmVFXKWIZBTqW6SGQ6204GQbaex/ce0qdEMWXnsOyE8cjVFsDbpBBHmkvweL2Js4mpRqZ6hWwys1VrHoHxV4dkgu2dtNiMWcSVAIdSi34KhBQE+a57SY45d3W9fuRO+Vn/AEKHlW9gzp5JO9xH1qX4PM3G7w7Ix1JRiWtYhsrCorK1rHVOPEjQkTS7k7dwWEqYkPUyU3qjmu5drope3BSRoV46ykl9O46u/wDZ8aRPeJi2MxJJufdFYeYOwH8gJL92d1cIuD92427KUNQLdgqoOGi6sTa9uGoFpCdr1lqYms6m6vWqupsRdWdipsdRoRJ5unvThGwgweMKqFQ07sDzbIeAJHekDTW3C95pydUwmk3emLR3wwFNwMLs+mGzAKzBEa5Nge5Vj/ObnlX8EpfaV/KqzA/6DhH56m3OuvdIis9QZhqLfFB7WOkz94NtbMx2D/iVlBANREzFagqBWCgqNTqxHVMv6pZKr8u7YFNn2FlS5ZsPXVQOJJNSwlRiXHulieZ2OlW2bJTrPa9r5XdrX6OE0qU9gYl+fZlRiczIztTGY6m63t902lsM+m5dvlMuttpuYCmyM1Tvcld9fkXc+jifPKgt3Nuz+ksPfLfCi9A4XB6qwCM4UqoUfFQEa3ta/C3C8r+acUve35TitSnyl4MADmsVoAO9pfuT6+E3B/M4v7tL9yVTEehidMXNsPffDYysKFOniFYqzXdUC2UXPByf5Te7Sxq0KL1mDFURnIFsxCi5AuQL+MiVNyb/APcU+pU9mWbvb4Bifs9X2DOfkwmOckVs1UG3q34w2Mwj0KdPEKzGmQXWmF7mojm+VyeCnokBiJ2YYTCai8miIiWSREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQLC2XvLhE2ScM9QisaFdMuVz3Tl8ozBbfGHTK9iJXHCY718ok0RESySIiBwROAg6hPqICIiAiIgIiIH//Z', 1, N'Front-End Web UI Frameworks and Tools: Bootstrap 4', N'Bootstrap', N'This course will give you an overview of client-side web UI frameworks, in particular Bootstrap 4', 3, 0, CAST(N'2022-07-12T00:00:00.000' AS DateTime), 1, 150.0000, 99.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (13, N'https://topdev.vn/blog/wp-content/uploads/2019/08/the-nao-la-mot-lap-trinh-vien-full-stack.png', 1, N'Full-Stack Web Development with React', N'Full stack', N'Learn front-end and hybrid mobile development, with server-side support, for implementing a multi-platform solution.', 3, 1, CAST(N'2022-08-05T00:00:00.000' AS DateTime), 1, 90.0000, 70.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (14, N'https://i.ytimg.com/vi/3a8KsB5wJDE/maxresdefault.jpg', 1, N'Jenkins: Bootstrap and configure real team environment', N'Framework', N'By the end of this course you will be able to set up your own instances of Jenkins on your own server. You will be able to connect it to a source code management tool like Github and you will know how to set up a team with different levels of access using a plugin that handles authorisation in Jenkins.', 10, 1, CAST(N'2022-08-22T00:00:00.000' AS DateTime), 1, 180.0000, 160.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (15, N'https://miro.medium.com/max/1200/1*72cktuehSngJboCjBkOXew.jpeg', 1, N'Operating Systems and You: Becoming a Power User', N'Operating systems', N'In this course -- through a combination of video lectures, demonstrations, and hands-on practice -- you’ll learn about the main components of an operating system and how to perform critical tasks like managing software and users, and configuring hardware.', 10, 0, CAST(N'2022-05-15T00:00:00.000' AS DateTime), 1, 111.0000, 100.0000)
INSERT [dbo].[Subject] ([ID], [Thumbnail], [Status], [Title], [TagLine], [Description], [CategoryID], [Featured], [UpdateDate], [UserID], [ListedPrice], [SalePrice]) VALUES (18, N'/quiz-practicing-system/assets/images/courses/2975068c-e40e-4c9f-90ff-0a8e9fb8c4f0.jpg', 1, N'Spring & Hibernate for Beginners (includes Spring Boot)', N'Spring', N'<p><strong>SPRING BOOT SECTION NOW INCLUDES OVER 6.5 HOURS OF NEW VIDEOS</strong></p><p><strong>#1 BEST SELLING SPRING-HIBERNATE&nbsp;COURSE ON UDEMY - OVER 55,000 REVIEWS - 5 STARS!</strong></p><p><strong>---</strong></p><p><strong>THIS COURSE COVERS SPRING 5</strong></p><p><strong>LEARN these HOT TOPICS in Spring 5:</strong></p><p><strong>Spring Framework 5</strong></p><p><strong>Spring Core</strong></p><p><strong>Spring Annotations</strong></p><p><strong>Spring Java Configuration (all Java, no xml)</strong></p><p><strong>Spring AOP</strong></p><p><strong>Spring MVC</strong></p><p><strong>Hibernate CRUD</strong></p><p><strong>JPA CRUD</strong></p><p><strong>Spring Security</strong></p><p><strong>Spring REST</strong></p><p><strong>Maven</strong></p><p><strong>SPRING BOOT</strong></p><p><strong>Spring Boot Starters</strong></p><p><strong>Spring Boot and Hibernate</strong></p><p><strong>Spring Boot and Spring Data JPA</strong></p><p><strong>Spring Boot and Spring Data REST</strong></p><p><strong>Spring Boot, Thymeleaf and Spring MVC</strong></p><p><strong>REAL-TIME PROJECTS</strong></p><p><strong>Spring MVC and Hibernate CRUD real-time project</strong></p><p><strong>Spring Security (with password encryption in the database)</strong></p><p><strong>Spring REST (with full database CRUD real-time project)</strong></p><p><strong>Spring Boot REST (with full database CRUD real-time project)</strong></p><p><strong>Spring Boot with JPA and Spring Data JPA (with full database CRUD real-time project)</strong></p><p><strong>Spring Boot with Spring Data REST (with full database CRUD real-time project)</strong></p><p><strong>Spring Boot with Thymeleaf (with full database CRUD real-time project)</strong></p><p><strong>---</strong></p><p><i><strong>[COURSE UPDATES]:</strong></i></p><p><i><strong>Updated course to SPRING 5 and Tomcat 9</strong></i></p><p><i><strong>Added Spring Boot and Thymeleaf videos ... 12 videos, 2.5 hours of new content&nbsp;</strong></i></p><p><i><strong>Added Spring Boot videos ... 34 videos, 4 hours of new content&nbsp;</strong></i></p><p><i><strong>Added Spring Security Role-based Registration lecture&nbsp;&nbsp;</strong></i></p><p><i><strong>Added ADVANCED Spring&nbsp;REST videos ... 40 videos, 3 hours of new content&nbsp;&nbsp;</strong></i></p><p><i><strong>Added Spring REST videos ... 18 videos, 1.5 hours of new content&nbsp;</strong></i></p><p><i><strong>Added Spring Security User Registration Lecture&nbsp;&nbsp;</strong></i></p><p><i><strong>Added Spring Security JDBC videos ... 16 new videos, 1 hour&nbsp; of new content</strong></i></p><p><i><strong>Added more&nbsp;Spring Security videos ... 52 videos, 3.5 hours of new content&nbsp;</strong></i></p><p><i><strong>Added Spring&nbsp;Security videos ... 16 new videos, 1 hour of new content</strong></i></p><p><i><strong>New Advanced Hibernate videos ... 57 new videos, 4 hours of new content</strong></i></p><p><strong>---</strong></p><p><strong>This course covers the LATEST VERSIONS&nbsp;of&nbsp;Spring 5 and Hibernate 5! The course also includes Spring Boot and Spring Data JPA</strong></p><p><strong>Build a&nbsp;complete Spring MVC + Hibernate CRUD web app ... all from scratch! (real-time project)</strong></p><p><strong>You will learn about: Spring Core, AOP, Spring MVC, Spring Security, Spring REST, Spring Boot, Spring Data JPA, Spring Data REST, Thymeleaf and Hibernate ... all connected to a MySQL database</strong></p><p><strong>---</strong><br><br><strong>By the end of this course, you will create all of the source code for a complete Spring MVC - Hibernate CRUD real-time project.</strong><br><br><strong>You will also develop Spring REST APIs for a full CRUD REST API real-time project.</strong><br><br><strong>You will type in every line of code with me in the videos ... all from scratch.</strong><br><br><strong>I explain every line of code that we create. So this isn''t a copy/paste exercise, you will have a full understanding of the code.&nbsp;</strong><br><br><strong>--- &nbsp;</strong></p><p><strong>I am a RESPONSIVE INSTRUCTOR .... post your questions and I will RESPOND in 24 hours.</strong></p><p><strong>---&nbsp;</strong></p><p><strong>Join 215,000+ students that are already enrolled!&nbsp;</strong><br>&nbsp;</p><p><strong>Over 55,000+&nbsp;Reviews! (the most reviews for any Spring-Hibernate course on Udemy, nearly TRIPLE the nearest competitor)&nbsp;</strong></p>', 4, 1, CAST(N'2022-07-19T16:37:43.983' AS DateTime), 1, 100.0000, 70.0000)
SET IDENTITY_INSERT [dbo].[Subject] OFF
GO
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (1, 2, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (1, 3, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (1, 7, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (1, 8, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (2, 2, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (2, 3, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (2, 7, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (3, 2, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (3, 3, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (3, 7, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (3, 8, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (4, 2, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (4, 3, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (4, 7, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (5, 2, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (5, 7, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (5, 8, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (6, 2, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (6, 3, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (6, 7, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (6, 8, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (7, 2, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (7, 3, 0)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (7, 8, 0)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (11, 2, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (11, 3, 1)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (11, 7, NULL)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (18, 2, 0)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (18, 3, NULL)
INSERT [dbo].[Subject_PricePackage] ([SubjectID], [PricePackageID], [Status]) VALUES (18, 7, NULL)
GO
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (7, 1)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (7, 3)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (7, 4)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (7, 5)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (7, 6)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (7, 8)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (7, 10)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (8, 2)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (8, 7)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (8, 9)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (8, 11)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (8, 12)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (8, 13)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (8, 14)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (8, 15)
INSERT [dbo].[Teach] ([UserID], [SubjectID]) VALUES (8, 18)
GO
SET IDENTITY_INSERT [dbo].[Topic] ON 

INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (1, 1, N'Welcome and First Steps')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (2, 1, N'HTML Fundamentals')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (3, 1, N'CSS Fundamentals')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (4, 1, N'Layouts: Floats, Flexbox, and CSS Grid Fundamentals')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (5, 2, N'Web Design for Everybody Capstone')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (6, 2, N'AWS Cloud Technical Essentials')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (7, 3, N'Building Modern Node.js Applications on AWS')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (8, 3, N'Amazon DynamoDB: Building NoSQL Database-Driven Applications')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (9, 4, N'O que é marketing digital?')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (10, 5, N'Tipos de canais e SEM')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (11, 6, N'SEO e Facebook Ads')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (12, 7, N'Email Marketing e Growth Hacks')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (13, 7, N'Introduction to the Digital Advertising Landscape')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (14, 8, N'Search Advertising')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (15, 8, N'Social Media Advertising')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (16, 8, N'Native Advertising')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (17, 8, N'design desk')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (18, 9, N'Identifying, Attracting, and Growing Your Digital Audience')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (19, 9, N'Strategies for Converting and Retaining Customers Online')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (20, 9, N'Mobile Marketing, Optimization Tactics, and Analytics')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (21, 10, N'Introduction to Optimizing a Website for Google Search')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (22, 10, N'Advanced SEO Strategies')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (23, 10, N'Mobile/App SEO and Metrics & KPIs')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (24, 10, N'Creating an SEO Campaign')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (25, 10, N'Creating an SEO Campaign')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (26, 11, N'Spring - Ecosystem and Core')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (27, 11, N'Spring MVC, Spring Boot and Rest Controllers')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (28, 11, N'Spring Data Repositories')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (29, 11, N'Spring - Cloud Overview')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (30, 12, N'Front-end Web UI Frameworks Overview: Bootstrap')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (31, 12, N'Bootstrap CSS Components')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (32, 12, N'Bootstrap Javascript Components')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (33, 12, N'Web Tools')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (34, 13, N'Front-end Web UI Frameworks Overview: Bootstrap')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (35, 13, N'Bootstrap CSS Components')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (36, 13, N'Bootstrap Javascript Components')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (37, 13, N'Web Tools')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (38, 14, N'Navigating the System')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (39, 15, N'Users and Permissions')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (40, 15, N'Package and Software Management')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (41, 15, N'Filesystems')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (43, 1, N'Web Design Rules and Framework')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (45, 1, N'Components and Layout Patterns')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (46, 1, N'The End!')
INSERT [dbo].[Topic] ([ID], [SubjectSID], [Name]) VALUES (48, 18, N'The End!')
SET IDENTITY_INSERT [dbo].[Topic] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([ID], [Email], [PassWord], [FullName], [Gender], [Phone], [Image], [RoleID], [Status], [Introduce]) VALUES (1, N'admin@gmail.com', N'A43C27C2BABEFD68DF8A694900F30A1C', N'Admin', 1, N'(094) 157-6972', N'/quiz-practicing-system/assets/images/user/user-2.jpg', 1, 1, NULL)
INSERT [dbo].[User] ([ID], [Email], [PassWord], [FullName], [Gender], [Phone], [Image], [RoleID], [Status], [Introduce]) VALUES (2, N'khuphuochue643@naver.com', N'A43C27C2BABEFD68DF8A694900F30A1C', N'Khu Phuoc Hue', 0, N'(086) 106-5489', N'/quiz-practicing-system/assets/images/user/c70c419a-ed3f-419c-b634-1fb032df5ea5.jpg', 4, 1, NULL)
INSERT [dbo].[User] ([ID], [Email], [PassWord], [FullName], [Gender], [Phone], [Image], [RoleID], [Status], [Introduce]) VALUES (3, N'nhamcaominh707@microsoft.com', N'A43C27C2BABEFD68DF8A694900F30A1C', N'Nham Cao Minh', 1, N'(085) 689-0374', N'/quiz-practicing-system/assets/images/user/031d8a8f-2889-4901-90e7-74bf536818cc.jpg', 4, 1, NULL)
INSERT [dbo].[User] ([ID], [Email], [PassWord], [FullName], [Gender], [Phone], [Image], [RoleID], [Status], [Introduce]) VALUES (4, N'ngoviethuong971@yahoo.com', N'A43C27C2BABEFD68DF8A694900F30A1C', N'Ngo Viet Huong', 0, N'(089) 591-2784', N'/quiz-practicing-system/assets/images/user/e1620b02-f5dc-43a2-8df6-89f4c11ed98c.jpg', 4, 1, NULL)
INSERT [dbo].[User] ([ID], [Email], [PassWord], [FullName], [Gender], [Phone], [Image], [RoleID], [Status], [Introduce]) VALUES (5, N'trieumanhnghiem962@hotmail.com', N'A43C27C2BABEFD68DF8A694900F30A1C', N'Trieu Manh Nghiem', 1, N'(099) 928-0375', N'/quiz-practicing-system/assets/images/user/3f165e7d-45ca-4a8c-b4e7-8d23158a6867.jpg', 4, 1, NULL)
INSERT [dbo].[User] ([ID], [Email], [PassWord], [FullName], [Gender], [Phone], [Image], [RoleID], [Status], [Introduce]) VALUES (6, N'ninhhoaibac919@gmail.com', N'A43C27C2BABEFD68DF8A694900F30A1C', N'Ninh Hoai Bac', 1, N'(058) 374-5981', N'/quiz-practicing-system/assets/images/user/17e8963c-0b89-4f5e-894d-531846017f4a.jpg', 4, 1, NULL)
INSERT [dbo].[User] ([ID], [Email], [PassWord], [FullName], [Gender], [Phone], [Image], [RoleID], [Status], [Introduce]) VALUES (7, N'buithienminh508@gmail.com', N'A43C27C2BABEFD68DF8A694900F30A1C', N'Bui Thien Minh', 1, N'(085) 638-2970', N'/quiz-practicing-system/assets/images/user/cb04df74-9597-4bc2-a9cc-88ea9d257982.jpg', 5, 1, N'Hi, I''m Bui Thien Minh! I have been identified as one of Udemy''s Top Instructors and all my premium courses have earned the best-selling status for outstanding performance and student satisfaction.

I''m a full-stack web developer and designer with a passion for building beautiful things from scratch. I''ve been building websites and apps since 2010 and also have a Master''s degree in Engineering.

I discovered my passion for teaching and helping others by sharing all I knew when I was in college, and that passion brought me to Udemy in 2015.

Here, in all my courses, what students love the most is that I take the time to explain every concept in a way that everyone can easily understand.')
INSERT [dbo].[User] ([ID], [Email], [PassWord], [FullName], [Gender], [Phone], [Image], [RoleID], [Status], [Introduce]) VALUES (8, N'phugiaanh456@microsoft.com', N'A43C27C2BABEFD68DF8A694900F30A1C', N'Phu Gia Anh', 1, N'(038) 372-6184', N'/quiz-practicing-system/assets/images/user/d7c2307e-23b7-4c68-9c6c-a44a55a20335.jpg', 5, 1, N'Brad Traversy has been programming for around 12 years and teaching for almost 5 years. He is the owner of Traversy Media which is a successful web development YouTube channel and specializes in everything from HTML5 to front end frameworks like Angular as well as server side technologies like Node.js, PHP and Python. Brad has mastered explaining very complex topics in a simple manner that is very understandable. Invest in your knowledge by watching Brad''s courses.')
INSERT [dbo].[User] ([ID], [Email], [PassWord], [FullName], [Gender], [Phone], [Image], [RoleID], [Status], [Introduce]) VALUES (9, N'nghibaoan519@icloud.com', N'A43C27C2BABEFD68DF8A694900F30A1C', N'Nghi Bao An', 1, N'(037) 736-5490', N'/quiz-practicing-system/assets/images/user/3bf52bf2-a769-4b79-8860-93b0f5978e70.jpg', 4, 1, NULL)
INSERT [dbo].[User] ([ID], [Email], [PassWord], [FullName], [Gender], [Phone], [Image], [RoleID], [Status], [Introduce]) VALUES (10, N'chauyenmai633@gmail.com', N'A43C27C2BABEFD68DF8A694900F30A1C', N'Chau Yen Mai', 0, N'(034) 316-0825', N'/quiz-practicing-system/assets/images/user/b3a94204-aa1f-44d7-b329-20b84c0b31f0.jpg', 4, 1, NULL)
INSERT [dbo].[User] ([ID], [Email], [PassWord], [FullName], [Gender], [Phone], [Image], [RoleID], [Status], [Introduce]) VALUES (11, N'lohaianh119@yahoo.com', N'A43C27C2BABEFD68DF8A694900F30A1C', N'Lo Hai Anh', 0, N'(035) 729-6831', N'/quiz-practicing-system/assets/images/user/d6f1a3b0-9de7-466e-93af-090442c76541.jpg', 4, 1, NULL)
INSERT [dbo].[User] ([ID], [Email], [PassWord], [FullName], [Gender], [Phone], [Image], [RoleID], [Status], [Introduce]) VALUES (15, N'xuantrinhxq2@gmail.com', N'A43C27C2BABEFD68DF8A694900F30A1C', N'xuantrinh151', 1, N'(032) 705-1618', N'/quiz-practicing-system/assets/images/user/user-default.jpg', 1, 1, NULL)
SET IDENTITY_INSERT [dbo].[User] OFF
GO
INSERT [dbo].[User_Quiz] ([UserID], [QuizID], [TimeTaken], [StartedOn], [CompletedOn], [Grade], [filterType], [filterGroup], [ExpireTime]) VALUES (3, 1, CAST(N'00:20:00' AS Time), CAST(N'2022-07-17T14:00:00.000' AS DateTime), CAST(N'2022-07-17T14:00:00.000' AS DateTime), 65, N'Subject Topic', 0, NULL)
INSERT [dbo].[User_Quiz] ([UserID], [QuizID], [TimeTaken], [StartedOn], [CompletedOn], [Grade], [filterType], [filterGroup], [ExpireTime]) VALUES (3, 8, CAST(N'00:20:00' AS Time), CAST(N'2022-07-17T14:00:00.000' AS DateTime), CAST(N'2022-07-17T14:00:00.000' AS DateTime), 70, N'Domain', 6, NULL)
INSERT [dbo].[User_Quiz] ([UserID], [QuizID], [TimeTaken], [StartedOn], [CompletedOn], [Grade], [filterType], [filterGroup], [ExpireTime]) VALUES (3, 17, CAST(N'00:20:00' AS Time), CAST(N'2022-07-17T14:00:00.000' AS DateTime), CAST(N'2022-07-17T14:00:00.000' AS DateTime), 55, N'Group', 2, NULL)
INSERT [dbo].[User_Quiz] ([UserID], [QuizID], [TimeTaken], [StartedOn], [CompletedOn], [Grade], [filterType], [filterGroup], [ExpireTime]) VALUES (4, 1, CAST(N'00:00:00' AS Time), CAST(N'2022-07-17T00:00:00.000' AS DateTime), CAST(N'2022-07-17T00:00:00.000' AS DateTime), 70, N'Domain', 6, NULL)
INSERT [dbo].[User_Quiz] ([UserID], [QuizID], [TimeTaken], [StartedOn], [CompletedOn], [Grade], [filterType], [filterGroup], [ExpireTime]) VALUES (4, 8, CAST(N'00:20:00' AS Time), CAST(N'2022-07-17T14:00:00.000' AS DateTime), CAST(N'2022-07-17T14:00:00.000' AS DateTime), 80, N'Group', 1, NULL)
INSERT [dbo].[User_Quiz] ([UserID], [QuizID], [TimeTaken], [StartedOn], [CompletedOn], [Grade], [filterType], [filterGroup], [ExpireTime]) VALUES (4, 34, CAST(N'00:20:00' AS Time), CAST(N'2022-07-17T14:00:00.000' AS DateTime), CAST(N'2022-07-17T14:00:00.000' AS DateTime), 80, N'Subject Topic', 0, CAST(N'2022-07-20T07:47:59.353' AS DateTime))
INSERT [dbo].[User_Quiz] ([UserID], [QuizID], [TimeTaken], [StartedOn], [CompletedOn], [Grade], [filterType], [filterGroup], [ExpireTime]) VALUES (4, 49, NULL, CAST(N'2022-07-20T08:26:19.857' AS DateTime), NULL, NULL, NULL, NULL, CAST(N'2022-07-20T08:26:19.857' AS DateTime))
INSERT [dbo].[User_Quiz] ([UserID], [QuizID], [TimeTaken], [StartedOn], [CompletedOn], [Grade], [filterType], [filterGroup], [ExpireTime]) VALUES (4, 50, NULL, CAST(N'2022-07-20T09:07:13.147' AS DateTime), NULL, NULL, NULL, NULL, CAST(N'2022-07-20T09:07:13.147' AS DateTime))
INSERT [dbo].[User_Quiz] ([UserID], [QuizID], [TimeTaken], [StartedOn], [CompletedOn], [Grade], [filterType], [filterGroup], [ExpireTime]) VALUES (4, 51, NULL, CAST(N'2022-07-20T09:09:53.697' AS DateTime), NULL, NULL, NULL, NULL, CAST(N'2022-07-21T00:00:00.000' AS DateTime))
GO
ALTER TABLE [dbo].[Answers]  WITH CHECK ADD  CONSTRAINT [FKAnswers178873] FOREIGN KEY([QuestionID])
REFERENCES [dbo].[Question] ([ID])
GO
ALTER TABLE [dbo].[Answers] CHECK CONSTRAINT [FKAnswers178873]
GO
ALTER TABLE [dbo].[Dimension_Subject]  WITH CHECK ADD  CONSTRAINT [FKDimension_234025] FOREIGN KEY([DimensionID])
REFERENCES [dbo].[Dimension] ([ID])
GO
ALTER TABLE [dbo].[Dimension_Subject] CHECK CONSTRAINT [FKDimension_234025]
GO
ALTER TABLE [dbo].[Dimension_Subject]  WITH CHECK ADD  CONSTRAINT [FKDimension_744103] FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subject] ([ID])
GO
ALTER TABLE [dbo].[Dimension_Subject] CHECK CONSTRAINT [FKDimension_744103]
GO
ALTER TABLE [dbo].[Dimension_Subject_Question]  WITH CHECK ADD  CONSTRAINT [FKDimension_553512] FOREIGN KEY([QuestionID])
REFERENCES [dbo].[Question] ([ID])
GO
ALTER TABLE [dbo].[Dimension_Subject_Question] CHECK CONSTRAINT [FKDimension_553512]
GO
ALTER TABLE [dbo].[Dimension_Subject_Question]  WITH CHECK ADD  CONSTRAINT [FKDimension_89537] FOREIGN KEY([Dimension_SubjectDimensionID], [Dimension_SubjectSubjectID])
REFERENCES [dbo].[Dimension_Subject] ([DimensionID], [SubjectID])
GO
ALTER TABLE [dbo].[Dimension_Subject_Question] CHECK CONSTRAINT [FKDimension_89537]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [FKLesson725591] FOREIGN KEY([TopicID], [TopicSubjectID])
REFERENCES [dbo].[Topic] ([ID], [SubjectSID])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [FKLesson725591]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FKPost23559] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FKPost23559]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FKPost321029] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([ID])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FKPost321029]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD FOREIGN KEY([LevelID])
REFERENCES [dbo].[Level] ([LevelID])
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FKQuestion385519] FOREIGN KEY([TopicID], [TopicSubjectID])
REFERENCES [dbo].[Topic] ([ID], [SubjectSID])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FKQuestion385519]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [FKQuiz115393] FOREIGN KEY([SubjectSID])
REFERENCES [dbo].[Subject] ([ID])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [FKQuiz115393]
GO
ALTER TABLE [dbo].[Register]  WITH CHECK ADD  CONSTRAINT [FKRegister760170] FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subject] ([ID])
GO
ALTER TABLE [dbo].[Register] CHECK CONSTRAINT [FKRegister760170]
GO
ALTER TABLE [dbo].[Register]  WITH CHECK ADD  CONSTRAINT [FKRegister884686] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Register] CHECK CONSTRAINT [FKRegister884686]
GO
ALTER TABLE [dbo].[Subject_PricePackage]  WITH CHECK ADD  CONSTRAINT [FKSubject_Pr501162] FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subject] ([ID])
GO
ALTER TABLE [dbo].[Subject_PricePackage] CHECK CONSTRAINT [FKSubject_Pr501162]
GO
ALTER TABLE [dbo].[Subject_PricePackage]  WITH CHECK ADD  CONSTRAINT [FKSubject_Pr702599] FOREIGN KEY([PricePackageID])
REFERENCES [dbo].[PricePackage] ([ID])
GO
ALTER TABLE [dbo].[Subject_PricePackage] CHECK CONSTRAINT [FKSubject_Pr702599]
GO
ALTER TABLE [dbo].[Teach]  WITH CHECK ADD  CONSTRAINT [FKTeach11007] FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subject] ([ID])
GO
ALTER TABLE [dbo].[Teach] CHECK CONSTRAINT [FKTeach11007]
GO
ALTER TABLE [dbo].[Teach]  WITH CHECK ADD  CONSTRAINT [FKTeach836067] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Teach] CHECK CONSTRAINT [FKTeach836067]
GO
ALTER TABLE [dbo].[Topic]  WITH CHECK ADD  CONSTRAINT [FKTopic580137] FOREIGN KEY([SubjectSID])
REFERENCES [dbo].[Subject] ([ID])
GO
ALTER TABLE [dbo].[Topic] CHECK CONSTRAINT [FKTopic580137]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FKUser349791] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FKUser349791]
GO
ALTER TABLE [dbo].[User_Quiz]  WITH CHECK ADD  CONSTRAINT [FKUser_Quiz289289] FOREIGN KEY([QuizID])
REFERENCES [dbo].[Quiz] ([ID])
GO
ALTER TABLE [dbo].[User_Quiz] CHECK CONSTRAINT [FKUser_Quiz289289]
GO
ALTER TABLE [dbo].[User_Quiz]  WITH CHECK ADD  CONSTRAINT [FKUser_Quiz949221] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[User_Quiz] CHECK CONSTRAINT [FKUser_Quiz949221]
GO
ALTER TABLE [dbo].[User_Quiz_Question]  WITH CHECK ADD  CONSTRAINT [FKUser_Quiz_916874] FOREIGN KEY([QuestionID])
REFERENCES [dbo].[Question] ([ID])
GO
ALTER TABLE [dbo].[User_Quiz_Question] CHECK CONSTRAINT [FKUser_Quiz_916874]
GO
ALTER TABLE [dbo].[User_Quiz_Question]  WITH CHECK ADD  CONSTRAINT [FKUser_Quiz_976715] FOREIGN KEY([User_QuizUserID], [User_QuizQuizID])
REFERENCES [dbo].[User_Quiz] ([UserID], [QuizID])
GO
ALTER TABLE [dbo].[User_Quiz_Question] CHECK CONSTRAINT [FKUser_Quiz_976715]
GO
USE [master]
GO
ALTER DATABASE [QuizPracticingSystem] SET  READ_WRITE 
GO
