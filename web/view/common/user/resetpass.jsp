<%@page contentType="text/html" pageEncoding="UTF-8" %>
    <!DOCTYPE html>

    <html>
    <jsp:include page="../../component/common/head.jsp" />

    <body>

        <div class="main-wrapper">

            <jsp:include page="../../component/common/header.jsp" />

            <!-- Overlay Start -->
            <div class="overlay"></div>
            <!-- Overlay End -->

            <!-- Page Banner Start -->
            <div class="section page-banner">

                <img class="shape-1 animation-round"
                    src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                <img class="shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png"
                    alt="Shape">

                <div class="container">
                    <!-- Page Banner Start -->
                    <div class="page-banner-content">
                        <ul class="breadcrumb">
                            <li><a href="${pageContext.request.contextPath}/common/home">Home</a></li>
                            <li class="active">Reset Password</li>
                        </ul>
                        <h2 class="title">Reset <span>Password</span></h2>
                    </div>
                    <!-- Page Banner End -->
                </div>

                <!-- Shape Icon Box Start -->
                <div class="shape-icon-box">

                    <img class="icon-shape-1 animation-left"
                        src="${pageContext.request.contextPath}/assets/images/shape/shape-5.png" alt="Shape">

                    <div class="box-content">
                        <div class="box-wrapper">
                            <i class="flaticon-badge"></i>
                        </div>
                    </div>

                    <img class="icon-shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-6.png"
                        alt="Shape">

                </div>
                <!-- Shape Icon Box End -->

                <img class="shape-3" src="${pageContext.request.contextPath}/assets/images/shape/shape-24.png"
                    alt="Shape">

                <img class="shape-author" src="${pageContext.request.contextPath}/assets/images/author/author-11.jpg"
                    alt="Shape">

            </div>
            <!-- Page Banner End -->

            <!-- Register & Login Start -->
            <div class="section section-padding">
                <div class="container">

                    <!-- Register & Login Wrapper Start -->
                    <div class="register-login-wrapper">
                        <div class="row align-items-center">
                            <div class="col-lg-6">

                                <!-- Register & Login Images Start -->
                                <div class="register-login-images">
                                    <div class="shape-1">
                                        <img src="${pageContext.request.contextPath}/assets/images/shape/shape-26.png"
                                            alt="Shape">
                                    </div>


                                    <div class="images">
                                        <img src="${pageContext.request.contextPath}/assets/images/register-login.png"
                                            alt="Register Login">
                                    </div>
                                </div>
                                <!-- Register & Login Images End -->

                            </div>
                            <div class="col-lg-6">

                                <!-- Register & Login Form Start -->
                                <div class="register-login-form">
                                    <h3 class="title">Reset <span>Password</span></h3>
                                    <div class="form-wrapper">
                                        <form action="./reset" method="post" id="form_user-reset">
                                            <p style="color: red; align-items: center">${error}</p>
                                            <!-- Single Form Start -->
                                            <div class="single-form">
                                                <input class="form-control" id="password" name="password"
                                                    type="password" placeholder="Password">
                                                <div id="password-error"></div>
                                            </div>
                                            <div class="single-form">
                                                <input class="form-control" id="cpassword" name="cpassword"
                                                    type="password" placeholder="Re-enter Password">
                                                <div id="cpassword-error"></div>
                                            </div>
                                            <!-- Single Form End -->
                                            <!-- Single Form Start -->
                                            <div class="single-form">
                                                <button type="submit"
                                                    class="btn btn-primary btn-hover-dark w-100">Submit</button>
                                            </div>
                                            <!-- Single Form End -->
                                        </form>
                                    </div>
                                </div>
                                <!-- Register & Login Form End -->

                            </div>
                        </div>
                    </div>
                    <!-- Register & Login Wrapper End -->

                </div>
            </div>
            <!-- Register & Login End -->

            <!--Back To Start-->
            <a href="#" class="back-to-top">
                <i class="icofont-simple-up"></i>
            </a>
            <!--Back To End-->

        </div>
        <jsp:include page="../../component/common/footer.jsp" />
        <jsp:include page="../../component/common/js.jsp" />

    </body>

    </html>