<%-- Document : register Created on : Jun 4, 2022, 11:16:16 AM Author : dclon --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <!DOCTYPE html>
        <html>

        <jsp:include page="../../component/common/head.jsp" />

        <body>

            <div class="main-wrapper">
                <jsp:include page="../../component/common/header.jsp" />

                <!-- Overlay Start -->
                <div class="overlay"></div>
                <!-- Overlay End -->

                <!-- Page Banner Start -->
                <div class="section page-banner">

                    <img class="shape-1 animation-round"
                        src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                    <img class="shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png"
                        alt="Shape">

                    <div class="container">
                        <!-- Page Banner Start -->
                        <div class="page-banner-content">
                            <ul class="breadcrumb">
                                <li><a href="${pageContext.request.contextPath}/common/home">Home</a></li>
                                <li class="active">Confirm</li>
                            </ul>
                            <h2 class="title">Confirm <span>Form</span></h2>
                        </div>
                        <!-- Page Banner End -->
                    </div>

                    <!-- Shape Icon Box Start -->
                    <div class="shape-icon-box">

                        <img class="icon-shape-1 animation-left"
                            src="${pageContext.request.contextPath}/assets/images/shape/shape-5.png" alt="Shape">

                        <div class="box-content">
                            <div class="box-wrapper">
                                <i class="flaticon-badge"></i>
                            </div>
                        </div>

                        <img class="icon-shape-2"
                            src="${pageContext.request.contextPath}/assets/images/shape/shape-6.png" alt="Shape">

                    </div>
                    <!-- Shape Icon Box End -->

                    <img class="shape-3" src="${pageContext.request.contextPath}/assets/images/shape/shape-24.png"
                        alt="Shape">

                    <img class="shape-author"
                        src="${pageContext.request.contextPath}/assets/images/author/author-11.jpg" alt="Shape">

                </div>
                <!-- Page Banner End -->

                <!-- Register & Login Start -->
                <div class="section section-padding">
                    <div class="container">

                        <!-- Register & Login Wrapper Start -->
                        <div class="register-login-wrapper">
                            <div class="row align-items-center">
                                <div class="col-lg-6">

                                    <!-- Register & Login Images Start -->
                                    <div class="register-login-images">
                                        <div class="shape-1">
                                            <img src="${pageContext.request.contextPath}/assets/images/shape/shape-26.png"
                                                alt="Shape">
                                        </div>


                                        <div class="images">
                                            <img src="${pageContext.request.contextPath}/assets/images/register-login.png"
                                                alt="Register Login">
                                        </div>
                                    </div>
                                    <!-- Register & Login Images End -->

                                </div>
                                <div class="col-lg-6">

                                    <!-- Register & Login Form Start -->
                                    <div class="register-login-form">
                                        <h3 class="title">System <span>Notification</span></h3>
                                        <h5 style="text-align: center; justify-content: center"
                                            class="text-success text-center mt-3">${status}</h5>
                                        <h6 style="text-align: center; justify-content: center"
                                            class="text-danger text-center mt-3">${error}</h6>
                                        <div class="form-wrapper w-100">
                                            <a href="${pageContext.request.contextPath}/common/home">
                                                <button class="btn btn-primary w-100">Go to Home</button>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- Register & Login Form End -->

                                </div>
                            </div>
                        </div>
                        <!-- Register & Login Wrapper End -->

                    </div>
                </div>
                <!-- Register & Login End -->

                <!-- Footer Start  -->
                <jsp:include page="../../component/common/footer.jsp" />
                <!-- Footer End -->

                <!--Back To Start-->
                <a href="#" class="back-to-top">
                    <i class="icofont-simple-up"></i>
                </a>
                <!--Back To End-->

            </div>

            <jsp:include page="../../component/common/js.jsp" />

        </body>

        </html>