<%-- Document : register Created on : Jun 4, 2022, 11:16:16 AM Author : dclon --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <!DOCTYPE html>
        <html>
        <jsp:include page="../../component/common/head.jsp" />

        <body>

            <div class="main-wrapper">
                <jsp:include page="../../component/common/header.jsp" />
                <!-- Overlay Start -->
                <div class="overlay"></div>
                <!-- Overlay End -->

                <!-- Page Banner Start -->
                <div class="section page-banner">

                    <img class="shape-1 animation-round"
                        src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                    <img class="shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png"
                        alt="Shape">

                    <div class="container">
                        <!-- Page Banner Start -->
                        <div class="page-banner-content">
                            <ul class="breadcrumb">
                                <li><a href="${pageContext.request.contextPath}/common/home">Home</a></li>
                                <li class="active">Register</li>
                            </ul>
                            <h2 class="title">Registration <span>Form</span></h2>
                        </div>
                        <!-- Page Banner End -->
                    </div>

                    <!-- Shape Icon Box Start -->
                    <div class="shape-icon-box">

                        <img class="icon-shape-1 animation-left"
                            src="${pageContext.request.contextPath}/assets/images/shape/shape-5.png" alt="Shape">

                        <div class="box-content">
                            <div class="box-wrapper">
                                <i class="flaticon-badge"></i>
                            </div>
                        </div>

                        <img class="icon-shape-2"
                            src="${pageContext.request.contextPath}/assets/images/shape/shape-6.png" alt="Shape">

                    </div>
                    <!-- Shape Icon Box End -->

                    <img class="shape-3" src="${pageContext.request.contextPath}/assets/images/shape/shape-24.png"
                        alt="Shape">

                    <img class="shape-author"
                        src="${pageContext.request.contextPath}/assets/images/author/author-11.jpg" alt="Shape">

                </div>
                <!-- Page Banner End -->

                <!-- Register & Login Start -->
                <div class="section section-padding">
                    <div class="container">

                        <!-- Register & Login Wrapper Start -->
                        <div class="register-login-wrapper">
                            <div class="row align-items-center">
                                <div class="col-lg-6">

                                    <!-- Register & Login Images Start -->
                                    <div class="register-login-images">
                                        <div class="shape-1">
                                            <img src="${pageContext.request.contextPath}/assets/images/shape/shape-26.png"
                                                alt="Shape">
                                        </div>


                                        <div class="images">
                                            <img src="${pageContext.request.contextPath}/assets/images/register-login.png"
                                                alt="Register Login">
                                        </div>
                                    </div>
                                    <!-- Register & Login Images End -->

                                </div>
                                <div class="col-lg-6">

                                    <!-- Register & Login Form Start -->
                                    <div class="register-login-form">
                                        <h3 class="title">Registration <span>Now</span></h3>
                                        <div class="form-wrapper">
                                            <form action="./register" method="POST" id="form_user-register">
                                                <!-- Single Form Start -->
                                                <h5 class="message-content text-center text-danger mt-3">${error}</h5>
                                                <div class="single-form">
                                                    <input type="text" name="name" id="name" class="form-control"
                                                        placeholder="Name">
                                                    <div id="name-error"></div>
                                                </div>
                                                <!-- Single Form End -->
                                                <!-- Single Form Start -->
                                                <div class="single-form">
                                                    <input type="email" name="email" id="email" class="form-control"
                                                        placeholder="Email">
                                                    <div id="email-error"></div>
                                                </div>
                                                <!-- Single Form End -->
                                                <div class="btn-group single-form d-flex justify-content-between"
                                                    role="group" aria-label="Basic radio toggle button group">
                                                    <input type="radio" class="btn-check" name="gender" value="1"
                                                        id="radio-male" autocomplete="off" checked>
                                                    <label class="btn btn-outline-primary" for="radio-male">Male</label>
                                                    <input type="radio" class="btn-check" name="gender" value="0"
                                                        id="radio-female" autocomplete="off">
                                                    <label class="btn btn-outline-primary"
                                                        for="radio-female">Female</label>
                                                </div>
                                                <!-- Single Form Start -->
                                                <div class="single-form">
                                                    <input type="tel" name="phone" id="phone" class="form-control"
                                                        placeholder="Phone">
                                                    <div id="phone-error"></div>
                                                </div>
                                                <!-- Single Form End -->
                                                <!-- Single Form Start -->
                                                <div class="single-form">
                                                    <input type="password" name="password" id="password"
                                                        class="form-control" placeholder="Password">
                                                    <div id="password-error"></div>
                                                </div>
                                                <div class="single-form">
                                                    <input type="password" name="cpassword" id="cpassword"
                                                        class="form-control" placeholder="Confirm Passowrd">
                                                    <div id="cpassword-error"></div>
                                                </div>
                                                <!-- Single Form End -->
                                                <!-- Single Form Start -->
                                                <div class="single-form">
                                                    <button class="btn btn-primary btn-hover-dark w-100">Create an
                                                        account</button>
                                                </div>
                                                <!-- Single Form End -->
                                            </form>
                                        </div>
                                    </div>
                                    <!-- Register & Login Form End -->

                                </div>
                            </div>
                        </div>
                        <!-- Register & Login Wrapper End -->

                    </div>
                </div>
                <!-- Register & Login End -->

                <!--Back To Start-->
                <a href="#" class="back-to-top">
                    <i class="icofont-simple-up"></i>
                </a>
                <!--Back To End-->

            </div>
            <jsp:include page="../../component/common/footer.jsp" />
            <jsp:include page="../../component/common/js.jsp" />
        </body>

        </html>