<%@page contentType="text/html" pageEncoding="UTF-8" %>
    <!DOCTYPE html>

    <html>
    <jsp:include page="../../component/common/head.jsp" />

    <body>
        <jsp:include page="../../component/common/header.jsp" />
        <div class="main-wrapper">
            <!-- Overlay Start -->
            <div class="overlay"></div>
            <!-- Overlay End -->

            <!-- Page Banner Start -->
            <div class="section page-banner">

                <img class="shape-1 animation-round"
                    src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                <img class="shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png"
                    alt="Shape">

                <div class="container">
                    <!-- Page Banner Start -->
                    <div class="page-banner-content">
                        <ul class="breadcrumb">
                            <li><a href="${pageContext.request.contextPath}/common/home">Home</a></li>
                            <li class="active">Login</li>
                        </ul>
                        <h2 class="title">Login <span>Form</span></h2>
                    </div>
                    <!-- Page Banner End -->
                </div>

                <!-- Shape Icon Box Start -->
                <div class="shape-icon-box">

                    <img class="icon-shape-1 animation-left"
                        src="${pageContext.request.contextPath}/assets/images/shape/shape-5.png" alt="Shape">

                    <div class="box-content">
                        <div class="box-wrapper">
                            <i class="flaticon-badge"></i>
                        </div>
                    </div>

                    <img class="icon-shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-6.png"
                        alt="Shape">

                </div>
                <!-- Shape Icon Box End -->

                <img class="shape-3" src="${pageContext.request.contextPath}/assets/images/shape/shape-24.png"
                    alt="Shape">

                <img class="shape-author" src="${pageContext.request.contextPath}/assets/images/author/author-11.jpg"
                    alt="Shape">

            </div>
            <!-- Page Banner End -->

            <!-- Register & Login Start -->
            <div class="section section-padding">
                <div class="container">

                    <!-- Register & Login Wrapper Start -->
                    <div class="register-login-wrapper">
                        <div class="row align-items-center">
                            <div class="col-lg-6">

                                <!-- Register & Login Images Start -->
                                <div class="register-login-images">
                                    <div class="shape-1">
                                        <img src="${pageContext.request.contextPath}/assets/images/shape/shape-26.png"
                                            alt="Shape">
                                    </div>


                                    <div class="images">
                                        <img src="${pageContext.request.contextPath}/assets/images/register-login.png"
                                            alt="Register Login">
                                    </div>
                                </div>
                                <!-- Register & Login Images End -->

                            </div>
                            <div class="col-lg-6">

                                <!-- Register & Login Form Start -->
                                <div class="register-login-form">
                                    <h3 class="title">Login <span>Now</span></h3>
                                    <div class="form-wrapper">
                                        <form action="./login" method="post" id="form_user-login">
                                            <!-- Single Form Start -->
                                            <p style="color: red; align-items: center">${error}</p>
                                            <input type="hidden" value="${subjectId}" name="subjectId" />
                                            <input type="hidden" value="${pricePackageId}" name="pricePackageId" />
                                            <div class="single-form">
                                                <input class="form-control" id="user" name="user" type="email"
                                                    placeholder="Username">
                                                <div id="user-error"></div>
                                            </div>
                                            <!-- Single Form End -->
                                            <!-- Single Form Start -->
                                            <div class="single-form">
                                                <input name="password" type="password" placeholder="Password">
                                            </div>
                                            <div style="margin-top: 20px; text-align: center"
                                                class="form-group d-md-flex">
                                                <div class="w-50 text-left">
                                                    <label style="color: #309255"
                                                        class="checkbox-wrap checkbox-primary mb-0">
                                                        <input name="remember" type="checkbox" ${(cookie.remember.value
                                                            eq 'ON' )?"checked":""}>
                                                        <span class="checkmark"></span>
                                                        Remember Me</label>
                                                </div>
                                                <div class="w-50 text-md-right">
                                                    <a style="color: #309255" href="./forgot">Forgot
                                                        Password</a>
                                                </div>
                                            </div>
                                            <!-- Single Form End -->
                                            <!-- Single Form Start -->
                                            <div class="single-form" style="margin-top: -7px">
                                                <button type="submit"
                                                    class="btn btn-primary btn-hover-dark w-100">Login</button>
                                            </div>
                                            <p style="margin-top: 10px" class="text-center">Not a member? <a
                                                    style="color: #309255" href="./register">Sign Up</a>
                                            </p>
                                            <!-- Single Form End -->
                                        </form>
                                    </div>
                                </div>
                                <!-- Register & Login Form End -->

                            </div>
                        </div>
                    </div>
                    <!-- Register & Login Wrapper End -->

                </div>
            </div>
            <!-- Register & Login End -->

            <!--Back To Start-->
            <a href="#" class="back-to-top">
                <i class="icofont-simple-up"></i>
            </a>
            <!--Back To End-->

        </div>
        <jsp:include page="../../component/common/footer.jsp" />
        <jsp:include page="../../component/common/js.jsp" />

    </body>

    </html>