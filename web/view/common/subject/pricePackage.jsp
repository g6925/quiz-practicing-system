<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

         <title>Price Package</title>


        <style>
            .card {
                border:none;
                padding: 10px 50px;
            }

            .card::after {
                position: absolute;
                z-index: -1;
                opacity: 0;
                -webkit-transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
                transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
            }

            .card:hover {


                transform: scale(1.02, 1.02);
                -webkit-transform: scale(1.02, 1.02);
                backface-visibility: hidden; 
                will-change: transform;
                box-shadow: 0 1rem 3rem rgba(0,0,0,.75) !important;
            }

            .card:hover::after {
                opacity: 1;
            }

            .card:hover .btn-outline-success{
                color:white;
                background:#309255;
            }
            .title{
                text-align: center;
                margin-bottom: 38px;
                margin-left: 302px;
            }
            .button-return{
                background-color: #198754;
                color: white;
                border: #198754;
                padding: 5px;
            }
            .hearder{
                display: -webkit-inline-box;
            }
        </style>

    </head>
    <body>


        <div class="container-fluid" style="background: linear-gradient(90deg, #00C9FF 0%, #92FE9D 100%);height: 100vh;">
            <div class="container p-5">
                <div class="row">
                    <input type="hidden" value="${subject.getId()}" name="subjectId" /> 
                    <div class="hearder">
                        <input class="button-return" type="button" onclick="location.href = '${pageContext.request.contextPath}/subject/details?sId=${subject.id}';" value="Return" />
                        <h2 class="title">Please select a price plan you want.</h2>

                    </div>
                    <c:forEach items="${pricePackages}" var="p" varStatus="a">

                        <div class="col-lg-${size} col-md-12 mb-${size}">
                            <div class="card h-90 shadow-lg">
                                <div class="card-body">
                                    <div class="text-center p-3">
                                        <h5 class="card-title">${p.getDescription()}</h5>
                                        <small>${not a.last ? "Month" : ""}</small>
                                        <br><br>
                                        <span class="h2">$${subject.getSalePrice()* p.getDuration() * 0.01 * p.getPriceRate()}</span>
                                        <br><br>
                                    </div>
                                    <p class="card-text">When you sign up you can save ${(subject.getSalePrice()* p.getDuration() )- (subject.getSalePrice()* p.getDuration() * 0.01 * p.getPriceRate())}$.</p>
                                </div>

                                <div class="card-body text-center">
                                    <button class="btn btn-outline-success btn-lg" onclick="location.href = '${pageContext.request.contextPath}/common/subject/register?subjectId=${subject.getId()}&pricePackageId=${p.getId()}';" style="border-radius:30px">Register</button>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>    


            </div>


    </body>


    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>


</html>