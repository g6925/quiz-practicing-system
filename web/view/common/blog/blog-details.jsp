<%-- Document : blog Created on : May 28, 2022, 10:07:55 PM Author : hiepx --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
            <!DOCTYPE html>
            <html lang="en">

            <jsp:include page="../../component/common/head.jsp" />

            <body>
                <div class="main-wrapper">

                    <jsp:include page="../../component/common/header.jsp" />
                    <!-- Overlay Start -->
                    <div class="overlay"></div>
                    <!-- Overlay End -->

                    <!-- Page Banner Start -->
                    <div class="section page-banner">

                        <img class="shape-1 animation-round"
                            src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                        <img class="shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png"
                            alt="Shape">

                        <div class="container">
                            <!-- Page Banner Start -->
                            <div class="page-banner-content">
                                <ul class="breadcrumb">
                                    <li><a href="${pageContext.request.contextPath}/common/home">Home</a></li>
                                    <li class="active">Blog</li>
                                </ul>
                                <!--<h2 class="title">The principles every <span>UI/UX</span> designer needs.</h2>-->
                            </div>
                            <!-- Page Banner End -->
                        </div>

                        <!-- Shape Icon Box Start -->
                        <div class="shape-icon-box">

                            <img class="icon-shape-1 animation-left"
                                src="${pageContext.request.contextPath}/assets/images/shape/shape-5.png" alt="Shape">

                            <div class="box-content">
                                <div class="box-wrapper">
                                    <i class="flaticon-badge"></i>
                                </div>
                            </div>

                            <img class="icon-shape-2"
                                src="${pageContext.request.contextPath}/assets/images/shape/shape-6.png" alt="Shape">

                        </div>
                        <!-- Shape Icon Box End -->

                        <img class="shape-3" src="${pageContext.request.contextPath}/assets/images/shape/shape-24.png"
                            alt="Shape">

                        <img class="shape-author"
                            src="${pageContext.request.contextPath}/assets/images/author/author-11.jpg" alt="Shape">

                    </div>
                    <!-- Page Banner End -->

                    <!-- Blog Details Start -->
                    <div class="section section-padding mt-n10">
                        <div class="container">

                            <div class="row gx-10">
                                <div class="col-lg-8">

                                    <!-- Blog Details Wrapper Start -->
                                    <div class="blog-details-wrapper">
                                        <h2 class="title mb-5">${detail.title}</h2>

                                        <div class="blog-details-admin-meta">
                                            <div class="author">
                                                <div class="author-thumb">
                                                    <a href="#"><img src="${detail.user.image}" alt="Author"></a>
                                                </div>
                                                <div class="author-name">
                                                    <a class="name" href="#">${detail.user.name}</a>
                                                </div>
                                            </div>
                                            <div class="blog-meta">
                                                <span> <i class="icofont-calendar"></i>${detail.getTimeAgo()}</span>
                                                <span> <i class="icofont-heart"></i> 2,568+ </span>
                                                <span class="tag"><a style="width: fit-content; block-size: fit-content"
                                                        href="${pageContext.request.contextPath}/common/blog/category?categoryId=${detail.category.id}">${detail.category.name}</a></span>
                                            </div>
                                        </div>

                                        <div class="blog-details-brief">
                                            <p>${detail.brief}</p>
                                        </div>

                                        <div class="blog-details-description">
                                            <p>${detail.content}</p>
                                        </div>

                                        <div class="blog-details-label">
                                            <h4 class="label">Tags:</h4>
                                            <ul class="tag-list">
                                                <li><a
                                                        href="${pageContext.request.contextPath}/common/blog/category?categoryId=${detail.category.id}">${detail.category.name}</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="blog-details-label">
                                            <h4 class="label">Share:</h4>
                                            <ul class="social">
                                                <li><a href="#"><i class="flaticon-facebook"></i></a></li>
                                                <li><a href="#"><i class="flaticon-linkedin"></i></a></li>
                                                <li><a href="#"><i class="flaticon-twitter"></i></a></li>
                                                <li><a href="#"><i class="flaticon-skype"></i></a></li>
                                                <li><a href="#"><i class="flaticon-instagram"></i></a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-4">

                                    <!-- Blog Sidebar Start -->
                                    <div class="sidebar">

                                        <!-- Sidebar Widget Search Start -->
                                        <div class="sidebar-widget widget-search">
                                            <form action="./blog/search" method='get'>
                                                <input name="search" type="text" placeholder="Search here">
                                                <button type="submit"><i class="icofont-search-1"></i></button>
                                            </form>
                                        </div>
                                        <!-- Sidebar Widget Search End -->

                                        <!-- Sidebar Widget Category Start -->
                                        <div class="sidebar-widget">
                                            <h4 class="widget-title">Post Category</h4>

                                            <div class="widget-category">
                                                <ul class="category-list">
                                                    <c:forEach var="i" items="${listCategory}">
                                                        <li><a
                                                                href="./blog/category?categoryId=${i.id}">${i.name}<span>(${categorydao.countCategory(i.id)})</span></a>
                                                        </li>
                                                    </c:forEach>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Sidebar Widget Category End -->

                                        <!-- Sidebar Widget Post Start -->
                                        <div class="sidebar-widget">
                                            <h4 class="widget-title">Recent Post</h4>

                                            <div class="widget-post">
                                                <ul class="post-items">
                                                    <c:forEach var="i" items="${listRecent}">
                                                        <!-- Sidebar Widget Post Start -->
                                                        <div class="single-post">
                                                            <div class="post-thumb">
                                                                <a
                                                                    href="${pageContext.request.contextPath}/blog?id=${i.id}"><img
                                                                        src="${i.thumbnail}" alt="Post"></a>

                                                            </div>
                                                            <div class="post-content">
                                                                <h5 class="title"><a
                                                                        href="${pageContext.request.contextPath}/blog?id=${i.id}">${i.title}</a>
                                                                </h5>
                                                                <span class="date"><i class="icofont-calendar"></i>
                                                                    ${i.date}</span>
                                                            </div>
                                                        </div>
                                                        <!-- Sidebar Widget Post End -->
                                                    </c:forEach>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Blog Details End -->

                    <!-- Download App Start -->
                    <!-- Download App End -->

                    <!-- Footer Start  -->
                    <jsp:include page="../../component/common/footer.jsp" />
                    <!-- Footer End -->

                    <!--Back To Start-->
                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                </div>
                <!-- JS ============================================ -->
                <jsp:include page="../../component/common/js.jsp" />

            </body>

            </html>