<%-- Document : BlogList Created on : May 27, 2022, 12:06:26 AM Author : hiepx --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
            <!DOCTYPE html>
            <html lang="en">

            <jsp:include page="../../component/common/head.jsp" />

            <body>
                <div class="main-wrapper">
                    <jsp:include page="../../component/common/header.jsp" />
                    <!-- Overlay Start -->
                    <div class="overlay"></div>
                    <!-- Overlay End -->

                    <!-- Page Banner Start -->
                    <div class="section page-banner">

                        <img class="shape-1 animation-round"
                            src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                        <img class="shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png"
                            alt="Shape">

                        <div class="container">
                            <!-- Page Banner Start -->
                            <div class="page-banner-content">
                                <ul class="breadcrumb">
                                    <li><a href="${pageContext.request.contextPath}/common/home">Home</a></li>
                                    <li class="active">Blog</li>
                                </ul>
                                <h2 class="title">Our <span>Blog</span></h2>
                            </div>
                            <!-- Page Banner End -->
                        </div>

                        <!-- Shape Icon Box Start -->
                        <div class="shape-icon-box">

                            <img class="icon-shape-1 animation-left"
                                src="${pageContext.request.contextPath}/assets/images/shape/shape-5.png" alt="Shape">

                            <div class="box-content">
                                <div class="box-wrapper">
                                    <i class="flaticon-badge"></i>
                                </div>
                            </div>

                            <img class="icon-shape-2"
                                src="${pageContext.request.contextPath}/assets/images/shape/shape-6.png" alt="Shape">

                        </div>
                        <!-- Shape Icon Box End -->

                        <img class="shape-3" src="${pageContext.request.contextPath}/assets/images/shape/shape-24.png"
                            alt="Shape">

                        <img class="shape-author"
                            src="${pageContext.request.contextPath}/assets/images/author/author-11.jpg" alt="Shape">

                    </div>
                    <!-- Page Banner End -->

                    <!-- Blog Start -->
                    <div class="section section-padding mt-n10">
                        <div class="container">

                            <div class="row gx-10">
                                <div class="col-lg-8">

                                    <!-- Blog Wrapper Start -->
                                    <div class="blog-wrapper">
                                        <div class="row">

                                            <c:forEach var="i" items="${listPaging}">
                                                <c:if test="${i.status}">
                                                    <div class="col-md-6">

                                                        <!-- Single Blog Start -->
                                                        <div class="single-blog">
                                                            <div class="blog-image">
                                                                <a
                                                                    href="${pageContext.request.contextPath}/common/blog?id=${i.id}"><img
                                                                        src="${i.thumbnail}" alt="Blog"></a>
                                                            </div>
                                                            <div class="blog-content">
                                                                <div class="blog-author">
                                                                    <div class="author">
                                                                        <div class="author-thumb">
                                                                            <a href="#"><img src="${i.user.image}"
                                                                                    alt="Author"></a>
                                                                        </div>
                                                                        <div class="author-name">
                                                                            <a class="name" href="#">${i.user.name}</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tag">
                                                                        <a style="width: fit-content; block-size: fit-content"
                                                                            href="${pageContext.request.contextPath}/common/blog/category?categoryId=${i.category.id}">${i.category.name}</a>
                                                                    </div>
                                                                </div>

                                                                <h4 class="title"><a
                                                                        href="${pageContext.request.contextPath}/common/blog?id=${i.id}">${i.title}</a>
                                                                </h4>

                                                                <div class="blog-meta">
                                                                    <span> <i class="icofont-calendar"></i>
                                                                        ${i.getTimeAgo()}</span>
                                                                    <span> <i class="icofont-heart"></i> 2,568+ </span>
                                                                </div>
                                                                <!--<p>${i.brief}</p>-->

                                                                <a href="${pageContext.request.contextPath}/common/blog?id=${i.id}"
                                                                    class="btn btn-secondary btn-hover-primary">Read
                                                                    More</a>
                                                            </div>
                                                        </div>
                                                        <!-- Single Blog End -->
                                                        s
                                                    </div>
                                                </c:if>
                                            </c:forEach>

                                        </div>
                                    </div>
                                    <!-- Blog Wrapper End -->

                                    <!-- Page Pagination End -->
                                    <div class="page-pagination">
                                        <ul class="pagination justify-content-center">
                                            <li><a
                                                    href="${pageContext.request.contextPath}/common/blog/category?page=${(page-1)==0?num:page-1}&&categoryId=${categoryId}"><i
                                                        class="icofont-rounded-left"></i></a></li>
                                            <c:forEach begin="1" end="${num}" var="i">
                                                <li><a class="${i == page?" active":""}"
                                                        href="${pageContext.request.contextPath}//common/blog/category?page=${i}&&categoryId=${categoryId}">
                                                        ${i} </a></li>
                                            </c:forEach>
                                            <li><a
                                                    href="${pageContext.request.contextPath}//common/blog/category?page=${(page+1) < num ? (page+1):1}"><i
                                                        class="icofont-rounded-right"></i></a></li>

                                        </ul>
                                    </div>
                                    <!-- Page Pagination End -->

                                </div>
                                <div class="col-lg-4">

                                    <!-- Blog Sidebar Start -->
                                    <div class="sidebar">

                                        <!-- Sidebar Widget Search Start -->
                                        <div class="sidebar-widget widget-search">
                                            <form action="./search" method='get'>
                                                <input name="search" type="text" placeholder="Search here">
                                                <input name="categoryId" type="text" hidden value="${categoryId}">
                                                <button type="submit"><i class="icofont-search-1"></i></button>
                                            </form>
                                        </div>
                                        <!-- Sidebar Widget Search End -->

                                        <!-- Sidebar Widget Category Start -->
                                        <div class="sidebar-widget">
                                            <h4 class="widget-title">Post Category</h4>

                                            <div class="widget-category">
                                                <ul class="category-list">
                                                    <c:forEach var="i" items="${listCategory}">
                                                        <li><a <c:if test="${categoryId == i.id}">
                                                                style="background-color: #309255; border-color: #309255;
                                                                color: #fff;"
                                                                </c:if>
                                                                href="${pageContext.request.contextPath}/common/blog/category?categoryId=${i.id}">${i.name}
                                                                <span>(${categorydao.countCategory(i.id)}) </span></a>
                                                        </li>
                                                    </c:forEach>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Sidebar Widget Category End -->

                                        <!-- Sidebar Widget Post Start -->
                                        <div class="sidebar-widget">
                                            <h4 class="widget-title">Recent Post</h4>

                                            <div class="widget-post">
                                                <ul class="post-items">
                                                    <li>
                                                        <c:forEach var="i" items="${listRecent}">
                                                            <!-- Sidebar Widget Post Start -->
                                                            <div class="single-post">
                                                                <div class="post-thumb">
                                                                    <a
                                                                        href="${pageContext.request.contextPath}/common/blog/?id=${i.id}"><img
                                                                            src="${i.thumbnail}" alt="Post"></a>

                                                                </div>
                                                                <div class="post-content">
                                                                    <h5 class="title"><a
                                                                            href="${pageContext.request.contextPath}/common/blog?id=${i.id}">${i.title}</a>
                                                                    </h5>
                                                                    <span class="date"><i class="icofont-calendar"></i>
                                                                        ${i.getTimeAgo()}</span>
                                                                </div>
                                                            </div>
                                                            <!-- Sidebar Widget Post End -->
                                                        </c:forEach>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Blog End -->

                    <!-- Download App Start -->

                    <!-- Download App Wrapper End -->

                    <!-- Download App End -->

                    <jsp:include page="../../component/common/footer.jsp" />

                </div>
                <jsp:include page="../../component/common/js.jsp" />

            </body>

            </html>