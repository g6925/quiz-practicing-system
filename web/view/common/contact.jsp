<%-- Document : contact Created on : Jul 4, 2022, 9:55:20 PM Author : dclon --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <!DOCTYPE html>
        <html lang="en">

        <jsp:include page="../component/common/head.jsp" />

        <body>

            <div class="main-wrapper">

                <!-- Header Section Start -->
                <jsp:include page="../component/common/header.jsp" />
                <!-- Header Section End -->

                <!-- Mobile Menu Start -->

                <!-- Mobile Menu End -->

                <!-- Overlay Start -->
                <div class="overlay"></div>
                <!-- Overlay End -->

                <!-- Page Banner Start -->
                <div class="section page-banner">

                    <img class="shape-1 animation-round"
                        src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                    <img class="shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png"
                        alt="Shape">

                    <div class="container">
                        <!-- Page Banner Start -->
                        <div class="page-banner-content">
                            <ul class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li class="active">Contact Us</li>
                            </ul>
                            <h2 class="title">Contact <span>Us</span></h2>
                        </div>
                        <!-- Page Banner End -->
                    </div>

                    <!-- Shape Icon Box Start -->
                    <div class="shape-icon-box">

                        <img class="icon-shape-1 animation-left"
                            src="${pageContext.request.contextPath}/assets/images/shape/shape-5.png" alt="Shape">

                        <div class="box-content">
                            <div class="box-wrapper">
                                <i class="flaticon-badge"></i>
                            </div>
                        </div>

                        <img class="icon-shape-2"
                            src="${pageContext.request.contextPath}/assets/images/shape/shape-6.png" alt="Shape">

                    </div>
                    <!-- Shape Icon Box End -->

                    <img class="shape-3" src="${pageContext.request.contextPath}/assets/images/shape/shape-24.png"
                        alt="Shape">

                    <img class="shape-author"
                        src="${pageContext.request.contextPath}/assets/images/author/author-11.jpg" alt="Shape">

                </div>
                <!-- Page Banner End -->

                <!-- Contact Map Start -->
                <div class="section section-padding-02">
                    <div class="container">

                        <!-- Contact Map Wrapper Start -->
                        <div class="contact-map-wrapper">
                            <iframe id="gmap_canvas"
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.4855342246715!2d105.52706429999999!3d21.013250000000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345b465a4e65fb%3A0xaae6040cfabe8fe!2zVHLGsOG7nW5nIMSQ4bqhaSBI4buNYyBGUFQ!5e0!3m2!1svi!2s!4v1656947442535!5m2!1svi!2s"
                                width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                                referrerpolicy="no-referrer-when-downgrade"></iframe>
                        </div>
                        <!-- Contact Map Wrapper End -->

                    </div>
                </div>
                <!-- Contact Map End -->

                <!-- Contact Start -->
                <div class="section section-padding">
                    <div class="container">

                        <!-- Contact Wrapper Start -->
                        <div class="contact-wrapper">
                            <div class="row align-items-center">
                                <div class="col-lg-6">

                                    <!-- Contact Info Start -->
                                    <div class="contact-info">

                                        <img class="shape animation-round"
                                            src="${pageContext.request.contextPath}/assets/images/shape/shape-12.png"
                                            alt="Shape">

                                        <!-- Single Contact Info Start -->
                                        <div class="single-contact-info">
                                            <div class="info-icon">
                                                <i class="flaticon-phone-call"></i>
                                            </div>
                                            <div class="info-content">
                                                <h6 class="title">Phone No.</h6>
                                                <p><a href="tel:88193326867">(88) 193 326 867</a></p>
                                            </div>
                                        </div>
                                        <!-- Single Contact Info End -->
                                        <!-- Single Contact Info Start -->
                                        <div class="single-contact-info">
                                            <div class="info-icon">
                                                <i class="flaticon-email"></i>
                                            </div>
                                            <div class="info-content">
                                                <h6 class="title">Email Address.</h6>
                                                <p><a href="mailto:edule100@gmail.com">edule100@gmail.com</a></p>
                                            </div>
                                        </div>
                                        <!-- Single Contact Info End -->
                                        <!-- Single Contact Info Start -->
                                        <div class="single-contact-info">
                                            <div class="info-icon">
                                                <i class="flaticon-pin"></i>
                                            </div>
                                            <div class="info-content">
                                                <h6 class="title">Office Address.</h6>
                                                <p>Talga, Alabama, USA</p>
                                            </div>
                                        </div>
                                        <!-- Single Contact Info End -->
                                    </div>
                                    <!-- Contact Info End -->

                                </div>
                                <div class="col-lg-6">

                                    <!-- Contact Form Start -->
                                    <div class="contact-form">
                                        <h3 class="title">Get in Touch <span>With Us</span></h3>

                                        <div class="form-wrapper">
                                            <form id="contact-form" action="#" method="POST">
                                                <!-- Single Form Start -->
                                                <div class="single-form">
                                                    <input type="text" name="name" placeholder="Name">
                                                </div>
                                                <!-- Single Form End -->
                                                <!-- Single Form Start -->
                                                <div class="single-form">
                                                    <input type="email" name="email" placeholder="Email">
                                                </div>
                                                <!-- Single Form End -->
                                                <!-- Single Form Start -->
                                                <div class="single-form">
                                                    <input type="text" name="subject" placeholder="Subject">
                                                </div>
                                                <!-- Single Form End -->
                                                <!-- Single Form Start -->
                                                <div class="single-form">
                                                    <textarea name="message" placeholder="Message"></textarea>
                                                </div>
                                                <!-- Single Form End -->
                                                <p class="form-message"></p>
                                                <!-- Single Form Start -->
                                                <div class="single-form">
                                                    <button class="btn btn-primary btn-hover-dark w-100">Send Message <i
                                                            class="flaticon-right"></i></button>
                                                </div>
                                                <!-- Single Form End -->
                                            </form>
                                        </div>
                                    </div>
                                    <!-- Contact Form End -->

                                </div>
                            </div>
                        </div>
                        <!-- Contact Wrapper End -->

                    </div>
                </div>
                <!-- Contact End -->

                <!-- Footer Start  -->
                <jsp:include page="../component/common/footer.jsp" />
                <!-- Footer End -->

                <!--Back To Start-->
                <a href="#" class="back-to-top">
                    <i class="icofont-simple-up"></i>
                </a>
                <!--Back To End-->

                <!-- JS
    ============================================ -->
                <jsp:include page="../component/common/js.jsp" />

        </body>

        </html>