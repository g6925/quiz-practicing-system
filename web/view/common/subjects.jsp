<%-- Document : subjectList Created on : Jun 6, 2022, 2:07:43 PM Author : xuant --%>

    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@page contentType="text/html" pageEncoding="UTF-8" %>
            <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

                <!DOCTYPE html>
                <html lang="en">


                <jsp:include page="../component/common/head.jsp" />
                <style>
                    .category-menu li {
                        margin: auto;
                    }

                    .courses-select .nice-select {
                        background-color: rgb(48 146 85);
                        font-size: 15px;
                        font-weight: 500;
                        color: white;
                    }

                    .courses-category-wrapper .category-menu li {
                        padding: 0px 7px;
                    }

                    .courses-select .nice-select .list li {
                        color: black;
                    }

                    .courses-category-wrapper .category-menu {
                        margin-top: 26px;
                    }
                </style>

                <body>

                    <div class="main-wrapper">

                        <!-- Header Section Start -->
                        <jsp:include page="../component/common/header.jsp" />
                        <!-- Header Section End -->

                        <!-- Mobile Menu Start -->
                        <div class="mobile-menu">

                            <!-- Menu Close Start -->
                            <a class="menu-close" href="javascript:void(0)">
                                <i class="icofont-close-line"></i>
                            </a>
                            <!-- Menu Close End -->

                            <!-- Mobile Top Medal Start -->
                            <div class="mobile-top">
                                <p><i class="flaticon-phone-call"></i> <a href="tel:9702621413">(970) 262-1413</a></p>
                                <p><i class="flaticon-email"></i> <a
                                        href="mailto:address@gmail.com">address@gmail.com</a></p>
                            </div>
                            <!-- Mobile Top Medal End -->

                            <!-- Mobile Sing In & Up Start -->
                            <div class="mobile-sign-in-up">
                                <ul>
                                    <li><a class="sign-in" href="login.html">Sign In</a></li>
                                    <li><a class="sign-up" href="register.html">Sign Up</a></li>
                                </ul>
                            </div>
                            <!-- Mobile Sing In & Up End -->

                            <!-- Mobile Menu Start -->
                            <div class="mobile-menu-items">
                                <ul class="nav-menu">
                                    <li><a href="index.html">Home</a></li>
                                    <li>
                                        <a href="#">All Course</a>
                                        <ul class="sub-menu">
                                            <li><a href="courses.html">Courses</a></li>
                                            <li><a href="courses-details.html">Courses Details</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Pages </a>
                                        <ul class="sub-menu">
                                            <li><a href="about.html">About</a></li>
                                            <li><a href="register.html">Register</a></li>
                                            <li><a href="login.html">Login</a></li>
                                            <li><a href="faq.html">FAQ</a></li>
                                            <li><a href="404-error.html">404 Error</a></li>
                                            <li><a href="after-enroll.html">After Enroll</a></li>
                                            <li><a href="courses-admin.html">Instructor Dashboard (Course List)</a></li>
                                            <li><a href="overview.html">Instructor Dashboard (Performance)</a></li>
                                            <li><a href="students.html">Students</a></li>
                                            <li><a href="reviews.html">Reviews</a></li>
                                            <li><a href="engagement.html">Course engagement</a></li>
                                            <li><a href="traffic-conversion.html">Traffic & conversion</a></li>
                                            <li><a href="messages.html">Messages</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Blog</a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="#">Blog</a>
                                                <ul class="sub-menu">
                                                    <li><a href="blog-grid.html">Blog</a></li>
                                                    <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                                    <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#">Blog Details</a>
                                                <ul class="sub-menu">
                                                    <li><a href="blog-details-left-sidebar.html">Blog Details Left
                                                            Sidebar</a></li>
                                                    <li><a href="blog-details-right-sidebar.html">Blog Details Right
                                                            Sidebar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="contact.html">Contact</a></li>
                                </ul>

                            </div>
                            <!-- Mobile Menu End -->

                            <!-- Mobile Menu End -->
                            <div class="mobile-social">
                                <ul class="social">
                                    <li><a href="#"><i class="flaticon-facebook"></i></a></li>
                                    <li><a href="#"><i class="flaticon-twitter"></i></a></li>
                                    <li><a href="#"><i class="flaticon-skype"></i></a></li>
                                    <li><a href="#"><i class="flaticon-instagram"></i></a></li>
                                </ul>
                            </div>
                            <!-- Mobile Menu End -->

                        </div>
                        <!-- Mobile Menu End -->

                        <!-- Overlay Start -->
                        <div class="overlay"></div>
                        <!-- Overlay End -->

                        <!-- Page Banner Start -->
                        <div class="section page-banner">

                            <img class="shape-1 animation-round"
                                src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                            <img class="shape-2"
                                src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png" alt="Shape">

                            <div class="container">
                                <!-- Page Banner Start -->
                                <div class="page-banner-content">
                                    <ul class="breadcrumb">
                                        <li><a href="#">Home</a></li>
                                        <li class="active">Courses</li>
                                    </ul>
                                    <h2 class="title">My <span>Courses</span></h2>
                                </div>
                                <!-- Page Banner End -->
                            </div>

                            <!-- Shape Icon Box Start -->
                            <div class="shape-icon-box">

                                <img class="icon-shape-1 animation-left"
                                    src="${pageContext.request.contextPath}/assets/images/shape/shape-5.png"
                                    alt="Shape">

                                <div class="box-content">
                                    <div class="box-wrapper">
                                        <i class="flaticon-badge"></i>
                                    </div>
                                </div>

                                <img class="icon-shape-2"
                                    src="${pageContext.request.contextPath}/assets/images/shape/shape-6.png"
                                    alt="Shape">

                            </div>
                            <!-- Shape Icon Box End -->

                            <img class="shape-3"
                                src="${pageContext.request.contextPath}/assets/images/shape/shape-24.png" alt="Shape">

                            <img class="shape-author"
                                src="${pageContext.request.contextPath}/assets/images/author/author-11.jpg" alt="Shape">

                        </div>
                        <!-- Page Banner End -->

                        <!-- Courses Start -->
                        <div class="section section-padding">
                            <div class="container">
                                <form action="subjects" method="GET">
                                    <div class="courses-category-wrapper">
                                        <div class="courses-search search-2">
                                            <input type="text" placeholder="Search here" name="keyWord"
                                                value="${keyWord}">
                                            <button><i class="icofont-search"></i></button>
                                        </div>

                                        <ul class="category-menu">
                                            <li><a class="active"
                                                    href="${pageContext.request.contextPath}/common/subjects">All
                                                    Courses</a></li>


                                            <li>
                                                <div class="courses-select" style="margin-top: -18px">
                                                    <select " onchange=" this.form.submit()" name="category">
                                                        <option disabled selected value> Category
                                                        </option>
                                                        <c:forEach items="${categorys}" var="c">
                                                            <option <c:if test="${c.id == categoryID}">
                                                                selected="selected"
                                                                </c:if>
                                                                value="${c.id}">${c.name}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </li>


                                            <li>
                                                <div class="courses-select" style="margin-top: -18px">
                                                    <select onchange="this.form.submit()" name="sortByDate">
                                                        <option disabled selected value> Sort
                                                        </option>

                                                        <option <c:if test="${sortByDate == 'asc'}">
                                                            selected="selected"
                                                            </c:if>
                                                            value="asc">Ascending
                                                        </option>
                                                        <option <c:if test="${sortByDate == 'desc'}">
                                                            selected="selected"
                                                            </c:if>
                                                            value="desc">Descending
                                                        </option>

                                                    </select>
                                                </div>
                                            </li>
                                            <li>
                                                <a class="active" style="display: flex;
                                       align-items: center;
                                       justify-content: space-evenly;">
                                                    <input type="checkbox" style="width: 20px;height: 20px;" <c:if
                                                        test="${featured != null}">checked="checked" </c:if>
                                                    name="featured" value="1" onchange="this.form.submit()">
                                                    <label class="featured-label" for=""> Featured</label>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </form>
                                <!-- Courses Category Wrapper Start  -->



                                <!-- Courses Category Wrapper End  -->

                                <!-- Courses Wrapper Start  -->
                                <div class="courses-wrapper-02">
                                    <div class="row">
                                        <c:forEach items="${subjects}" var="subject">
                                            <div class="col-lg-4 col-md-6">
                                                <!-- Single Courses Start -->
                                                <div class="single-courses">
                                                    <div class="courses-images">
                                                        <a
                                                            href="${pageContext.request.contextPath}/subject/details?sId=${subject.id}"><img
                                                                style="width: 328px;
                                                                                                              height: 199px;
                                                                                                              border-radius: 15px;"
                                                                src="${subject.getThumbnail()}" alt="Courses"></a>

                                                        <div class="courses-option dropdown">
                                                            <button class="option-toggle" data-bs-toggle="dropdown"
                                                                aria-expanded="false">
                                                                <span></span>
                                                                <span></span>
                                                                <span></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#"><i class="icofont-share-alt"></i>
                                                                        Share</a></li>
                                                                <li><a href="#"><i class="icofont-plus"></i> Create
                                                                        Collection</a></li>
                                                                <li><a href="#"><i class="icofont-star"></i>
                                                                        Favorite</a></li>
                                                                <li><a href="#"><i class="icofont-archive"></i>
                                                                        Archive</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="courses-content">
                                                        <div class="courses-author">
                                                            <div class="author">

                                                                <div class="author-name" style="padding-left: 0px;">
                                                                    <a class="name"
                                                                        href="#">${subject.getTaught().getName()}</a>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <h4 class="title"><a
                                                                href="${pageContext.request.contextPath}/subject/details?sId=${subject.id}">${subject.getTitle()}</a>
                                                        </h4>
                                                        <p>${subject.getTagline()}</p>

                                                        <div class="courses-price-review">
                                                            <div class="courses-price">
                                                                <span
                                                                    class="sale-parice">$${subject.getSalePrice()}</span>
                                                                <span
                                                                    class="old-parice">$${subject.getListedPrice()}</span>
                                                            </div>
                                                            <div class="courses-review">
                                                                <span class="rating-count">4.9</span>
                                                                <span class="rating-star">
                                                                    <span class="rating-bar" style="width: 80%;"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Single Courses End -->
                                            </div>
                                        </c:forEach>


                                    </div>
                                </div>

                                <!-- Courses Wrapper End  -->

                                <div class="page-pagination">
                                    <ul class="pagination justify-content-center">


                                        <c:if test="${pageindex - 2 > 1}">
                                            <li class=""><a data="1" class="pagination-link">First</a></li>
                                        </c:if>
                                        <c:if test="${pageindex - 2 >= 0}">
                                            <c:forEach begin="${pageindex - 2}" end="${pageindex - 1}" var="i">
                                                <c:if test="${i >= 1}">
                                                    <li class=""><a data="${i}" class="pagination-link">${i}</a></li>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                        <li class=""><a class="pagination-link"
                                                style="background-color: #309255;color: white;"
                                                data="${pageindex}">${pageindex}</a></li>
                                        <c:forEach begin="${pageindex +1}" end="${pageindex + 2}" var="i">
                                            <c:if test="${i <= totalpage}">
                                                <li class=""><a data="${i}" class="pagination-link">${i}</a></li>
                                            </c:if>
                                        </c:forEach>
                                        <c:if test="${pageindex + 2 < totalpage}">
                                            <li class=""><a data="${totalpage}" class="pagination-link">Last</a></li>
                                        </c:if>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Courses End -->

                        <!-- Download App Start -->

                        <!-- Download App End -->

                        <!-- Footer Start  -->
                        <jsp:include page="../component/common/footer.jsp" />
                        <!-- Footer End -->

                        <!--Back To Start-->
                        <a href="#" class="back-to-top">
                            <i class="icofont-simple-up"></i>
                        </a>
                        <!--Back To End-->

                    </div>

                    <jsp:include page="../component/common/js.jsp" />
                    <script>
                        const url_string = window.location.href;
                        const url = new URL(url_string);
                        const search = url.searchParams.get("keyWord");
                        const paginationLinks = document.querySelectorAll(".pagination-link");
                        if (paginationLinks) {
                            paginationLinks.forEach(item => {
                                var search = location.search.substring(1);
                                const params = search ? JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"')
                                    .replace(/&/g, '","').replace(/=/g, '":"') + '"}') : {};
                                const page = item.getAttribute("data");
                                params.page = page;
                                const href = decodeURIComponent(new URLSearchParams(params).toString());
                                item.setAttribute("href", "?" + href);
                            })
                        }
                    </script>
                </body>

                </html>