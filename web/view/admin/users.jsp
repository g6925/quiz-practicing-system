<%-- Document : ManageUser Created on : Jun 1, 2022, 4:50:13 PM Author : Duong-PC --%>

    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@page contentType="text/html" pageEncoding="UTF-8" %>
            <!DOCTYPE html>
            <html>
            <jsp:include page="../component/common/head.jsp" />

            <head>
                <!-- Main CSS-->
                <link rel="stylesheet" type="text/css"
                    href="${pageContext.request.contextPath}/assets/css/admin/main.css">
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
                <!-- or -->
                <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

                <!-- Font-icon css-->
                <!--        <link rel="stylesheet" type="text/css"
                      href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
                <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">-->
                <link rel="stylesheet"
                    href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
            </head>

            <body onload="time()" class="app sidebar-mini rtl">
                <div class="main-wrapper main-wrapper-02">

                    <jsp:include page="../component/administration/header.jsp" />

                    <!-- Courses Admin Start -->
                    <div class="section overflow-hidden position-relative" id="wrapper">

                        <!-- Sidebar Wrapper Start -->
                        <jsp:include page="../component/administration/menu.jsp" />
                        <!-- Sidebar Wrapper End -->

                        <!-- Page Content Wrapper Start -->
                        <div class="page-content-wrapper">
                            <div class="container-fluid custom-container">

                                <main class="app-content" style="margin-left: 0">
                                    <div class="app-title">
                                        <ul class="app-breadcrumb breadcrumb side">
                                            <li class="breadcrumb-item active"><a href="#"><b>Management Users</b></a>
                                            </li>
                                        </ul>
                                        <div id="clock"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tile">
                                                <div class="tile-body">
                                                    <table class="table table-hover table-bordered" id="sampleTable">
                                                        <thead>
                                                            <tr>
                                                                <th>ID</th>
                                                                <th>Image</th>
                                                                <th>Name</th>
                                                                <th>Email</th>
                                                                <th>Gender</th>
                                                                <th>Phone</th>
                                                                <th>Subject</th>
                                                                <th>Role</th>
                                                                <th>Status</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <c:forEach items="${users}" var="u">
                                                                <tr>
                                                                    <td>${u.getId()}</td>
                                                                    <td>
                                                                        <image src="${u.getImage()}" width="100px"
                                                                            height="100px">
                                                                    </td>
                                                                    <td>${u.getName()}</td>
                                                                    <td>${u.getEmail()}</td>
                                                                    <td>${u.getGender()?'Male':'Female'}</td>
                                                                    <td>${u.getPhone()}</td>
                                                                    <td>
                                                                        <c:forEach items="${u.getSubjects()}" var="s">
                                                                            ${s.getId()} - ${s.getTitle()} <br>
                                                                        </c:forEach>
                                                                    </td>
                                                                    <td>${u.getRole().getName()}</td>
                                                                    <td>${u.isStatus()?'<span
                                                                            class="badge bg-success">Is-Active</span>':'<span
                                                                            class="badge bg-danger">Non-Active</span>'}
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn btn-primary btn-sm edit"
                                                                            data-id="${u.getId()}"
                                                                            data-role="${u.getRole().getId()}"
                                                                            data-status="${u.isStatus()}"
                                                                            data-toggle="modal" data-target="#change"
                                                                            type="button" title="Sửa"><i
                                                                                class="fa fa-edit"></i></button>
                                                                    </td>
                                                                </tr>
                                                            </c:forEach>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </main>
                                <jsp:include page="../component/administration/courses-resources.jsp" />
                            </div>
                            <!-- Page Content Wrapper End -->

                        </div>
                        <!-- Courses Admin End -->


                        <!--Back To End-->

                    </div>
                    <jsp:include page="../component/common/footer.jsp" />

                    <!--Back To Start-->
                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                </div>
                <!-- Modal -->
                <div class="modal fade" id="change" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Role</label>
                                        <select name="role" class="form-control">
                                            <c:forEach items="${roles}" var="ro">
                                                <option value="${ro.getId()}">${ro.getName()}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            <option value="1">Active</option>
                                            <option value="0">Non-Active</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                                <button type="button" id="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                    <!-- Essential javascripts for application to work-->
                    <jsp:include page="../component/common/js.jsp" />
                    <script src="${pageContext.request.contextPath}/assets/js/admin/popper.min.js"></script>
                    <script src="${pageContext.request.contextPath}/assets/js/admin/bootstrap.min.js"></script>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                    <!-- The javascript plugin to display page loading on top-->
                    <script src="${pageContext.request.contextPath}/assets/js/plugins/pace.min.js"></script>
                    <!-- Page specific javascripts-->
                    <script
                        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
                    <!-- Data table plugin-->
                    <script type="text/javascript"
                        src="${pageContext.request.contextPath}/assets/js/plugins/jquery.dataTables.min.js"></script>
                    <script type="text/javascript"
                        src="${pageContext.request.contextPath}/assets/js/plugins/dataTables.bootstrap.min.js"></script>
                    <script type="text/javascript">$('#sampleTable').DataTable();</script>
                    <script>
                        function updateRow(r) {
                            var i = r.parentNode.parentNode.rowIndex;
                            document.getElementById("myTable").deleteRow(i);
                        }
                        oTable = $('#sampleTable').dataTable();
                        $('#all').click(function (e) {
                            $('#sampleTable tbody :checkbox').prop('checked', $(this).is(':checked'));
                            e.stopImmediatePropagation();
                        });

                        $('.edit').click(function () {
                            var id = $(this).data('id');
                            var roleId = $(this).data('role');
                            var status = $(this).data('status');
                            $('#change').find('select[name=role]').val(roleId);
                            $('#change').find('select[name=status]').val(status ? 1 : 0);
                            $('#submit').click(function () {
                                var roleId = $('#change').find('select[name="role"]').val();
                                var status = $('#change').find('select[name="status"]').val();
                                $.ajax({
                                    url: './users',
                                    type: 'POST',
                                    data: {
                                        id: id,
                                        roleId: roleId,
                                        status: status
                                    },
                                    success: function (data) {
                                        if (data === 'success') {
                                            alert("Edit Success")

                                        } else {
                                            alert("Edit Failed");
                                        }
                                        location.reload();
                                    }
                                });
                            });
                        });


                        //Thời Gian
                        function time() {
                            var today = new Date();
                            var weekday = new Array(7);
                            weekday[0] = "Chủ Nhật";
                            weekday[1] = "Thứ Hai";
                            weekday[2] = "Thứ Ba";
                            weekday[3] = "Thứ Tư";
                            weekday[4] = "Thứ Năm";
                            weekday[5] = "Thứ Sáu";
                            weekday[6] = "Thứ Bảy";
                            var day = weekday[today.getDay()];
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1;
                            var yyyy = today.getFullYear();
                            var h = today.getHours();
                            var m = today.getMinutes();
                            var s = today.getSeconds();
                            m = checkTime(m);
                            s = checkTime(s);
                            nowTime = h + " giờ " + m + " phút " + s + " giây";
                            if (dd < 10) {
                                dd = '0' + dd
                            }
                            if (mm < 10) {
                                mm = '0' + mm
                            }
                            today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                            tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                                '</span>';
                            document.getElementById("clock").innerHTML = tmp;
                            clocktime = setTimeout("time()", "1000", "Javascript");

                            function checkTime(i) {
                                if (i < 10) {
                                    i = "0" + i;
                                }
                                return i;
                            }
                        }
                        //In dữ liệu
                        var myApp = new function () {
                            this.printTable = function () {
                                var tab = document.getElementById('sampleTable');
                                var win = window.open('', '', 'height=700,width=700');
                                win.document.write(tab.outerHTML);
                                win.document.close();
                                win.print();
                            }
                        }

                        //Modal
                        $("#show-emp").on("click", function () {
                            $("#ModalUP").modal({ backdrop: false, keyboard: false })
                        });
                    </script>
            </body>

            </html>