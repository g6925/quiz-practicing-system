<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <jsp:include page="../../component/common/head.jsp" />
    <style>
        .select_form {
            width: 100%;
            margin-bottom: 30px;
        }

        .select_fom option {
            width: 300px;
        }

        .btn {
            width: 20%;
            margin-top: 30px;
        }

        .dowload {
            margin-top: 30px;
        }

        .button {
            width: 100%;
            text-align: center;
        }

        #input {
            padding: 0;
            border: 0;
            border-radius: 0;
            margin: 0;
            height: auto;
            margin: 30px 0px;
        }
    </style>
    <script>
        function submitMyForm() {
            document.forms["form"].submit();
        }
    </script>

    <body>
        <%--<jsp:include page="../../component/common/header.jsp" />--%>
        <div class="main-wrapper">
            <!-- Overlay Start -->
            <div class="overlay"></div>
            <!-- Overlay End -->

            <!-- Page Banner Start -->

            <!-- Page Banner End -->

            <!-- Register & Login Start -->
            <div class="section section-padding">
                <div class="container">

                    <!-- Register & Login Wrapper Start -->
                    <div class="register-login-wrapper">

                        <!-- Register & Login Form Start -->
                        <h3 class="title" style="text-align: center">Import Question</h3>
                        <div class="form-wrapper">
                            <form action="import" method="post" enctype="multipart/form-data" id="form">
                                <div class="single-form">
                                    <c:if test="${listTopic == null}">
                                        <select class="select_form" style="margin-bottom: 30px"
                                                onchange="this.form.submit()" name="subjectID">
                                            <c:if test="${subjectID == null}">
                                                <option style="text-align: center" disabled selected value>Subject
                                                </c:if>
                                                <c:if test="${subjectID != null}">
                                                <option value="${subjectID}" disabled selected>${subjectID}
                                                </option>
                                            </c:if>
                                            <c:forEach items="${listSubject}" var="subject">
                                                <option value="${subject.id}">${subject.title}</option>
                                            </c:forEach>
                                        </select>
                                        <div class="delect_form">
                                            <a class="dowload"
                                               href="${pageContext.request.contextPath}/dowload">Download
                                                Template</a>
                                        </div>

                                    </c:if>
                                    <!-- Single Form Start -->
                                    <p style="color: red; align-items: center">${error}</p>

                                </div>
                                <!-- Single Form End -->
                                <!-- Single Form Start -->
                                <div class="single-form">
                                    <c:if test="${listTopic != null}">
                                        <select class="select_form" onchange="submitMyForm()" name="subjectID">
                                            <c:forEach items="${listSubject}" var="subject">
                                                <option <c:if test="${subject.id == subjectID}">selected</c:if> value="${subject.id}">${subject.title}</option>
                                            </c:forEach>
                                        </select>
                                        Topic:
                                        <select class="select_form" name="topicID">
                                            <c:forEach items="${listTopic}" var="topic">
                                                <option value="${topic.id}">${topic.name}</option>
                                            </c:forEach>
                                        </select><br/>
                                        Group :
                                        <select class="select_form" name="dimension_group">
                                            <c:forEach items="${dimensions}" var="d">
                                                <c:if test="${d.type=='Group'}">
                                                    <option value="${d.id}">${d.name}</option>
                                                </c:if>
                                            </c:forEach>
                                        </select><br/>
                                        Domain :
                                        <select class="select_form" name="dimension_domain">
                                            <c:forEach items="${dimensions}" var="d">
                                                <c:if test="${d.type=='Domain'}">
                                                    <option value="${d.id}">${d.name}</option>
                                                </c:if>
                                            </c:forEach>
                                        </select><br/>

                                        <!--upload file-->
                                        <!--<label for="files" class="btn" >Select file import</label>-->
                                        <!--dowload file-->
                                        <div class="dowload">

                                            <input accept=".xlsx" id="input" type="file" name="tec" size="60">
                                            <p style="color: red">${mess}</p>
                                            <a href="${pageContext.request.contextPath}/dowload">Download
                                                Template</a>
                                        </div>

                                        <div class="button">
                                            <button type="submit" class="btn btn-primary btn-hover-dark " name="submit" value="submit">
                                                Submit </button>

                                        </div>
                                    </c:if>
                                </div>


                                <!-- Single Form End -->
                                <!-- Single Form Start -->


                                <!-- Single Form End -->
                            </form>
                        </div>
                        <!-- Register & Login Form End -->
                    </div>
                    <!-- Register & Login Wrapper End -->

                </div>
            </div>
            <!-- Register & Login End -->

            <!--Back To Start-->
            <a href="#" class="back-to-top">
                <i class="icofont-simple-up"></i>
            </a>
            <!--Back To End-->

        </div>
        <%--<jsp:include page="../../component/common/footer.jsp" />--%>
        <jsp:include page="../../component/common/js.jsp" />

    </body>

</html>