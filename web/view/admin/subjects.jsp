<%-- Document : courses-admin Created on : Jun 2, 2022, 3:30:04 PM Author : xuant --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

                <html>
                <jsp:include page="../component/common/head.jsp" />
                <style>
                    .search-filter {
                        width: 660px;
                        display: flex;
                        align-items: center;
                        justify-content: space-between;
                    }

                    .search-input {
                        margin-right: 12px;
                        padding-left: 10px;
                        height: 50px;
                        border-radius: 10px;
                        border: 1px solid rgba(48, 146, 85, 0.2);
                    }

                    .search-button {
                        color: #fff;
                        line-height: 50px;
                        font-size: 15px;
                        padding: 0 30px;
                        border-radius: 10px;
                        border: 0 solid transparent;
                        background-color: #309255;
                        border-color: #309255;
                        font-weight: 500;
                    }

                    .featured {
                        margin-top: 20px;
                        display: flex;
                        align-items: center;
                    }

                    .featured-label {
                        font-size: 16px;
                        font-weight: 450;
                        color: rgb(33 40 50);
                    }

                    /*                    .item-thumb img{
                                
                            }*/
                </style>

                <body>

                    <div class="main-wrapper main-wrapper-02">
                        <jsp:include page="../component/administration/header.jsp" />


                        <!-- Courses Admin Start -->
                        <div class="section overflow-hidden position-relative" id="wrapper">

                            <!-- Sidebar Wrapper Start -->
                            <jsp:include page="../component/administration/menu.jsp" />
                            <!-- Sidebar Wrapper End -->

                            <!-- Page Content Wrapper Start -->
                            <div class="page-content-wrapper">
                                <div class="container-fluid custom-container">

                                    <!-- Message Start -->

                                    <!-- Message End -->

                                    <!-- Admin Courses Tab Start -->
                                    <div class="admin-courses-tab">
                                        <h3 class="title">Courses</h3>



                                        <div class="courses-tab-wrapper">


                                            <form action="./subjects" method="GET">
                                                <div class="search-filter">


                                                    <div class="tab-btn">

                                                        <input type="text" class="search-input"
                                                            placeholder="Search here" name="keyWord" value="${keyWord}">
                                                        <input class="search-button" type="submit" value="Search" />
                                                    </div>
                                                    <div class="courses-select">
                                                        <select onchange="this.form.submit()" name="category">
                                                            <option disabled selected value> -- Filter Category --
                                                            </option>
                                                            <c:forEach items="${categorys}" var="c">
                                                                <option <c:if test="${c.id == categoryID}">
                                                                    selected="selected"
                                                                    </c:if>
                                                                    value="${c.id}">${c.name}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                    <div class="featured">
                                                        <input type="checkbox" style="width: 20px;height: 20px;" <c:if
                                                            test="${featured != null}">checked="checked" </c:if>
                                                        name="featured" value="1" onchange="this.form.submit()">
                                                        <label class="featured-label" for=""> Featured</label>
                                                    </div>
                                                </div>

                                            </form>


                                            <div class="tab-btn">
                                                <a href="${pageContext.request.contextPath}/course/subjects"
                                                    class="btn btn-primary btn-hover-dark">Clear</a>
                                            </div>
                                            <div class="tab-btn">
                                                <a href="${pageContext.request.contextPath}/course/subject/add"
                                                    class="btn btn-primary btn-hover-dark">New Course</a>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- Admin Courses Tab End -->

                                    <!-- Admin Courses Tab Content Start -->
                                    <div class="admin-courses-tab-content" style="    margin-top: 10px;">
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="tab1">

                                                <!-- Courses Item Start -->

                                                <!-- Courses Item End -->

                                                <%-- <div id="paggerClick" class="container">
                                            </div>
                                        </div> --%>
                                        <c:forEach items="${subjects}" var="subject">
                                            <div class="courses-item">
                                                <div class="item-thumb">
                                                    <img src="${subject.thumbnail}" alt="Courses">
                                                </div>



                                                <div class="content-title">
                                                    <div class="meta">
                                                        <c:if test="${subject.status}">
                                                            <a href="#" class="action">Active</a>
                                                        </c:if>
                                                        <c:if test="${!subject.status}">
                                                            <a href="#" class="action">Inactive</a>
                                                        </c:if>
                                                    </div>
                                                    <h3 class="title"><a
                                                            href="${pageContext.request.contextPath}/course/subject/detail?sId=${subject.id}">${subject.title}</a>
                                                    </h3>
                                                </div>

                                                <div class="content-wrapper">

                                                    <div class="content-box">
                                                        <p>Category</p>
                                                        <span class="count">${subject.category.name}</span>
                                                    </div>

                                                    <div class="content-box">
                                                        <p>Owner</p>
                                                        <span class="count">${subject.taught.name}</span>
                                                    </div>

                                                    <div class="content-box">
                                                        <p>Lectures</p>
                                                        <a href="lessons?subjectId=${subject.id}" class="count">
                                                            ${subject.numbersOfLesson}
                                                        </a>
                                                    </div>

                                                    <div class="tab-btn">
                                                        <a href="${pageContext.request.contextPath}/course/subject/edit?sId=${subject.id}"
                                                            class="btn btn-primary btn-hover-dark">Edit </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </c:forEach>






                                    </div>

                                </div>

                            </div>


                            <!-- Admin Courses Tab Content End -->
                            <div class="page-pagination">



                                <ul class="pagination justify-content-center">


                                    <c:if test="${pageindex - 2 > 1}">
                                        <li class=""><a data="1" class="pagination-link">First</a></li>
                                    </c:if>
                                    <c:if test="${pageindex - 2 >= 0}">
                                        <c:forEach begin="${pageindex - 2}" end="${pageindex - 1}" var="i">
                                            <c:if test="${i >= 1}">
                                                <li class=""><a data="${i}" class="pagination-link">${i}</a></li>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                    <li class=""><a class="pagination-link"
                                            style="background-color: #309255;color: white;"
                                            data="${pageindex}">${pageindex}</a></li>
                                    <c:forEach begin="${pageindex +1}" end="${pageindex + 2}" var="i">
                                        <c:if test="${i <= totalpage}">
                                            <li class=""><a data="${i}" class="pagination-link">${i}</a></li>
                                        </c:if>
                                    </c:forEach>
                                    <c:if test="${pageindex + 2 < totalpage}">
                                        <li class=""><a data="${totalpage}" class="pagination-link">Last</a></li>
                                    </c:if>
                                </ul>

                            </div>

                            <jsp:include page="../component/administration/courses-resources.jsp" />
                        </div>
                    </div>
                    <!-- Page Content Wrapper End -->

                    </div>
                    <!-- Courses Admin End -->
                    <jsp:include page="../component/common/footer.jsp" />

                    <!--Back To Start-->
                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                    </div>






                    <!-- JS ============================================ -->
                    <jsp:include page="../component/common/js.jsp" />
                    <script>
                        const url_string = window.location.href;
                        const url = new URL(url_string);
                        const search = url.searchParams.get("keyWord");
                        const paginationLinks = document.querySelectorAll(".pagination-link");
                        if (paginationLinks) {
                            paginationLinks.forEach(item => {
                                var search = location.search.substring(1);
                                const params = search ? JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"')
                                    .replace(/&/g, '","').replace(/=/g, '":"') + '"}') : {};
                                const page = item.getAttribute("data");
                                params.page = page;
                                const href = decodeURIComponent(new URLSearchParams(params).toString());
                                item.setAttribute("href", "?" + href);
                            })
                        }
                    </script>

                </body>

                </html>