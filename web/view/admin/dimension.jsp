<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html>
    <jsp:include page="../component/common/head.jsp" />
    <style>
        .search-filter {
            width: 660px;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .search-input {
            margin-right: 12px;
            padding-left: 10px;
            height: 50px;
            border-radius: 10px;
            border: 1px solid rgba(48, 146, 85, 0.2);
        }

        .search-button {
            color: #fff;
            line-height: 50px;
            font-size: 15px;
            padding: 0 30px;
            border-radius: 10px;
            border: 0 solid transparent;
            background-color: #309255;
            border-color: #309255;
            font-weight: 500;
        }

        .featured {
            margin-top: 20px;
            display: flex;
            align-items: center;
        }

        .featured-label {
            font-size: 16px;
            font-weight: 450;
            color: rgb(33 40 50);
        }

        /*                    .item-thumb img{

                }*/
    </style>

    <body>

        <div class="main-wrapper main-wrapper-02">
            <jsp:include page="../component/administration/header.jsp" />
            <!-- Courses Admin Start -->
            <div class="section overflow-hidden position-relative" id="wrapper">

                <!-- Sidebar Wrapper Start -->
                <jsp:include page="../component/administration/menu.jsp" />
                <!-- Sidebar Wrapper End -->
                <!-- Page Content Wrapper Start -->
                <div class="page-content-wrapper">
                    <div class="container-fluid custom-container">
                        <!-- Admin Courses Tab Start -->
                        <div class="admin-courses-tab">
                            <h3 class="title">Dimension</h3>
                            <div class="courses-tab-wrapper">
                                <form method="GET">
                                    <div class="search-filter">
                                        <div class="tab-btn">
                                            <input type="text" class="search-input" placeholder="Search here"
                                                   name="name" value="${keyWord}">
                                            <input class="search-button" type="submit" value="Search" />
                                        </div>
                                        <div class="courses-select">
                                            <select onchange="this.form.submit()" name="type">
                                                <option disabled selected value> -- Filter Type --
                                                </option>
                                                <option value="">All</option>
                                                <option value="domain" ${type eq 'domain' ? 'selected' : '' }>
                                                    Domain</option>
                                                <option value="group" ${type eq 'group' ? 'selected' : '' }>
                                                    Group</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                                <div class="tab-btn">
                                    <a href="${pageContext.request.contextPath}/${user.role.name}/dimension?action=add"
                                       class="btn btn-primary btn-hover-dark">New Dimension</a>
                                </div>
                            </div>
                        </div>
                        <!-- Admin Courses Tab End -->

                        <!-- Admin Courses Tab Content Start -->
                        <div class="admin-courses-tab-content" style="    margin-top: 10px;">
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="tab1">
                                    <div id="paggerClick" class="container">
                                    </div>
                                </div>
                                <c:forEach items="${dimensions}" var="dimension">
                                    <div class="courses-item">
                                        <div class="content-title">
                                            <div class="meta">
                                                <a class="action">${dimension.type}</a>
                                            </div>
                                            <h3 class="title"><a href="#">${dimension.name}</a></h3>
                                        </div>

                                        <div class="content-wrapper">
                                            <div class="tab-btn">
                                                <a href="${pageContext.request.contextPath}/${user.role.name}/dimension?action=edit&dimensionId=${dimension.id}"
                                                   class="btn btn-primary btn-hover-dark">Edit </a>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <jsp:include page="../component/administration/courses-resources.jsp" />
                </div>
                <!-- Page Content Wrapper End -->

            </div>
            <!-- Courses Admin End -->

            <jsp:include page="../../component/common/footer.jsp" />

            <!--Back To Start-->
            <a href="#" class="back-to-top">
                <i class="icofont-simple-up"></i>
            </a>
            <!--Back To End-->

        </div>

        <!-- JS ============================================ -->
        <jsp:include page="../../component/js.jsp" />
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script>
                                                function changeStatus(id, status) {
                                                    swal({
                                                        title: "Are you sure?",
                                                        text: "You want to change status of this lesson!",
                                                        icon: "warning",
                                                        buttons: true,
                                                        dangerMode: true,
                                                    })
                                                            .then((willDelete) => {
                                                                if (willDelete) {
                                                                    $.ajax({
                                                                        url: 'lessons',
                                                                        type: 'POST',
                                                                        data: {
                                                                            lessonId: id,
                                                                            action: 'changeStatus',
                                                                            status: !status
                                                                        },
                                                                        success: function (data) {
                                                                            if (data === 'success') {
                                                                                swal({
                                                                                    title: "Success!",
                                                                                    text: "Status has been changed!",
                                                                                    icon: "success",
                                                                                    button: "OK",
                                                                                })
                                                                                        .then(function () {
                                                                                            location.reload();
                                                                                        });
                                                                            } else {
                                                                                swal("Error!", "Status not changed!", "error");
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    swal("Cancelled", "Status not changed!", "error");
                                                                }
                                                            });

                                                }
        </script>

    </body>

</html>