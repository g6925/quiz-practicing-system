<!------ Include the above in your HEAD tag ---------->

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <jsp:include page="../../component/common/head.jsp" />
    <style>
        .form-group {
            display: flex;
            align-items: center;
            font-size: 20px;
            justify-content: center;
        }

        .control-label {
            width: 340px;
        }

        #featured {
            width: 20px;
            height: 20px;
        }

        .form-horizontal {
            color: rgb(33 40 50);
            font-weight: 500;
        }

        .error {
            color: red;
            font-size: 15px;
        }

        .single-form label {
            color: red;
            font-size: 15px;
        }
    </style>

    <body>

        <div class="main-wrapper main-wrapper-02">
            <jsp:include page="../../component/administration/header.jsp" />
            <!-- Courses Admin Start -->
            <div class="section overflow-hidden position-relative" id="wrapper">

                <!-- Sidebar Wrapper Start -->
                <jsp:include page="../../component/administration/menu.jsp" />
                <!-- Sidebar Wrapper End -->

                <!-- Page Content Wrapper Start -->
                <div class="page-content-wrapper">
                    <div class="container-fluid custom-container">

                        <!-- Message Start -->

                        <!-- Message End -->
                        <div class="right">
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-2">
                                    <c:if test="${not empty message}">
                                        <div class="alert alert-${alert}">
                                            <h5 style="color: black">${message}</h5>
                                        </div>
                                    </c:if>
                                </div>
                            </div>

                            <form method="POST" class="form-horizontal" id="form_dimension">
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Dimension
                                        Name</label>
                                    <div class="single-form col-md-4 ">
                                        <input id="name" name="name" placeholder="Dimension Name"
                                               class="form-control input-md" type="text" value="${dimension.name}">
                                        <div id="name-error"></div>
                                    </div>
                                </div>
                                <div class="form-group courses-select">
                                    <label class="col-md-4 control-label" for="type">Type</label>
                                    <div class="single-form col-md-4">
                                        <select id="type" name="type" class="form-control">
                                            <option disabled selected> -- Dimesion Type -- </option>
                                            <option value="Domain" ${dimension.type=='Domain' ? 'selected' : '' }>
                                                Domain
                                            </option>
                                            <option value="Group" ${dimension.type=='Group' ? 'selected' : '' }>
                                                Group</option>
                                        </select>
                                        <div id="type-error"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="description">Description</label>
                                    <div class="single-form col-md-4">
                                        <textarea class="form-control" id="description"
                                                  name="description">${dimension.description}</textarea>
                                        <div id="description-error"></div>
                                    </div>
                                </div>

                                <!-- Button -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="singlebutton"></label>
                                    <div class="single-form col-md-4">
                                        <c:if test="${action eq 'add'}">
                                            <button id="singlebutton" class="btn btn-primary">Add Dimension
                                            </button>
                                        </c:if>
                                        <c:if test="${action eq 'edit'}">
                                            <button id="singlebutton" class="btn btn-primary">Update Dimension
                                            </button>
                                        </c:if>
                                    </div>
                                </div>
                            </form>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="singlebutton"></label>
                                <div class="single-form col-md-4">
                                    <a href="dimension">
                                        <button class="btn btn-primary">Back to Dimension List</button>
                                    </a>
                                </div>
                            </div>
                        </div>


                        <!-- Admin Courses Tab Start -->

                        <!-- Admin Courses Tab End -->

                        <!-- Admin Courses Tab Content Start -->

                        <!-- Admin Courses Tab Content End -->

                        <!-- Courses Resources Start -->
                        <%-- <jsp:include page="../../component/administration/courses-resources.jsp" />--%>
                        <!-- Courses Resources End -->

                    </div>
                </div>
                <!-- Page Content Wrapper End -->

            </div>
            <!-- Courses Admin End -->

            <jsp:include page="../../component/common/footer.jsp" />

            <!--Back To Start-->
            <a href="#" class="back-to-top">
                <i class="icofont-simple-up"></i>
            </a>
            <!--Back To End-->

        </div>

        <!-- JS
============================================ -->
        <jsp:include page="../../component/common/js.jsp" />
        <script>
            ClassicEditor
                    .create(document.querySelector('#description'), {
                        // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
                    })
                    .then(editor => {
                        window.editor = editor;
                    })
                    .catch(err => {
                        console.error(err.stack);
                    });
        </script>
    </body>

</html>