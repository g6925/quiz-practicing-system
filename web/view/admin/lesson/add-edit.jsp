<!------ Include the above in your HEAD tag ---------->

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <jsp:include page="../../component/common/head.jsp" />
    <style>
        .form-group {
            display: flex;
            align-items: center;
            font-size: 20px;
            justify-content: center;
        }

        .control-label {
            width: 340px;
        }

        #featured {
            width: 20px;
            height: 20px;
        }

        .form-horizontal {
            color: rgb(33 40 50);
            font-weight: 500;
        }

        .error {
            color: red;
            font-size: 15px;
        }

        .single-form label {
            color: red;
            font-size: 15px;
        }
    </style>

    <body>

        <div class="main-wrapper main-wrapper-02">
            <jsp:include page="../../component/administration/header.jsp" />
            <!-- Courses Admin Start -->
            <div class="section overflow-hidden position-relative" id="wrapper">

                <!-- Sidebar Wrapper Start -->
                <jsp:include page="../../component/administration/menu.jsp" />
                <!-- Sidebar Wrapper End -->

                <!-- Page Content Wrapper Start -->
                <div class="page-content-wrapper">
                    <div class="container-fluid custom-container">

                        <!-- Message Start -->

                        <!-- Message End -->
                        <div class="right">
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-2">
                                    <c:if test="${not empty message}">
                                        <div class="alert alert-${alert}">
                                            <h5 style="color: black">${message}</h5>
                                        </div>
                                    </c:if>
                                </div>
                            </div>

                            <form method="POST" class="form-horizontal" id="form_lesson">
                                <!-- Text input-->
                                <div class="form-group">
                                    <label style="font-weight: bold" class="col-md-4 control-label"
                                           for="name">NAME</label>
                                    <div class="single-form col-md-4 ">
                                        <input name="name" id="name" class="form-control input-md" type="text"
                                               value="${lesson.name}">
                                        <div id="name-error"></div>
                                    </div>
                                </div>

                                
                                    <div class="form-group courses-select">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                               for="type">TYPE</label>
                                        <div class="single-form col-md-4">
                                            <select id="type" name="type" class="form-control">
                                                <option disabled selected> -- Type -- </option>
                                                <option ${lesson.type == "Subject Topic" ? "selected" : ""} value="0">Subject Topic</option>
                                                <option ${lesson.type == "Lesson" ? "selected" : ""} value="1">Lesson</option>
                                                <option ${lesson.type == "Quiz" ? "selected" : ""} value="2">Quiz</option>
                                            </select>
                                            <div class="type-error"></div>
                                        </div>
                                    </div>
                                
                                <div class="form-group courses-select">
                                    <label style="font-weight: bold" class="col-md-4 control-label"
                                           for="topic">TOPIC</label>
                                    <div class="single-form col-md-4">
                                        <select class="form-control" name="topicId" id="topicId">
                                            <option disabled selected> -- Topic ID -- </option>
                                            <c:forEach var="topic" items="${topics}">
                                                <option value="${topic.id}" ${lesson.topic.id==topic.id ? "selected" : "" }>${topic.name}</option>
                                            </c:forEach>
                                        </select>
                                        <div id="topicId-error"></div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label style="font-weight: bold" class="col-md-4 control-label" for="order">
                                        ORDER</label>
                                    <div class="single-form col-md-4">
                                        <input type="number" name="order" id="order" data-min="0" class="form-control input-md"
                                               value="${lesson.order}">
                                        <div id="order-error"></div>
                                    </div>
                                </div>

                                <div id="divvideo" class="form-group">
                                    <label style="font-weight: bold" class="col-md-4 control-label"
                                           for="video">VIDEO LINK</label>
                                    <div class="single-form col-md-4 ">
                                        <input name="video" class="form-control input-md" id="video" type="text"
                                               value="${lesson.url}">
                                        <div id="video-error"></div>
                                    </div>
                                </div>

                                <div id="divcontent" class="form-group">
                                    <label style="font-weight: bold" class="col-md-4 control-label" for="content">
                                        HTML CONTENT</label>
                                    <div class="single-form col-md-4">
                                        <textarea class="form-control" id="content"
                                                  name="content">${lesson.description}</textarea>
                                        <div id="content-error"></div>
                                    </div>
                                </div>

                                <div class="form-group courses-select">
                                    <label style="font-weight: bold" class="col-md-4 control-label"
                                           for="status">STATUS</label>
                                    <div class="single-form col-md-4">
                                        <select name="status" id="status" class="form-control">
                                            <option disabled selected> -- Status -- </option>
                                            <option value="1" ${lesson.status==true ? "selected" : "" }>Active
                                            </option>
                                            <option value="0" ${lesson.status==false ? "selected" : "" }>
                                                Inactive
                                            </option>
                                        </select>
                                        <div id="status-error"></div>
                                    </div>
                                </div>

                                <!-- Button -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="singlebutton"></label>
                                    <div class="single-form col-md-4">
                                        <c:if test="${action eq 'add'}">
                                            <button id="singlebutton" class="btn btn-primary">ADD
                                            </button>
                                        </c:if>
                                        <c:if test="${action eq 'edit'}">
                                            <button id="singlebutton" class="btn btn-primary">UPDATE
                                            </button>
                                        </c:if>
                                    </div>
                                </div>
                            </form>
                        </div>


                        <!-- Admin Courses Tab Start -->

                        <!-- Admin Courses Tab End -->

                        <!-- Admin Courses Tab Content Start -->

                        <!-- Admin Courses Tab Content End -->

                        <!-- Courses Resources Start -->
                        <jsp:include page="../../component/administration/courses-resources.jsp" />
                        <!-- Courses Resources End -->

                    </div>
                </div>
                <!-- Page Content Wrapper End -->

            </div>
            <!-- Courses Admin End -->

            <jsp:include page="../../component/common/footer.jsp" />

            <!--Back To Start-->
            <a href="#" class="back-to-top">
                <i class="icofont-simple-up"></i>
            </a>
            <!--Back To End-->

        </div>

        <!-- JS
============================================ -->
        <jsp:include page="../../component/common/js.jsp" />

        <script>
            addEventListener('DOMContentLoaded', (event) => {
                t = document.getElementById("type").value;
                if (t == '0') {
                    $("#divvideo").hide();
                    $("#divcontent").hide();
                }
                if (t == '1') {
                    $("#divvideo").show();
                    $("#divcontent").show();
                }
                if (t == '2') {
                    $("#divcontent").show();
                    $("#divvideo").hide();
                }
            });

            $(document).ready(function () {
                $('#type').on('change', function () {
                    if (this.value == '0') {
                        $("#divvideo").hide();
                        $("#divcontent").hide();
                    }
                    if (this.value == '1') {
                        $("#divvideo").show();
                        $("#divcontent").show();
                    }
                    if (this.value == '2') {
                        $("#divcontent").show();
                        $("#divvideo").hide();
                    }
                });
            });
            ClassicEditor
                    .create(document.querySelector('#content'), {
                        // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
                    })
                    .then(editor => {
                        window.editor = editor;
                    })
                    .catch(err => {
                        console.error(err.stack);
                    });
        </script>
    </body>

</html>