<%-- Document : questionList Created on : Jun 28, 2022, 4:55:42 PM Author : hiepx --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
                <!DOCTYPE html>
                <html lang="en">
                <style>
                    .search-filter {
                        display: flex;
                        align-items: center;
                        justify-content: space-between;
                    }

                    .search-input {
                        margin-right: 12px;
                        padding-left: 10px;
                        height: 50px;
                        border-radius: 10px;
                        border: 1px solid rgba(48, 146, 85, 0.2);
                    }

                    .search-button {
                        color: #fff;
                        line-height: 50px;
                        font-size: 15px;
                        padding: 0 30px;
                        border-radius: 10px;
                        border: 0 solid transparent;
                        background-color: #309255;
                        border-color: #309255;
                        font-weight: 500;
                    }

                    .featured {
                        margin-top: 20px;
                        display: flex;
                        align-items: center;
                    }

                    .featured-label {
                        font-size: 16px;
                        font-weight: 450;
                        color: rgb(33 40 50);
                    }


                    /*                    .item-thumb img{
                                
                            }*/
                </style>
                <script>
                    function submitMyForm() {
                        document.forms["form"].submit();
                    }
                </script>

                <jsp:include page="../../component/common/head.jsp" />

                <body>

                    <div class="main-wrapper main-wrapper-02">

                        <!-- Login Header Start -->
                        <jsp:include page="../../component/administration/header.jsp" />
                        <!-- Login Header End -->

                        <!-- Courses Admin Start -->
                        <div class="section overflow-hidden position-relative" id="wrapper">

                            <!-- Sidebar Wrapper Start -->
                            <jsp:include page="../../component/administration/menu.jsp" />
                            <!-- Sidebar Wrapper End -->

                            <!-- Page Content Wrapper Start -->
                            <div class="page-content-wrapper">
                                <div class="container-fluid custom-container">

                                    <!-- Message Start -->

                                    <!-- Message End -->

                                    <!-- Admin Courses Tab Start -->
                                    <div class="admin-courses-tab">
                                        <h3 class="title">Quizzes</h3>



                                        <div class="courses-tab-wrapper">


                                            <form id="form" action="./quizzes" method="Post">
                                                <div class="search-filter">


                                                    <div class="tab-btn">

                                                        <input type="text" class="search-input"
                                                            placeholder="Search here" name="keyWord" value="${keyWord}">
                                                        <input class="search-button" type="submit" value="Search" />
                                                    </div>
                                                    <div class="courses-select">
                                                        <select onchange="this.form.submit()" name="subject">
                                                            <option disabled selected value> -- Filter Subject --
                                                            </option>
                                                            <c:forEach items="${listsubject}" var="s">
                                                                <option <c:if test="${s.id == subjectID}">
                                                                    selected="selected"
                                                                    </c:if>
                                                                    value="${s.id}">${s.title}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                    <div class="courses-select">
                                                        <select onchange="this.form.submit()" name="type">
                                                            <option disabled selected value> -- Filter Type --
                                                            </option>
                                                            <option <c:if test="${type == 'Practice'}"> selected </c:if>
                                                                value="Practice">Practice</option>
                                                            <option <c:if test="${type == 'Exam'}"> selected </c:if>
                                                                value="Exam">Exam</option>
                                                        </select>
                                                    </div>

                                                    <div class="tab-btn">
                                                        <a href="${pageContext.request.contextPath}/course/quizzes/details"
                                                            class="btn btn-primary btn-hover-dark">New Quiz</a>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <!-- Admin Courses Tab End -->

                                    <!-- Admin Courses Tab Content Start -->
                                    <div class="admin-courses-tab-content">
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="tab1">
                                                <!-- Courses Item Start -->
                                                <c:if test="${listquizzes.size()>0}">
                                                    <c:forEach items="${listquizzes}" var="i">
                                                        <div class="courses-item">
                                                            <div class="item-thumb">
                                                                <p>${i.id}</p>

                                                            </div>


                                                            <div class="content-title">

                                                                <h3 class="title"><a href="">${i.name}</a></h3>
                                                            </div>

                                                            <div class="content-wrapper">

                                                                <div class="content-box" style=" width: 190px;">
                                                                    <p>Subject</p>
                                                                    <span class="count">${i.subject.title}</span>
                                                                </div>

                                                                <div class="content-box" style=" width: 190px;">
                                                                    <p>Duratation</p>
                                                                    <span class="count">${i.duration}</span>
                                                                </div>

                                                                <div class="content-box" style=" width: 190px;">
                                                                    <p>Level</p>
                                                                    <span class="count">
                                                                        ${i.level}

                                                                    </span>
                                                                </div>
                                                                <div class="content-box"
                                                                    style=" width: 190px;background-color: #e5ecff;">
                                                                    <p>Pass Rate</p>
                                                                    <span class="count">
                                                                        ${i.passRate}

                                                                    </span>
                                                                </div>
                                                                <div class="content-box"
                                                                    style=" width: 190px;background-color: #def2e6;">
                                                                    <p>Type</p>
                                                                    <span class="count">
                                                                        ${i.type}

                                                                    </span>
                                                                </div>

                                                                <div class="tab-btn">
                                                                    <a href="${pageContext.request.contextPath}/course/quizzes/edit?id=${i.id}"
                                                                        class="btn btn-primary btn-hover-dark">Edit </a>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </c:forEach>
                                                </c:if>


                                                <!-- Courses Item End -->

                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="page-pagination">



                                        <ul class="pagination justify-content-center">


                                            <c:if test="${pageindex - 2 > 1}">
                                                <input type="hidden" name="page" value="1">
                                                <li class=""><a <c:if test="${filter == 0}">
                                                        href="${pageContext.request.contextPath}/course/quizzes?page=1"
                                            </c:if>
                                            <c:if test="${filter == 1}"> onclick="submitMyForm()"</c:if>
                                            class="pagination-link">First</a></li>
                                            </c:if>
                                            <c:if test="${pageindex - 2 >= 0}">
                                                <c:forEach begin="${pageindex - 2}" end="${pageindex - 1}" var="i">
                                                    <c:if test="${i >= 1}">
                                                        <input type="hidden" name="page" value="${i}">
                                                        <li class=""><a <c:if test="${filter == 0}">
                                                                href="${pageContext.request.contextPath}/course/quizzes?page=${i}"
                                                    </c:if>
                                                    <c:if test="${filter == 1}"> onclick="submitMyForm()"</c:if>
                                                    class="pagination-link">${i}</a></li>
                                            </c:if>
                                            </c:forEach>
                                            </c:if>
                                            <li class=""><a class="pagination-link"
                                                    style="background-color: #309255;color: white;" <c:if
                                                    test="${filter == 0}">
                                                    href="${pageContext.request.contextPath}/course/quizzes?page=${pageindex}"
                                                    </c:if>
                                                    <c:if test="${filter == 1}"> onclick="submitMyForm()"</c:if>
                                                    >${pageindex}
                                                </a></li>
                                            <c:forEach begin="${pageindex +1}" end="${pageindex + 2}" var="i">
                                                <c:if test="${i <= totalpage}">
                                                    <input type="hidden" name="page" value="${i}">
                                                    <li class=""><a <c:if test="${filter == 0}">
                                                            href="${pageContext.request.contextPath}/course/quizzes?page=${i}"
                                                </c:if>
                                                <c:if test="${filter == 1}"> onclick="submitMyForm()"</c:if>
                                                class="pagination-link">${i}</a></li>
                                                </c:if>
                                            </c:forEach>
                                            <c:if test="${pageindex + 2 < totalpage}">
                                                <input type="hidden" name="page" value="${totalpage}">
                                                <li class=""><a <c:if test="${filter == 0}">
                                                        href="${pageContext.request.contextPath}/course/quizzes?page=${totalpage}"
                                            </c:if>
                                            <c:if test="${filter == 1}"> onclick="submitMyForm()"</c:if>
                                            class="pagination-link">Last</a></li>
                                            </c:if>
                                        </ul>

                                    </div>
                                    </form>

                                    <!-- Admin Courses Tab Content End -->

                                    <!-- Courses Resources Start -->

                                    <!-- Courses Resources End -->
                                    <jsp:include page="../../component/administration/courses-resources.jsp" />
                                </div>
                            </div>
                            </form>

                            <!-- Page Content Wrapper End -->

                        </div>
                        <!-- Courses Admin End -->
                        <jsp:include page="../../component/common/footer.jsp" />
                        <!-- Footer Start  -->

                        <!-- Footer End -->

                        <!--Back To Start-->
                        <a href="#" class="back-to-top">
                            <i class="icofont-simple-up"></i>
                        </a>
                        <!--Back To End-->

                    </div>






                    <!-- JS
        ============================================ -->

                    <!-- Modernizer & jQuery JS -->
                    <jsp:include page="../../component/common/js.jsp" />

                    <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->
                    <!-- <script src="assets/js/plugins.min.js"></script> -->


                    <!-- Main JS -->


                </body>

                </html>