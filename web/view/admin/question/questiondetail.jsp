<!------ Include the above in your HEAD tag ---------->




<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<html lang="en">

    <jsp:include page="../../component/common/head.jsp" />

    <style>
        .form-group {
            display: flex;
            align-items: center;
            font-size: 20px;
            justify-content: center;
        }

        .control-label {
            width: 340px;
        }

        #featured {
            width: 20px;
            height: 20px;
        }

        .form-horizontal {
            color: rgb(33 40 50);
            font-weight: 500;
        }
        .table{
            max-width: 80%;
        }
    </style>
    <script>
        function showMes(id, qid) {
            var check = confirm("Are you want to remove this answer ?");
            if (check === true) {
                window.location.href = "deleteanswer?id=" + id + "&&qid=" + qid;
            }
        }
    </script>
    <body>

        <div class="main-wrapper main-wrapper-02">
            <jsp:include page="../../component/administration/header.jsp" />
            <!-- Courses Admin Start -->
            <div class="section overflow-hidden position-relative" id="wrapper">

                <!-- Sidebar Wrapper Start -->
                <jsp:include page="../../component/administration/menu.jsp" />
                <!-- Sidebar Wrapper End -->

                <!-- Page Content Wrapper Start -->
                <div class="page-content-wrapper">
                    s
                    <div class="container-fluid">


                        <div class="right">



                            <form action="questiondetail" method="post" class="form-horizontal">
                                <input name="questionID" class=" input-md" value="${question.id}" type="hidden">
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="question_content">Content</label>
                                    <div class="single-form  col-md-4">
                                        <input name="question_content" class="form-control input-md" <c:if test="${check==false}"> readonly</c:if>
                                               value="${question.content}" type="text">
                                        <div id="subject_name-error"></div>
                                    </div>
                                </div>

                                <!-- Select Basic -->
                                <div class="form-group courses-select">
                                    <label class="col-md-4 control-label" for="question_subject">Subject</label>
                                    <div class="single-form col-md-4">
                                        <c:if test="${check==false}">                                    
                                            ${question.subject.title}
                                            <input value="${question.subject.id}" name="question_subject" hidden/>
                                        </c:if>
                                        <c:if test="${check!=false}">
                                            <select id="subject_category" name="question_subject" onchange="this.form.submit()"
                                                    class="form-control1" style="overflow: scroll">
                                                <c:forEach items="${subjects}" var="s">
                                                    <option <c:if
                                                            test="${s.id == subjectID}">
                                                            selected="selected"
                                                        </c:if>
                                                        value="${s.id}" >${s.title}</option>
                                                </c:forEach>
                                            </select>
                                        </c:if>
                                        <div id="subject_category-error"></div>
                                    </div>
                                </div>
                                <!-- Select Basic -->
                                <div class="form-group courses-select">
                                    <label class="col-md-4 control-label" for="question_topic">Topic</label>
                                    <div class="single-form col-md-4">
                                        <c:if test="${check==false}">${question.topic.name}
                                            <input class="col-md-4 control-label" value="${question.topic.id}" name="question_topic" hidden/>
                                        </c:if>
                                        <c:if test="${check!=false}">
                                            <select id="subject_category" name="question_topic"
                                                    class="form-control1" style="overflow: scroll">
                                                <c:forEach items="${topics}" var="t">
                                                    <option <c:if
                                                            test="${t.name == question.topic.name}">
                                                            selected="selected" style="opacity: 50%"
                                                        </c:if>
                                                        value="${t.id}">${t.name}</option>
                                                </c:forEach>
                                            </select>
                                        </c:if>

                                        <div id="subject_category-error"></div>
                                    </div>
                                </div> 


                                <div class="form-group courses-select">
                                    <label class="col-md-4 control-label" for="question_group">Group</label>
                                    <div class="single-form col-md-4">
                                        <c:if test="${check==false}">
                                            <c:if test="${question.dimension.get(0).type=='Group'}">${question.dimension.get(0).name} <input class="col-md-4 control-label" value="${question.dimension.get(0).id}" name="question_group" hidden/></c:if>
                                            <c:if test="${question.dimension.get(1).type=='Group'}">${question.dimension.get(1).name} <input class="col-md-4 control-label" value="${question.dimension.get(1).id}" name="question_group" hidden></c:if>
                                        </c:if>
                                        <c:if test="${check!=false}">
                                            <select id="subject_category" name="question_group"
                                                    class="form-control1" style="overflow: scroll">
                                                <c:forEach items="${dimensions}" var="d">
                                                    <c:if test="${d.type =='Group'}">
                                                        <option <c:if
                                                                test="${d.name == question.dimension.get(0).name || d.name == question.dimension.get(1).name}">
                                                                selected="selected" style="opacity: 50%"
                                                            </c:if>
                                                            value="${d.id}">${d.name}</option>
                                                    </c:if>

                                                </c:forEach>
                                            </select>
                                        </c:if>
                                        <div id="subject_category-error"></div>
                                    </div>
                                </div>

                                <div class="form-group courses-select">
                                    <label class="col-md-4 control-label" for="question_domain">Domain</label>
                                    <div class="single-form col-md-4">
                                        <c:if test="${check==false}"> 
                                            <c:if test="${question.dimension.get(0).type=='Domain'}">${question.dimension.get(0).name}<input class="col-md-4 control-label" value="${question.dimension.get(0).id}" name="question_domain" hidden/></c:if>
                                            <c:if test="${question.dimension.get(1).type=='Domain'}">${question.dimension.get(1).name}<input class="col-md-4 control-label" value="${question.dimension.get(1).id}" name="question_domain" hidden/></c:if>
                                        </c:if>
                                        <c:if test="${check!=false}">
                                            <select id="subject_category" name="question_domain"
                                                    class="form-control1" style="overflow: scroll">
                                                <c:forEach items="${dimensions}" var="d">
                                                    <c:if test="${d.type =='Domain'}">
                                                        <option <c:if
                                                                test="${d.name == question.dimension.get(0).name || d.name == question.dimension.get(1).name}">
                                                                selected="selected" style="opacity: 50%"
                                                            </c:if>
                                                            value="${d.id}">${d.name}</option>
                                                    </c:if>

                                                </c:forEach>
                                            </select>
                                        </c:if>
                                        <div id="subject_category-error"></div>
                                    </div>
                                </div>
                                <!-- Select Basic -->
                                <div class="form-group courses-select">
                                    <label class="col-md-4 control-label" for="question_level">Level</label>
                                    <div class="single-form col-md-4">
                                        <c:if test="${check==false}">${question.leve.name}<input class="col-md-4 control-label" value="${question.leve.id}" name="question_level" hidden/></c:if>
                                        <c:if test="${check!=false}">
                                            <select id="subject_category" name="question_level"
                                                    class="form-control1" style="overflow: scroll">
                                                <c:forEach items="${levels}" var="l">
                                                    <option value="${l.id}"<c:if test="${question.leve.id==l.id}">selected</c:if>>${l.name}</option>
                                                </c:forEach>
                                            </select>
                                        </c:if>

                                        <div id="subject_category-error"></div>
                                    </div>
                                </div>
                                <!-- Select Basic -->
                                <div class="form-group courses-select">
                                    <label class="col-md-4 control-label" for="status">Status</label>
                                    <div class="single-form col-md-4">
                                        <select id="status" name="status" class="form-control1" style="overflow: scroll" >
                                            <option value="0" <c:if test="${question.status=='true'}">selected</c:if>>Active</option>
                                            <option value="1" <c:if test="${question.status=='false'}">selected</c:if>>Inactive</option>
                                            </select>
                                            <div id="status-error"></div>
                                        </div>
                                    </div>

                                    <!-- Button -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="singlebutton" ></label>
                                        <div class="single-form col-md-4">
                                            <button id="singlebutton" name="singlebutton" value="edit"
                                                    class="btn btn-primary">Edit Question</button>
                                            <a class="btn btn-primary" href="questiondetail?questionID=${question.id}&&add=add" <c:if test="${check==false}">style="pointer-events: none;"</c:if>>Add answer</a>
                                        </div>
                                    </div>

                                </form>

                                <!--Answer-->

                                <div class="form-group courses-select">

                                    <table class="table">

                                        <thead>
                                            <tr>
                                                <th scope="col">ID</th>
                                                <th scope="col">Content</th>
                                                <th scope="col">Correct</th>
                                                <th scope="col">Explanation</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>

                                    <c:forEach items="${question.answers}" var="a">
                                        <form action="editanswer" method="get">
                                            <tr>
                                            <input name="answer_id" hidden value="${a.id}"/>
                                            <input name="question_id" hidden value="${question.id}"/>
                                            <th scope="row"> ${a.id}</th>
                                            <td><input name="content" value="${a.content}" style="border: none;" <c:if test="${check==false}">readonly</c:if>></td>
                                                <td>
                                                <c:if test="${check==false}"><input value="${a.correct}" readonly name="correct" style="border: none"/></c:if>
                                                <c:if test="${check!=false}" >
                                                    <select name="correct">
                                                        <option value="0" <c:if test="${a.correct == 'true'}">selected</c:if>>
                                                                True
                                                            </option>
                                                            <option value="1" <c:if test="${a.correct == 'false'}">selected</c:if>>
                                                                False
                                                            </option>
                                                        </select>
                                                </c:if>

                                            </td>
                                            <td><input <c:if test="${check==false}">readonly</c:if> name="explanation" value="${a.explanation}" style="border: none;"></td>
                                                <td>
                                                    <button class="btn btn-primary"  <c:if test="${check==false}">disabled</c:if>>Edit</button>
                                                <a class="btn btn-primary" name="delete" value="delete" onclick="showMes(${a.id},${question.id})" <c:if test="${check==false}">style="pointer-events: none;"</c:if>>Delete</a>
                                                </td>
                                                </tr>
                                            </form>
                                    </c:forEach>
                                    <c:if test="${add == 'add'}">
                                        <form action="addanswer" method="get">
                                            <input hidden name="questionID" value="${question.id}">
                                            <tr>
                                                <td></td>
                                                <td><input name="content"><p style="color: red">${mess}</p></td>
                                                <td>
                                                    <select name="correct">
                                                        <option value="0">True</option>
                                                        <option value="1">False</option>
                                                    </select>
                                                </td>
                                                <td><input name="explanation"></td>
                                                <td><button class="btn btn-primary">Add</button></td>
                                            </tr>
                                        </form>

                                    </c:if>


                                </table><br/>

                            </div>





                        </div>
                    </div>
                    <!-- Page Content Wrapper End -->




                    <!-- Page Content Wrapper End -->

                </div>
                <!-- Courses Admin End -->






                <jsp:include page="../../component/common/footer.jsp" />

                <!--Back To Start-->
                <a href="#" class="back-to-top">
                    <i class="icofont-simple-up"></i>
                </a>
                <!--Back To End-->

            </div>

            <!-- JS
    ============================================ -->
            <jsp:include page="../../component/common/js.jsp" />
            <script>
                ClassicEditor
                        .create(document.querySelector('#subject_description'), {
                            // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
                        })
                        .then(editor => {
                            window.editor = editor;
                        })
                        .catch(err => {
                            console.error(err.stack);
                        });
            </script>
    </body>

</html>