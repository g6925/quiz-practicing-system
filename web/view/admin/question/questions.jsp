<%-- Document : questionList Created on : Jun 28, 2022, 4:55:42 PM Author : hiepx --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
    <style>
        .search-filter {
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .search-input {
            margin-right: 12px;
            padding-left: 10px;
            height: 50px;
            border-radius: 10px;
            border: 1px solid rgba(48, 146, 85, 0.2);
        }

        .search-button {
            color: #fff;
            line-height: 50px;
            font-size: 15px;
            padding: 0 30px;
            border-radius: 10px;
            border: 0 solid transparent;
            background-color: #309255;
            border-color: #309255;
            font-weight: 500;
        }

        .featured {
            margin-top: 20px;
            display: flex;
            align-items: center;
        }

        .featured-label {
            font-size: 16px;
            font-weight: 450;
            color: rgb(33 40 50);
        }


        /*                    .item-thumb img{
                    
                }*/
    </style>
    <script>
        function submitMyForm() {
            document.forms["form"].submit();
        }
        function submitMyForm1(page) {
            document.getElementById('page').value = page;
            document.forms["form"].submit();
        }
    </script>

    <jsp:include page="../../component/common/head.jsp" />

    <body>

        <div class="main-wrapper main-wrapper-02">

            <!-- Login Header Start -->
            <jsp:include page="../../component/administration/header.jsp" />
            <!-- Login Header End -->

            <!-- Courses Admin Start -->
            <div class="section overflow-hidden position-relative" id="wrapper">

                <!-- Sidebar Wrapper Start -->
                <jsp:include page="../../component/administration/menu.jsp" />
                <!-- Sidebar Wrapper End -->

                <!-- Page Content Wrapper Start -->
                <form id="form" action="./questions" method="Post">
                    <div class="page-content-wrapper">
                        <div class="container-fluid custom-container">

                            <!-- Message Start -->

                            <!-- Message End -->

                            <!-- Admin Courses Tab Start -->
                            <div class="admin-courses-tab">
                                <h3 class="title">Question</h3>



                                <div class="courses-tab-wrapper">



                                    <div class="search-filter">


                                        <div class="tab-btn">

                                            <input type="text" class="search-input"
                                                   placeholder="Search here" name="keyWord" value="${keyWord}">
                                            <input class="search-button" type="submit" value="Search" />
                                        </div>
                                        <div class="courses-select">
                                            <select onchange="this.form.submit()" name="topic">
                                                <option disabled selected value> -- Filter Topic --
                                                </option>
                                                <c:forEach items="${topics}" var="t">
                                                    <option <c:if test="${t.id == topicID}">
                                                            selected="selected"
                                                        </c:if>
                                                        value="${t.id}">${t.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="courses-select">
                                            <select onchange="this.form.submit()" name="dimension">
                                                <option disabled selected value> -- Filter Dimension --
                                                </option>
                                                <c:forEach items="${dimensions}" var="d">
                                                    <option <c:if test="${d.name == dimension}"> selected
                                                                                                 </c:if>
                                                                                                 value="${d.name}">${d.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>

                                        <div class="courses-select">
                                            <select onchange="this.form.submit()" name="level">
                                                <option disabled selected value> -- Filter Level --
                                                </option>
                                                <c:forEach items="${levels}" var="l">
                                                    <option <c:if test="${l.id== levelID}">
                                                            selected="selected"
                                                        </c:if>
                                                        value="${l.id}">${l.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="courses-select">
                                            <select onchange="this.form.submit()" name="status">
                                                <option disabled selected value> -- Filter status --
                                                </option>
                                                <option <c:if test="${status == 'true'}">
                                                        selected="selected"
                                                    </c:if>
                                                    value="true">Active
                                                </option>
                                                <option <c:if test="${status == 'false'}">
                                                        selected="selected"
                                                    </c:if>
                                                    value="false">Inactive
                                                </option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="tab-btn">
                                        <a href="${pageContext.request.contextPath}/admin/import"
                                           class="btn btn-primary btn-hover-dark">New Question</a>
                                    </div>

                                </div>
                            </div>
                            <!-- Admin Courses Tab End -->

                            <!-- Admin Courses Tab Content Start -->
                            <div class="admin-courses-tab-content">
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="tab1">
                                        <!-- Courses Item Start -->
                                        <c:if test="${listquestion.size()>0}">
                                            <c:forEach begin="0" end="${listquestion.size()-1}" step="1"
                                                       var="i">
                                                <c:if
                                                    test="${listquestion[i].content!=listquestion[i+1].content}">
                                                    <div class="courses-item">
                                                        <div class="item-thumb">
                                                            <p>${listquestion[i].id}</p>

                                                        </div>


                                                        <div class="content-title">
                                                            <div class="meta">
                                                                <c:if
                                                                    test="${listquestion[i].status==true}">
                                                                    <a href="#" class="action">Active</a>
                                                                </c:if>
                                                                <c:if
                                                                    test="${listquestion[i].status==false}">
                                                                    <a href="#" class="action">Inactive</a>
                                                                </c:if>


                                                            </div>
                                                            <h3 class="title"><a
                                                                    href="">${listquestion[i].content}</a>
                                                            </h3>
                                                        </div>

                                                        <div class="content-wrapper">

                                                            <div class="content-box" style=" width: 300px;">
                                                                <p>Dimension</p>
                                                                <c:if
                                                                    test="${listquestion[i].content==listquestion[i-1].content}">
                                                                    <span
                                                                        class="count">${listquestion[i].getDimension().get(0).getName()}/${listquestion[i-1].getDimension().get(1).getName()}</span>
                                                                </c:if>
                                                                <c:if
                                                                    test="${listquestion[i].content!=listquestion[i-1].content}">
                                                                    <span
                                                                        class="count">${listquestion[i].dimension.get(0).name}</span>
                                                                </c:if>
                                                            </div>

                                                            <div class="content-box" style=" width: 300px;">
                                                                <p>Topic</p>
                                                                <span
                                                                    class="count">${listquestion[i].topic.name}</span>
                                                            </div>

                                                            <div class="content-box" style=" width: 300px;">
                                                                <p>Level</p>
                                                                <span class="count">
                                                                    <c:if
                                                                        test="${listquestion[i].level == 1}">
                                                                        Easy</c:if>
                                                                    <c:if
                                                                        test="${listquestion[i].level == 2}">
                                                                        Medium</c:if>
                                                                    <c:if
                                                                        test="${listquestion[i].level == 3}">
                                                                        Hard</c:if>
                                                                    </span>
                                                                </div>

                                                                <div class="tab-btn">
                                                                    <a href="${pageContext.request.contextPath}/admin/questiondetail?questionID=${listquestion[i].id}"
                                                                   class="btn btn-primary btn-hover-dark">Edit
                                                                </a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </c:if>

                                            </c:forEach>
                                        </c:if>


                                        <!-- Courses Item End -->

                                    </div>
                                </div>
                            </div>
                            <div class="page-pagination">
                                <input id="page" type="hidden" name="page">
                                <ul class="pagination justify-content-center">
                                    <c:if test="${pageindex - 2 > 1}">

                                        <li class=""><a <c:if test="${filter == 0}">
                                                    href="${pageContext.request.contextPath}/admin/questions?page=1"
                                                </c:if>
                                                <c:if test="${filter == 1}"> onclick="submitMyForm1(1)"</c:if>
                                                    class="pagination-link">First </a>
                                            </li>
                                    </c:if>
                                    <c:if test="${pageindex - 2 >= 0}">
                                        <c:forEach begin="${pageindex - 2}" end="${pageindex - 1}" var="i">
                                            <c:if test="${i >= 1}">

                                                <li class=""><a <c:if test="${filter == 0}">
                                                            href="${pageContext.request.contextPath}/admin/questions?page=${i}"
                                                        </c:if>
                                                        <c:if test="${filter == 1}"> onclick="submitMyForm1(${i})"</c:if>
                                                        class="pagination-link">${i}</a>
                                                </li>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                    <li class=""><a class="pagination-link"
                                                    style="background-color: #309255;color: white;" <c:if
                                                        test="${filter == 0}">
                                                        href="${pageContext.request.contextPath}/admin/questions?page=${pageindex}"
                                                    </c:if>
                                                    <c:if test="${filter == 1}"> onclick="submitMyForm1(${pageindex})"</c:if>
                                                    >${pageindex}
                                        </a>

                                    </li>
                                    <c:forEach begin="${pageindex +1}" end="${pageindex + 2}" var="i">
                                        <c:if test="${i <= totalpage}">

                                            <li class=""><a <c:if test="${filter == 0}">
                                                        href="${pageContext.request.contextPath}/admin/questions?page=${i}"
                                                    </c:if>
                                                    <c:if test="${filter == 1}"> onclick="submitMyForm1(${i})"</c:if>
                                                    class="pagination-link">${i}</a>
                                            </li>
                                        </c:if>
                                    </c:forEach>
                                    <c:if test="${pageindex + 2 < totalpage}">

                                        <li class=""><a <c:if test="${filter == 0}">
                                                    href="${pageContext.request.contextPath}/admin/questions?page=${totalpage}"
                                                </c:if>
                                                <c:if test="${filter == 1}"> onclick="submitMyForm1(${totalpage})"</c:if>
                                                class="pagination-link">Last</a>
                                        </li>
                                    </c:if>
                                </ul>

                            </div>
                            <!-- Admin Courses Tab Content End -->

                            <!-- Courses Resources Start -->

                            <!-- Courses Resources End -->
                            <jsp:include page="../../component/administration/courses-resources.jsp" />
                        </div>
                        <jsp:include page="../../component/administration/courses-resources.jsp" />
                    </div>
                </form>

                <!-- Page Content Wrapper End -->

            </div>
            <!-- Courses Admin End -->
            <jsp:include page="../../component/common/footer.jsp" />
            <!-- Footer Start  -->

            <!-- Footer End -->

            <!--Back To Start-->
            <a href="#" class="back-to-top">
                <i class="icofont-simple-up"></i>
            </a>
            <!--Back To End-->

        </div>






        <!-- JS
============================================ -->

        <!-- Modernizer & jQuery JS -->
        <jsp:include page="../../component/common/js.jsp" />


    </body>

</html>
