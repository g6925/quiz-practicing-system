<!------ Include the above in your HEAD tag ---------->




<%@page contentType="text/html" pageEncoding="UTF-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


            <html lang="en">

            <jsp:include page="../../component/common/head.jsp" />

            <style>
                .form-group {
                    display: flex;
                    align-items: center;
                    font-size: 20px;
                    justify-content: center;
                }

                .control-label {
                    width: 340px;
                }

                #featured {
                    width: 20px;
                    height: 20px;
                }

                .form-horizontal {
                    color: rgb(33 40 50);
                    font-weight: 500;
                }
            </style>

            <body>

                <div class="main-wrapper main-wrapper-02">
                    <jsp:include page="../../component/administration/header.jsp" />
                    <!-- Courses Admin Start -->
                    <div class="section overflow-hidden position-relative" id="wrapper">

                        <!-- Sidebar Wrapper Start -->
                        <div class="sidebar-wrapper">
                            <div class="menu-list">
                                <a class="active" href="courses-admin.html"><img
                                        src="../../assets/images/menu-icon/icon-1.png" alt="Icon"></a>
                                <a href="messages.html"><img src="../../assets/images/menu-icon/icon-2.png"
                                        alt="Icon"></a>
                                <a href="overview.html"><img src="../../assets/images/menu-icon/icon-3.png"
                                        alt="Icon"></a>
                                <a href="engagement.html"><img src="../../assets/images/menu-icon/icon-4.png"
                                        alt="Icon"></a>
                                <a href="traffic-conversion.html"><img src="../../assets/images/menu-icon/icon-5.png"
                                        alt="Icon"></a>
                            </div>
                        </div>
                        <!-- Sidebar Wrapper End -->

                        <!-- Page Content Wrapper Start -->
                        <div class="page-content-wrapper">
                            <div class="container-fluid custom-container">

                                <!-- Message Start -->

                                <!-- Message End -->




                                <div class="right">



                                    <form action="edit" method="POST" class="form-horizontal"
                                        enctype="multipart/form-data" id="form_subject-edit">



                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for=""></label>
                                            <div class="col-md-4">
                                                <input id="" name="subject_id" class=" input-md"
                                                    value="${subject.getId()}" type="hidden">

                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="subject_name">SUBJECT
                                                NAME</label>
                                            <div class="single-form  col-md-4">
                                                <input id="subject_name" name="subject_name" placeholder="SUBJECT NAME"
                                                    class="form-control input-md" value="${subject.getTitle()}"
                                                    type="text">
                                                <div id="subject_name-error"></div>
                                            </div>
                                        </div>



                                        <!-- Select Basic -->
                                        <div class="form-group courses-select">
                                            <label class="col-md-4 control-label" for="subject_category">SUBJECT
                                                CATEGORY</label>
                                            <div class="single-form col-md-4">
                                                <select id="subject_category" name="subject_category"
                                                    class="form-control">
                                                    <c:forEach items="${categorys}" var="c">
                                                        <option <c:if
                                                            test="${c.getId() == subject.getCategory().getId()}">
                                                            selected="selected"
                                                            </c:if>
                                                            value="${c.getId()}">${c.getName()}</option>
                                                    </c:forEach>
                                                </select>
                                                <div id="subject_category-error"></div>
                                            </div>
                                        </div>



                                        <div class="form-group" style="height: 80px;
                                     ">
                                            <label class="col-md-4 control-label" for="product_categorie"></label>
                                            <div class="col-md-4">
                                                <input type="checkbox" id="featured" <c:if
                                                    test="${subject.isFeatured()}">
                                                checked="checked"
                                                </c:if>
                                                name="featured" value="1">
                                                <label for="featured">Featured</label>
                                            </div>
                                        </div>





                                        <!-- Text input-->
                                        <div class="form-group" style="    margin-top: -20px;">
                                            <label class="col-md-4 control-label" for="tag_line">TAG LINE</label>
                                            <div class="single-form col-md-4">
                                                <input id="tag_line" name="tag_line" placeholder="TAG LINE"
                                                    class="form-control input-md" value="${subject.getTagline()}"
                                                    type="text">
                                                <div id="tag_line-error"></div>
                                            </div>
                                        </div>






                                        <div class="form-group courses-select">
                                            <label class="col-md-4 control-label" for="expert">EXPERT</label>
                                            <div class="single-form col-md-4">
                                                <select id="expert" name="expert" class="form-control">
                                                    <c:forEach items="${experts}" var="e">


                                                        <option <c:if
                                                            test="${e.getId() == subject.getTaught().getId()}">
                                                            selected="selected"
                                                            </c:if>
                                                            value="${e.getId()}">${e.getName()}
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                                <div id="expert-error"></div>
                                            </div>
                                        </div>


                                        <!-- Select Basic -->
                                        <div class="form-group courses-select">
                                            <label class="col-md-4 control-label" for="status">STATUS</label>
                                            <div class="single-form col-md-4">
                                                <select id="status" name="status" class="form-control">

                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                                <div id="status-error"></div>
                                            </div>
                                        </div>




                                        <!-- Preview Thumbnail-->
                                        <div class="form-group" style="margin-top: 33px;">
                                            <label class="col-md-4 control-label">THUMBNAIL IMAGE</label>
                                            <div class="col-md-4">
                                                <img id="image-preview" class="thumbnail img-responsive"
                                                    src="${subject.thumbnail}" alt="">
                                            </div>
                                        </div>


                                        <!-- File Button -->
                                        <div class="form-group" style="height: 80px;
                                     ">
                                            <label class="col-md-4 control-label" for="filebutton"></label>
                                            <div class="single-form col-md-4">
                                                <input id="image" name="image" class="input-file form-control"
                                                    type="file" data-can-empty="true">
                                                <div id="image-error"></div>
                                            </div>

                                        </div>


                                        <!-- Text input Listed Price-->
                                        <div class="form-group" style="margin-top: 20px">
                                            <label class="col-md-4 control-label" for="listedPrice">LISTED PRICE</label>
                                            <div class="single-form col-md-4">
                                                <input id="listedPrice" name="listedPrice" placeholder="LISTED PRICE"
                                                    class="form-control input-md" type="number"
                                                    value="${subject.getListedPrice()}">
                                                <div id="listedPrice-error"></div>
                                            </div>
                                        </div>

                                        <!-- Text input Sale Price-->
                                        <div class="form-group" style="margin-top: 20px;
                                     margin-bottom: 20px;">
                                            <label class="col-md-4 control-label" for="salePrice">SAlE PRICE</label>
                                            <div class="single-form col-md-4">
                                                <input id="salePrice" name="salePrice" placeholder="SAlE PRICE"
                                                    class="form-control input-md" type="number"
                                                    value="${subject.getSalePrice()}">
                                                <div id="salePrice-error"></div>
                                            </div>
                                        </div>


                                        <!-- Textarea -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="subject_description">SUBJECT
                                                DESCRIPTION</label>
                                            <div class="single-form col-md-4">
                                                <textarea class="form-control" id="subject_description"
                                                    name="subject_description">${subject.description}</textarea>
                                                <div id="subject_description-error"></div>
                                            </div>
                                        </div>



                                        <!-- Button -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="singlebutton"></label>
                                            <div class="single-form col-md-4">
                                                <button id="singlebutton" name="singlebutton"
                                                    class="btn btn-primary">Edit Subject</button>
                                            </div>
                                        </div>





                                    </form>
                                </div>









                                <!-- Admin Courses Tab Start -->

                                <!-- Admin Courses Tab End -->

                                <!-- Admin Courses Tab Content Start -->

                                <!-- Admin Courses Tab Content End -->

                                <!-- Courses Resources Start -->
                                <jsp:include page="../../component/administration/courses-resources.jsp" />

                                <!-- Courses Resources End -->

                            </div>
                        </div>
                        <!-- Page Content Wrapper End -->

                    </div>
                    <!-- Courses Admin End -->






                    <jsp:include page="../../component/common/footer.jsp" />

                    <!--Back To Start-->
                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                </div>

                <!-- JS
============================================ -->
                <jsp:include page="../../component/common/js.jsp" />
                <script>
                    ClassicEditor
                        .create(document.querySelector('#subject_description'), {
                            // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
                        })
                        .then(editor => {
                            window.editor = editor;
                        })
                        .catch(err => {
                            console.error(err.stack);
                        });
                </script>
            </body>

            </html>