<%-- Document : addDimension Created on : Jun 29, 2022, 11:09:07 PM Author : xuant --%>



<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!------ Include the above in your HEAD tag ---------->
<html lang="en">

    <jsp:include page="../../component/common/head.jsp" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
        body {}

        .table-responsive {
            margin: 30px 0;
        }

        .table-wrapper {
            min-width: 1000px;
            background: #fff;
            padding: 20px 25px;
            border-radius: 3px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
        }

        .table-title {
            padding-bottom: 15px;
            background: #309255;
            color: #fff;
            padding: 16px 30px;
            margin: -20px -25px 10px;
            border-radius: 3px 3px 0 0;
        }

        .table-title h2 {
            margin: 5px 0 0;
            font-size: 24px;
        }

        .table-title .btn {
            color: #566787;
            float: right;
            font-size: 13px;
            background: #fff;
            border: none;
            min-width: 50px;
            border-radius: 2px;
            border: none;
            outline: none !important;
            margin-left: 10px;
        }

        .table-title .btn:hover,
        .table-title .btn:focus {
            color: #566787;
            background: #f2f2f2;
        }

        .table-title .btn i {
            float: left;
            font-size: 21px;
            margin-right: 5px;
        }

        .table-title .btn span {
            float: left;
            margin-top: 2px;
        }

        table.table tr th,
        table.table tr td {
            border-color: #e9e9e9;
            padding: 12px 15px;
            vertical-align: middle;
        }

        table.table tr th:first-child {
            width: 60px;
        }

        table.table tr th:last-child {
            width: 100px;
        }

        table.table-striped tbody tr:nth-of-type(odd) {
            background-color: #fcfcfc;
        }

        table.table-striped.table-hover tbody tr:hover {
            background: #f5f5f5;
        }

        table.table th i {
            font-size: 13px;
            margin: 0 5px;
            cursor: pointer;
        }

        table.table td:last-child i {
            opacity: 0.9;
            font-size: 22px;
            margin: 0 5px;
        }

        table.table td a {
            font-weight: bold;
            color: #566787;
            display: inline-block;
            text-decoration: none;
        }

        table.table td a:hover {
            color: #2196F3;
        }

        table.table td a.settings {
            color: #2196F3;
        }

        table.table td a.delete {
            color: #F44336;
        }

        table.table td i {
            font-size: 19px;
        }

        table.table .avatar {
            border-radius: 50%;
            vertical-align: middle;
            margin-right: 10px;
        }

        .status {
            font-size: 30px;
            margin: 2px 2px 0 0;
            display: inline-block;
            vertical-align: middle;
            line-height: 10px;
        }

        .text-success {
            color: #10c469;
        }

        .text-info {
            color: #62c9e8;
        }

        .text-warning {
            color: #FFC107;
        }

        .text-danger {
            color: #ff5b5b;
        }

        .pagination {
            float: right;
            margin: 0 0 5px;
        }

        .pagination li a {
            border: none;
            font-size: 13px;
            min-width: 30px;
            min-height: 30px;
            color: #999;
            margin: 0 2px;
            line-height: 30px;
            border-radius: 2px !important;
            text-align: center;
            padding: 0 6px;
        }

        .pagination li a:hover {
            color: #666;
        }

        .pagination li.active a,
        .pagination li.active a.page-link {
            background: #03A9F4;
        }

        .pagination li.active a:hover {
            background: #0397d6;
        }

        .pagination li.disabled i {
            color: #ccc;
        }

        .pagination li i {
            font-size: 16px;
            padding-top: 6px
        }

        .hint-text {
            float: left;
            margin-top: 10px;
            font-size: 13px;
        }

        .modal-body {
            display: flex;
            color: black;
            align-items: center;
            font-weight: 500;
        }

        .btn-secondary {
            display: flex;
            align-items: center;
            height: 30px;
            padding: 10px;
        }

        .choose {
            color: black;
            margin-right: 20px;
        }

        .submit {
            background-color: rgb(79 162 111);
            border-radius: 5px;
            border: rgb(79 162 111);
            color: white;
        }

        .close {
            border: none;
            background-color: white;
            font-size: 20px;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
        $(document).on("click", ".settings", function () {
            var oldId = $(this).data('id');
            $(".modal-body #oldId").val(oldId);
            // As pointed out in comments, 
            // it is unnecessary to have to manually call the modal.
            // $('#addBookDialog').modal('show');
        });
        $(document).on("click", ".delete", function () {
            var did = $(this).data('id');
            $(".modal-body #packageId").val(did);
            // As pointed out in comments, 
            // it is unnecessary to have to manually call the modal.
            // $('#addBookDialog').modal('show');
        });

    </script>



    <body>

        <div class="main-wrapper main-wrapper-02">
            <jsp:include page="../../component/administration/header.jsp" />
            <!-- Courses Admin Start -->
            <div class="section overflow-hidden position-relative" id="wrapper">

                <!-- Sidebar Wrapper Start -->
                <jsp:include page="../../component/administration/menu.jsp" />
                <!-- Sidebar Wrapper End -->

                <!-- Page Content Wrapper Start -->
                <div class="page-content-wrapper">

                    <!-- Admin Tab Menu Start -->
                    <div class="nav flex-column nav-pills admin-tab-menu">
                        <a href="./detail?sId=${subjectId}">Overview</a>
                        <a href="./dimensions?subjectId=${subjectId}">Subject Dimension</a>
                        <a href="./packages?subjectId=${subjectId}" class="active">Subject Package</a>

                    </div>
                    <!-- Admin Tab Menu End -->

                    <!-- Page Content Wrapper Start -->
                    <div class="main-content-wrapper">
                        <div class="container-fluid">
                            <div class="table-responsive">
                                <div class="table-wrapper">
                                    <div class="table-title">
                                        <div class="row">

                                            <div class="col-sm-4">

                                                <h2>Subject <b>Packages</b></h2>
                                            </div>
                                            <div class="col-sm-4">
                                                <c:if test="${not empty message}">
                                                    <div id="message" class="alert alert-${alert}"
                                                         style="margin-bottom: 0px">
                                                        <h5 style="color: black">${message}</h5>
                                                    </div>
                                                </c:if>
                                            </div>
                                            <div class="col-sm-4">
                                                <button type="button" data-toggle="modal" data-target="#add"
                                                        class="btn btn-secondary"><i
                                                        class="material-icons">&#xE147;</i>
                                                    <span>Add</span></button>
                                                <!-- Button trigger modal -->



                                            </div>
                                            <!-- Modal -->
                                            <div class="modal fade" id="add" tabindex="-1" role="dialog"
                                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog ">
                                                    <div class="modal-content">
                                                        <div class="modal-header">

                                                            <h5 class="modal-title" id="exampleModalLongTitle"
                                                                style="color:black;">Add a package</h5>
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="./packages" method="post">
                                                            <div class="modal-body">
                                                                <input type="hidden" value="add" name="type" />
                                                                <label for="" class="choose">Choose a
                                                                    package</label>
                                                                <input type="hidden" value="${subjectId}"
                                                                       name="subjectId" />
                                                                <select id="cars" name="packageId">
                                                                    <c:forEach items="${all}" var="a">
                                                                        <option value=${a.id}>${a.name}</option>

                                                                    </c:forEach>

                                                                </select>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close</button>
                                                                <input class="submit" type="submit"
                                                                       value="Add" />

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Modal -->
                                            <div class="modal fade" id="edit" tabindex="-1" role="dialog"
                                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog ">
                                                    <div class="modal-content">
                                                        <div class="modal-header">

                                                            <h5 class="modal-title" id="exampleModalLongTitle"
                                                                style="color:black;">Edit a package</h5>
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="./packages" method="post">
                                                            <div class="modal-body">
                                                                <input type="hidden" value="edit" name="type" />
                                                                <label for="" class="choose">Choose a
                                                                    package</label>
                                                                <input type="hidden" value="${subjectId}"
                                                                       name="subjectId" />
                                                                <input type="hidden" value="" name="oldId"
                                                                       id="oldId" />
                                                                <select id="cars" name="packageId">
                                                                    <c:forEach items="${all}" var="a">
                                                                        <option value=${a.id}>${a.name}</option>

                                                                    </c:forEach>

                                                                </select>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close</button>
                                                                <input class="submit" type="submit"
                                                                       value="Edit" />

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>




                                            <!-- Modal -->
                                            <div class="modal fade" id="delete" tabindex="-1" role="dialog"
                                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel"
                                                                style="color:black;">Delete Package</h5>
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="./packages" method="post">
                                                            <div class="modal-body">
                                                                <input type="hidden" value="delete"
                                                                       name="type" />
                                                                <p style="color:black;"> Are you sure you want
                                                                    to delete?</p>
                                                                <input type="hidden" value="" name="packageId"
                                                                       id="packageId" />
                                                                <input type="hidden" value="${subjectId}"
                                                                       name="subjectId" />
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close</button>
                                                                <input class="submit" type="submit"
                                                                       value="Delete" />
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Duration</th>
                                                <th>Lister Price</th>
                                                <th>Sale Price</th>
                                                <th>Status </th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${packages}" var="p" varStatus="status">
                                                <tr>
                                                    <td>${status.count}</td>

                                                    <td>${p.name}</td>
                                                    <td>${p.duration == 18 ? '' : p.duration}</td>
                                                    <td>${subject.listedPrice * p.duration * p.priceRate * 0.01}
                                                    </td>
                                                    <td>${subject.salePrice * p.duration * p.priceRate * 0.01}
                                                    </td>
                                                    <td>${p.status ? 'Active' : 'Inactive'}</td>
                                                    <td>
                                                        <div style="display: flex;align-items: stretch;">
                                                            <a href="#" class="settings" title="Edit"
                                                               data-toggle="modal" data-id="${p.id}"
                                                               data-target="#edit"><i
                                                                    class="material-icons">&#xE8B8;</i></a>
                                                            <c:if test="${p.status}"><a
                                                                    href="${pageContext.request.contextPath}/admin/subject/packages/status?sId=${subjectId}&&pId=${p.id}&&status=0"
                                                                    class="">Deactive</a>
                                                            </c:if>
                                                            <c:if test="${!p.status}"><a
                                                                    href="${pageContext.request.contextPath}/admin/subject/packages/status?sId=${subjectId}&&pId=${p.id}&&status=1"
                                                                    class="">Active</a>
                                                            </c:if>
                                                        </div>
                                                    </td>

                                                </tr>
                                            </c:forEach>




                                    </table>
                                    <div class="clearfix">
                                        <%-- <ul class="pagination">
                                            <li class="page-item disabled"><a href="#">Previous</a></li>
                                            <li class="page-item"><a href="#" class="page-link">1</a></li>
                                            <li class="page-item"><a href="#" class="page-link">2</a></li>
                                            <li class="page-item active"><a href="#" class="page-link">3</a>
                                            </li>
                                            <li class="page-item"><a href="#" class="page-link">4</a></li>
                                            <li class="page-item"><a href="#" class="page-link">5</a></li>
                                            <li class="page-item"><a href="#" class="page-link">Next</a></li>
                                            </ul>
                                        --%>

                                    </div>
                                </div>
                            </div>






                        </div>
                    </div>
                    <!-- Page Content Wrapper End -->



                </div>
                <!-- Page Content Wrapper End -->

            </div>
            <!-- Courses Admin End -->






            <jsp:include page="../../component/common/footer.jsp" />

            <!--Back To Start-->
            <a href="#" class="back-to-top">
                <i class="icofont-simple-up"></i>
            </a>
            <!--Back To End-->

        </div>

        <!-- JS
============================================ -->
        <jsp:include page="../../component/common/js.jsp" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>

        setTimeout(function () {
            $('#message').fadeOut('fast');
        }, 3000); // <-- time in milliseconds
        </script>
    </body>

</html>