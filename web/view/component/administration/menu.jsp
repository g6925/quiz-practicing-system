<%-- Document : menu Created on : Jun 8, 2022, 7:33:25 AM Author : Nguyen Van Long --%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<div class="sidebar-wrapper">
    <div class="menu-list">
        <!--Admin or Marketing-->
        <c:if test="${user.role.id == 1 || user.role.id == 3}">
            <a href="${pageContext.request.contextPath}/${user.role.name}/dashboard">
                <img src="${pageContext.request.contextPath}/assets/images/menu-icon/dashboard.png" alt="Icon">
            </a>
            <a href="${pageContext.request.contextPath}/${user.role.name}/posts">
                <img src="${pageContext.request.contextPath}/assets/images/menu-icon/post.png" alt="Icon">
            </a>
        </c:if>

        <!--Admin-->
        <c:if test="${user.role.id == 1}">
            <a href="${pageContext.request.contextPath}/${user.role.name}/users">
                <img src="${pageContext.request.contextPath}/assets/images/menu-icon/user.png" alt="Icon">
            </a>
        </c:if>

        <!--Marketing-->
        <c:if test="${user.role.id == 3}">
            <a href="${pageContext.request.contextPath}/${user.role.name}/sliders">
                <img src="${pageContext.request.contextPath}/assets/images/menu-icon/slider.png" alt="Icon">
            </a>
        </c:if>
        
        <!--Expert-->
        <c:if test="${user.role.id == 5}">
            <a href="${pageContext.request.contextPath}/${user.role.name}/dimension">
                <img src="${pageContext.request.contextPath}/assets/images/menu-icon/dimension.png" alt="Icon">
            </a>
        </c:if>
        
        <c:if test="${user.role.id == 1 || user.role.id == 5}">
            <a href="${pageContext.request.contextPath}/course/subjects">
                <img src="${pageContext.request.contextPath}/assets/images/menu-icon/subject.png" alt="Icon">
            </a>
            <a href="${pageContext.request.contextPath}/course/price-packages">
                <img src="${pageContext.request.contextPath}/assets/images/menu-icon/price.png" alt="Icon">
            </a>
            <a href="${pageContext.request.contextPath}/course/questions">
                <img src="${pageContext.request.contextPath}/assets/images/menu-icon/question.png" alt="Icon">
            </a>
            <a href="${pageContext.request.contextPath}/course/quizzes">
                <img src="${pageContext.request.contextPath}/assets/images/menu-icon/quiz.png" alt="Icon">
            </a>
        </c:if>

        <a href="#">
            <img src="${pageContext.request.contextPath}/assets/images/menu-icon/setting.png" alt="Icon">
        </a>
    </div>
</div>