<%-- Document : course-resources Created on : Jun 8, 2022, 7:39:25 AM Author : Nguyen Van Long --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <!DOCTYPE html>
        <!-- Courses Resources Start -->
        <div class="courses-resources" style="clear: both">
            <h4 class="title">Here are our most popular instructor resources.</h4>

            <!-- Resources Wrapper Start -->
            <div class="resources-wrapper">
                <div class="row row-cols-xl-6 row-cols-md-3 row-cols-2">
                    <div class="col">

                        <!-- Single Resources Start -->
                        <div class="single-resources">
                            <div class="resources-icon">
                                <a href="#">
                                    <img src="${pageContext.request.contextPath}/assets/images/resources-icon/icon-1-1.png"
                                        alt="Icon">
                                    <img class="hover"
                                        src="${pageContext.request.contextPath}/assets/images/resources-icon/icon-2-1.png"
                                        alt="Icon">
                                </a>
                            </div>
                            <h5 class="title"><a href="#">Test Video</a></h5>
                        </div>
                        <!-- Single Resources Start -->

                    </div>
                    <div class="col">

                        <!-- Single Resources Start -->
                        <div class="single-resources">
                            <div class="resources-icon">
                                <a href="#">
                                    <img src="${pageContext.request.contextPath}/assets/images/resources-icon/icon-1-2.png"
                                        alt="Icon">
                                    <img class="hover"
                                        src="${pageContext.request.contextPath}/assets/images/resources-icon/icon-2-2.png"
                                        alt="Icon">
                                </a>
                            </div>
                            <h5 class="title"><a href="#">Community</a></h5>
                        </div>
                        <!-- Single Resources Start -->

                    </div>
                    <div class="col">

                        <!-- Single Resources Start -->
                        <div class="single-resources">
                            <div class="resources-icon">
                                <a href="#">
                                    <img src="${pageContext.request.contextPath}/assets/images/resources-icon/icon-1-3.png"
                                        alt="Icon">
                                    <img class="hover"
                                        src="${pageContext.request.contextPath}/assets/images/resources-icon/icon-2-3.png"
                                        alt="Icon">
                                </a>
                            </div>
                            <h5 class="title"><a href="#">Teaching Center</a></h5>
                        </div>
                        <!-- Single Resources Start -->

                    </div>
                    <div class="col">

                        <!-- Single Resources Start -->
                        <div class="single-resources">
                            <div class="resources-icon">
                                <a href="#">
                                    <img src="${pageContext.request.contextPath}/assets/images/resources-icon/icon-1-4.png"
                                        alt="Icon">
                                    <img class="hover"
                                        src="${pageContext.request.contextPath}/assets/images/resources-icon/icon-2-4.png"
                                        alt="Icon">
                                </a>
                            </div>
                            <h5 class="title"><a href="#">Insight Courses</a></h5>
                        </div>
                        <!-- Single Resources Start -->

                    </div>
                    <div class="col">

                        <!-- Single Resources Start -->
                        <div class="single-resources">
                            <div class="resources-icon">
                                <a href="#">
                                    <img src="${pageContext.request.contextPath}/assets/images/resources-icon/icon-1-5.png"
                                        alt="Icon">
                                    <img class="hover"
                                        src="${pageContext.request.contextPath}/assets/images/resources-icon/icon-2-5.png"
                                        alt="Icon">
                                </a>
                            </div>
                            <h5 class="title"><a href="#">Help & Support</a></h5>
                        </div>
                        <!-- Single Resources Start -->

                    </div>
                    <div class="col">

                        <!-- Single Resources Start -->
                        <div class="single-resources">
                            <div class="resources-icon">
                                <a href="#">
                                    <img src="${pageContext.request.contextPath}/assets/images/resources-icon/icon-1-6.png"
                                        alt="Icon">
                                    <img class="hover"
                                        src="${pageContext.request.contextPath}/assets/images/resources-icon/icon-2-6.png"
                                        alt="Icon">
                                </a>
                            </div>
                            <h5 class="title"><a href="#">Insight Courses</a></h5>
                        </div>
                        <!-- Single Resources Start -->

                    </div>
                </div>
            </div>
            <!-- Resources Wrapper End -->

        </div>
        <!-- Courses Resources End -->