<!-- JS
============================================ -->
<!-- Modernizer & jQuery JS -->
<script src="${pageContext.request.contextPath}/assets/js/vendor/modernizr-3.11.2.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-3.5.1.min.js"></script>

<!-- Bootstrap JS -->
<script src="${pageContext.request.contextPath}/assets/js/plugins/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/plugins/bootstrap.min.js"></script>

<!-- Plugins JS -->
<script src="${pageContext.request.contextPath}/assets/js/plugins/swiper-bundle.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/plugins/jquery.magnific-popup.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/plugins/video-playlist.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/plugins/jquery.nice-select.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/plugins/ajax-contact.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/plugins/ckeditor.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/plugins/apexcharts.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/plugins/jquery.vmap.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/plugins/jquery.vmap.world.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/plugins/jquery.vmap.sampledata.js"></script>


<!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->
 <!--<script src="${pageContext.request.contextPath}/assets/js/plugins.min.js"></script>--> 


<!-- Main JS -->
<script src="${pageContext.request.contextPath}/assets/js/main.js"></script>
<script type="module" src="${pageContext.request.contextPath}/assets/js/form.js"></script>

