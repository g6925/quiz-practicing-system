<%-- 
    Document   : header__user
    Created on : Jun 8, 2022, 3:46:48 PM
    Author     : dclon
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="user" value="${sessionScope.user}" />

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="login-header-action ml-auto">
    <div class="dropdown">
        <button class="action notification" data-bs-toggle="dropdown">
            <i class="flaticon-notification"></i>
            <span class="active"></span>
        </button>
        <div class="dropdown-menu dropdown-notification">
            <ul class="notification-items-list">
                <li class="notification-item">
                    <span class="notify-icon bg-success text-white"><i
                            class="icofont-ui-user"></i></span>
                    <div class="dropdown-body">
                        <a href="#">
                            <p><strong>Martin</strong> has added a <strong>customer</strong>
                                Successfully
                            </p>
                        </a>
                    </div>
                    <span class="notify-time">3:20 am</span>
                </li>
                <li class="notification-item">
                    <span class="notify-icon bg-success text-white"><i
                            class="icofont-shopping-cart"></i></span>
                    <div class="dropdown-body">
                        <a href="#">
                            <p><strong>Jennifer</strong> purchased Light Dashboard 2.0.</p>
                        </a>
                    </div>
                    <span class="notify-time">3:20 am</span>
                </li>
                <li class="notification-item">
                    <span class="notify-icon bg-danger text-white"><i
                            class="icofont-book-mark"></i></span>
                    <div class="dropdown-body">
                        <a href="#">
                            <p><strong>Robin</strong> marked a <strong>ticket</strong> as unsolved.
                            </p>
                        </a>
                    </div>
                    <span class="notify-time">3:20 am</span>
                </li>
                <li class="notification-item">
                    <span class="notify-icon bg-success text-white"><i
                            class="icofont-heart-alt"></i></span>
                    <div class="dropdown-body">
                        <a href="#">
                            <p><strong>David</strong> purchased Light Dashboard 1.0.</p>
                        </a>
                    </div>
                    <span class="notify-time">3:20 am</span>
                </li>
                <li class="notification-item">
                    <span class="notify-icon bg-success text-white"><i class="icofont-image"></i></span>
                    <div class="dropdown-body">
                        <a href="#">
                            <p><strong> James.</strong> has added a<strong>customer</strong>
                                Successfully
                            </p>
                        </a>
                    </div>
                    <span class="notify-time">3:20 am</span>
                </li>
            </ul>
            <a class="all-notification" href="#">See all notifications <i
                    class="icofont-simple-right"></i></a>
        </div>
    </div>

    <a class="action author" href="#">
        <img src="${user.image}" alt="User-Image">
    </a>

    <div class="dropdown">
        <button class="action more display-4" data-bs-toggle="dropdown">
            <i class="icofont-dotted-down"></i>
        </button>
        <ul class="dropdown-menu">
            <li><a class="" href="${pageContext.request.contextPath}/${user.role.name}"><i class="icofont-card"></i> ${user.role.getNameDefault()}</a></li>
            <li><a class="" href="${pageContext.request.contextPath}/customer/profile"><i class="icofont-user"></i> Profile</a></li>
            <li><a class="" href="#"><i class="icofont-inbox"></i> Inbox</a></li>
            <li><a class="" href="${pageContext.request.contextPath}/common/user/logout"><i class="icofont-logout"></i> Sign Out</a></li>
        </ul>
    </div>
</div>
