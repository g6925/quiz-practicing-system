<%-- Document : header Created on : Jun 4, 2022, 2:28:00 PM Author : Jarnsaurus --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!-- Header Section Start -->
<div class="header-section">
    <!-- Header Main Start -->
    <div class="header-main">
        <div class="container">

            <!-- Header Main Start -->
            <div class="login-header-wrapper header-main-wrapper">

                <!-- Header Logo Start -->
                <div class="header-logo">
                    <a href="${pageContext.request.contextPath}/common/home"><img src="${pageContext.request.contextPath}/assets/images/logo.png"
                                                                                  alt="Logo"></a>
                </div>
                <!-- Header Logo End -->

                <!-- Header Menu Start -->
                <div class="header-menu d-none d-lg-block">
                    <ul class="nav-menu">
                        <li><a href="${pageContext.request.contextPath}/common/home">Home</a></li>
                        <li><a href="${pageContext.request.contextPath}/common/subjects">All Course</a></li>
                        <li><a href="${pageContext.request.contextPath}/common/blogs">Blog</a></li>
                        <li><a href="${pageContext.request.contextPath}/customer/myregistrations">My Registrations</a></li>
                        <li><a href="${pageContext.request.contextPath}/practicelist">My Practice</a></li>
                    </ul>
                </div>
                <!-- Header Menu End -->

                <c:choose>
                    <c:when test="${sessionScope.user != null}" >
                        <!-- Header Action Start -->
                        <jsp:include page="header_user.jsp" />
                        <!-- Header Action End -->
                    </c:when>
                    <c:otherwise>
                        <!-- Header Sing In & Up Start -->
                        <div class="header-sign-in-up d-none d-lg-block">
                            <ul>
                                <li><a class="sign-in" href="${pageContext.request.contextPath}/common/user/login">Sign In</a></li>
                                <li><a class="sign-up" href="${pageContext.request.contextPath}/common/user/register">Sign Up</a></li>
                            </ul>
                        </div>
                        <!-- Header Sing In & Up End -->
                    </c:otherwise>
                </c:choose>

                <!-- Header Mobile Toggle Start -->
                <div class="header-toggle d-lg-none">
                    <a class="menu-toggle" href="javascript:void(0)">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
                <!-- Header Mobile Toggle End -->

            </div>
            <!-- Header Main End -->

        </div>
    </div>
    <!-- Header Main End -->

</div>
<!-- Header Section End -->