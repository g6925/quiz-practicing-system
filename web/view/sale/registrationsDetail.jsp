<!------ Include the above in your HEAD tag ---------->




<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<html lang="en">

    <jsp:include page="../component/common/head.jsp" />

    <style>
        .form-group {
            display: flex;
            align-items: center;
            font-size: 20px;
            justify-content: center;
        }

        .control-label {
            width: 340px;
        }

        #featured {
            width: 20px;
            height: 20px;
        }

        .form-horizontal {
            color: rgb(33 40 50);
            font-weight: 500;
        }
    </style>

    <body>

        <div class="main-wrapper main-wrapper-02">
            <jsp:include page="../component/administration/header.jsp" />
            <!-- Courses Admin Start -->
            <div class="section overflow-hidden position-relative" id="wrapper">

                <!-- Sidebar Wrapper Start -->
                <div class="sidebar-wrapper">
                    <div class="menu-list">
                        <a class="active" href="courses-admin.html"><img
                                src="../../assets/images/menu-icon/icon-1.png" alt="Icon"></a>
                        <a href="messages.html"><img src="../../assets/images/menu-icon/icon-2.png"
                                                     alt="Icon"></a>
                        <a href="overview.html"><img src="../../assets/images/menu-icon/icon-3.png"
                                                     alt="Icon"></a>
                        <a href="engagement.html"><img src="../../assets/images/menu-icon/icon-4.png"
                                                       alt="Icon"></a>
                        <a href="traffic-conversion.html"><img src="../../assets/images/menu-icon/icon-5.png"
                                                               alt="Icon"></a>
                    </div>
                </div>
                <!-- Sidebar Wrapper End -->

                <!-- Page Content Wrapper Start -->
                <div class="page-content-wrapper">
                    <div class="container-fluid custom-container">

                        <!-- Message Start -->

                        <!-- Message End -->




                        <div class="right">



                            





                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="subject_name">SUBJECT
                                        NAME</label>
                                    <div class="single-form  col-md-4">
                                        <input id="subject_name" name="subject_name" placeholder="SUBJECT NAME"
                                               class="form-control input-md" value="${register.subject.title}"
                                               type="text" readonly>
                                        <div id="subject_name-error"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="subject_name">PACKAGE
                                        NAME</label>
                                    <div class="single-form  col-md-4">
                                        <input id="subject_name" name="subject_name" placeholder="SUBJECT NAME"
                                               class="form-control input-md" value="${register.pricePackage.name}"
                                               type="text" readonly>
                                        <div id="subject_name-error"></div>
                                    </div>
                                </div>

                                <!-- Text input Listed Price-->
                                <div class="form-group" style="margin-top: 20px">
                                    <label class="col-md-4 control-label" for="listedPrice">LISTED PRICE</label>
                                    <div class="single-form col-md-4">
                                        <input id="listedPrice" name="listedPrice" placeholder="LISTED PRICE"
                                               class="form-control input-md" type="number"
                                               value="${register.subject.listedPrice}" readonly>
                                        <div id="listedPrice-error"></div>
                                    </div>
                                </div>

                                <!-- Text input Listed Price-->
                                <div class="form-group" style="margin-top: 20px">
                                    <label class="col-md-4 control-label" for="listedPrice">SALE PRICE</label>
                                    <div class="single-form col-md-4">
                                        <input id="listedPrice" name="listedPrice" placeholder="LISTED PRICE"
                                               class="form-control input-md" type="number"
                                               value="${register.subject.salePrice}" readonly>
                                        <div id="listedPrice-error"></div>
                                    </div>
                                </div>        

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="subject_name">FULL
                                        NAME</label>
                                    <div class="single-form  col-md-4">
                                        <input id="subject_name" name="subject_name" placeholder="SUBJECT NAME"
                                               class="form-control input-md" value="${register.user.name}"
                                               type="text" readonly>
                                        <div id="subject_name-error"></div>
                                    </div>
                                </div>

                                <!-- Select Basic -->
                                <div class="form-group courses-select">
                                    <label class="col-md-4 control-label" for="subject_category">GENDER</label>
                                    <div class="single-form col-md-4">
                                        <input id="subject_name" name="subject_name" placeholder="SUBJECT NAME"
                                               class="form-control input-md" value="${register.user.getGender() ? 'Male' : 'Female'}"
                                               type="text" readonly>

                                    </div>
                                </div>               

                                <div class="form-group">
                                    <label class="col-md-4 control-label" >EMAIL</label>
                                    <div class="single-form  col-md-4">
                                        <input id="" name="" 
                                               class="form-control input-md" value="${register.user.email}"
                                               type="text" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">PHONE</label>
                                    <div class="single-form  col-md-4">
                                        <input id="1" name="1" 
                                               class="form-control input-md" value="${register.user.phone}"
                                               type="text" readonly>
                                    </div>
                                </div>   



                                <div class="form-group">
                                    <label class="col-md-4 control-label" >VALID FROM</label>
                                    <div class="single-form  col-md-4">
                                        <input id="" name="" 
                                               class="form-control input-md" value="${register.dateStart}"
                                               type="text" readonly>
                                    </div>
                                </div>   

                                <div class="form-group">
                                    <label class="col-md-4 control-label" >VALID TO</label>
                                    <div class="single-form  col-md-4">
                                        <input id="" name="" 
                                               class="form-control input-md" value="${register.dateEnd == null ?'Lifetime':register.dateEnd}"
                                               type="text" readonly>
                                    </div>
                                </div>              





















                                <!-- Select Basic 
                                <div class="form-group courses-select">
                                    <label class="col-md-4 control-label" for="status">GENDER</label>
                                    <div class="single-form col-md-4">
                                        <select id="status" name="status" class="form-control">

                                            <option value="1">Male</option>
                                            <option value="0">Female</option>
                                        </select>
                                        <div id="status-error"></div>
                                    </div>
                                </div>
                                -->





                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="singlebutton"></label>
                                    <div class="single-form col-md-4">
                                        <button id="singlebutton" name="singlebutton"
                                                class="btn btn-primary"
                                                onclick="location.href='${pageContext.request.contextPath}/sale/registrations';"/>Back</button>

                                    </div>
                                </div>




                            
                        </div>









                        <!-- Admin Courses Tab Start -->

                        <!-- Admin Courses Tab End -->

                        <!-- Admin Courses Tab Content Start -->

                        <!-- Admin Courses Tab Content End -->

                        <!-- Courses Resources Start -->
                        <jsp:include page="../component/administration/courses-resources.jsp" />

                        <!-- Courses Resources End -->

                    </div>
                </div>
                <!-- Page Content Wrapper End -->

            </div>
            <!-- Courses Admin End -->






            <jsp:include page="../component/common/footer.jsp" />

            <!--Back To Start-->
            <a href="#" class="back-to-top">
                <i class="icofont-simple-up"></i>
            </a>
            <!--Back To End-->

        </div>

        <!-- JS
============================================ -->
        <jsp:include page="../component/common/js.jsp" />
        <script>
            ClassicEditor
                    .create(document.querySelector('#subject_description'), {
                        // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
                    })
                    .then(editor => {
                        window.editor = editor;
                    })
                    .catch(err => {
                        console.error(err.stack);
                    });
        </script>
    </body>

</html>