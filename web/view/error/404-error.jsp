<%-- Document : 404-error Created on : Jul 3, 2022, 10:30:16 AM Author : dclon --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <!DOCTYPE html>
        <html>
        <jsp:include page="../component/common/head.jsp" />

        <body>

            <div class="main-wrapper">

                <jsp:include page="../component/common/header.jsp" />

                <!-- Overlay Start -->
                <div class="overlay"></div>
                <!-- Overlay End -->

                <!-- Page Banner Start -->
                <div class="section page-banner">

                    <img class="shape-1 animation-round"
                        src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                    <img class="shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png"
                        alt="Shape">

                    <div class="container">
                        <!-- Page Banner Start -->
                        <div class="page-banner-content">
                            <ul class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li class="active">404 Error</li>
                            </ul>
                            <h2 class="title">Page Not <span>Found</span></h2>
                        </div>
                        <!-- Page Banner End -->
                    </div>

                    <!-- Shape Icon Box Start -->
                    <div class="shape-icon-box">

                        <img class="icon-shape-1 animation-left"
                            src="${pageContext.request.contextPath}/assets/images/shape/shape-5.png" alt="Shape">

                        <div class="box-content">
                            <div class="box-wrapper">
                                <i class="flaticon-badge"></i>
                            </div>
                        </div>

                        <img class="icon-shape-2"
                            src="${pageContext.request.contextPath}/assets/images/shape/shape-6.png" alt="Shape">

                    </div>
                    <!-- Shape Icon Box End -->

                    <img class="shape-3" src="${pageContext.request.contextPath}/assets/images/shape/shape-24.png"
                        alt="Shape">

                    <img class="shape-author"
                        src="${pageContext.request.contextPath}/assets/images/author/author-11.jpg" alt="Shape">

                </div>
                <!-- Page Banner End -->

                <!-- Error Start -->
                <div class="section section-padding mt-n10">
                    <div class="container">

                        <!-- Error Wrapper Start -->
                        <div class="error-wrapper">
                            <div class="row align-items-center">
                                <div class="col-lg-6">
                                    <!-- Error Images Start -->
                                    <div class="error-images">
                                        <img src="${pageContext.request.contextPath}/assets/images/error/404-error.jpg"
                                            alt="Error">
                                    </div>
                                    <!-- Error Images End -->
                                </div>
                                <div class="col-lg-6">
                                    <!-- Error Content Start -->
                                    <div class="error-content">
                                        <h5 class="sub-title">This Page is Not Found.</h5>
                                        <h2 class="main-title">We are very sorry for error. We <span> can’t find
                                                this</span> page.</h2>
                                        <p>The URL you are looking for may be incorrect or does not exist. Please check
                                            again.</p>
                                        <a href="${pageContext.request.contextPath}/common/home"
                                            class="btn btn-primary btn-hover-dark">Back To Home</a>
                                    </div>
                                    <!-- Error Content End -->
                                </div>
                            </div>
                        </div>
                        <!-- Error Wrapper End -->

                    </div>
                </div>
                <!-- Error End -->

                <!-- Footer Start  -->
                <jsp:include page="../component/common/footer.jsp" />
                <!-- Footer End -->

                <!--Back To Start-->
                <a href="#" class="back-to-top">
                    <i class="icofont-simple-up"></i>
                </a>
                <!--Back To End-->

            </div>
            <!-- JS
    ============================================ -->
            <jsp:include page="../component/common/js.jsp" />

        </body>

        </html>