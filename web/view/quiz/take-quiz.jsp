<%-- Document : profile Created on : May 30, 2022, 9:40:01 AM Author : Duong-PC --%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

    <jsp:include page="../component/common/head.jsp" />
    <style>
        .quiz-time {
            border-top: 2px black solid;
            border-bottom: 2px black solid;
        }

        .wraper-radio {
            display: inline-block;
            padding: 8px;
            border-radius: 5px;
            line-height: 20px;
        }

        .wraper-radio input {
            height: 20px;
            width: 20px;
        }

        .question-tool {
            display: flex;
            justify-content: end;
            margin: 10px 0;
        }

        .question-tool button {
            margin-right: 20px;
        }

        .answered {
            background-color: green;
            color: white;
        }

        #btn-review,
        #btn-peek {
            padding: 5px;
            border-radius: 5px;
            border: 2px gray solid;
        }
        .btn {
            line-height: 1.125rem;
        }

        .btn-green {
            color: white;
            border: 2px white solid;
            margin-left: 10px;
            padding: 5px 10px;
            background-color: green;
            border-radius: 5px;
            font-weight: bold;
        }

        .page-content {
            overflow-x: hidden;
        }

        .small-question-box {
            height: 2rem;
            width: 2rem;
            border: 1px solid steelblue;
            border-radius: 5px;
            display: inline-block;
            margin: 3px;
            position: relative;
        }

        .marked-question-box i {
            color: rgb(255, 136, 0);
            font-size: 15px;
            position: absolute;
            top: 0;
            right: 0;

        }

        .btn-review {
            border: 2px gray solid;
            border-radius: 5px;
            padding: 4px 8px;
            display: inline-block;
        }

        .vh-70 {
            height: 65vh;
        }

        #question_content {
            max-height: 20%;
            overflow-y: auto;
        }

        .btn-white,
        .btn-grey {
            border: 2px grey solid;
        }

        .btn-white i {
            margin-right: 10px;
        }
    </style>
    <body>
        <div class="main-wrapper">
            <jsp:include page="../component/common/header.jsp" />
            <!-- Overlay Start -->
            <div class="overlay"></div>
            <!-- Overlay End -->

            <!-- Page Banner Start -->
            <div class="section page-banner" style="height: 440px">
                <img class="shape-1 animation-round"
                     src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                <img class="shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png"
                     alt="Shape">

                <div class="container">
                    <!-- Page Banner Start -->
                    <div class="page-banner-content">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Quiz</li>
                        </ul>
                        <h2 class="title">Take Quiz</h2>
                    </div>
                    <!-- Page Banner End -->
                </div>
            </div>
            <!-- Page Banner End -->
            <div class="section section-padding">
                <div class="container">
                    <div class="container-fluid p-5">
                        <div class="container-fluid min-vh-100 p-0 page-content">
                            <div class="quiz-time m-1 p-1">
                                <div>
                                    <span>Time: </span>
                                    <span id="countDown" class="text-center col-1 fs-5 m-0 p-1" style="font-weight: bold">

                                    </span>
                                </div>
                                <div>
                                    <span>Question: </span>
                                    <span id="number_question" class=" col-1 fs-4 m-0"></span>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div id="question_content" class="col-10">
                                    <h5 id="question_text" class="mt-2"></h5>
                                    <!--Answers go in here-->
                                </div>
                            </div>

                            <div class="w-100">
                                <div class="">
                                    <div class="row p-3">
                                        <div class="col d-flex justify-content-end">
                                            <!-- <button class="btn btn-green" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                                Peek at Answer</button> -->
                                            <button id="btn_mark" class="btn btn-green">Mark for Review</button>
                                        </div>
                                    </div>
                                    <div class="row bg-success p-3">
                                        <div class="col">
                                            <button id="btn_review_question" class="btn btn-green" data-bs-toggle="modal"
                                                    data-bs-target="#review-process-modal">
                                                Review Progress
                                            </button>
                                        </div>
                                        <div class="col d-flex justify-content-end">
                                            <button id="btn_previous" class="btn btn-green">Previous</button>
                                            <button id="btn_next" class="btn btn-green">Next</button>
                                            <button id="btn_score" class="btn btn-green btn-score-exam" data-bs-toggle="modal" data-bs-target="#scoreExamModal">
                                                Score
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Confirm score exam box -->
                            <div class="modal fade" id="scoreExamModal" tabindex="-1" aria-labelledby="scoreExamModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="scoreExamModalLabel"></h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <p id="notAnsweredQuestion" class="text-danger font-weight-bold"></p>
                                            <p id="helpMessage"></p>
                                        </div>
                                        <div class="modal-footer">
                                            <a id="reviewLink" href="./review-quiz?quizId=${quizId}" class="btn btn-primary" hidden>Review</a>
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Back</button>
                                            <button id="btn_submit_answer" type="button" class="btn btn-success">Score Exam</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="review-process-modal" tabindex="-1" aria-labelledby="exampleModalLabel"
                                 aria-hidden="true">
                                <div class="modal-dialog modal-xl">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="">Review Process</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row review-question-btn-filter">
                                                <div class="col-9">
                                                    <button id="btn_unanswered" class="btn btn-white">
                                                        <i class="fa-regular fa-square"></i>UNANSWERED</button>
                                                    <button id="btn_marked" class="btn btn-white"><i
                                                            class="fa-solid fa-bookmark text-warning"></i>MARKED</button>
                                                    <button id="btn_answered" class="btn btn-white">
                                                        <i class="fa-solid fa-square text-success"></i>ANSWERED
                                                    </button>
                                                </div>
                                                <div class="col d-flex justify-content-end">

                                                    <button id="score_exam" class="btn btn-white btn-score-exam" data-bs-toggle="modal"
                                                            data-bs-target="#scoreExamModal">
                                                        SCORE EXAM NOW</button>
                                                </div>
                                                <div id="question_review">
                                                    <!--Question boxes go here.-->
                                                    <!-- <button class="small-question-box">1</button>
                                                    <button class="small-question-box marked-question-box">
                                                        <i class="fa-solid fa-bookmark"></i>2
                                                    </button> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <script src="${pageContext.request.contextPath}/assets/js/question.js"></script>
                    </div>

                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                </div>
            </div>
            <jsp:include page="../component/common/footer.jsp" />
            <jsp:include page="../component/common/js.jsp" />
    </body>

</html>