<%-- Document : profile Created on : May 30, 2022, 9:40:01 AM Author : Duong-PC --%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <style>
        .btn-redo {
            background-color: #20c997;
            padding: 20px;
            display: inline-block;
            width: 150px;
            text-align: center;
            color: white;
            font-weight: bold;
            border-radius: 5px;
        }
    </style>
    <jsp:include page="../component/common/head.jsp" />

    <body>
        <div class="main-wrapper">
            <jsp:include page="../component/common/header.jsp" />
            <!-- Overlay Start -->
            <div class="overlay"></div>
            <!-- Overlay End -->

            <!-- Page Banner Start -->
            <div class="section page-banner" style="height: 440px">
                <img class="shape-1 animation-round"
                     src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                <img class="shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png"
                     alt="Shape">

                <div class="container">
                    <!-- Page Banner Start -->
                    <div class="page-banner-content">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Quiz</li>
                        </ul>
                        <h2 class="title">Quiz Info</h2>
                    </div>
                    <!-- Page Banner End -->
                </div>

                <!-- Shape Icon Box Start -->
                <div class="shape-icon-box">
                    <img class="icon-shape-1 animation-left"
                         src="${pageContext.request.contextPath}/assets/images/shape/shape-5.png" alt="Shape">

                    <div class="box-content">
                        <div class="box-wrapper">
                            <i class="flaticon-badge"></i>
                        </div>
                    </div>

                    <img class="icon-shape-2"
                         src="${pageContext.request.contextPath}/assets/images/shape/shape-6.png" alt="Shape">
                </div>
                <!-- Shape Icon Box End -->

                <img class="shape-3" src="${pageContext.request.contextPath}/assets/images/shape/shape-24.png"
                     alt="Shape">

                <img class="shape-author"
                     src="${pageContext.request.contextPath}/assets/images/author/author-11.jpg" alt="Shape">
            </div>
            <!-- Page Banner End -->
            <div class="section section-padding">
                <div class="container">
                    <div class="container-fluid p-5">
                        <div class="mt-5">
                            <h3 class="mt-1 mb-4">
                                <strong>Quiz name: ${quiz.name}
                                </strong>
                            </h3>

                            <p class="mt-2 mb-4">
                                <strong>Quiz time:
                                    <c:if test="${quiz.getType() eq 'Practice'}">
                                        Unlimited time </strong>
                                    </c:if>
                                    <c:if test="${quiz.getType() eq 'Exam'}">
                                        <fmt:formatDate pattern="mm:ss" value="${quiz.duration}" /> min </strong>
                                </c:if>
                            </p>
                            <p class="mt-1 mb-4">
                                <strong>Quiz level: ${quiz.getLevel()}
                                </strong>
                            </p>

                        </div>

                        <div class="row mt-5">
                            <div class="col-9 mt-4">
                                <h4 class="mb-2">Submit your assignment</h4>
                                <p>
                                    <c:if test="${quiz.getType() eq 'Exam'}">
                                        <strong class="text-danger">
                                            Note that this is a test and you will not be able to retake it once you have submitted it
                                        </strong>
                                    </c:if>
                                    <c:if test="${quiz.getType() eq 'Practice'}">
                                        <strong class="text-danger">
                                            You can practice this test again after you submit it
                                        </strong>
                                    </c:if>
                                </p>
                            </div>

                            <div class="col-3 position-relative">
                                <c:if test="${hasDo==0}">
                                    <a class="btn-redo position-absolute end-0" 
                                       href="./quiz?quizId=${quiz.id}&takeQuiz=true${setTypeGroup}">
                                        Start Quiz
                                    </a>
                                </c:if>
                                <c:if test="${hasDo==1}">
                                    <c:if test="${quiz.getType() eq 'Exam'}">
                                        <a class="btn-redo position-absolute end-0">
                                            You have submitted
                                        </a>
                                    </c:if>
                                    <c:if test="${quiz.getType() eq 'Practice'}">
                                        <a class="btn-redo position-absolute end-0" 
                                           href="./quiz?quizId=${quiz.id}&takeQuiz=true&retake=true">
                                            Take the Quiz Again
                                        </a>
                                    </c:if>
                                </c:if>

                            </div>
                        </div>

                        <div class="mt-4">
                            <hr>
                        </div>

                        <div class="row mt-5">
                            <div class="col-9 mt-4">
                                <h4 class="mb-2">Review </h4>
                            </div>

                            <div class="col-3 position-relative">
                                <c:if test="${hasDo==1}">
                                    <a class="btn-redo position-absolute end-0" 
                                       href="./review-quiz?quizId=${quiz.getId()}">
                                        Review
                                    </a>
                                </c:if>
                            </div>
                        </div>
                        <div class="mt-4">
                            <hr>
                        </div>
                    </div>

                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                </div>
            </div>
            <jsp:include page="../component/common/footer.jsp" />
            <jsp:include page="../component/common/js.jsp" />
    </body>

</html>