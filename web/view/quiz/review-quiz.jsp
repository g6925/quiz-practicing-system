<%-- 
    Document   : review-quiz
    Created on : Jul 20, 2022, 1:17:43 AM
    Author     : duong
--%>

<%-- Document : profile Created on : May 30, 2022, 9:40:01 AM Author : Duong-PC --%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600&family=Nunito:wght@600;700;800&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
          integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <jsp:include page="../component/common/head.jsp"/>
    <body>
        <div class="main-wrapper">
            <jsp:include page="../component/common/header.jsp"/>
            <!-- Overlay Start -->
            <div class="overlay"></div>
            <!-- Overlay End -->

            <!-- Page Banner Start -->
            <div class="section page-banner" style="height: 440px">
                <img class="shape-1 animation-round"
                     src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                <img class="shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png"
                     alt="Shape">

                <div class="container">
                    <!-- Page Banner Start -->
                    <div class="page-banner-content">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Quiz</li>
                        </ul>
                        <h2 class="title">Quiz Review </h2>
                    </div>
                    <!-- Page Banner End -->
                </div>
            </div>
            <!-- Page Banner End -->
            <div class="section section-padding p-0">
                <div class="container">
                    <div class="container-fluid p-5">
                        <div class="container-fluid min-vh-100 p-0 page-content">
                            <!-- Course Description Starts Here -->
                            <div class="container-fluid">
                                <div class="container mb-100">
                                    <c:choose>
                                        <c:when test="${userQuiz.getGrade()>=6}">
                                            <div style="background-color: #defae1; padding: 50px;">
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <div class="row">
                                                            <div class="col-md-1">
                                                                <svg style="color: rgb(29, 124, 80)" aria-hidden="true" fill="none" focusable="false" height="30" viewBox="0 0 20 20" width="30" class="css-md7hvk"><path d="M10 1a9 9 0 100 18 9 9 0 000-18zM8.36 14.63l-4-4L5.8 9.24l2.56 2.56L14.2 6l1.42 1.42-7.26 7.21z" fill="currentColor"></path></svg>
                                                            </div>
                                                            <c:if test="${userQuiz.quiz.getType() eq 'Practice'}">
                                                                <div class="col-md-10">
                                                                    <h2 style="font-weight: 650">You are doing well. Try again once you are ready</h2>
                                                                </div>
                                                            </c:if>
                                                            <c:if test="${userQuiz.quiz.getType() eq 'Exam'}">
                                                                <div class="col-md-10">
                                                                    <h2 style="font-weight: 650">You are doing well. Try your best!!</h2>
                                                                </div>
                                                            </c:if>
                                                        </div>
                                                        <div class="row" style="margin-top: 20px;">
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-10">
                                                                <h5>
                                                                    <span style=" padding-right: 20px;">
                                                                        <span style="font-weight: 620;">Grade received</span>  
                                                                        <span class="text-success"> <fmt:formatNumber type="number" maxFractionDigits="1" value="${userQuiz.getGrade()}" /></span>/ 10
                                                                    </span> 
                                                                </h5>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 20px;">
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-10">
                                                                <h5>
                                                                    <span style=" padding-right: 20px;">
                                                                        <span style="font-weight: 620;">Time taken: ${userQuiz.timeTaken}</span>  
                                                                    </span> 
                                                                </h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <c:if test="${userQuiz.quiz.getType() eq 'Practice'}">
                                                        <div class="col-md-2">
                                                            <div class="container d-flex h-100">
                                                                <div class="row">
                                                                    <a href="quiz?quizId=${userQuiz.quiz.getId()}" class="btn btn-primary btn-lg text-white">Try again</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </div>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <div style="background-color: #fadcdf; padding: 50px;">
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <div class="row">
                                                            <div class="col-md-1">
                                                                <svg style="color: rgb(211, 0, 1);" aria-hidden="true" fill="none" focusable="false" height="30" viewBox="0 0 20 20" width="30" class="css-1hltn8p"><path d="M18.9 17.25L10.67 1.91a.75.75 0 00-1.34 0L1.1 17.25a.84.84 0 00.67 1.25h16.46a.84.84 0 00.67-1.25zM9 6.05h2v6.63H9V6.05zm1 10.74a1.25 1.25 0 110-2.5 1.25 1.25 0 010 2.5z" fill="currentColor"></path></svg>
                                                            </div>
                                                            <c:if test="${userQuiz.quiz.getType() eq 'Practice'}">
                                                                <div class="col-md-10">
                                                                    <h2 style="font-weight: 650">Try again once you are ready</h2>
                                                                </div>
                                                            </c:if>
                                                            <c:if test="${userQuiz.quiz.getType() eq 'Exam'}">
                                                                <div class="col-md-10">
                                                                    <h2 style="font-weight: 650">  Your score is a bit low, try to do better in the next test!</h2>
                                                                </div>
                                                            </c:if>
                                                        </div>
                                                        <div class="row" style="margin-top: 20px;">
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-10">
                                                                <h5>
                                                                    <span style=" padding-right: 20px;">
                                                                        <span style="font-weight: 620;">Grade received</span>  
                                                                        <span class="text-danger" > <fmt:formatNumber type="number" maxFractionDigits="1" value="${userQuiz.getGrade()}" /></span>/ 10
                                                                    </span> 
                                                                </h5>
                                                            </div>
                                                        </div>
                                                    </div>  
                                                    <c:if test="${userQuiz.quiz.getType() eq 'Practice'}">
                                                        <div class="col-md-2">
                                                            <div class="container d-flex h-100">
                                                                <div class="row">
                                                                    <a href="quiz?quizId=${userQuiz.quiz.getId()}" class="btn btn-primary btn-lg text-white">Try again</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </div>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                    <br>
                                    <c:forEach items="${questions}" var="question">
                                        <div style="padding-bottom: 45px;">
                                            <!-- hien thi question-->
                                            <p class="fw-bold">${question.getContent()}</p>
                                            <ul class="p-1">
                                                <!-- hiển thị option và checked câu trả lời của student-->
                                                <c:set var="optionRight" value="${0}"/>
                                                <c:set var="userAnswerRight" value="${0}"/>
                                                <c:set var="userAnswerCount" value="${0}"/>
                                                <c:forEach items="${question.getAnswers()}" var="option">
                                                    <c:set var="ans" value="${-1}"/>
                                                    <c:if test="${option.getCorrect()}">
                                                        <c:set var="optionRight" value="${optionRight+1}"/>
                                                    </c:if>
                                                    <li class="list-group-item border-0">
                                                        <div class="form-check">    
                                                            <input class="form-check-input" name="${question.getId()}" type="checkbox" 
                                                                   <c:forEach items="${userAnswers}" var="u">
                                                                       <c:if test="${u.getUserAnswer()==option.getId()}">
                                                                           <c:set var="userAnswerCount" value="${userAnswerCount+1}"/>
                                                                       </c:if>
                                                                       <c:choose>
                                                                           <c:when test="${u.getUserAnswer()==option.getId() and option.getCorrect()}">
                                                                               checked style="margin-right: 10px;opacity: 1;background-color: cornflowerblue;border: none;"
                                                                               <c:set var="ans" value="${1}" />
                                                                               <c:set var="userAnswerRight" value="${userAnswerRight+1}" />
                                                                           </c:when>    
                                                                           <c:when test="${u.getUserAnswer()==option.getId() and !option.getCorrect()}">
                                                                               checked style="margin-right: 10px; background-color: cornflowerblue;border: none;opacity: 1;"
                                                                               <c:set var="ans" value="${0}" />
                                                                           </c:when>   
                                                                           <c:otherwise>

                                                                           </c:otherwise>
                                                                       </c:choose>
                                                                   </c:forEach> disabled> 
                                                            ${option.getContent()}
                                                            <c:if test="${ans==1}">
                                                                <p class="p-3 text-success">
                                                                    <i class="fas fa-check-circle"></i> Your answer is correct
                                                                </p>
                                                            </c:if>
                                                            <c:if test="${ans==0}">
                                                                <p class="p-3 text-danger">
                                                                    <i class="fas fa-times-circle"></i> Your answer is wrong
                                                                </p>
                                                            </c:if>
                                                        </div>
                                                    </li>
                                                </c:forEach> 
                                                <c:choose>
                                                    <c:when test="${optionRight==userAnswerRight and userAnswerCount==optionRight}">
                                                        <p class="p-3 text-success" style="background-color:#e1f8ed;"><i class="fas fa-check-circle"></i> Your answer is correct (You have earned <fmt:formatNumber type="number" value="${scorePerQuestion}" /> points)
                                                        </p>
                                                    </c:when>   
                                                    <c:otherwise>
                                                        <p class="p-3 text-danger" style="background-color: #fbe6ed;"><i class="fas fa-times-circle" ></i> Your answer is wrong 
                                                        </p>
                                                    </c:otherwise>
                                                </c:choose>
                                                <c:set var="userAnswerRight" value="${0}"/>
                                                <c:set var="optionRight" value="${0}"/>
                                                <c:set var="userAnswerCount" value="${0}"/>
                                            </ul>  
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                            <!-- Course Description Ends Here -->

                        </div>
                        <script src="${pageContext.request.contextPath}/assets/js/question.js"></script>
                    </div>

                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                </div>
            </div>
            <jsp:include page="../component/common/footer.jsp"/>
            <jsp:include page="../component/common/js.jsp"/>
        </div>
    </body>
</html>