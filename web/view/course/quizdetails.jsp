<!------ Include the above in your HEAD tag ---------->

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <jsp:include page="../component/common/head.jsp" />
    <style>
        .form-group {
            display: flex;
            align-items: center;
            font-size: 20px;
            justify-content: center;
        }

        .input_fields_wrap {
            text-align: center;
            align-items: center;
            justify-content: center;
        }

        .control-label {
            width: 340px;
        }

        #featured {
            width: 20px;
            height: 20px;
        }

        .form-horizontal {
            color: rgb(33 40 50);
            font-weight: 500;
        }

        .error {
            color: red;
            font-size: 15px;
        }

        .single-form label {
            color: red;
            font-size: 15px;
        }
    </style>

    <body>

        <div class="main-wrapper main-wrapper-02">
            <jsp:include page="../component/administration/header.jsp" />
            <!-- Courses Admin Start -->
            <div class="section overflow-hidden position-relative" id="wrapper">

                <!-- Sidebar Wrapper Start -->
                <jsp:include page="../component/administration/menu.jsp" />
                <!-- Sidebar Wrapper End -->

                <!-- Page Content Wrapper Start -->
                <div class="page-content-wrapper">
                    <div class="container-fluid custom-container">

                        <!-- Message Start -->

                        <!-- Message End -->
                        <div class="right">
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-2">
                                    <c:if test="${not empty message}">
                                        <div class="alert alert-${alert}">
                                            <h5 style="color: green">${message}</h5>
                                        </div>
                                    </c:if>
                                    <c:if test="${not empty warning}">
                                        <div class="alert alert-${alert}">
                                            <h5 style="color: red">${warning}</h5>
                                        </div>
                                    </c:if>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="se"></label>
                                <label class="col-md-4 control-label" for="ov"></label>
                                <div class="single-form col-md-6">
                                    <button onclick="showOrHide()" id="ov" class="btn btn-primary">OVERVIEW
                                    </button>
                                    <button onclick="showOrHide2()" id="se" class="btn btn-primary">SETTING
                                    </button>
                                </div>
                            </div>
                            <form method="POST" action="${pageContext.request.contextPath}/quizdetails"
                                  class="form-horizontal" id="form_QD">
                                <!-- Text input-->
                                <div id="overview">
                                    <div class="form-group">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                               for="name">NAME</label>
                                        <div class="single-form col-md-4 ">
                                            <input name="name" id="name" class="form-control input-md" type="text"
                                                   value="${name}">
                                            <div id="name-error"></div>
                                        </div>
                                    </div>

                                    <div class="form-group courses-select">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                               for="subject">SUBJECT</label>
                                        <div class="single-form col-md-4">
                                            <select class="form-control1" name="subjectId" id="subjectId">
                                                <c:if test="${ssubject=='0' || ssubject==null}">
                                                    <option disabled selected> -- Subject -- </option>
                                                </c:if>                                                
                                                <c:forEach var="s" items="${subject}">                                                    
                                                    <option style="overflow: scroll" value="${s.getId()}" ${s.getId()==ssubject ? "selected"
                                                                                              : ""}>${s.getTitle()}</option>
                                                </c:forEach>
                                            </select>
                                            <div id="subjectId-error"></div>
                                        </div>
                                    </div>

                                    <div class="form-group courses-select">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                               for="level">EXAM LEVEL</label>
                                        <div class="single-form col-md-4">
                                            <select id="level" name="level" class="form-control">
                                                <c:if test="${levell=='0' || levell==null}">
                                                    <option disabled selected> -- Level -- </option>
                                                    <option value="Easy">Easy</option>
                                                    <option value="Medium">Medium</option>
                                                    <option value="Hard">Hard</option>
                                                </c:if>
                                                <c:if test="${levell=='Easy'}">
                                                    <option disabled> -- Level -- </option>
                                                    <option value="Easy" selected>Easy</option>
                                                    <option value="Medium">Medium</option>
                                                    <option value="Hard">Hard</option>
                                                </c:if>
                                                <c:if test="${levell=='Medium'}">
                                                    <option disabled> -- Level -- </option>
                                                    <option value="Easy">Easy</option>
                                                    <option value="Medium" selected>Medium</option>
                                                    <option value="Hard">Hard</option>
                                                </c:if>
                                                <c:if test="${levell=='Hard'}">
                                                    <option disabled> -- Level -- </option>
                                                    <option value="Easy">Easy</option>
                                                    <option value="Medium">Medium</option>
                                                    <option value="Hard" selected>Hard</option>
                                                </c:if>
                                            </select>
                                            <div id="level-error"></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                               for="duration">
                                            DURATION</label>
                                        <div class="single-form col-md-4">
                                            <input type="number" name="duration" id="duration"
                                                   class="form-control input-md" value="${duration}">
                                            <div id="duration-error"></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" class="col-md-4 control-label" for="rate">
                                            PASS RATE</label>
                                        <div class="single-form col-md-4">
                                            <input type="number" name="rate" id="rate" class="form-control input-md"
                                                   value="${rate}">
                                            <div id="rate-error"></div>
                                        </div>
                                    </div>

                                    <div class="form-group courses-select">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                               for="type">QUIZ TYPE</label>
                                        <div class="single-form col-md-4">
                                            <select class="form-control" name="type" id="type">
                                                <c:if test="${ttype=='Practice'}">
                                                    <option disabled> -- Type -- </option>
                                                    <option value="Practice" selected> Practice </option>
                                                    <option value="Exam"> Exam </option>
                                                </c:if>
                                                <c:if test="${ttype=='Exam'}">
                                                    <option disabled> -- Type -- </option>
                                                    <option value="Practice"> Practice </option>
                                                    <option value="Exam" selected> Exam </option>
                                                </c:if>
                                                <c:if test="${ttype=='0' || levell==null}">
                                                    <option disabled selected> -- Type -- </option>
                                                    <option value="Practice"> Practice </option>
                                                    <option value="Exam"> Exam </option>
                                                </c:if>
                                            </select>
                                            <div id="type-error"></div>
                                        </div>
                                    </div>

                                    <div id="divcontent" class="form-group">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                               for="description">
                                            DESCRIPTION</label>
                                        <div class="single-form col-md-4">
                                            <textarea class="form-control" id="description"
                                                      name="description">${description}</textarea>
                                            <div id="description-error"></div>
                                        </div>
                                    </div>
                                </div>

                                <div id="setting">
                                    <div class="form-group">
                                        <label style="font-weight: bold" class="col-md-4 control-label" for="total">
                                            TOTAL QUESTIONS</label>
                                        <div class="single-form col-md-4">
                                            <input type="number" name="total" id="total"
                                                   class="form-control input-md" value="${total}">
                                            <div id="total-error"></div>
                                        </div>
                                    </div>

                                    <div class="form-group courses-select">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                               for="qType">TYPE OF QUESTIONS</label>
                                        <div class="single-form col-md-4">
                                            <select onchange="this.form.submit()" id="qType" name="qType"
                                                    class="form-control">
                                                <c:if test="${qttype==null}">
                                                    <option disabled selected> -- Type -- </option>
                                                    <option value="Topic">Topic</option>
                                                    <option value="Group">Group</option>
                                                    <option value="Domain">Domain</option>
                                                </c:if>
                                                <c:if test="${qttype=='Topic'}">
                                                    <option disabled> -- Type -- </option>
                                                    <option value="Topic" selected>Topic</option>
                                                    <option value="Group">Group</option>
                                                    <option value="Domain">Domain</option>
                                                </c:if>
                                                <c:if test="${qttype=='Group'}">
                                                    <option disabled> -- Type -- </option>
                                                    <option value="Topic">Topic</option>
                                                    <option value="Group" selected>Group</option>
                                                    <option value="Domain">Domain</option>
                                                </c:if>
                                                <c:if test="${qttype=='Domain'}">
                                                    <option disabled> -- Type -- </option>
                                                    <option value="Topic">Topic</option>
                                                    <option value="Group">Group</option>
                                                    <option value="Domain" selected>Domain</option>
                                                </c:if>
                                            </select>
                                            <div id="qType-error"></div>
                                        </div>
                                    </div>


                                    <div class="a" style="display: flex;
                                         text-align: center;
                                         justify-content: center;">
                                        <div class="input_fields_wrap courses-select">
                                            <div id="filter" class="single-form col-md-4">
                                                <select id="qqType" name="qqType" class="form-control1">
                                                    <option disabled selected> -- Choices -- </option>
                                                    <c:forEach var="qq" items="${qqType}">
                                                        <option style="overflow: scroll" name="myText[]" value="${qq.getId()}">${qq.getName()}
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                        <button style="height: 54px;
                                                margin-top: 41px;" class="add_field_button btn btn-primary">Add More Fields</button>
                                    </div>
                                    <input name="qTypeB" type="text" value="${qTypeB}" disabled hidden>
                                    <!-- Button -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="singlebutton"></label>
                                        <div class="single-form col-md-4">
                                            <button type="submit" id="singlebutton" class="btn btn-primary">ADD
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>


                            <!-- Admin Courses Tab Start -->

                            <!-- Admin Courses Tab End -->

                            <!-- Admin Courses Tab Content Start -->

                            <!-- Admin Courses Tab Content End -->

                            <!-- Courses Resources Start -->
                            <jsp:include page="../component/administration/courses-resources.jsp" />
                            <!-- Courses Resources End -->

                        </div>
                    </div>
                    <!-- Page Content Wrapper End -->

                </div>
                <!-- Courses Admin End -->

                <jsp:include page="../component/common/footer.jsp" />
                <input id="count" type="number" value="${qqTypeC}" disabled hidden>
                <!--Back To Start-->
                <a href="#" class="back-to-top">
                    <i class="icofont-simple-up"></i>
                </a>
                <!--Back To End-->

            </div>

            <!-- JS
    ============================================ -->
            <jsp:include page="../component/common/js.jsp" />

            <script>
                addEventListener('DOMContentLoaded', (event) => {
                    //                t = document.getElementById("setting").value;
                    $("#setting").hide();
                });

                $(document).ready(function () {
                    var c = document.getElementById("count").value;
                    var max_fields = Number(c); //maximum input boxes allowed
                    var wrapper = $(".input_fields_wrap"); //Fields wrapper
                    var add_button = $(".add_field_button"); //Add button ID
                    var f = document.getElementById("filter");


                    var x = 1; //initlal text box count
                    $(add_button).click(function (e) { //on add input button click
                        e.preventDefault();
                        if (x < max_fields) { //max input box allowed
                            x++; //text box increment
                            $(wrapper).append('<div class="single-form col-md-4">' + f.innerHTML + '<a href="#" class="remove_field">Remove</a></div><br/>'); //add input box
                        }
                    });
                    $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                        e.preventDefault();
                        $(this).parent('div').remove();
                        x--;
                    })
                });
                //            $(document).ready(function () {
                //                $('#overview').on('click', function () {
                //                    if (this)
                //                    {
                //                        $("#divvideo").hide();
                //                        $("#divcontent").hide();
                //                    }
                //                    if (this.value == '1') {
                //                        $("#divvideo").show();
                //                        $("#divcontent").show();
                //                    }
                //                    if (this.value == '2')
                //                    {
                //                        $("#divcontent").show();
                //                        $("#divvideo").hide();
                //                    }
                //                });
                //            });

                //            function onChange() {
                //                var qType = document.getElementById("qType").value;
                //                var name = document.getElementById("name").value;
                //                var subject = document.getElementById("subjectId").value;
                //                var level = document.getElementById("level").value;               
                //                var duration = document.getElementById("duration").value;               
                //                var passRate = document.getElementById("rate").value;               
                //                var type = document.getElementById("type").value; 
                //                var des = document.getElementById("description").value; 
                //                var total = document.getElementById("total").value;  
                //                document.getElementById("name").value = name;
                //                document.getElementById("subjectId").value = subject;
                //                document.getElementById("level").value = level;
                //                document.getElementById("duration").value = duration;
                //            }

                function showOrHide() {
                    var s = document.getElementById("se");
                    var s2 = document.getElementById("setting");
                    var o = document.getElementById("ov");
                    var o2 = document.getElementById("overview");
                    if (o2.style.display === "none") {
                        o2.style.display = "block";
                        s2.style.display = "none";
                    }
                }

                function showOrHide2() {
                    var o = document.getElementById("ov");
                    var o2 = document.getElementById("overview");
                    var s = document.getElementById("se");
                    var s2 = document.getElementById("setting");
                    if (s2.style.display === "none") {
                        s2.style.display = "block";
                        o2.style.display = "none";
                    }
                }

                ClassicEditor
                        .create(document.querySelector('#content'), {
                            // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
                        })
                        .then(editor => {
                            window.editor = editor;
                        })
                        .catch(err => {
                            console.error(err.stack);
                        });
            </script>
    </body>

</html>