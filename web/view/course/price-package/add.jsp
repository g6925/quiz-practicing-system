<!------ Include the above in your HEAD tag ---------->




<%@page contentType="text/html" pageEncoding="UTF-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


            <html lang="en">

            <jsp:include page="../../component/common/head.jsp" />

            <style>
                .form-group {
                    display: flex;
                    align-items: center;
                    font-size: 20px;
                    justify-content: center;
                }

                .control-label {
                    width: 340px;
                }

                #featured {
                    width: 20px;
                    height: 20px;
                }

                .form-horizontal {
                    color: rgb(33 40 50);
                    font-weight: 500;
                }

                .thumbnail {
                    width: 300px;
                    border-radius: 15px;
                }
            </style>

            <body>
                <div class="main-wrapper main-wrapper-02">
                    <jsp:include page="../../component/administration/header.jsp" />
                    <!-- Courses Admin Start -->
                    <div class="section overflow-hidden position-relative" id="wrapper">

                        <!-- Sidebar Wrapper Start -->
                        <jsp:include page="../../component/administration/menu.jsp" />
                        <!-- Sidebar Wrapper End -->

                        <!-- Page Content Wrapper Start -->
                        <div class="page-content-wrapper">
                            <div class="container-fluid custom-container">
                                <div class="right">

                                    <div class="form-group">
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-2"
                                            style="display: flex; white-space: nowrap; margin-right: 120px">
                                            <c:if test="${not empty message}">
                                                <div class="alert alert-${alert}">
                                                    <h11 style="color: red">${message}</h11>
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                    <form action="./add" method="POST" class="form-horizontal">
                                        <!-- Text input-->
                                        <div class="form-group" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="name">PRICE PACKAGE NAME</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <input id="name" name="name" placeholder="PRICE PACKAGE NAME"
                                                    class="form-control input-md" required type="text">
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="duration">PRICE PACKAGE DURATION</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <input id="name" name="duration" placeholder="PRICE PACKAGE DURATION"
                                                    class="form-control input-md" type="number" required>
                                            </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group courses-select" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="status">PRICE PACKAGE STATUS</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <select id="product_categorie" name="status" class="form-control"
                                                    disabled>
                                                    <option value="1" selected>Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                                <input name="status" hidden value="1" />
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="priceRate">PRICE PACKAGE PRICE RATE</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <input id="name" name="priceRate" placeholder="PRICE PACKAGE PRICE RATE"
                                                    class="form-control input-md" type="number" required>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="description">PRICE PACKAGE DESCRIPTION</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <textarea class="form-control" id="product_description"
                                                    name="description" required
                                                    placeholder="PRICE PACKAGE DESCRIPTION"></textarea>
                                            </div>
                                        </div>

                                        <!-- Button -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="singlebutton"></label>
                                            <div class="single-form col-md-4">
                                                <button id="singlebutton" name="singlebutton"
                                                    class="btn btn-primary">Add New</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!-- Courses Resources Start -->
                                <jsp:include page="../../component/administration/courses-resources.jsp" />

                                <!-- Courses Resources End -->

                            </div>
                        </div>
                        <!-- Page Content Wrapper End -->

                    </div>
                    <!-- Courses Admin End -->

                    <jsp:include page="../../component/common/footer.jsp" />

                    <!--Back To Start-->
                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                </div>

                <!-- JS
============================================ -->
                <jsp:include page="../../component/common/js.jsp" />

            </body>

            </html>