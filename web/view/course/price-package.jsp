<%-- Document : sliderlist Created on : Jun 12, 2022, 4:16:04 PM Author : taina --%>

    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@page contentType="text/html" pageEncoding="UTF-8" %>
            <!DOCTYPE html>
            <html lang="en">

            <jsp:include page="../component/common/head.jsp" />
            <style>
                .search-filter {
                    width: 660px;
                    display: flex;
                    align-items: center;
                    justify-content: flex-end;
                    margin-left: 50px;
                }

                .search-input {
                    margin-right: 12px;
                    padding-left: 10px;
                    height: 50px;
                    border-radius: 10px;
                    border: 1px solid rgba(48, 146, 85, 0.2);
                }

                .search-button {
                    color: #fff;
                    line-height: 50px;
                    font-size: 15px;
                    padding: 0 30px;
                    border-radius: 10px;
                    border: 0 solid transparent;
                    background-color: #309255;
                    border-color: #309255;
                    font-weight: 500;
                    margin-right: 20px;
                }

                /*                    .item-thumb img{
                                
                            }*/
            </style>

            <body>

                <div class="main-wrapper main-wrapper-02">

                    <jsp:include page="../component/administration/header.jsp" />

                    <!-- Courses Admin Start -->
                    <div class="section overflow-hidden position-relative" id="wrapper">

                        <!-- Sidebar Wrapper Start -->
                        <jsp:include page="../component/administration/menu.jsp" />
                        <!-- Sidebar Wrapper End -->

                        <!-- Page Content Wrapper Start -->
                        <div class="page-content-wrapper">
                            <div class="container-fluid custom-container">

                                <!-- Admin Courses Tab Start -->
                                <div class="admin-courses-tab">
                                    <h3 class="title">Price Package</h3>

                                    <div class="courses-tab-wrapper">
                                        <div class="tab-btn">
                                            <a href="${pageContext.request.contextPath}/course/price-package/add"
                                                class="btn btn-primary btn-hover-dark">New Price Package</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Admin Courses Tab End -->
                                <c:forEach items="${pricePackage}" var="p">
                                    <!-- Admin Courses Tab Content Start -->
                                    <div class="admin-courses-tab-content">
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="tab1">
                                                <!-- Courses Item Start -->
                                                <div class="courses-item">


                                                    <div class="content-title">
                                                        <h3 class="title" style="font-size: 34px">${p.name}</h3>
                                                    </div>

                                                    <div class="content-wrapper">

                                                        <div class="content-box" style="width: 21%">
                                                            <p>Duration in Months</p>
                                                            <span class="count">${p.duration}</span>
                                                        </div>

                                                        <div class="content-box">
                                                            <p>Status</p>
                                                            <c:if test="${p.status==true}">
                                                                <span class="count">Active</span>
                                                            </c:if>
                                                            <c:if test="${p.status==false}">
                                                                <span class="count">Inactive</span>
                                                            </c:if>
                                                        </div>

                                                        <div class="content-box">
                                                            <p>priceRate</p>
                                                            <span class="count">${p.priceRate}%</span>
                                                        </div>

                                                        <div class="tab-btn">
                                                            <a href="${pageContext.request.contextPath}/course/price-package/edit?id=${p.id}"
                                                                class="btn btn-primary btn-hover-dark">Edit</a>
                                                        </div>
                                                        <div class="tab-btn" style="width: 152px">
                                                            <c:if test="${p.status==true}"><a
                                                                    href="${pageContext.request.contextPath}/course/price-package/status?id=${p.id}"
                                                                    class="btn btn-primary btn-hover-dark w-100">Deactive</a>
                                                            </c:if>
                                                            <c:if test="${p.status==false}"><a
                                                                    href="${pageContext.request.contextPath}/course/price-package/status?id=${p.id}"
                                                                    class="btn btn-primary btn-hover-dark w-100">Active</a>
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </div>
                                </c:forEach>
                                <!-- Courses Item End -->
                            </div>
                        </div>
                    </div>
                    <!-- Admin Courses Tab Content End -->
                    <jsp:include page="../component/administration/courses-resources.jsp" />
                </div>
                </div>
                <!-- Page Content Wrapper End -->

                <!-- Courses Admin End -->
                </div>
                <jsp:include page="../component/common/footer.jsp" />

                <!--Back To Start-->
                <a href="#" class="back-to-top">
                    <i class="icofont-simple-up"></i>
                </a>
                <!--Back To End-->
                </div>

                <jsp:include page="../component/common/js.jsp" />

            </body>

            </html>