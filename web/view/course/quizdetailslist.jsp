<!------ Include the above in your HEAD tag ---------->

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <jsp:include page="../component/common/head.jsp" />
    <style>
        .form-group {
            display: flex;
            align-items: center;
            font-size: 20px;
            justify-content: center;
        }

        .input_fields_wrap {
            text-align: center;
            align-items: center;
            justify-content: center;
        }

        .control-label {
            width: 340px;
        }

        #featured {
            width: 20px;
            height: 20px;
        }

        .form-horizontal {
            color: rgb(33 40 50);
            font-weight: 500;
        }

        .error {
            color: red;
            font-size: 15px;
        }

        .single-form label {
            color: red;
            font-size: 15px;
        }

        td, th {
            border: 2px solid black;
            /* border-width border-style border-color */
        }
    </style>

    <body>

        <div class="main-wrapper main-wrapper-02">
            <jsp:include page="../component/administration/header.jsp" />
            <!-- Courses Admin Start -->
            <div class="section overflow-hidden position-relative" id="wrapper">

                <!-- Sidebar Wrapper Start -->
                <jsp:include page="../component/administration/menu.jsp" />
                <!-- Sidebar Wrapper End -->

                <!-- Page Content Wrapper Start -->
                <div class="page-content-wrapper">
                    <div class="container-fluid custom-container">                
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-2">
                                <c:if test="${not empty message}">
                                    <div class="alert alert-${alert}">
                                        <h5 style="color: green">${message}</h5>
                                    </div>
                                </c:if>
                                <c:if test="${not empty warning}">
                                    <div class="alert alert-${alert}">
                                        <h5 style="color: red">${warning}</h5>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                        <div style="justify-content: center; text-align: center; align-content: center">
                            <table border="2px solid" style="width: 100%;">
                                <tr style="text-align: center">
                                    <th>ID</th>
                                    <th>Content</th>
                                    <th>Topic</th>
                                    <th>Subject</th>
                                    <th>Level</th>
                                </tr>
                                <c:forEach var="l" items="${list}">
                                    <tr>
                                        <td style="text-align: center">${l.id}</td>
                                        <td>${l.content}</td>
                                        <td>${l.topic.name}</td>
                                        <td>${l.subject.title}</td>
                                        <c:if test="${l.level == '1'}"><td style="text-align: center">Easy</td></c:if>
                                        <c:if test="${l.level == '2'}"><td style="text-align: center">Medium</td></c:if>
                                        <c:if test="${l.level == '3'}"><td style="text-align: center">Hard</td></c:if>
                                        <c:if test="${l.level == null}"><td style="text-align: center"></td></c:if>
                                        </tr>
                                </c:forEach>                                    
                            </table>
                        </div>
                        <!-- Courses Resources Start -->
                        <jsp:include page="../component/administration/courses-resources.jsp" />
                        <!-- Courses Resources End -->                        
                    </div>
                    <!-- Page Content Wrapper End -->
                </div>
                <!-- Courses Admin End -->

                <jsp:include page="../component/common/footer.jsp" />
                <!--Back To Start-->
                <a href="#" class="back-to-top">
                    <i class="icofont-simple-up"></i>
                </a>
                <!--Back To End-->
            </div>
            <!-- JS
    ============================================ -->
            <jsp:include page="../component/common/js.jsp" />
    </body>

</html>