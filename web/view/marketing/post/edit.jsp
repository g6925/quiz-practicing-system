<!------ Include the above in your HEAD tag ---------->




<%@page contentType="text/html" pageEncoding="UTF-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


            <html lang="en">

            <jsp:include page="../../component/common/head.jsp" />

            <style>
                .form-group {
                    display: flex;
                    align-items: center;
                    font-size: 20px;
                    justify-content: center;
                }

                .control-label {
                    width: 340px;
                }

                #featured {
                    width: 20px;
                    height: 20px;
                }

                .form-horizontal {
                    color: rgb(33 40 50);
                    font-weight: 500;
                }

                #thumbnail-preview {
                    border-radius: 15px;
                }
            </style>

            <body>

                <div class="main-wrapper main-wrapper-02">
                    <jsp:include page="../../component/administration/header.jsp" />
                    <!-- Posts Marketing Start -->
                    <div class="section overflow-hidden position-relative" id="wrapper">

                        <!-- Sidebar Wrapper Start -->
                        <jsp:include page="../../component/administration/menu.jsp" />
                        <!-- Sidebar Wrapper End -->

                        <!-- Page Content Wrapper Start -->
                        <div class="page-content-wrapper">
                            <div class="container-fluid custom-container">

                                <!-- Message Start -->

                                <!-- Message End -->




                                <div class="right">



                                    <form action="./edit" method="POST" id="form_post-edit" class="form-horizontal"
                                        enctype="multipart/form-data">



                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="post_id"></label>
                                            <div class="col-md-4">
                                                <input id="id" name="id" class="input-md" value="${post.id}"
                                                    type="hidden">
                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="title">POST
                                                NAME</label>
                                            <div class="single-form  col-md-4">
                                                <input id="title" name="title" placeholder="POST NAME"
                                                    class="form-control input-md" value="${post.title}" type="text">
                                                <div id="post-title-error"></div>
                                            </div>
                                        </div>



                                        <!-- Select Basic -->
                                        <div class="form-group posts-select">
                                            <label class="col-md-4 control-label" for="category">POST
                                                CATEGORY</label>
                                            <div class="single-form col-md-4">
                                                <select id="category" name="category" class="form-control">
                                                    <c:forEach items="${categorys}" var="c">
                                                        <option <c:if test="${c.id == post.category.id}">
                                                            selected="selected"
                                                            </c:if>
                                                            value="${c.id}">${c.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div id="category-error"></div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group courses-select">
                                            <label class="col-md-4 control-label" for="status">STATUS</label>
                                            <div class="single-form col-md-4">
                                                <select id="status" name="status" class="form-control">
                                                    <option value="1" ${post.status==true ? "selected" : "" }>Active
                                                    </option>
                                                    <option value="0" ${post.status==false ? "selected" : "" }>Inactive
                                                    </option>
                                                </select>
                                            </div>
                                            <div id="status-error"></div>
                                        </div>

                                        <!-- Preview Thumbnail-->
                                        <div class="form-group" style="margin-top: 33px;">
                                            <label class="col-md-4 control-label">THUMBNAIL IMAGE</label>
                                            <div class="col-md-4">
                                                <img id="thumbnail-preview" class="thumbnail img-responsive"
                                                    src="${post.thumbnail}" alt="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="single-form col-md-4 mb-4">
                                                <input id="thumbnail" name="thumbnail" class="input-file form-control"
                                                    data-can-empty="true" type="file">
                                                <div id="thumbnail-error"></div>
                                            </div>
                                        </div>

                                        <!-- Textarea -->
                                        <div class="form-group" style="margin-top: -20px;">
                                            <label class="col-md-4 control-label" for="brief">POST BRIEF</label>
                                            <div class="single-form col-md-4">
                                                <textarea class="form-control" id="brief"
                                                    name="brief">${post.brief}</textarea>
                                                <div id="brief-error"></div>

                                            </div>
                                        </div>

                                        <!-- Textarea -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="content">POST
                                                Content</label>
                                            <div class="single-form col-md-4">
                                                <textarea class="form-control" id="content"
                                                    name="content">${post.content}</textarea>
                                                <div id="content-error"></div>
                                            </div>
                                        </div>



                                        <!-- Button -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="singlebutton"></label>
                                            <div class="single-form col-md-4">
                                                <button id="singlebutton" name="singlebutton"
                                                    class="btn btn-primary">EDIT POST</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Page Content Wrapper End -->

                    </div>
                    <!-- Posts Marketing End -->
                    <jsp:include page="../../component/common/footer.jsp" />

                    <!--Back To Start-->
                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                </div>

                <!-- JS
============================================ -->
                <jsp:include page="../../component/common/js.jsp" />
                <script>
                    ClassicEditor
                        .create(document.querySelector('#content'), {
                            // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
                        })
                        .then(editor => {
                            window.editor = editor;
                        })
                        .catch(err => {
                            console.error(err.stack);
                        });
                </script>
            </body>

            </html>