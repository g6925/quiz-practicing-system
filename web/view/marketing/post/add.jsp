<!------ Include the above in your HEAD tag ---------->

<%@page contentType="text/html" pageEncoding="UTF-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <html>
        <jsp:include page="../../component/common/head.jsp" />
        <style>
            .form-group {
                display: flex;
                align-items: center;
                font-size: 20px;
                justify-content: center;
            }

            .control-label {
                width: 340px;
            }

            #featured {
                width: 20px;
                height: 20px;
            }

            .form-horizontal {
                color: rgb(33 40 50);
                font-weight: 500;
            }

            #thumbnail-preview {
                border-radius: 15px;
                margin-top: 20px;
            }
        </style>

        <body>

            <div class="main-wrapper main-wrapper-02">
                <jsp:include page="../../component/administration/header.jsp" />
                <!-- Posts Marketing Start -->
                <div class="section overflow-hidden position-relative" id="wrapper">

                    <!-- Sidebar Wrapper Start -->
                    <jsp:include page="../../component/administration/menu.jsp" />
                    <!-- Sidebar Wrapper End -->

                    <!-- Page Content Wrapper Start -->
                    <div class="page-content-wrapper">
                        <div class="container-fluid custom-container">
                            <div class="right">
                                <div class="form-group">
                                    <label class="col-md-4 control-label"></label>
                                    <div class="col-md-2">
                                        <c:if test="${not empty message}">
                                            <div class="alert alert-${alert}">
                                                ${message}
                                            </div>
                                        </c:if>
                                    </div>
                                </div>

                                <form action="./add" method="POST" id="form_post-add" class="form-horizontal"
                                    enctype="multipart/form-data">
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="title">POST TITTLE</label>
                                        <div class="single-form col-md-4 ">
                                            <input id="title" name="title" placeholder="POST TITTLE"
                                                class="form-control input-md" type="text">
                                            <div id="title-error"></div>
                                        </div>
                                    </div>



                                    <!-- Select Basic -->
                                    <div class="form-group posts-select">
                                        <label class="col-md-4 control-label" for="category">POST CATEGORY</label>
                                        <div class="single-form col-md-4">
                                            <select id="category" name="category" class="form-control">
                                                <option disabled selected> -- Category -- </option>
                                                <c:forEach items="${categorys}" var="c">
                                                    <option <c:if test="${c.getId() == categoryID}">
                                                        selected="selected"
                                                        </c:if>
                                                        value="${c.getId()}">${c.getName()}</option>
                                                </c:forEach>
                                            </select>
                                            <div id="category-error"></div>
                                        </div>
                                    </div>

                                    <!-- Select Basic -->
                                    <div class="form-group courses-select">
                                        <label class="col-md-4 control-label" for="status">STATUS</label>
                                        <div class="single-form col-md-4">
                                            <select id="status" name="status" class="form-control">
                                                <option disabled selected> -- Status -- </option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                            <div id="status-error"></div>
                                        </div>
                                    </div>

                                    <!-- File Button -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="thumbnail">THUMBNAIL
                                            IMAGE</label>
                                        <div class="single-form col-md-4">
                                            <input id="thumbnail" name="thumbnail" data-can-empty="false"
                                                class="form-control" type="file">
                                            <div id="thumbnail-error"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-4 ">
                                            <img id="thumbnail-preview" class="thumbnail img-responsive d-none" src=""
                                                alt="">
                                        </div>
                                    </div>

                                    <!-- Textarea -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="brief">POST BRIEF</label>
                                        <div class="single-form col-md-4">
                                            <textarea class="form-control" id="brief" name="brief"></textarea>
                                            <div id="brief-error"></div>
                                        </div>
                                    </div>

                                    <!-- Textarea -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="content">POST CONTENT</label>
                                        <div class="single-form col-md-4">
                                            <textarea class="form-control" id="content" name="content"></textarea>
                                            <div id="content-error"></div>
                                        </div>
                                    </div>

                                    <!-- Button -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="btn-submit"></label>
                                        <div class="single-form col-md-4">
                                            <button id="btn-submit" name="btn-submit" class="btn btn-primary">ADD
                                                POST</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Page Content Wrapper End -->

                </div>
                <!-- Posts Marketing End -->

                <jsp:include page="../../component/common/footer.jsp" />

                <!--Back To Start-->
                <a href="#" class="back-to-top">
                    <i class="icofont-simple-up"></i>
                </a>
                <!--Back To End-->

            </div>

            <!-- JS
============================================ -->
            <jsp:include page="../../component/common/js.jsp" />
            <script>
                ClassicEditor
                    .create(document.querySelector('#content'), {
                        // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
                    })
                    .then(editor => {
                        window.editor = editor;
                    })
                    .catch(err => {
                        console.error(err.stack);
                    });
            </script>
        </body>

        </html>