<%-- Document : dashboard Created on : Jun 23, 2022, 10:54:15 PM Author : dclon --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

            <!DOCTYPE html>
            <html>
            <jsp:include page="../component/common/head.jsp" />

            <body>
                <div class="main-wrapper main-wrapper-02">

                    <!-- Login Header Start -->
                    <jsp:include page="../component/administration/header.jsp" />
                    <!-- Login Header End -->

                    <!-- Courses Admin Start -->
                    <div class="section overflow-hidden position-relative" id="wrapper">
                        <div id="data" style="display: none">${dataCharts}</div>
                        <!-- Sidebar Wrapper Start -->
                        <jsp:include page="../component/administration/menu.jsp" />
                        <!-- Sidebar Wrapper End -->

                        <div class="page-content-wrapper py-0">

                            <!-- Admin Tab Menu Start -->
                            <jsp:include page="dashboard/component_nav.jsp" />
                            <!-- Admin Tab Menu End -->
                            <form action="${pageContext.request.contextPath}/${user.role.name}/dashboard/overview" method="GET">
                                <!-- Page Content Wrapper Start -->
                                <div class="main-content-wrapper">
                                    <!-- Overview Top Start -->
                                    <div class="container-fluid">
                                        <!-- Overview Top Start -->
                                        <div class="admin-top-bar flex-wrap">
                                            <div class="courses-select">
                                                <select onchange="this.form.submit()" name="category">
                                                    <option ${categoryID==0 ? "selected" : "" } value="0">All Courses
                                                    </option>
                                                    <c:forEach items="${categories}" var="c">

                                                        <option ${categoryID==c.id ? "selected" : "" } value="${c.id}">
                                                            ${c.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>

                                            <div class="overview-box">
                                                <div class="single-box" style="width: fit-content">
                                                    <h5 class="title">Total Revenue</h5>
                                                    <div class="count">$${rd.getTotalRevenue(categoryID)}</div>
                                                    <p><span>$${rd.getTotalRevenueInMonth(categoryID, time)}</span> This
                                                        months</p>
                                                </div>

                                                <div class="single-box">
                                                    <h5 class="title">Total Enrollment’s</h5>
                                                    <div class="count">${rd.getTotalEnrollment(categoryID)}</div>
                                                    <p><span>${rd.getTotalEnrollmentInMonth(categoryID, time)}</span>
                                                        This months</p>
                                                </div>

                                                <div class="single-box">
                                                    <h5 class="title">Total Courses’s</h5>
                                                    <div class="count">${sd.getTotalSubjects(categoryID)}</div>
                                                    <p><span>${sd.getTotalSubjectsInMonth(categoryID, time)}</span> This
                                                        months</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Overview Top End -->

                                        <!-- Graph Top Start -->
                                        <div class="graph">
                                            <div class="graph-title">
                                                <h4 class="title">Recent earnings report</h4>

                                                <div class="months-select">
                                                    <select onchange="this.form.submit()" name="duration">
                                                        <option ${duration=="12 months" ? "selected" : "" }
                                                            value="12 months">Last 12 months</option>
                                                        <option ${duration=="6 months" ? "selected" : "" }
                                                            value="6 months">Last 6 months</option>
                                                        <option ${duration=="2 months" ? "selected" : "" }
                                                            value="2 months">Last 2 months</option>
                                                        <option ${duration=="1 month" ? "selected" : "" }
                                                            value="1 month">Last 1 month</option>
                                                        <option ${duration=="1 week" ? "selected" : "" } value="1 week">
                                                            Last 1 week</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="graph-content">
                                                <div id="uniqueReport"></div>
                                            </div>

                                            <div class="graph-btn">
                                                <a class="btn btn-primary btn-hover-dark" href="#">Revenue Report <i
                                                        class="icofont-rounded-down"></i></a>
                                            </div>
                                        </div>
                                        <!-- Graph Top End -->

                                    </div>
                                    <!-- Overview Top End -->

                                    <!-- Page Content Wrapper End -->

                                </div>
                            </form>
                        </div>
                        <!-- Courses Admin End -->

                        <!-- Footer Start  -->
                        <jsp:include page="../component/common/footer.jsp" />
                        <!-- Footer End -->

                        <!--Back To Start-->
                        <a href="#" class="back-to-top">
                            <i class="icofont-simple-up"></i>
                        </a>
                        <!--Back To End-->

                    </div>

                    <!-- JS
============================================ -->

                    <!-- Modernizer & jQuery JS -->
                    <jsp:include page="../component/common/js.jsp" />
                    <script src="${pageContext.request.contextPath}/assets/js/overview.js"></script>
            </body>

            </html>