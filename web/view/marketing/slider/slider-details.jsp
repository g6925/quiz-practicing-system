<!------ Include the above in your HEAD tag ---------->




<%@page contentType="text/html" pageEncoding="UTF-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


            <html lang="en">

            <jsp:include page="../../component/common/head.jsp" />

            <style>
                .form-group {
                    display: flex;
                    align-items: center;
                    font-size: 20px;
                    justify-content: center;
                }

                .control-label {
                    width: 340px;
                }

                #featured {
                    width: 20px;
                    height: 20px;
                }

                .form-horizontal {
                    color: rgb(33 40 50);
                    font-weight: 500;
                }

                .thumbnail {
                    width: 300px;
                    border-radius: 15px;
                }
            </style>

            <body>
                <c:set value="${slider}" var="s" />
                <div class="main-wrapper main-wrapper-02">
                    <jsp:include page="../../component/administration/header.jsp" />
                    <!-- Courses Admin Start -->
                    <div class="section overflow-hidden position-relative" id="wrapper">

                        <!-- Sidebar Wrapper Start -->
                        <jsp:include page="../../component/administration/menu.jsp" />
                        <!-- Sidebar Wrapper End -->

                        <!-- Page Content Wrapper Start -->
                        <div class="page-content-wrapper">
                            <div class="container-fluid custom-container">
                                <div class="right">
                                    <div class="form-group" style="margin-bottom: 20px">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                            for="filebutton">SLIDER IMAGE</label>
                                        <div class="col-md-4">
                                            <img class="thumbnail img-responsive" src="${s.image}" alt="">
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group" style="margin-bottom: 20px">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                            for="name">SLIDER
                                            NAME</label>
                                        <div class="single-form col-md-4" style="margin-bottom: 20px">
                                            <h7>${s.title}</h7>
                                        </div>
                                    </div>

                                    <!-- Textarea -->
                                    <div class="form-group" style="margin-bottom: 20px">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                            for="subject_description">SLIDER
                                            CONTENT</label>
                                        <div class="single-form col-md-4" style="margin-bottom: 20px">
                                            <h7>${s.content}</h7>
                                        </div>
                                    </div>

                                    <div class="form-group" style="margin-bottom: 20px">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                            for="backlink">SLIDER
                                            BACKLINK</label>
                                        <div class="single-form col-md-4" style="margin-bottom: 20px">
                                            <c:if test="${empty s.backlink}">
                                                <h7 style="color: red">This Slider has no Backlink</h7>
                                            </c:if>
                                            <c:if test="${not empty s.backlink}">
                                                <h7>${s.backlink}</h7>
                                            </c:if>
                                        </div>
                                    </div>

                                    <!-- Select Basic -->
                                    <div class="form-group" style="margin-bottom: 20px">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                            for="status">SLIDER STATUS</label>
                                        <div class="single-form col-md-4" style="margin-bottom: 20px">
                                            <c:if test="${s.status==true}">
                                                <h7 style="color: red">Active</h7>
                                            </c:if>
                                            <c:if test="${s.status==false}">
                                                <h7 style="color: red">Inactive</h7>
                                            </c:if>
                                        </div>
                                    </div>

                                    <div class="form-group" style="margin-bottom: 20px">
                                        <label style="font-weight: bold" class="col-md-4 control-label"
                                            for="name">MODIFIED LAST BY</label>
                                        <div class="single-form col-md-4" style="margin-bottom: 20px">
                                            <h7>${s.user.name}</h7>
                                        </div>
                                    </div>

                                    <!-- Button -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="singlebutton"></label>
                                        <div class="single-form col-md-4">
                                            <button
                                                onclick="window.location.href = '${pageContext.request.contextPath}/marketing/slider/edit?id=${s.id}';"
                                                id="singlebutton" name="singlebutton" class="btn btn-primary">Edit
                                                Slider</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- Courses Resources Start -->
                                <jsp:include page="../../component/administration/courses-resources.jsp" />

                                <!-- Courses Resources End -->

                            </div>
                        </div>
                        <!-- Page Content Wrapper End -->

                    </div>
                    <!-- Courses Admin End -->

                    <jsp:include page="../../component/common/footer.jsp" />

                    <!--Back To Start-->
                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                </div>

                <!-- JS
============================================ -->
                <jsp:include page="../../component/common/js.jsp" />

            </body>

            </html>