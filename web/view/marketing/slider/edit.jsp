<!------ Include the above in your HEAD tag ---------->




<%@page contentType="text/html" pageEncoding="UTF-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


            <html lang="en">

            <jsp:include page="../../component/common/head.jsp" />

            <style>
                .form-group {
                    display: flex;
                    align-items: center;
                    font-size: 20px;
                    justify-content: center;
                }

                .control-label {
                    width: 340px;
                }

                #featured {
                    width: 20px;
                    height: 20px;
                }

                .form-horizontal {
                    color: rgb(33 40 50);
                    font-weight: 500;
                }

                .thumbnail {
                    width: 300px;
                    border-radius: 15px;
                }
            </style>

            <body>
                <c:set value="${slider}" var="s" />
                <div class="main-wrapper main-wrapper-02">
                    <jsp:include page="../../component/administration/header.jsp" />
                    <!-- Courses Admin Start -->
                    <div class="section overflow-hidden position-relative" id="wrapper">

                        <!-- Sidebar Wrapper Start -->
                        <jsp:include page="../../component/administration/menu.jsp" />
                        <!-- Sidebar Wrapper End -->

                        <!-- Page Content Wrapper Start -->
                        <div class="page-content-wrapper">
                            <div class="container-fluid custom-container">
                                <div class="right">
                                    <form action="./edit" method="POST" class="form-horizontal"
                                        enctype="multipart/form-data" id="form-slider">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for=""></label>
                                            <div class="col-md-4">
                                                <input id="" name="slider_id" class=" input-md" value="${s.id}" hidden>
                                            </div>
                                        </div>

                                        <!-- Preview Thumbnail-->
                                        <div class="form-group" style="margin-top: 33px;">
                                            <label class="col-md-4 control-label">SLIDER IMAGE</label>
                                            <div class="single-form col-md-4">
                                                <img id="image-preview" class="thumbnail img-responsive"
                                                    src="${s.image}" alt="">
                                            </div>
                                        </div>


                                        <!-- File Button -->
                                        <div class="form-group" style="height: 80px;
                                     ">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="filebutton"></label>
                                            <div class="single-form col-md-4">
                                                <input id="image" name="image" class="input-file form-control"
                                                    type="file" data-can-empty="true">
                                                <div id="image-error"></div>
                                            </div>

                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="slider_name">SLIDER
                                                NAME</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <input id="slider_name" name="slider_name" placeholder="SLIDER NAME"
                                                    class="form-control input-md" value="${s.title}" required
                                                    type="text">
                                                <div id="slider_name-error"></div>
                                            </div>
                                        </div>

                                        <!-- Textarea -->
                                        <div class="form-group" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="slider_content">SLIDER
                                                CONTENT</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <textarea class="form-control" id="slider_content" name="slider_content"
                                                    placeholder="SLIDER CONTENT">${s.content}</textarea>
                                                <div id="slider_content-error"></div>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="backlink">SLIDER
                                                BACKLINK</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <input id="backlink" name="backlink" placeholder="SLIDER BACKLINK"
                                                    class="form-control input-md" value="${s.backlink}" type="text">
                                                <div id="backlink-error"></div>
                                            </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group courses-select" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="status">SLIDER STATUS</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <select id="status" name="status" class="form-control">
                                                    <option value="1" ${s.status==true ? "selected" : "" }>Active
                                                    </option>
                                                    <option value="0" ${s.status==false ? "selected" : "" }>Inactive
                                                    </option>
                                                </select>
                                                <div id="status-error"></div>
                                            </div>
                                        </div>




                                        <div class="form-group" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="name">MODIFIED LAST BY</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <h7>${s.user.name}</h7>
                                            </div>
                                        </div>

                                        <!-- Button -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="singlebutton"></label>
                                            <div class="single-form col-md-4">
                                                <button id="singlebutton" name="singlebutton"
                                                    class="btn btn-primary">Edit Slider</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!-- Courses Resources Start -->
                                <jsp:include page="../../component/administration/courses-resources.jsp" />

                                <!-- Courses Resources End -->

                            </div>
                        </div>
                        <!-- Page Content Wrapper End -->

                    </div>
                    <!-- Courses Admin End -->

                    <jsp:include page="../../component/common/footer.jsp" />

                    <!--Back To Start-->
                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                </div>

                <!-- JS
============================================ -->
                <jsp:include page="../../component/common/js.jsp" />
                <script>
                    ClassicEditor
                        .create(document.querySelector('#slider_content'), {
                            // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
                        })
                        .then(editor => {
                            window.editor = editor;
                        })
                        .catch(err => {
                            console.error(err.stack);
                        });
                </script>
            </body>

            </html>