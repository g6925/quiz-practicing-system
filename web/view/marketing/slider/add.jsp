<!------ Include the above in your HEAD tag ---------->




<%@page contentType="text/html" pageEncoding="UTF-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


            <html lang="en">

            <jsp:include page="../../component/common/head.jsp" />

            <style>
                .form-group {
                    display: flex;
                    align-items: center;
                    font-size: 20px;
                    justify-content: center;
                }

                .control-label {
                    width: 340px;
                }

                #featured {
                    width: 20px;
                    height: 20px;
                }

                .form-horizontal {
                    color: rgb(33 40 50);
                    font-weight: 500;
                }

                .thumbnail {
                    width: 300px;
                    border-radius: 15px;
                }
            </style>

            <body>
                <div class="main-wrapper main-wrapper-02">
                    <jsp:include page="../../component/administration/header.jsp" />
                    <!-- Courses Admin Start -->
                    <div class="section overflow-hidden position-relative" id="wrapper">

                        <!-- Sidebar Wrapper Start -->
                        <jsp:include page="../../component/administration/menu.jsp" />
                        <!-- Sidebar Wrapper End -->

                        <!-- Page Content Wrapper Start -->
                        <div class="page-content-wrapper">
                            <div class="container-fluid custom-container">
                                <div class="right">

                                    <div class="form-group">
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-2">
                                            <c:if test="${not empty message}">
                                                <div class="alert alert-${alert}">
                                                    <h11 style="color: red">${message}</h11>
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                    <form action="./add" method="POST" class="form-horizontal"
                                        enctype="multipart/form-data" id="form_slider">


                                        <!-- Text input-->
                                        <div class="form-group" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="slider_name">SLIDER
                                                NAME</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <input id="slider_name" name="slider_name" placeholder="SLIDER NAME"
                                                    class="form-control input-md" type="text">
                                                <div id="slider_name-error"></div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-4 ">
                                                <img id="image-preview" class="thumbnail img-responsive d-none" src=""
                                                    alt="">
                                            </div>
                                        </div>

                                        <!-- File Button -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"
                                                style="font-weight: bold;margin-top: 20px" for="file_thumbnail">SLIDER
                                                IMAGE
                                            </label>
                                            <div class="single-form col-md-4">
                                                <input id="image" name="image" data-can-empty="false"
                                                    class="form-control" type="file">
                                                <div id="image-error"></div>
                                            </div>
                                        </div>




                                        <!-- Textarea -->
                                        <div class="form-group" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="slider_content">SLIDER
                                                CONTENT</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <textarea class="form-control" id="slider_content" name="slider_content"
                                                    placeholder="SLIDER CONTENT"></textarea>
                                                <div id="slider_content-error"></div>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="backlink">SLIDER
                                                BACKLINK</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <input id="backlink" name="backlink" placeholder="SLIDER BACKLINK"
                                                    class="form-control input-md" type="text">
                                                <div id="backlink-error"></div>
                                            </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group courses-select" style="margin-bottom: 20px">
                                            <label style="font-weight: bold" class="col-md-4 control-label"
                                                for="status">SLIDER STATUS</label>
                                            <div class="single-form col-md-4" style="margin-bottom: 20px">
                                                <select id="status" name="status" class="form-control">
                                                    <option disabled selected> -- Status -- </option>
                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                                <div id="status-error"></div>
                                            </div>
                                        </div>

                                        <!-- Button -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="singlebutton"></label>
                                            <div class="single-form col-md-4">
                                                <button id="singlebutton" name="singlebutton"
                                                    class="btn btn-primary">Add Slider</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!-- Courses Resources Start -->
                                <jsp:include page="../../component/administration/courses-resources.jsp" />

                                <!-- Courses Resources End -->

                            </div>
                        </div>
                        <!-- Page Content Wrapper End -->

                    </div>
                    <!-- Courses Admin End -->

                    <jsp:include page="../../component/common/footer.jsp" />

                    <!--Back To Start-->
                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                </div>

                <!-- JS
============================================ -->
                <jsp:include page="../../component/common/js.jsp" />
                <script>
                    ClassicEditor
                        .create(document.querySelector('#slider_content'), {
                            // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
                        })
                        .then(editor => {
                            window.editor = editor;
                        })
                        .catch(err => {
                            console.error(err.stack);
                        });
                </script>
            </body>

            </html>