<%-- Document : posts-marketing Created on : Jun 2, 2022, 3:30:04 PM Author : xuant --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

                <html>
                <jsp:include page="../component/common/head.jsp" />
                <style>
                    .search-filter {
                        width: 660px;
                        display: flex;
                        align-items: center;
                        justify-content: space-between;
                    }

                    .search-input {
                        margin-right: 12px;
                        padding-left: 10px;
                        height: 50px;
                        border-radius: 10px;
                        border: 1px solid rgba(48, 146, 85, 0.2);
                    }

                    .search-button {
                        color: #fff;
                        line-height: 50px;
                        font-size: 15px;
                        padding: 0 30px;
                        border-radius: 10px;
                        border: 0 solid transparent;
                        background-color: #309255;
                        border-color: #309255;
                        font-weight: 500;
                    }
                </style>

                <body>

                    <div class="main-wrapper main-wrapper-02">
                        <jsp:include page="../component/administration/header.jsp" />


                        <!-- Posts Marketing Start -->
                        <div class="section overflow-hidden position-relative" id="wrapper">

                            <!-- Sidebar Wrapper Start -->
                            <jsp:include page="../component/administration/menu.jsp" />
                            <!-- Sidebar Wrapper End -->

                            <!-- Page Content Wrapper Start -->
                            <div class="page-content-wrapper">
                                <div class="container-fluid custom-container">

                                    <!-- Message Start -->

                                    <!-- Message End -->

                                    <!-- Marketing Posts Tab Start -->
                                    <div class="marketing-posts-tab">
                                        <h3 class="title">Posts</h3>



                                        <div class="posts-tab-wrapper">


                                            <form action="./posts" method="GET">
                                                <div class="search-filter">


                                                    <div class="tab-btn">

                                                        <input type="text" class="search-input"
                                                            placeholder="Search here" name="keyWord" value="${keyWord}">
                                                        <input class="search-button" type="submit" value="Search" />
                                                    </div>
                                                    <div class="posts-select">
                                                        <select onchange="this.form.submit()" name="category">
                                                            <option disabled selected value> -- Filter Category --
                                                            </option>
                                                            <c:forEach items="${categorys}" var="c">
                                                                <option <c:if test="${c.id == categoryID}">
                                                                    selected="selected"
                                                                    </c:if>
                                                                    value="${c.id}">${c.name}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                            </form>


                                            <div class="tab-btn">
                                                <a href="${pageContext.request.contextPath}/${user.role.name}/posts"
                                                    class="btn btn-primary btn-hover-dark">Clear</a>
                                            </div>
                                            <div class="tab-btn">
                                                <a href="${pageContext.request.contextPath}/${user.role.name}/post/add"
                                                    class="btn btn-primary btn-hover-dark">New Course</a>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- Marketing Posts Tab End -->

                                    <!-- Marketing Posts Tab Content Start -->
                                    <div class="marketing-posts-tab-content" style="    margin-top: 10px;">
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="tab1">

                                                <!-- Posts Item Start -->

                                                <!-- Posts Item End -->

                                                <%-- <div id="paggerClick" class="container">
                                            </div>
                                        </div> --%>
                                        <c:forEach items="${posts}" var="post">
                                            <div class="posts-item">
                                                <div class="item-thumb">
                                                    <img src="${post.thumbnail}" alt="POST">
                                                </div>


                                                <div class="content-title">
                                                    <div class="meta">
                                                        <c:choose>
                                                            <c:when test="${post.status}">
                                                                <a href="#" class="action">Active</a>
                                                            </c:when>
                                                            <c:when test="${!post.status}">
                                                                <a href="#" class="action">Inactive</a>
                                                            </c:when>
                                                        </c:choose>
                                                    </div>
                                                    <h3 class="title"><a
                                                            href="${pageContext.request.contextPath}/common/blog?id=${post.id}">${post.title}</a>
                                                    </h3>
                                                </div>

                                                <div class="content-wrapper">

                                                    <div class="content-box">
                                                        <p>Category</p>
                                                        <span class="count">${post.category.name}</span>
                                                    </div>

                                                    <div class="content-box">
                                                        <p>Owner</p>
                                                        <span class="count">${post.user.name}</span>
                                                    </div>

                                                    <div class="content-box">
                                                        <p>Date</p>
                                                        <span class="count">${post.getTimeAgo()}</span>
                                                    </div>
                                                    <div class="tab-btn">
                                                        <a href="${pageContext.request.contextPath}/${user.role.name}/post/edit?pId=${post.id}"
                                                            class="btn btn-primary btn-hover-dark">Edit </a>
                                                    </div>


                                                </div>
                                            </div>
                                        </c:forEach>

                                    </div>

                                </div>

                            </div>

                            <!-- Marketing Posts Tab Content End -->
                            <div class="page-pagination">



                                <ul class="pagination justify-content-center">


                                    <c:if test="${pageindex - 2 > 1}">
                                        <li class=""><a data="1" class="pagination-link">First</a></li>
                                    </c:if>
                                    <c:if test="${pageindex - 2 >= 0}">
                                        <c:forEach begin="${pageindex - 2}" end="${pageindex - 1}" var="i">
                                            <c:if test="${i >= 1}">
                                                <li class=""><a data="${i}" class="pagination-link">${i}</a></li>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                    <li class=""><a class="pagination-link"
                                            style="background-color: #309255;color: white;"
                                            data="${pageindex}">${pageindex}</a></li>
                                    <c:forEach begin="${pageindex +1}" end="${pageindex + 2}" var="i">
                                        <c:if test="${i <= totalpage}">
                                            <li class=""><a data="${i}" class="pagination-link">${i}</a></li>
                                        </c:if>
                                    </c:forEach>
                                    <c:if test="${pageindex + 2 < totalpage}">
                                        <li class=""><a data="${totalpage}" class="pagination-link">Last</a></li>
                                    </c:if>
                                </ul>

                            </div>

                        </div>
                    </div>
                    <!-- Page Content Wrapper End -->

                    </div>
                    <!-- Posts Marketing End -->
                    <jsp:include page="../component/common/footer.jsp" />

                    <!--Back To Start-->
                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                    </div>






                    <!-- JS ============================================ -->
                    <jsp:include page="../component/common/js.jsp" />
                    <script>
                        const url_string = window.location.href;
                        const url = new URL(url_string);
                        const search = url.searchParams.get("keyWord");
                        const paginationLinks = document.querySelectorAll(".pagination-link");
                        if (paginationLinks) {
                            paginationLinks.forEach(item => {
                                var search = location.search.substring(1);
                                const params = search ? JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"')
                                    .replace(/&/g, '","').replace(/=/g, '":"') + '"}') : {};
                                const page = item.getAttribute("data");
                                params.page = page;
                                const href = decodeURIComponent(new URLSearchParams(params).toString());
                                item.setAttribute("href", "?" + href);
                            })
                        }
                    </script>

                </body>

                </html>