<%-- 
    Document   : component_nav
    Created on : Jul 4, 2022, 9:28:10 PM
    Author     : dclon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<div class="nav flex-column nav-pills admin-tab-menu">
    <a href="${pageContext.request.contextPath}/${user.role.name}/dashboard/overview">Overview</a>
    <a href="${pageContext.request.contextPath}/${user.role.name}/dashboard/customer">Customer’s</a>
    <a href="${pageContext.request.contextPath}/${user.role.name}/dashboard/subject">Subject’s</a>
    <a href="${pageContext.request.contextPath}/${user.role.name}/dashboard/trend">Trend’s</a>
</div>