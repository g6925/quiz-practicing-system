<%-- Document : dashboard Created on : Jun 23, 2022, 10:54:15 PM Author : dclon --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

            <!DOCTYPE html>
            <html>
            <jsp:include page="../../component/common/head.jsp" />

            <body>
                <div class="main-wrapper main-wrapper-02">

                    <!-- Login Header Start -->
                    <jsp:include page="../../component/administration/header.jsp" />
                    <!-- Login Header End -->

                    <!-- Courses Admin Start -->
                    <div class="section overflow-hidden position-relative" id="wrapper">

                        <!-- Sidebar Wrapper Start -->
                        <jsp:include page="../../component/administration/menu.jsp" />
                        <!-- Sidebar Wrapper End -->

                        <div class="page-content-wrapper py-0">

                            <!-- Admin Tab Menu Start -->
                            <jsp:include page="component_nav.jsp" />
                            <!-- Admin Tab Menu End -->
                            
                            <form action="./customer" method="GET">
                                <!-- Page Content Wrapper Start -->
                                <div class=" main-content-wrapper">
                                    <!-- Customers Top Start -->
                                    <div class="container-fluid">
                                        <!-- Student Top Start -->
                                        <div class="admin-top-bar students-top">
                                            <div class="courses-select">
                                                <select onchange="this.form.submit()" name="category">
                                                    <option ${categoryID==0 ? "selected" : "" } value="0">All Courses
                                                    </option>
                                                    <c:forEach items="${categories}" var="c">
                                                        <option ${categoryID==c.id ? "selected" : "" } value="${c.id}">
                                                            ${c.name}</option>
                                                    </c:forEach>
                                                </select>

                                                <h4 class="title">Meet people taking your courses</h4>
                                            </div>

                                            <div class="student-box" style="width: fit-content">
                                                <h5 class="title">Total Enrollment's</h5>
                                                <div class="count">${rd.getAllEnrollment(categoryID).size()}</div>
                                            </div>
                                        </div>
                                        <!-- Student Top End -->

                                        <!-- Student's Wrapper Start -->
                                        <div class="students-wrapper students-active">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <c:forEach var="r" items="${rd.getAllEnrollment(categoryID)}">
                                                        <div class="swiper-slide">
                                                            <!-- Single Student Start -->
                                                            <div class="single-student">
                                                                <div class="student-images">
                                                                    <img src="${r.user.image}" alt="Student">
                                                                </div>
                                                                <div class="student-content">
                                                                    <h5 class="name">${r.user.name}</h5>
                                                                    <span class="country">${r.user.phone}</span>
                                                                    <p>${r.subject.title}</p>
                                                                    <span class="date"><i
                                                                            class="icofont-ui-calendar"></i>
                                                                        ${r.getDateStartAgo()}</span>
                                                                </div>
                                                            </div>
                                                            <!-- Single Student End -->
                                                        </div>
                                                    </c:forEach>
                                                </div>

                                                <div class="students-arrow">
                                                    <!-- Add Pagination -->
                                                    <div class="swiper-button-prev"><i class="icofont-rounded-left"></i>
                                                    </div>
                                                    <div class="swiper-button-next"><i
                                                            class="icofont-rounded-right"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Student's Wrapper End -->

                                        <!-- Student's Map Start -->
                                        <div class="students-map">
                                            <h4 class="title">Student's locations and languages.</h4>

                                            <div class="map">
                                                <div id="vmap"></div>
                                            </div>
                                        </div>
                                        <!-- Student's Map End -->


                                    </div>
                                    <!-- Customers Top End -->

                                </div>
                                <!-- Page Content Wrapper End -->
                            </form>
                        </div>

                    </div>
                    <!-- Courses Admin End -->

                    <!-- Footer Start  -->
                    <jsp:include page="../../component/common/footer.jsp" />
                    <!-- Footer End -->

                    <!--Back To Start-->
                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->

                </div>

                <!-- JS
============================================ -->

                <!-- Modernizer & jQuery JS -->
                <jsp:include page="../../component/common/js.jsp" />
                <script src="${pageContext.request.contextPath}/assets/js/student-map.js"></script>
            </body>

            </html>