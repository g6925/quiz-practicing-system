<%-- Document : dashboard Created on : Jun 23, 2022, 10:54:15 PM Author : dclon --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <jsp:include page="../../component/common/head.jsp" />
    <style>
        .courses-tabs-menu {
            padding: 20px 100px;
            margin-top: 20px;
        }
    </style>

    <body>
        <div class="main-wrapper main-wrapper-02">

            <!-- Login Header Start -->
            <jsp:include page="../../component/administration/header.jsp" />
            <!-- Login Header End -->

            <!-- Courses Admin Start -->
            <div class="section overflow-hidden position-relative" id="wrapper">

                <!-- Sidebar Wrapper Start -->
                <jsp:include page="../../component/administration/menu.jsp" />
                <!-- Sidebar Wrapper End -->

                <div class="page-content-wrapper py-0">

                    <!-- Admin Tab Menu Start -->
                    <jsp:include page="component_nav.jsp" />
                    <!-- Admin Tab Menu End -->

                    <form action="./subject" method="GET">
                        <!-- Page Content Wrapper Start -->
                        <div class="main-content-wrapper">
                            <!-- Subjects Top Start -->
                            <div class="container-fluid">

                                <!-- Engagement Top Start -->
                                <div class="admin-top-bar">
                                    <div class="courses-select">
                                        <select onchange="this.form.submit()" name="state">
                                            <option ${state=="all" ? "selected" : "" } value="all">All Courses
                                            </option>
                                            <option ${state=="new" ? "selected" : "" } value="new">New Courses
                                            </option>
                                        </select>
                                    </div>

                                    <div class="engagement-meta">
                                        <p class="meta"><img
                                                src="${pageContext.request.contextPath}/assets/images/icon/icon-1.png"
                                                alt="Icon"> ${ld.findAllWithState(state).size()} Lesson</p>
                                        <p class="meta"><img
                                                src="${pageContext.request.contextPath}/assets/images/icon/icon-2.png"
                                                alt="Icon"> ${rd.getTotalCustomerWithState(state)} Active
                                            Students</p>
                                    </div>
                                </div>
                                <!-- Engagement Top End -->

                                <!-- Student's Widget Start -->
                                <div class="students-widget">
                                    <div class="row">
                                        <div class="col">
                                            <!-- Student's Widget Start -->
                                            <div class="single-student-widget widget-color-02">
                                                <h4 class="widget-title">Additional Student Interests</h4>

                                                <div class="widget-items">
                                                    <c:set var="subjects"
                                                           value="${sd.getTopEnrollmentWithStateSubjects(6, state)}" />
                                                    <c:forEach var="i" begin="0" end="${subjects.size() - 1}">
                                                        <!-- Single Item Start -->
                                                        <div class="single-item">
                                                            <div class="title">${i + 1}. ${subjects[i].title}
                                                            </div>
                                                            <div class="item-bar">
                                                                <div class="bar-line"
                                                                     style="width: ${Math.round((subjects[i].numbersOfEnrollments / rd.getTotalEnrollment(0)) * 100)}%;">
                                                                </div>
                                                            </div>
                                                            <div class="item-percentage">
                                                                <p><span>${Math.round((subjects[i].numbersOfEnrollments
                                                                           / rd.getTotalEnrollment(0)) *
                                                                           100)}%</span>
                                                                    (${subjects[i].numbersOfEnrollments})</p>
                                                            </div>
                                                        </div>
                                                        <!-- Single Item End -->
                                                    </c:forEach>
                                                </div>
                                            </div>
                                            <!-- Student's Widget End -->
                                        </div>
                                    </div>
                                </div>
                                <!-- Student's Widget End -->

                                <!-- Engagement Courses Start -->
                                <div class="engagement-courses">

                                    <div class="courses-top">
                                        <ul>
                                            <li>All Course’s</li>
                                            <li>Category</li>
                                            <li>Total Student</li>
                                        </ul>
                                    </div>

                                    <div class="tab-content courses-list">
                                        <c:forEach var="i" begin="0" end="${(subjects.size() + 1)/2 - 1}">
                                            <ul class="tab-pane fade ${i == 0 ? "show active" : ""}"
                                                id="tab_subject-${i}">
                                                <c:forEach var="j" begin="${i*2}" end="${i*2 + 1}">
                                                    <c:if test="${subjects[j] != null}">
                                                        <li>
                                                            <div class="courses">
                                                                <div class="thumb">
                                                                    <img src="${subjects[j].thumbnail}"
                                                                         alt="Courses">
                                                                </div>
                                                                <div class="content">
                                                                    <h4 class="title"><a
                                                                            href="#">${subjects[j].title}</a>
                                                                    </h4>
                                                                </div>
                                                            </div>
                                                            <div class="taught">
                                                                <span>${subjects[j].category.name}</span>
                                                            </div>
                                                            <div class="student">
                                                                <span>${subjects[j].numbersOfEnrollments}</span>
                                                            </div>
                                                            <div class="button">
                                                                <a class="btn" href="#">View Details</a>
                                                            </div>
                                                        </li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </c:forEach>
                                    </div>
                                    <div class="courses-tabs-menu page-pagination">
                                        <ul class="pagination nav justify-content-center">
                                            <c:forEach var="i" begin="0" end="${(subjects.size() + 1)/2 - 1}">
                                                <li><button class="${i == 0 ? " active" : "" }"
                                                            data-bs-toggle="tab"
                                                            data-bs-target="#tab_subject-${i}">${i + 1}</button></li>
                                                </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Engagement Courses End -->

                            </div>
                            <!-- Subjects Top End -->
                        </div>
                        <!-- Page Content Wrapper End -->
                    </form>
                </div>

            </div>
            <!-- Courses Admin End -->

            <!-- Footer Start  -->
            <jsp:include page="../../component/common/footer.jsp" />
            <!-- Footer End -->

            <!--Back To Start-->
            <a href="#" class="back-to-top">
                <i class="icofont-simple-up"></i>
            </a>
            <!--Back To End-->

        </div>

        <!-- JS
============================================ -->

        <!-- Modernizer & jQuery JS -->
        <jsp:include page="../../component/common/js.jsp" />
        <script src="${pageContext.request.contextPath}/assets/js/engagement.js"></script>
    </body>

</html>