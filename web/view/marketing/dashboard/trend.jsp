<%-- Document : dashboard Created on : Jun 23, 2022, 10:54:15 PM Author : dclon --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <jsp:include page="../../component/common/head.jsp" />
    <style>
        .courses-tabs-menu {
            padding: 20px 100px;
            margin-top: 20px;
        }
    </style>

    <body>
        <div class="main-wrapper main-wrapper-02">

            <!-- Login Header Start -->
            <jsp:include page="../../component/administration/header.jsp" />
            <!-- Login Header End -->

            <!-- Courses Admin Start -->
            <div class="section overflow-hidden position-relative" id="wrapper">
                <div id="data" style="display: none">${dataCharts}</div>
                <!-- Sidebar Wrapper Start -->
                <jsp:include page="../../component/administration/menu.jsp" />
                <!-- Sidebar Wrapper End -->

                <div class="page-content-wrapper py-0">

                    <!-- Admin Tab Menu Start -->
                    <jsp:include page="component_nav.jsp" />
                    <!-- Admin Tab Menu End -->

                    <form action="./trend" method="GET">
                        <!-- Page Content Wrapper Start -->
                        <div class="main-content-wrapper">
                            <!-- Trends Top Start -->
                            <div class="container-fluid">
                                <!-- Engagement Top Start -->
                                <div class="admin-top-bar">
                                    <div class="months-select">
                                        <select class="mb-4" onchange="this.form.submit()" name="duration">
                                            <option ${duration=="12 months" ? "selected" : "" }
                                                value="12 months">Last 12 months</option>
                                            <option ${duration=="6 months" ? "selected" : "" } value="6 months">
                                                Last 6 months</option>
                                            <option ${duration=="2 months" ? "selected" : "" } value="2 months">
                                                Last 2 months</option>
                                            <option ${duration=="1 month" ? "selected" : "" } value="1 month">
                                                Last 1 month</option>
                                            <option ${duration=="1 week" ? "selected" : "" } value="1 week">Last
                                                1 week</option>
                                        </select>

                                        <h4 class="title">Meet people taking your courses</h4>
                                    </div>
                                </div>
                                <!-- Engagement Top End -->

                                <!-- Student's Widget Start -->
                                <div class="students-widget">
                                    <div class="row">
                                        <div class="overview-box d-flex flex-xl-column col-xl-4"
                                             style="margin-left: 30px;">
                                            <div class="single-box" style="width: fit-content">
                                                <h5 class="title">Total Revenue</h5>
                                                <div class="count">$${rd.getTotalRevenueWithSinceTimeAgo(time)}
                                                </div>
                                                <p>During <span>${duration == null ? "12 months" :
                                                                  duration}</span> Ago</p>
                                            </div>

                                            <div class="single-box" style="width: fit-content">
                                                <h5 class="title">Total Enrollment’s</h5>
                                                <div class="count">
                                                    ${rd.getTotalEnrollmentWithSinceTimeAgo(time)}</div>
                                                <p>During <span>${duration == null ? "12 months" :
                                                                  duration}</span> Ago</p>
                                            </div>

                                            <div class="single-box" style="width: fit-content">
                                                <h5 class="title">Total Courses</h5>
                                                <div class="count">${sd.getTotalSubjectsWithSinceTimeAgo(time)}
                                                </div>
                                                <p>During <span>${duration == null ? "12 months" :
                                                                  duration}</span> Ago</p>
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="graph-content">
                                                <div id="trends-category"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Student's Widget End -->

                                <!-- Engagement Courses Start -->
                                <div class="engagement-courses">

                                    <div class="courses-top">
                                        <ul>
                                            <li>All Course’s</li>
                                            <li>Category</li>
                                            <li>Total Student</li>
                                        </ul>
                                    </div>

                                    <c:set var="subjects"
                                           value="${sd.getTopEnrollmentWithTimeAgoSubjects(10, time)}" />
                                    <div class="tab-content courses-list">
                                        <c:forEach var="i" begin="0" end="${(subjects.size() + 1)/2 - 1}">
                                            <ul class="tab-pane fade ${i == 0 ? "show active" : ""}"
                                                id="tab_subject-${i}">
                                                <c:forEach var="j" begin="${i*2}" end="${i*2 + 1}">
                                                    <c:if test="${subjects[j] != null}">
                                                        <li>
                                                            <div class="courses">
                                                                <div class="thumb">
                                                                    <img src="${subjects[j].thumbnail}"
                                                                         alt="Courses">
                                                                </div>
                                                                <div class="content">
                                                                    <h4 class="title"><a
                                                                            href="#">${subjects[j].title}</a>
                                                                    </h4>
                                                                </div>
                                                            </div>
                                                            <div class="taught">
                                                                <span>${subjects[j].category.name}</span>
                                                            </div>
                                                            <div class="student">
                                                                <span>${subjects[j].numbersOfEnrollments}</span>
                                                            </div>
                                                            <div class="button">
                                                                <a class="btn" href="#">View Details</a>
                                                            </div>
                                                        </li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </c:forEach>
                                    </div>
                                    <div class="courses-tabs-menu page-pagination">
                                        <ul class="pagination nav justify-content-center">
                                            <c:forEach var="i" begin="0" end="${(subjects.size() + 1)/2 - 1}">
                                                <li><button class="${i == 0 ? " active" : "" }"
                                                            data-bs-toggle="tab"
                                                            data-bs-target="#tab_subject-${i}">${i + 1}</button></li>
                                                </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Engagement Courses End -->

                            </div>
                            <!-- Trends Top End -->
                        </div>
                        <!-- Page Content Wrapper End -->
                    </form>
                </div>

            </div>
            <!-- Courses Admin End -->

            <!-- Footer Start  -->
            <jsp:include page="../../component/common/footer.jsp" />
            <!-- Footer End -->

            <!--Back To Start-->
            <a href="#" class="back-to-top">
                <i class="icofont-simple-up"></i>
            </a>
            <!--Back To End-->

        </div>

        <!-- JS
============================================ -->

        <!-- Modernizer & jQuery JS -->
        <jsp:include page="../../component/common/js.jsp" />
        <script src="${pageContext.request.contextPath}/assets/js/trends.js"></script>
    </body>

</html>