<%-- Document : sliderlist Created on : Jun 12, 2022, 4:16:04 PM Author : taina --%>

    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@page contentType="text/html" pageEncoding="UTF-8" %>
            <!DOCTYPE html>
            <html lang="en">

            <jsp:include page="../component/common/head.jsp" />
            <style>
                .search-filter {
                    width: 660px;
                    display: flex;
                    align-items: center;
                    justify-content: flex-end;
                }

                .search-input {
                    margin-right: 12px;
                    padding-left: 10px;
                    display: flex;
                    justify-content: space-around;
                    height: 50px;
                    border-radius: 10px;
                    border: 1px solid rgba(48, 146, 85, 0.2);
                }

                .search-button {
                    color: #fff;
                    line-height: 50px;
                    font-size: 15px;
                    padding: 0 30px;
                    border-radius: 10px;
                    border: 0 solid transparent;
                    background-color: #309255;
                    border-color: #309255;
                    font-weight: 500;
                    margin-right: 20px;
                }

                /*                    .item-thumb img{
                        
                    }*/
            </style>

            <body>
                <div class="main-wrapper main-wrapper-02">
                    <jsp:include page="../component/administration/header.jsp" />
                    <!-- Courses Admin Start -->
                    <div class="section overflow-hidden position-relative" id="wrapper">
                        <!-- Sidebar Wrapper Start -->
                        <jsp:include page="../component/administration/menu.jsp" />
                        <!-- Sidebar Wrapper End -->
                        <!-- Page Content Wrapper Start -->
                        <div class="page-content-wrapper">
                            <div class="container-fluid custom-container">
                                <!-- Admin Courses Tab Start -->
                                <div class="admin-courses-tab">
                                    <h3 class="title">Sliders</h3>
                                    <div class="courses-tab-wrapper">
                                        <form action="./sliders" method="GET">
                                            <div class="search-filter">
                                                <div class="tab-btn" style="display: flex">
                                                    <input type="text" class="search-input" placeholder="Search here"
                                                        name="keyWord" value="${keyWord}">
                                                    <input class="search-button" type="submit" value="Search" />
                                                </div>
                                                <div class="courses-select">
                                                    <select name="type">
                                                        <c:set value="${type}" var="t" />
                                                        <option disabled selected value> Sort by Type
                                                        </option>
                                                        <option <c:if test="${t==1}">
                                                            selected="selected"
                                                            </c:if>
                                                            value="1">Search By Title</option>
                                                        <option <c:if test="${t==0}">
                                                            selected="selected"
                                                            </c:if>
                                                            value="0">Search By Backlink</option>
                                                    </select>
                                                </div>
                                                <div class="courses-select" style="margin-left: 20px">
                                                    <select onchange="this.form.submit()" name="status">
                                                        <option disabled selected value> Sort by Status
                                                        </option>
                                                        <option <c:if test="${stt==1}">
                                                            selected="selected"
                                                            </c:if>
                                                            value="1">Active</option>
                                                        <option <c:if test="${stt==0}">
                                                            selected="selected"
                                                            </c:if>
                                                            value="0">Inactive</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="tab-btn">
                                            <a href="${pageContext.request.contextPath}/marketing/sliders"
                                                class="btn btn-primary btn-hover-dark">Clear</a>
                                        </div>
                                        <div class="tab-btn">
                                            <a href="${pageContext.request.contextPath}/marketing/slider/add"
                                                class="btn btn-primary btn-hover-dark">New Slider</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Admin Courses Tab End -->
                                <c:if test="${empty slider}">
                                    <div class="admin-courses-tab-content">
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="tab1">
                                                <div class="courses-item">
                                                    <div class="content-title"
                                                        style="display: flex; justify-content: center; align-items: center; height: 100vh; margin: 0">
                                                        <h3 class="title"><a
                                                                style="text-align: center; font-size: 140%; white-space: nowrap; margin-left: 800px;">There
                                                                is nothing that matched your search</a></h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>

                                <!-- Admin Courses Tab Content Start -->
                                <div class="admin-courses-tab-content">
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="tab1">
                                            <!-- Courses Item Start -->
                                            <c:forEach items="${slider}" var="s">
                                                <div class="courses-item">
                                                    <div class="item-thumb">
                                                        <img src="${s.image}" alt="Slider Image">
                                                    </div>

                                                    <div class="content-title">
                                                        <div class="meta">
                                                            <c:if test="${s.status}">
                                                                <a href="#" class="action">Active</a>
                                                            </c:if>
                                                            <c:if test="${!s.status}">
                                                                <a href="#" class="action">Inactive</a>
                                                            </c:if>
                                                        </div>
                                                        <h3 class="title"><a
                                                                href="${pageContext.request.contextPath}/marketing/slider?id=${s.id}">${s.title}</a>
                                                        </h3>
                                                    </div>

                                                    <div class="content-wrapper">

                                                        <div class="content-box" style="width: 270px">
                                                            <p>Backlink</p>
                                                            <span class="count"><a
                                                                    href="${s.backlink}">${s.backlink}</a></span>
                                                        </div>
                                                        <div class="content-box" style="width: 300px">
                                                            <p>Note</p>
                                                            <span class="count">${s.content}</span>
                                                        </div>
                                                        <div class="tab-btn">
                                                            <a href="${pageContext.request.contextPath}/marketing/slider/edit?id=${s.id}"
                                                                class="btn btn-primary btn-hover-dark">Edit</a>
                                                        </div>
                                                        <div class="tab-btn">
                                                            <c:if test="${s.status==true}"><a
                                                                    href="${pageContext.request.contextPath}/marketing/slider/status?id=${s.id}"
                                                                    class="btn btn-primary btn-hover-dark">Deactive</a>
                                                            </c:if>
                                                            <c:if test="${s.status==false}"><a
                                                                    href="${pageContext.request.contextPath}/marketing/slider/status?id=${s.id}"
                                                                    class="btn btn-primary btn-hover-dark">Active</a>
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                            <!-- Courses Item End -->
                                        </div>
                                    </div>
                                </div>
                                <!-- Admin Courses Tab Content End -->
                                <!-- Courses Resources End -->
                                <div class="page-pagination">
                                    <ul class="pagination justify-content-center">
                                        <c:set var="page" value="${page}" />
                                        <c:set var="num" value="${num}" />
                                        <c:if test="${page > 1}">
                                            <li class=""><a data="1" class="pagination-link">First</a></li>
                                        </c:if>
                                        <c:if test="${page >= 0}">
                                            <c:forEach begin="${1}" end="${num}" var="i">
                                                <li class=""><a data="${i}" class="pagination-link ${i==page?"
                                                        active":""}">${i}</a></li>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${page < num}">
                                            <li class=""><a data="${num}" class="pagination-link">Last</a></li>
                                        </c:if>
                                    </ul>
                                </div>
                                <jsp:include page="../component/administration/courses-resources.jsp" />
                            </div>
                        </div>
                        <!-- Page Content Wrapper End -->

                        <!-- Courses Admin End -->
                    </div>
                    <jsp:include page="../component/common/footer.jsp" />
                    <!--Back To Start-->
                    <a href="#" class="back-to-top">
                        <i class="icofont-simple-up"></i>
                    </a>
                    <!--Back To End-->
                </div>
                <jsp:include page="../component/common/js.jsp" />
                <script>
                    const url_string = window.location.href;
                    const url = new URL(url_string);
                    const search = url.searchParams.get("keyWord");
                    const paginationLinks = document.querySelectorAll(".pagination-link");
                    if (paginationLinks) {
                        paginationLinks.forEach(item => {
                            var search = location.search.substring(1);
                            const params = search ? JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"')
                                .replace(/&/g, '","').replace(/=/g, '":"') + '"}') : {};
                            const page = item.getAttribute("data");
                            params.page = page;
                            const href = decodeURIComponent(new URLSearchParams(params).toString());
                            item.setAttribute("href", "?" + href);
                        });
                    }
                </script>
            </body>

            </html>