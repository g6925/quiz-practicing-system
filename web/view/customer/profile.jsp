<%-- Document : profile Created on : May 30, 2022, 9:40:01 AM Author : Duong-PC --%>

    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@page contentType="text/html" pageEncoding="UTF-8" %>
            <!DOCTYPE html>
            <html>
            <jsp:include page="../component/common/head.jsp" />

            <body>

                <div class="main-wrapper">
                    <jsp:include page="../component/common/header.jsp" />
                    <!-- Overlay Start -->
                    <div class="overlay"></div>
                    <!-- Overlay End -->

                    <!-- Page Banner Start -->
                    <div class="section page-banner">

                        <img class="shape-1 animation-round"
                            src="${pageContext.request.contextPath}/assets/images/shape/shape-8.png" alt="Shape">

                        <img class="shape-2" src="${pageContext.request.contextPath}/assets/images/shape/shape-23.png"
                            alt="Shape">

                        <div class="container">
                            <!-- Page Banner Start -->
                            <div class="page-banner-content">
                                <ul class="breadcrumb">
                                    <li><a href="#">Home</a></li>
                                    <li class="active">User Profile</li>
                                </ul>
                                <h2 class="title">User Profile</h2>
                            </div>
                            <!-- Page Banner End -->
                        </div>

                        <!-- Shape Icon Box Start -->
                        <div class="shape-icon-box">

                            <img class="icon-shape-1 animation-left"
                                src="${pageContext.request.contextPath}/assets/images/shape/shape-5.png" alt="Shape">

                            <div class="box-content">
                                <div class="box-wrapper">
                                    <i class="flaticon-badge"></i>
                                </div>
                            </div>

                            <img class="icon-shape-2"
                                src="${pageContext.request.contextPath}/assets/images/shape/shape-6.png" alt="Shape">

                        </div>
                        <!-- Shape Icon Box End -->

                        <img class="shape-3" src="${pageContext.request.contextPath}/assets/images/shape/shape-24.png"
                            alt="Shape">

                        <img class="shape-author"
                            src="${pageContext.request.contextPath}/assets/images/author/author-11.jpg" alt="Shape">

                    </div>
                    <!-- Page Banner End -->
                    <div class="section section-padding">
                        <div class="container">

                            <!-- Register & Login Wrapper Start -->
                            <div class="register-login-wrapper">
                                <div class="row align-items-center">
                                    <c:if test="${action == null}">
                                        <section>
                                            <div class="text-center"> ${message}</div>
                                            <div class="container rounded">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div
                                                            class="d-flex flex-column align-items-center text-center p-3 py-5">
                                                            <img class="rounded-circle mt-5" width="150px"
                                                                src="${user.getImage() !=null ? user.getImage() : 'https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg'}"><span
                                                                class="font-weight-bold">Ảnh Profile</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="p-3 py-5">
                                                            <div class="row mt-2">
                                                                <div class="col-md-6 mt-2"><label class="labels">Full
                                                                        Name</label>
                                                                    <h5>${user.getName()}</h5>
                                                                </div>
                                                                <div class="col-md-6 mt-2"><label
                                                                        class="labels">Gmail</label>
                                                                    <h5>${user.getEmail()}</h5>
                                                                </div>
                                                                <div class="col-md-6 mt-2"><label
                                                                        class="labels">Gender</label>
                                                                    <h5>${user.getGender() ? 'Nam' : 'Nữ'}</h5>
                                                                </div>
                                                                <div class="col-md-6 mt-2"><label
                                                                        class="labels">Role</label>
                                                                    <h5>${user.getRole().getName()}</h5>
                                                                </div>
                                                                <div class="col-md-6 mt-2"><label
                                                                        class="labels">Phone</label>
                                                                    <h5>${user.getPhone()}</h5>
                                                                </div>
                                                            </div>
                                                            <div class="mt-5">
                                                                <a class="btn btn-primary profile-button"
                                                                    href="profile?action=edit">Edit Profile</a>
                                                                <a class="btn btn-primary profile-button"
                                                                    href="${pageContext.request.contextPath}/common/user/change-password">Change
                                                                    Password</a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </c:if>
                                    <c:if test="${action eq 'edit'}">
                                        <form method="post" action="./profile?action=edit">
                                            <div class="container rounded">
                                                <div class="row" style="background-color: #E6E6EA; border-radius:20px ">
                                                    <div class="col-md-4">
                                                        <div
                                                            class="d-flex flex-column align-items-center text-center p-3 py-5">
                                                            <img class="rounded-circle mt-5" width="150px"
                                                                src="${user.getImage() !=null ? user.getImage() : 'https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg'}">
                                                            <span class="font-weight-bold">Profile Image</span>
                                                            <span><input type="text" name="image"
                                                                    value="${user.getImage() !=null ? user.getImage() : 'https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg'}"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="p-3 py-5">
                                                            <div
                                                                class="d-flex justify-content-between align-items-center mb-3">
                                                                <h4 class="text-right">User Profile</h4>
                                                            </div>
                                                            <div class="row mt-2">
                                                                <div class="col-md-6 mt-2"><label class="labels">Full
                                                                        Name</label><input type="text" name="fullname"
                                                                        class="form-control" value="${user.getName()}">
                                                                </div>
                                                                <div class="col-md-6 mt-2"><label
                                                                        class="labels">Gmail</label><input type="text"
                                                                        disabled class="form-control"
                                                                        value="${user.getEmail()}"></div>
                                                                <div class="col-md-6 mt-2"><label
                                                                        class="labels">Role</label><input type="text"
                                                                        disabled class="form-control"
                                                                        value="${user.getRole().getName()}"></div>
                                                                <div class="col-md-6 mt-2">
                                                                    <label class="labels">Gender</label><br>
                                                                    <div class="btn-group" role="group"
                                                                        aria-label="Basic radio toggle button group">
                                                                        <input type="radio" class="btn-check"
                                                                            name="gender" value="1" id="btnradio1"
                                                                            autocomplete="off" ${user.getGender()
                                                                            ? 'checked' : '' }>
                                                                        <label class="btn btn-outline-secondary"
                                                                            for="btnradio1">Male</label>

                                                                        <input type="radio" class="btn-check"
                                                                            name="gender" value="0" id="btnradio2"
                                                                            autocomplete="off" ${!user.getGender()
                                                                            ? 'checked' : '' }>
                                                                        <label class="btn btn-outline-secondary"
                                                                            for="btnradio2">Female</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 mt-2"><label
                                                                        class="labels">Phone</label><input type="text"
                                                                        class="form-control" name="phone"
                                                                        value="${user.getPhone()}"></div>
                                                                <div class="mt-5"><button
                                                                        class="btn btn-secondary profile-button"
                                                                        type="submit">Save Profile</button> <span
                                                                        class="font-weight-bold ml-5 text-success">${message}</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </c:if>
                                    <!--                          
                </div>
            </div>
                            <!-- Register & Login Wrapper End -->

                                </div>
                            </div>
                            <a href="#" class="back-to-top">
                                <i class="icofont-simple-up"></i>
                            </a>
                            <!--Back To End-->

                        </div>
                    </div>
                    <jsp:include page="../component/common/footer.jsp" />
                    <jsp:include page="../component/common/js.jsp" />
            </body>

            </html>