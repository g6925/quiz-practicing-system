<!------ Include the above in your HEAD tag ---------->

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <jsp:include page="../component/common/head.jsp" />
    <style>
        .form-group {
            display: flex;
            align-items: center;
            font-size: 20px;
            justify-content: center;
        }

        .input_fields_wrap {
            text-align: center;
            align-items: center;
            justify-content: center;
        }

        .control-label {
            width: 340px;
        }

        #featured {
            width: 20px;
            height: 20px;
        }

        .form-horizontal {
            color: rgb(33 40 50);
            font-weight: 500;
        }

        .error {
            color: red;
            font-size: 15px;
        }

        .single-form label {
            color: red;
            font-size: 15px;
        }

        td, th {
            border: 2px solid black;
            /* border-width border-style border-color */
        }
    </style>

    <body>

        <div class="main-wrapper main-wrapper-02">
            <jsp:include page="../component/administration/header.jsp" />
            <!-- Courses Admin Start -->
            <div class="section overflow-hidden position-relative" id="wrapper">

                <!-- Sidebar Wrapper Start -->
                <jsp:include page="../component/administration/menu.jsp" />
                <!-- Sidebar Wrapper End -->

                <!-- Page Content Wrapper Start -->
                <div class="page-content-wrapper">
                    <div class="container-fluid custom-container">                
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-2">
                                <c:if test="${not empty message}">
                                    <div class="alert alert-${alert}">
                                        <h5 style="color: green">${message}</h5>
                                    </div>
                                </c:if>
                                <c:if test="${not empty warning}">
                                    <div class="alert alert-${alert}">
                                        <h5 style="color: red">${warning}</h5>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                        <form method="POST" action="${pageContext.request.contextPath}/editpracticedetails"
                              class="form-horizontal" id="">
                            <input hidden name="quizID" value="${q.quiz.id}">
                            <!-- Text input-->
                            <div id="overview">                               
                                <div class="form-group courses-select">
                                    <label style="font-weight: bold" class="col-md-4 control-label"
                                           for="subject">Subject</label>
                                    <div class="single-form col-md-4">
                                        <select name="subject" disabled>                                                                                     
                                            <option value="${q.quiz.subject.id}" selected>${q.quiz.subject.title}</option>
                                        </select>                                        
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label style="font-weight: bold" class="col-md-4 control-label"
                                           for="num">
                                        Number of practicing questions</label>
                                    <div class="single-form col-md-4">
                                        <input type="number" name="num" id="num"
                                               class="form-control input-md" value="${num}" readonly>
                                        <div id="num-error"></div>
                                    </div>
                                </div>


                                <div class="form-group courses-select">
                                    <label style="font-weight: bold" class="col-md-4 control-label"
                                           for="type">Questions are selected by topic(s) or a specific dimension?</label>
                                    <div class="single-form col-md-4">
                                        <select name="type" onchange="this.form.submit()" disabled>
                                            <option value="1" ${q.filterType=='Subject Topic' ? "selected" : ""}>By Subject Topic</option>
                                            <option value="2" ${q.filterType=='Group' ? "selected" : ""}>By Group</option>
                                            <option value="3" ${q.filterType=='Domain' ? "selected" : ""}>By Domain</option>
                                        </select>                                        
                                    </div>
                                </div>       

                                <div class="form-group courses-select">
                                    <label style="font-weight: bold" class="col-md-4 control-label"
                                           for="type">Question Group (Choose one or all Topic/Dimension)</label>
                                    <div class="single-form col-md-4">
                                        <select name="group" id="group" disabled>                                                                             
                                            <option value="${q.filterGroup}" selected>${groupName}</option>                                            
                                        </select>                                       
                                    </div>
                                </div>    
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="singlebutton"></label>
                                    <div class="single-form col-md-4">
                                        <button type="submit" onclick="" id="singlebutton" class="btn btn-primary">Practice Review
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- Courses Resources Start -->
                        <jsp:include page="../component/administration/courses-resources.jsp" />
                        <!-- Courses Resources End -->                        
                    </div>
                    <!-- Page Content Wrapper End -->
                </div>
                <!-- Courses Admin End -->

                <jsp:include page="../component/common/footer.jsp" />
                <!--Back To Start-->
                <a href="#" class="back-to-top">
                    <i class="icofont-simple-up"></i>
                </a>
                <!--Back To End-->
            </div>
            <!-- JS
    ============================================ -->
            <jsp:include page="../component/common/js.jsp" />
    </body>

</html>