// is email adress
export function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

// check phone number
export function isPhone(phone) {
  var regex = /^\d{10}$/;
  return regex.test(phone);
}

// check password
export function isPassword(password) {
  var regex =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/;
  return regex.test(password);
}

// check image file
export function isImage(value) {
  var regex = /[\/.](gif|jpg|jpeg|tiff|png)$/;
  return regex.test(value);
}

// check audio file
export function validateAudio(file) {
  var regex = /^(audio\/mp3|audio\/wav|audio\/ogg)$/;
  return regex.test(file);
}

// check if two passwords are equal
export function isEqual(password, cpassword) {
  return password === cpassword;
}

// check if the string is empty
export function isEmpty(str) {
  return !str || 0 === str.length;
}

// check if the number
export function isNumber(number) {
  return !isNaN(number);
}

// validate password
export function validatePassword(password, cpassword) {
  const flag = isEmpty(cpassword);
  console.log(flag);
  console.log(password);
  if (isEmpty(password)) {
    $("input#password").addClass("is-invalid");
    $("input#password").removeClass("is-valid");
    $("#password-error").text("Password is required");
    return false;
  } else if (!isPassword(password)) {
    $("input#password").addClass("is-invalid");
    $("input#password").removeClass("is-valid");
    $("#password-error").text(
      "Password must be at least 8 characters long and contain at least one number, one uppercase letter and one special character"
    );
    return false;
  } else if (isEmpty(cpassword)) {
    $("input#cpassword").addClass("is-invalid");
    $("input#cpassword").removeClass("is-valid");
    $("#cpassword-error").text("Password is required");
    return false;
  } else if (!isPassword(cpassword)) {
    $("input#cpassword").addClass("is-invalid");
    $("input#cpassword").removeClass("is-valid");
    $("#cpassword-error").text(
      "Password must be at least 8 characters long and contain at least one number, one uppercase letter and one special character"
    );
    return false;
  } else if (!isEqual(password, cpassword)) {
    $("input#password").addClass("is-invalid");
    $("input#password").removeClass("is-valid");
    !flag && $("input#cpassword").addClass("is-invalid");
    !flag && $("input#cpassword").removeClass("is-valid");
    $("#password-error").text("Passwords do not match");
    !flag && $("#cpassword-error").text("Passwords do not match");
    return false;
  } else {
    $("input#password").addClass("is-valid");
    $("input#password").removeClass("is-invalid");
    !flag && $("input#cpassword").addClass("is-valid");
    !flag && $("input#cpassword").removeClass("is-invalid");
    $("#password-error").text("");
    !flag && $("#cpassword-error").text("");
    return true;
  }
}

// format phone number
function formatPhone(phone) {
  var phone = phone.replace(/[^0-9]/g, "");
  var phone = phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
  return phone;
}

console.log(formatPhone("0941576972"));

// check validate phone number
export function validatePhone(name, phone) {
  if (isEmpty(phone)) {
    $("input#" + name).addClass("is-invalid");
    $("input#" + name).removeClass("is-valid");
    $("#" + name + "-error").text("Phone number is required");
    return false;
  } else if (!isPhone(phone)) {
    $("input#" + name).addClass("is-invalid");
    $("input#" + name).removeClass("is-valid");
    $("#" + name + "-error").text("Phone number must be 10 digits");
    return false;
  } else {
    $("input#" + name).addClass("is-valid");
    $("input#" + name).removeClass("is-invalid");
    $("#" + name + "-error").text("");
    return true;
  }
}

// check validate email address
export function validateEmail(name, email) {
  if (isEmpty(email)) {
    $("input#" + name).addClass("is-invalid");
    $("input#" + name).removeClass("is-valid");
    $("#" + name + "-error").text("Email is required");
    return false;
  } else if (!isEmail(email)) {
    $("input#" + name).addClass("is-invalid");
    $("input#" + name).removeClass("is-valid");
    $("#" + name + "-error").text("Email is not valid");
    return false;
  } else {
    $("input#" + name).addClass("is-valid");
    $("input#" + name).removeClass("is-invalid");
    $("#" + name + "-error").text("");
    return true;
  }
}

// check validate input empty
export function validateText(name, value) {
  if (isEmpty(value)) {
    $("#" + name).addClass("is-invalid");
    $("#" + name).removeClass("is-valid");
    $("#" + name + "-error").text("This field is required");
    return false;
  } else {
    $("#" + name).addClass("is-valid");
    $("#" + name).removeClass("is-invalid");
    $("#" + name + "-error").text("");
    return true;
  }
}

// check validate input empty
export function validateNumber(name, value) {
  if (!isNumber(value)) {
    $("#" + name).addClass("is-invalid");
    $("#" + name).removeClass("is-valid");
    $("#" + name + "-error").text("This file must be numeric");
    return false;
  } else {
    const min = $("#" + name)[0].dataset.min;
    const max = $("#" + name)[0].dataset.max;
    if ((isNaN(min) || value >= min) && (isNaN(max) || value <= max)) {
      $("#" + name).addClass("is-valid");
      $("#" + name).removeClass("is-invalid");
      $("#" + name + "-error").text("");
      return true;
    } else {
      $("#" + name).addClass("is-invalid");
      $("#" + name).removeClass("is-valid");
      if (!isNaN(min) && !isNaN(max)) {
        $("#" + name + "-error").text(
          `This number must be greater than ${min} and smaller than ${max}`
        );
      } else if (!isNaN(min)) {
        $("#" + name + "-error").text(
          `This number must be greater than ${min}`
        );
      } else {
        $("#" + name + "-error").text(
          `This number must be smaller than ${max}`
        );
      }
      return false;
    }
  }
}

// check validate textarea empty
export function validateTextarea(name, value) {
  const ckEditor = $("#" + name)
    .siblings(".ck-editor")
    .not("#" + name)[0];
  if (isEmpty(value)) {
    !ckEditor && $("#" + name).addClass("is-invalid");
    !ckEditor && $("#" + name).removeClass("is-valid");
    ckEditor && $(".ck.ck-content").addClass("is-invalid");
    ckEditor && $(".ck.ck-content").removeClass("is-valid");
    $("#" + name + "-error").text("This field is required");
    return false;
  } else {
    !ckEditor && $("#" + name).addClass("is-valid");
    !ckEditor && $("#" + name).removeClass("is-invalid");
    ckEditor && $(".ck.ck-content").addClass("is-valid");
    ckEditor && $(".ck.ck-content").removeClass("is-invalid");
    $("#" + name + "-error").text("");
    return true;
  }
}

// validate checkbox
export function validateCheckbox(name) {
  if ($("checked#" + name).is(":checked")) {
    $("checked#" + name).addClass("is-valid");
    $("checked#" + name).removeClass("is-invalid");
    $("#" + name + "-error").text("");
    return true;
  } else {
    $("checked#" + name).addClass("is-invalid");
    $("checked#" + name).removeClass("is-valid");
    $("#" + name + "-error").text("This field is required");
    return false;
  }
}

// validate select option
export function validateSelect(name) {
  if ($("select#" + name).val() === null) {
    $("select#" + name)
      .siblings(".nice-select")
      .addClass("is-invalid");
    $("select#" + name)
      .siblings(".nice-select")
      .removeClass("is-valid");
    $("#" + name + "-error").text("This field is required");
    return false;
  } else {
    $("select#" + name)
      .siblings(".nice-select")
      .addClass("is-valid");
    $("select#" + name)
      .siblings(".nice-select")
      .removeClass("is-invalid");
    $("#" + name + "-error").text("");
    return true;
  }
}

// check validate image file
export function validateImage(name, value, canEmpty) {
  if (!canEmpty && isEmpty(value)) {
    $("input#" + name).addClass("is-invalid");
    $("input#" + name).removeClass("is-valid");
    $("#" + name + "-error").text("Image is required");
    return false;
  } else if (!canEmpty && !isImage(value)) {
    $("input#" + name).addClass("is-invalid");
    $("input#" + name).removeClass("is-valid");
    $("#" + name + "-error").text("Image must be jpeg, jpg, tiff, png or gif");
    return false;
  } else {
    $("input#" + name).addClass("is-valid");
    $("input#" + name).removeClass("is-invalid");
    $("#" + name + "-error").text("");
    return true;
  }
}

// handle image preview
export function handleImagePreview(name, file) {
  if (file.files && file.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $("#" + name + "-preview").removeClass("d-none");
      $("#" + name + "-preview").attr("src", e.target.result);
    };
    reader.readAsDataURL(file.files[0]);
  }
}

// handle form.element on change
export function handleElementOnChange(form, element) {
  if (element.type === "text") {
    form.on("change", `input[name="${element.name}"]`, function () {
      element.validation && validateText(element.name, $(this).val());
    });
  } else if (element.type === "number") {
    form.on("change", `input[name="${element.name}"]`, function () {
      element.validation && validateNumber(element.name, $(this).val());
    });
  } else if (element.type === "textarea") {
    form.on("change", `textarea[name="${element.name}"]`, function () {
      element.validation && validateTextarea(element.name, $(this).val());
    });
  } else if (element.type === "email") {
    form.on("change", `input[name="${element.name}"]`, function () {
      element.validation && validateEmail(element.name, $(this).val());
    });
  } else if (element.type === "phone") {
    form.on("change", `input[name="${element.name}"]`, function () {
      element.validation && validatePhone(element.name, $(this).val());
    });
  } else if (element.type === "password") {
    form.on("change", `input[name="${element.name}"]`, function () {
      element.validation &&
        validatePassword(
          $('input[name="password"]').val(),
          $('input[name="cpassword"]').val()
        );
    });
  } else if (element.type === "checkbox") {
    form.on("change", `input[name="${element.name}"]`, function () {
      element.validation && validateCheckbox(element.name);
    });
  } else if (element.type === "select") {
    form.on("change", `select[name="${element.name}"]`, function () {
      element.validation && validateSelect(element.name);
    });
  } else if (element.type === "image") {
    form.on("change", `input[name="${element.name}"]`, function () {
      element.validation &&
        validateImage(element.name, $(this).val(), false) &&
        handleImagePreview(element.name, this);
      $(this)[0].dataset.canEmpty = "false";
    });
  } else {
  }
}

// validate form
export function validateForm(form, data) {
  var valid = true;
  data.forEach((element) => {
    var node = form.querySelector(`input[name="${element.name}"]`);
    if (element.type === "text") {
      if (element.validation && !validateText(element.name, node.value)) {
        valid = false;
      }
    } else if (element.type === "number") {
      if (element.validation && !validateNumber(element.name, node.value)) {
        valid = false;
      }
    } else if (element.type === "email") {
      if (element.validation && !validateEmail(element.name, node.value)) {
        valid = false;
      }
    } else if (element.type === "phone") {
      if (element.validation && !validatePhone(element.name, node.value)) {
        valid = false;
      }
    } else if (element.type === "password") {
      var password = $('input[name="password"]').val();
      var cpassword = $('input[name="cpassword"]').val();
      if (element.name === "password") {
        if (element.validation && !validatePassword(password, cpassword)) {
          valid = false;
        }
      }
    } else if (element.type === "checkbox") {
      if (element.validation && !validateCheckbox(element.name)) {
        valid = false;
      }
    } else if (element.type === "select") {
      if (element.validation && !validateSelect(element.name)) {
        valid = false;
      }
    } else if (element.type === "image") {
      if (
        element.validation &&
        !validateImage(
          element.name,
          node.value,
          node.dataset.canEmpty === "true"
        )
      ) {
        valid = false;
      }
    } else if (element.type === "textarea") {
      node = form.querySelector(`textarea[name="${element.name}"]`);
      if (element.validation && !validateTextarea(element.name, node.value)) {
        valid = false;
      }
    } else {
    }
  });
  return valid;
}

// handle sumbmit form
export function handleSubmit(form, data) {
  if (validateForm(form, data)) {
    form.phone.value = formatPhone(form.phone.value);
    form.submit();
  } else {
    return false;
  }
}
