(function ($) {
  "use strict";

  var data = JSON.parse($("#data")[0].innerText);
  $("#data")[0].remove();
  console.log(data);

  /*--
        Apex Charts
    -----------------------------------*/

  var options = {
    series: data.reduce(function (previousValue, currentValue) {
      previousValue.push(currentValue.value);
      return previousValue;
    }, []),
    chart: {
      width: 580,
      type: "pie",
    },
    labels: data.reduce(function (previousValue, currentValue) {
      previousValue.push(currentValue.label);
      return previousValue;
    }, []),
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200,
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  };

  var chart = new ApexCharts(
    document.querySelector("#trends-category"),
    options
  );
  chart.render();
})(jQuery);
