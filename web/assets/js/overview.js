(function ($) {
  "use strict";

  var data = JSON.parse($("#data")[0].innerText).reverse();
  $("#data")[0].remove();
  console.log(data);
  /*--
        Apex Charts
    -----------------------------------*/

  var options = {
    series: [
      {
        name: "Revenue",
        type: "line",
        data: data.reduce(function (previousValue, currentValue) {
          previousValue.push(currentValue.value);
          return previousValue;
        }, []),
      },
    ],
    chart: {
      height: 370,
      type: "line",
      stacked: false,
      toolbar: {
        show: false,
      },
    },
    stroke: {
      width: [3],
      curve: "smooth",
    },
    plotOptions: {
      bar: {
        columnWidth: "50%",
      },
    },
    markers: {
      size: 0,
    },
    colors: ["#309255"],
    xaxis: {
      categories: data.reduce(function (previousValue, currentValue) {
        previousValue.push([currentValue.label]);
        return previousValue;
      }, []),
      labels: {
        style: {
          colors: ["#52565b"],
          fontSize: "14px",
          fontFamily: "Gordita",
          fontWeight: 400,
        },
      },
    },
    yaxis: {
      tickAmount: 10,
      min: 0,
      labels: {
        style: {
          colors: ["#52565b"],
          fontSize: "14px",
          fontFamily: "Gordita",
          fontWeight: 400,
        },
      },
    },
    tooltip: {
      shared: true,
      intersect: false,
      y: {
        formatter: function (y) {
          if (typeof y !== "undefined") {
            return y.toFixed(0) + " $";
          }
          return y;
        },
      },
    },
  };

  var chart = new ApexCharts(document.querySelector("#uniqueReport"), options);
  chart.render();
})(jQuery);
