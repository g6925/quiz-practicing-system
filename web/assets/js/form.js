import * as MyValidation from "./validation.js";
(function ($) {
  const dataRegister = [
    {
      name: "name",
      type: "text",
      validation: true,
    },
    {
      name: "email",
      type: "email",
      validation: true,
    },
    {
      name: "phone",
      type: "phone",
      validation: true,
    },
    {
      name: "password",
      type: "password",
      validation: true,
    },
    {
      name: "cpassword",
      type: "password",
      validation: true,
    },
  ];

  const dataPost = [
    {
      name: "title",
      type: "text",
      validation: true,
    },
    {
      name: "category",
      type: "select",
      validation: true,
    },
    {
      name: "status",
      type: "select",
      validation: true,
    },
    {
      name: "thumbnail",
      type: "image",
      validation: true,
    },
    {
      name: "brief",
      type: "textarea",
      validation: true,
    },
    {
      name: "content",
      type: "textarea",
      validation: true,
    },
  ];

  const dataResetPassword = [
    {
      name: "password",
      type: "password",
      validation: true,
    },
    {
      name: "cpassword",
      type: "password",
      validation: true,
    },
  ];

  const dataChangePassword = [
    {
      name: "old-password",
      type: "password",
      validation: false,
    },
    {
      name: "password",
      type: "password",
      validation: true,
    },
    {
      name: "cpassword",
      type: "password",
      validation: true,
    },
  ];

  const dataForgotPassword = [
    {
      name: "user",
      type: "email",
      validation: true,
    },
  ];

  const dataLogin = [
    {
      name: "user",
      type: "email",
      validation: true,
    },
    {
      name: "password",
      type: "password",
      validation: false,
    },
  ];

  const dataSubject = [
    {
      name: "subject_name",
      type: "text",
      validation: true,
    },
    {
      name: "subject_category",
      type: "select",
      validation: true,
    },
    {
      name: "featured",
      type: "checkbox",
      validation: false,
    },

    {
      name: "tag_line",
      type: "text",
      validation: true,
    },
    {
      name: "expert",
      type: "select",
      validation: true,
    },
    {
      name: "status",
      type: "select",
      validation: true,
    },
    {
      name: "image",
      type: "image",
      validation: true,
    },
    {
      name: "subject_description",
      type: "textarea",
      validation: true,
    },
  ];

  const dataSlider = [
    {
      name: "image",
      type: "image",
      validation: true,
    },
    {
      name: "slider_name",
      type: "text",
      validation: true,
    },
    {
      name: "slider_content",
      type: "textarea",
      validation: true,
    },

    {
      name: "backlink",
      type: "text",
      validation: true,
    },
    {
      name: "status",
      type: "select",
      validation: true,
    },
  ];

  const dataDimension = [
    {
      name: "name",
      type: "text",
      validation: true,
    },
    {
      name: "type",
      type: "select",
      validation: true,
    },
    {
      name: "description",
      type: "textarea",
      validation: true,
    },
  ];

  const dataLesson = [
    {
      name: "name",
      type: "text",
      validation: true,
    },
    {
      name: "type",
      type: "select",
      validation: true,
    },
    {
      name: "topicId",
      type: "select",
      validation: true,
    },
    {
      name: "order",
      type: "number",
      validation: true,
    },
    {
      name: "video",
      type: "text",
      validation: false,
    },
    {
      name: "content",
      type: "textarea",
      validation: false,
    },
    {
      name: "status",
      type: "select",
      validation: true,
    },
  ];

  const dataQuizDetails = [
    {
      name: "name",
      type: "text",
      validation: true,
    },
    {
      name: "subjectId",
      type: "select",
      validation: true,
    },
    {
      name: "level",
      type: "select",
      validation: true,
    },
    {
      name: "duration",
      type: "number",
      validation: true,
    },
    {
      name: "rate",
      type: "number",
      validation: true,
    },
    {
      name: "type",
      type: "select",
      validation: true,
    },
    {
      name: "description",
      type: "textarea",
      validation: true,
    },
    {
      name: "total",
      type: "number",
      validation: true,
    },
    {
      name: "qType",
      type: "select",
      validation: true,
    },
  ];

  const editDataQuizDetails = [
    {
      name: "name",
      type: "text",
      validation: true,
    },
    {
      name: "subjectId",
      type: "select",
      validation: true,
    },
    {
      name: "level",
      type: "select",
      validation: true,
    },
    {
      name: "duration",
      type: "text",
      validation: true,
    },
    {
      name: "rate",
      type: "number",
      validation: true,
    },
    {
      name: "type",
      type: "select",
      validation: true,
    },
    {
      name: "description",
      type: "textarea",
      validation: true,
    },
    {
      name: "total",
      type: "number",
      validation: true,
    },
    {
      name: "qType",
      type: "select",
      validation: true,
    },
  ];

  const practiceDetails = [
    {
      name: "subject",
      type: "select",
      validation: true,
    },
    {
      name: "num",
      type: "number",
      validation: true,
    },
    {
      name: "type",
      type: "select",
      validation: true,
    },
    {
      name: "group",
      type: "select",
      validation: true,
    },
  ];

  const handleForm = function (form, data) {
    // validate form.element on change
    data.forEach((element) => {
      MyValidation.handleElementOnChange(form, element);
    });

    // validate form on submit
    form.on("submit", function () {
      return MyValidation.handleSubmit(this, data);
    });
  };

  $(function () {
    handleForm($("#form_user-changePassword"), dataChangePassword);

    handleForm($("#form_user-forgot"), dataForgotPassword);

    handleForm($("#form_user-login"), dataLogin);

    handleForm($("#form_user-reset"), dataResetPassword);

    handleForm($("#form_user-register"), dataRegister);

    handleForm($("#form_post-add"), dataPost);

    handleForm($("#form_post-edit"), dataPost);

    handleForm($("#form_subject-add"), dataSubject);
    handleForm($("#form_subject-edit"), dataSubject);
    handleForm($("#form_slider"), dataSlider);
    handleForm($("#form_dimension"), dataDimension);

    handleForm($("#form_lesson"), dataLesson);
    handleForm($("#form_QD"), dataQuizDetails);
    handleForm($("#form_EQD"), editDataQuizDetails);
    handleForm($("#form_practiceDetails"), practiceDetails);
  });
})(jQuery);
