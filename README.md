# Project Quiz Praticing System for SWP391
Authored and Manage by Group 3 Class 1609-NET SU22

## Description
This project served the propose of helping to boost the selling rate of the Quiz Pratice Service for organisation/expert. The Web System supports several type of roles such as: Admin, Sale, Marketing, Expert and Customer. Admin is the organization leader/manager, acts as the system administrator. Sale will help to manage contents on the web system while Marketing manages information in the system. Expert will be able to create the testing-related contents (questions bank & quizzes list). Customers can search and access any kind of subject that they are looking for and will be able to easily start studying and practicing them without any difficulities. 

## Built With
- Java
- HTML
- Bootstrap
- CSS
- JavaScript

## Contributors
**Tran Hai Long (Team Leader)**
- [Profile](https://gitlab.com/longth1407 "Hai Long")
- [Email](mailto:longth1407@gmail.com?subject=Hi% "Hi!")

**Nguyen Van Long**
- [Profile](https://gitlab.com/longnvhe151244 "Van Long")
- [Email](mailto:longnvhe151244@fpt.edu.vn?subject=Hi% "Hi!")

**Nghiem Xuan Trinh**
- [Profile](https://gitlab.com/trinhnxhe151020 "Xuan Trinh")
- [Email](mailto:trinhnxhe151020@fpt.edu.vn?subject=Hi% "Hi!")

**Do Hoang Hiep**
- [Profile](https://gitlab.com/hiepdhhe150990 "Hoang Hiep")
- [Email](mailto:hiepdhhe150990@fpt.edu.vn?subject=Hi% "Hi!")

**Han Binh Duong**
- [Profile](https://gitlab.com/duonghbhe150397 "Binh Duong")
- [Email](mailto:duonghbhe150397@fpt.edu.vn?subject=Hi% "Hi!")

## Acknowledgment
**Nguyen Thi Hai Nang**
- [Profile](https://gitlab.com/nangnth "Hai Nang")

## 🤝 Support
Contributions, issues, and feature requests are welcome!
Give a ⭐️ if you like this project!
