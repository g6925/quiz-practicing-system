alter table Register
add PriceRate int 

alter table Register
add SalePrice money 

update Register
set SalePrice = (
	select SalePrice
	from Subject 
	where Subject.ID = Register.SubjectID
)

update Register
set PriceRate = (
	select PriceRate
	from PricePackage 
	where PricePackage.ID = Register.PricePackageID
)

select *
from Register